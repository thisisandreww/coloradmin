<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BetLimit extends Model
{
	public function getCheckedPools()
	{
		$checkedArr = [];
		$pools = \App\Pool::get();
		foreach($pools AS $pool){
			$property = $pool->shortname;
			if($this->$property == 1){
				array_push($checkedArr, $property);
			}
		}
		return $checkedArr;
	}
}
