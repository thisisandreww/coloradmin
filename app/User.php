<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
	use HasApiTokens, Notifiable;

    //Custom column for Passport Authentication
	public function findForPassport($username) {
		return $this->where('username', $username)->first();
	}
	
    public function role()
    {
        return $this->hasOne('App\Role', 'role_id', 'user_role');
    }

    public function getBetMethod(){
    	if($this->mode == 0){
    		return "Multiply";
    	} else {
    		return "Slash";
    	}
    }

    public function getBoxIBox(){
    	return $this->hasOne('App\Box', 'box_id', 'box_id');
    }
}
