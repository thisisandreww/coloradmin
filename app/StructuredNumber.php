<?php
namespace app;

class StructuredNumber
{
    // Structured Extra
    public $number;
    public $draw_date;
    public $pool;
    public $bettype; //3D,4D,5D,6D,Spcl
    public $agent;
    public $package;
    public $amount_Big;
    public $amount_Small;
    public $amount_3A;
    public $amount_4A;
    public $amount_3ABC;
    public $amount_4B;
    public $amount_4C;
    public $amount_4D;
    public $amount_4E;
    public $amount_4ABC;
    public $amount_2A;
    public $amount_2ABC;
    public $amount_5D;
    public $amount_6D;
    
    public function structurizeBetList($bet_tokenObj)
    {
        $structured_bet_entries = [];
        
        $drawdates = $bet_tokenObj->date;
        // 1. separate them by drawdate and pool
        foreach($drawdates as $d)
        {
            $numbers = $bet_tokenObj->number;
            $final_numbers = [];
            foreach($numbers as $n)
            {
                // 1. normal               
                
                // 2. every number if it has IBOX, BOX
                /*if ($bet_tokenObj->type == "IBox" || $bet_tokenObj->type == "Box")
                {
                    
                }*/
                
                $pools = $n->pools;
                foreach($pools as $p)
                {
                    $structured_bet_entry = new \App\StructuredNumber();
                    $structured_bet_entry->number = $n->number;
                    $structured_bet_entry->agent = $n->agent;
                    $structured_bet_entry->pool = $p;
                    $structured_bet_entry->package = $n->package;
                    $structured_bet_entry->amount_Big = $n->amount_Big;
                    $structured_bet_entry->amount_Small = $n->amount_Small;
                    $structured_bet_entry->amount_3A = $n->amount_3A;
                    $structured_bet_entry->amount_3ABC = $n->amount_3ABC;
                    $structured_bet_entry->amount_4A = $n->amount_4A;
                    $structured_bet_entry->amount_4B = $n->amount_4B;
                    $structured_bet_entry->amount_4C = $n->amount_4C;
                    $structured_bet_entry->amount_4D = $n->amount_4D;
                    $structured_bet_entry->amount_4E = $n->amount_4E;
                    $structured_bet_entry->amount_4ABC = $n->amount_4ABC;
                    $structured_bet_entry->amount_2A = $n->amount_2A;
                    $structured_bet_entry->amount_2ABC = $n->amount_2ABC;
                    $structured_bet_entry->amount_5D = $n->amount_5D;
                    $structured_bet_entry->amount_6D = $n->amount_6D;
                    $structured_bet_entry->draw_date = $d;
                    $structured_bet_entry->bettype = 0;
                    
                    // add the entry in the array
                    array_push($structured_bet_entries, $structured_bet_entry);
                }  
                
            }
            
            
            
            // 3. every number if it has PH
            
            
            // 4. every number if it has PB
        }
        
        return $structured_bet_entries;
    }
    
    public function permuteNumber($number){
        $arr = collect();
        $this->permute($arr, $number,0,strlen($number));
        return response()->json([$arr->unique()->all()]);
    }
    
    // function to generate and print all N! permutations of $str. (N = strlen($str)).
    private function permute($arr, $str, $i, $n) {
        if ($i == $n)
            $arr->push($str);
            else {
                for ($j = $i; $j < $n; $j++) {
                    $this->swap($str,$i,$j);
                    $this->permute($arr, $str, $i+1, $n);
                    $this->swap($str,$i,$j);
                }
            }
    }
    
    // function to swap the char at pos $i and $j of $str.
    private function swap(&$str,$i,$j) {
        $temp = $str[$i];
        $str[$i] = $str[$j];
        $str[$j] = $temp;
    }

    public function retreiveBaseBetData()
    {
        
    }

}

