<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Arrangement extends Model
{
    public static function getCode($arrangement_id){
    	$str = "";
    	switch($arrangement_id){
    		case "1":
    		$str = "B-S-4A-C-A";
    		break;

    		case "2":
    		$str = "B-S-C-3A-4A";
    		break;

    		case "3":
    		$str = "B-S-3A-C-4A";
    		break;

    		case "4":
    		$str = "S-B-A-C-4A";
    		break;

    		case "5":
    		$str = "S-B-C-A-4A";
    		break;

    		case "6":
    		$str = "B-S-4A-A-C";
    		break;
    	}
    	return $str;
    }
}
