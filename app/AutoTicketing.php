<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class AutoTicketing extends Model
{
    //
	use SoftDeletes;

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];
    
    public function getUser(){
		return $this->hasOne('App\User', 'id', 'userid');
	}

	 public function getTicket(){
		return $this->hasOne('App\Ticket', 'id', 'ticketid');
	}
}
