<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\PackageDetail;
use App\PackageAssignment;
use Illuminate\Support\Facades\Auth;
use App\Package;

class PackageController extends BaseController
{
	private function getPackageByUserId($id, $package_type)
	{
        $redonly = 0;
        if ($package_type >= 3)
        {
            $package_type = $package_type - 2;
            $redonly = 1;
        }
		$user = collect(DB::SELECT("select a.*, b.* from users as a LEFT JOIN roles as b on a.user_role = b.role_id where a.id = '$id'"))->first();
		$queryString = "SELECT DISTINCT d.package_type_name,b.red_package ,d.package_type_id, b.package_id, b.package_name,b.created_at,b.owner_id,((select COALESCE(count(*),0) from 
                        package_assignments where package_id = b.package_id and user_id != '$id') + (select COALESCE(count(*),0) from packages where 
                        reference_package_id = b.package_id)) AS reference_count FROM packages AS b LEFT JOIN package_assignments AS c ON c.package_id = b.package_id 
						LEFT JOIN package_types as d on d.package_type_id = b.package_type_id where b.package_type_id = '$package_type' AND b.red_package ='$redonly'";
					
		if ($user->role_name == "System")
        {
			$queryString = $queryString . " AND b.master_package = 1";
        }
		else
        {
		     $queryString = $queryString . " AND c.user_id = '$id'";
        }
		return DB::SELECT($queryString);		  
	}
	
	private function getPackageInfo($type, $id)
	{
        $mainAcc = parent::getUserById($id)->user_main_account;
		$user = parent::getUserById($mainAcc);
		$packageList = self::getPackageByUserId($mainAcc, $type);
		$packageType = DB::table('package_types')->get();
		$packageDetailList = DB::SELECT("SELECT d.* FROM package_assignments as a LEFT JOIN users as b on a.user_id = b.id LEFT JOIN 
		                                 packages as c on c.package_id = a.package_id LEFT JOIN package_details as d on d.package_id = c.package_id 
										 where b.id = '$mainAcc'");
        $data = array('loginUser' => Auth::User(),
                      'user' => $user,
		              'packageList' => $packageList,
					  'packageType' => $type,
					  'packageDetailList'=>$packageDetailList);
		return $data;
	}
	
    public function showPackageView($type)
	{
        if ($type >= $this->lastPackageType || $type <= 0)
        {
            return back()->with('error', "Invalid URL Access");
        }
        $data = self::getPackageInfo($type, Auth::User()->id);
		return \view('package')->with('data', $data);
	}
	
	private function redirectToPackageView($type, $id)
    {
		$data = self::getPackageInfo($type,$id);
		return redirect("package/".$type);
    }
	
	private function getPackageOption($optionName, $id)
	{
		return DB::SELECT("SELECT a.package_id, a.package_name from packages as a LEFT JOIN package_assignments as b on b.package_id = a.package_id LEFT JOIN 
		                   package_types as c on c.package_type_id = a.package_type_id where c.package_type_name = '$optionName' and (a.owner_id = '$id' OR b.user_id = '$id')");
	}

	public function showPackageDetailView(Request $request, $id, $packageTypeId,$packageId)
	{
        if (!parent::checkIsIDBelongsToLoginUserHierachy($id))
        {
            return back()->with('error', "Invalid URL Access");
        }
		$package = collect(DB::SELECT("SELECT c.package_name, c.red_package, c.reference_package_id,d.package_type_name, b.package_detail_category_name, b.package_detail_subcategory_name, a.* FROM package_details as a LEFT JOIN 
		                       package_detail_categories as b on b.package_detail_category_id = a.package_detail_category_id LEFT JOIN packages as c on a.package_id = c.package_id 
		                       LEFT JOIN package_types as d on d.package_type_id = c.package_type_id WHERE a.package_id = '$packageId'"))->keyBy('package_detail_subcategory_name');
		$selectedPackage = collect(DB::SELECT("select * from packages where package_id = '$packageId'"))->first();
        if ($selectedPackage->reference_package_id == 0)
            $referencePackageId = $packageId;
        else    
            $referencePackageId = $selectedPackage->reference_package_id;
        $referencePackage = collect(DB::SELECT("SELECT c.red_package, d.package_type_name, b.package_detail_category_name, b.package_detail_subcategory_name, a.* FROM package_details as a LEFT JOIN 
		                       package_detail_categories as b on b.package_detail_category_id = a.package_detail_category_id LEFT JOIN packages as c on a.package_id = c.package_id 
		                       LEFT JOIN package_types as d on d.package_type_id = c.package_type_id WHERE a.package_id = '$referencePackageId'"))->keyBy('package_detail_subcategory_name');
		
        $packageType = collect(DB::SELECT("select * from package_types where package_type_id = '$packageTypeId'"))->first();
		$packageOption = self::getPackageOption($packageType->package_type_name, $id);
		$user = parent::getUserById($id);
		if($request->ajax())
		{
			$data = array('user'  => $user,
		              'packageOption' => $packageOption,
					  'packageType' => $packageType,
					  'packageId'=> $packageId,
                      'referencePackage'=> $referencePackage,
					  'package'=>$package,
                      'selectedPackage'=>$selectedPackage);
		    return response()->json(['data'=>$data]);
		}
		/*else
		{
			$data = array('user'  => $user,
		              'packageOption' => $packageOption,
					  'packageType' => $packageType,
					  'packageId'=> $packageId,
					  'package'=>$package);
			return \view('packageDetail')->with('data', $data);
		}*/
	}
	
    private function updateAndSaveAsNewPackage($request,$packageId, $newPackageId, $userId)
    {
        $packageDetailToUpdate = DB::SELECT("select b.*,c.* from packages as a LEFT JOIN package_details as b on b.package_id = a.package_id LEFT JOIN package_detail_categories
                                         		 as c on c.package_detail_category_id = b.package_detail_category_id where a.package_id ='$packageId'");
        $nextPackageDetailId = DB::table('package_details')->max('package_detail_id');
        foreach($packageDetailToUpdate as $packageDet)
		{
			$ticketPrizeVal = $request->input('ticket_prize_'.$packageDet->package_detail_subcategory_name, false);
			$commissionVal = $request->input('commission_'.$packageDet->package_detail_subcategory_name, false);
			$firstPrizeVal = $request->input('first_prize_'.$packageDet->package_detail_subcategory_name, false);
			$secondPrizeVal = $request->input('second_prize_'.$packageDet->package_detail_subcategory_name, false);
			$thirdPrizeVal = $request->input('third_prize_'.$packageDet->package_detail_subcategory_name, false);
			$specialPrizeVal = $request->input('special_prize_'.$packageDet->package_detail_subcategory_name, false);
			$consolationPrizeVal = $request->input('consolation_prize_'.$packageDet->package_detail_subcategory_name, false);
			$sixthPrizeVal = $request->input('sixth_prize_'.$packageDet->package_detail_subcategory_name, false);
            $newPackageDetail = new PackageDetail;
			$newPackageDetail->package_detail_id = ++$nextPackageDetailId;
			$newPackageDetail->package_id = $newPackageId;
			$newPackageDetail->package_detail_category_id = $packageDet->package_detail_category_id;
			$newPackageDetail->ticket_prize = $ticketPrizeVal;
			$newPackageDetail->commission = $commissionVal;
			$newPackageDetail->first_prize = $firstPrizeVal;
			$newPackageDetail->second_prize = $secondPrizeVal;
			$newPackageDetail->third_prize = $thirdPrizeVal;
			$newPackageDetail->special_prize = $specialPrizeVal;
			$newPackageDetail->consolation_prize = $consolationPrizeVal;
			$newPackageDetail->sixth_prize = $sixthPrizeVal;
			$newPackageDetail->save();
        }
               
        $childList= DB::SELECT("SELECT COALESCE(GROUP_CONCAT(lv SEPARATOR ','),0) AS child FROM (
                                SELECT @pv:=(SELECT GROUP_CONCAT(id SEPARATOR ',') FROM users 
                                WHERE FIND_IN_SET(user_upline, @pv)) AS lv FROM users 
                                JOIN (SELECT @pv:='$userId') tmp) a");

        $childPackageList= DB::SELECT("SELECT COALESCE(GROUP_CONCAT(lv SEPARATOR ','),-1) AS child FROM (
                                       SELECT @pv:=(SELECT GROUP_CONCAT(package_id SEPARATOR ',') FROM packages 
                                       WHERE FIND_IN_SET(reference_package_id, @pv)) AS lv FROM packages
                                       JOIN (SELECT @pv:='$packageId') tmp) a");
		DB::statement("UPDATE package_assignments SET package_id = '$newPackageId' WHERE package_id in ('$packageId',".$childPackageList[0]->child.") AND user_id in ($userId,".$childList[0]->child.")");
        DB::statement("UPDATE users SET activated_package_3D4D_id = '$newPackageId' WHERE activated_package_3D4D_id in ('$packageId',".$childPackageList[0]->child.") AND id in ($userId,".$childList[0]->child.")");
        DB::statement("UPDATE users SET activated_package_5D6D_id = '$newPackageId' WHERE activated_package_5D6D_id in ('$packageId',".$childPackageList[0]->child.") AND id in ($userId,".$childList[0]->child.")");
        parent::performPackageCleanup();
    }
    
	public function updateMemberPackageDetail(Request $request, $id)
	{
		$userId = $request->input("selected_package_owner_user_id");
		$packageId = $request->input("selected_package_id");
		$referencePackage = DB::table('packages')->where('package_id','=',$packageId)->first();
		$nextPackageId = DB::table('packages')->max('package_id') + 1;
		if($referencePackage->owner_id != $id)
		{
			$package = new Package;
			$package->package_id = $nextPackageId;
			$package->owner_id = $id;
			if (strpos($referencePackage->package_name, '*') !== false)
			{
			    $package->package_name = $referencePackage->package_name;
			}
			else
			{
				$package->package_name = $referencePackage->package_name."*";
			}
			$package->package_type_id = $referencePackage->package_type_id;
            $package->reference_package_id = $referencePackage->package_id;
			DB::beginTransaction();
            try
            {
                $package->save();
		    	$assignment = new PackageAssignment;
		        $assignment->user_id = $userId;
		        $assignment->package_id = $nextPackageId;
		        $assignment->push();
				
		    	DB::DELETE("DELETE from package_assignments where package_id='$packageId' AND user_id = '$userId'");
            
		    	self::updateAndSaveAsNewPackage($request, $packageId, $nextPackageId, $userId);
                $data = array('packageName'  => $package->package_name,
		                  'packageId' => $nextPackageId);
                DB::commit();
            }
            catch (\Exception $e)
            {
                DB::rollback();
                return response()->json(['data'=>$e->getMessage()]);
            }
            return response()->json(['data'=>$data]);
		}
        else
        {
            self::updateExistingPackage($request, $packageId);
            $data = array('packageName'  => $referencePackage->package_name,
		                  'packageId' => $packageId);
            return response()->json(['data'=>$data]);
        }
	}
    
    public function updateExistingPackage(Request $request, $packageId)
    {
        $packageDetailToUpdate = DB::SELECT("select b.*,c.* from packages as a LEFT JOIN package_details as b on b.package_id = a.package_id LEFT JOIN package_detail_categories
                                         		 as c on c.package_detail_category_id = b.package_detail_category_id where a.package_id ='$packageId'");
		$nextPackageDetailId = DB::table('package_details')->max('package_detail_id');
		$nextPackageDetailId = DB::table('package_details')->max('package_detail_id');
        if ($request->has('red_3D4D_package'))
        {
            DB::SELECT("UPDATE packages SET red_package='".$request->get('red_3D4D_package')."' where package_id = '".$packageId."'");
        }
        else if($request->has('red_5D6D_package'))
        {
            DB::SELECT("UPDATE packages SET red_package='".$request->get('red_5D6D_package')."' where package_id = '".$packageId."'");
        }
        DB::beginTransaction();
        try
        {
		    foreach($packageDetailToUpdate as $packageDet)
		    {
		    	$ticketPrizeVal = $request->input('ticket_prize_'.$packageDet->package_detail_subcategory_name, false);
		        $commissionVal = $request->input('commission_'.$packageDet->package_detail_subcategory_name, false);
		    	$firstPrizeVal = $request->input('first_prize_'.$packageDet->package_detail_subcategory_name, false);
		    	$secondPrizeVal = $request->input('second_prize_'.$packageDet->package_detail_subcategory_name, false);
		    	$thirdPrizeVal = $request->input('third_prize_'.$packageDet->package_detail_subcategory_name, false);
		    	$specialPrizeVal = $request->input('special_prize_'.$packageDet->package_detail_subcategory_name, false);
			    $consolationPrizeVal = $request->input('consolation_prize_'.$packageDet->package_detail_subcategory_name, false);
			    $sixthPrizeVal = $request->input('sixth_prize_'.$packageDet->package_detail_subcategory_name, false);
                $packageDetailCatId = $packageDet->package_detail_category_id;
                DB::statement("UPDATE package_details SET ticket_prize = '$ticketPrizeVal', commission = '$commissionVal', 
                               first_prize = '$firstPrizeVal', second_prize = '$secondPrizeVal', third_prize = '$thirdPrizeVal', 
                               special_prize = '$specialPrizeVal', consolation_prize = '$consolationPrizeVal', sixth_prize = '$sixthPrizeVal' 
                               WHERE package_detail_category_id = '$packageDetailCatId' and package_id = '$packageId'");
            }
            $childPackageList= DB::SELECT("SELECT COALESCE(GROUP_CONCAT(lv SEPARATOR ','),-1) AS child FROM (
                                           SELECT @pv:=(SELECT GROUP_CONCAT(package_id SEPARATOR ',') FROM packages 
                                           WHERE FIND_IN_SET(reference_package_id, @pv)) AS lv FROM packages
                                           JOIN (SELECT @pv:='$packageId') tmp) a");
		    DB::statement("UPDATE package_assignments SET package_id = '$packageId' WHERE package_id in (".$childPackageList[0]->child.")");
            DB::statement("UPDATE users set activated_package_3D4D_id = '$packageId' where activated_package_3D4D_id in (".$childPackageList[0]->child.")");
            DB::statement("UPDATE users set activated_package_5D6D_id = '$packageId' where activated_package_5D6D_id in (".$childPackageList[0]->child.")");
            parent::performPackageCleanup();
            $selectedPackage = collect(DB::SELECT("select * from packages where package_id = '$packageId'"))->first();
            DB::commit();
        }
        catch (\Exception $e)
        {
            DB::rollback();
            return back()->with('error', "Data Add Failed");
        }
        return response()->json(['data'=>$selectedPackage]);
    }
    
	public function showAddPackageView($packageTypeId)
	{
        if ($packageTypeId >= $this->lastPackageType || $packageTypeId <= 0)
        {
            return back()->with('error', "Invalid URL Access");
        }
		$user = parent::getUserById(Auth::User()->id);
        $redPackage = 0;
        if ($packageTypeId > 2)
        {
            $packageTypeId = $packageTypeId - 2;
            $redPackage = 1;
        }
		$packageType = collect(DB::SELECT("select * from package_types where package_type_id = '$packageTypeId'"))->first();
		$data = array('user'  => $user,
		              'packageType' => $packageType,
                      'redPackage' => $redPackage);
		return \view('addPackage')->with('data', $data);
	}
	
	public function addNewPackage(Request $request, $packageTypeId)
	{
        if (DB::table('packages')->WHERE('package_name', '=', $request->get('package_name'))
            ->where('package_type_id', '=', $packageTypeId)
            ->count())
        if ($packageTypeId >= $this->lastPackageType || $packageTypeId <= 0)
        {
            return back()->with('error', "Invalid URL Access");
        }
        
        if (DB::table('packages')->WHERE([['package_name', '=', $request->get('package_name')],['package_type_id','=',$packageTypeId]])->count())
        {
            return back()->with('error', 'Package name used')->withInput();
        }
        $mainAcc = parent::getUserById(Auth::user()->id)->user_main_account;
		$packageName = $request->input('package_name');
        if($request->has('red_package'))
            $redPackage = $request->input('red_package');
        else
            $redPackage = 0;
        
        DB::beginTransaction();
        try
        {
	    	$nextPackageId = DB::table('packages')->max('package_id') + 1;
	    	$package = new Package;
	    	$package->package_id = $nextPackageId;
	    	$package->owner_id = 0;
            $package->created_by = $mainAcc;
            $package->reference_package_id = 0;
            $package->red_package= $redPackage;
	    	$package->package_name = $packageName;
            $package->master_package = 1;
	    	$package->package_type_id = $packageTypeId;
	    	$package->save();
	    	$assignment = new PackageAssignment;
	    	$assignment->user_id = $mainAcc;
	    	$assignment->package_id = $nextPackageId;
	    	$assignment->save();
	        $nextPackageDetailId = DB::table('package_details')->max('package_detail_id');
	    	$packageTypeName = DB::table('package_types')->where('package_type_id', '=', $packageTypeId)->first();
	    	$packageDetailToUpdate = null;
	    	if ($packageTypeName->package_type_name == '5D/6D')
	    	{
	    	    $packageDetailToUpdate = DB::SELECT("SELECT * FROM package_detail_categories where package_detail_category_name = '5d/6d'");
		    }
		    else
		    {
		        $packageDetailToUpdate = DB::SELECT("SELECT * FROM package_detail_categories where package_detail_category_name != '5d/6d'");
		    }
				
		    foreach($packageDetailToUpdate as $packageDet)
		    {
		    	$ticketPrizeVal = $request->input('ticket_prize_'.$packageDet->package_detail_subcategory_name, false);
		    	$commissionVal = $request->input('commission_'.$packageDet->package_detail_subcategory_name, false);
		    	$firstPrizeVal = $request->input('first_prize_'.$packageDet->package_detail_subcategory_name, false);
		    	$secondPrizeVal = $request->input('second_prize_'.$packageDet->package_detail_subcategory_name, false);
		    	$thirdPrizeVal = $request->input('third_prize_'.$packageDet->package_detail_subcategory_name, false);
		    	$specialPrizeVal = $request->input('special_prize_'.$packageDet->package_detail_subcategory_name, false);
		    	$consolationPrizeVal = $request->input('consolation_prize_'.$packageDet->package_detail_subcategory_name, false);
		    	$sixthPrizeVal = $request->input('sixth_prize_'.$packageDet->package_detail_subcategory_name, false);
			
                $newPackageDetail = new PackageDetail;
		    	$newPackageDetail->package_detail_id = ++$nextPackageDetailId;
		    	$newPackageDetail->package_id = $nextPackageId;
		    	$newPackageDetail->package_detail_category_id = $packageDet->package_detail_category_id;
		    	$newPackageDetail->ticket_prize = $ticketPrizeVal;
		    	$newPackageDetail->commission = $commissionVal;
		    	$newPackageDetail->first_prize = $firstPrizeVal;
		    	$newPackageDetail->second_prize = $secondPrizeVal;
		    	$newPackageDetail->third_prize = $thirdPrizeVal;
		    	$newPackageDetail->special_prize = $specialPrizeVal;
		    	$newPackageDetail->consolation_prize = $consolationPrizeVal;
		    	$newPackageDetail->sixth_prize = $sixthPrizeVal;
		    	$newPackageDetail->save();
		    }
        
            $packageType = $packageTypeId;
            if ($redPackage)
                $packageType = $packageType +2;
		    DB::commit();
        }
        catch (\Exception $e)
        {
            DB::rollback();
            return back()->with('error', "Data Add Failed")->withInput();
        }
        
        return self::redirectToPackageView($packageType, $mainAcc);
	}
	
	public function deletePackages($packageId)
	{
        DB::beginTransaction();
        try
        {
	        self::deletePackageById($packageId);
            $childPackageList= DB::SELECT("SELECT COALESCE(GROUP_CONCAT(lv SEPARATOR ','),0) AS child FROM (
                                           SELECT @pv:=(SELECT GROUP_CONCAT(package_id SEPARATOR ',') FROM packages 
                                           WHERE FIND_IN_SET(reference_package_id, @pv)) AS lv FROM packages
                                           JOIN (SELECT @pv:='$packageId') tmp) a");
            self::deletePackageById($childPackageList[0]->child);
            DB::commit();
        }
        catch (\Exception $e)
        {
            DB::rollback();
            return back()->with('error', "Data Add Failed");
        }
		return back();
	}
    
    private function deletePackageById($package)
    {
        DB::delete("Delete from packages where package_id in (".$package.")");
        DB::delete("Delete from package_details where package_id in (".$package.")");
        DB::delete("Delete from package_assignments where package_id in (".$package.")");
        DB::statement("update users set activated_package_3D4D_id = 0 where activated_package_3D4D_id in (".$package.")");
        DB::statement("update users set activated_package_5D6D_id = 0 where activated_package_5D6D_id in (".$package.")");
        DB::statement("update users set assigned_red_package_3D4D_id = 0 where assigned_red_package_3D4D_id in (".$package.")");
        DB::statement("update users set assigned_red_package_5D6D_id = 0 where assigned_red_package_5D6D_id in (".$package.")");
    }
}
