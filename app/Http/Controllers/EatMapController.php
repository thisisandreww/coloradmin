<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;
use App\EatMapReferences;
use App\Pool;
use App\User;
use App\EatMap;

class EatMapController extends BaseController
{
    /*
     * showReferenceEatMap()
     * To get the reference eat map value from DB that will render in 
     * Edit Member Details
     * $viewOnBehalf: is current viewing as which downline (the user id)
     */
    public function showReferenceEatMap($viewOnBehalf, $targetid)
    {
        // Step 1: check if $viewOnBehalf having correct id
        $user = User::where('id', '=', $viewOnBehalf)->first();
        if ($user === null) {
            // user doesn't exist
            return response()->json(['status'=> false, 'message'=>'Invalid user id']);
        }
        
        $target = User::where('id', '=', $targetid)->first();
        if ($target === null) {
            // user doesn't exist
            return response()->json(['status'=> false, 'message'=>'Invalid taregt user id']);
        }
        
        // Step 2: Settle all the identity relationship
        $me = self::getRealMe();
        $user = User::with('role')->where('id', '=', $me->id)->first();
        $authorized = false;
        if (self::amIAuthorizeToView($me, $viewOnBehalf))
        {
            $authorized = true;
        }
        else {
            return response()->json(['status'=> false, 'message'=>'Unauthorized Access.']);
        }
        
        if (self::amIAuthorizeToView($me, $target))
        {
            $authorized = true;
        }
        else {
            return response()->json(['status'=> false, 'message'=>'Unauthorized Access.']);
        }
        
        
        // Step 3: Perform the checking
        // Concat the key for easy use in the front end
        $mymapreference = collect(DB::SELECT("SELECT CONCAT(cat.eat_detail_type_name, '_', cat.eat_detail_subtype_name, '_', cat.eat_detail_tertiertype_name)  AS thekey,
                                             map.value
                                      FROM eat_map_references as map 
                                      INNER JOIN eat_detail_categories as cat
                                      ON map.pdc_id = cat.id
									  WHERE map.user_id =".$viewOnBehalf))->keyBy('thekey');
        
        $targetreference = collect(DB::SELECT("SELECT CONCAT(cat.eat_detail_type_name, '_', cat.eat_detail_subtype_name, '_', cat.eat_detail_tertiertype_name)  AS thekey,
                                             map.value
                                      FROM eat_map_references as map
                                      INNER JOIN eat_detail_categories as cat
                                      ON map.pdc_id = cat.id
									  WHERE map.user_id =".$targetid))->keyBy('thekey');        
        
        //$mymapreference = EatMapReferences::where("user_id", "=", $viewOnBehalf)->get();  
        $data = array('uplineReference'  => $mymapreference, 'targetReference'=>$targetreference);
        return response()->json(['status'=> true,'data'=>$data, 'user'=>$user, 'authorized'=>$authorized]);
    }
    
    /*
     * saveMapReference($request)
     * Save the map references into database called by the editMemberDetail form: eatmapform
     * $request: all the form data
     */
    public function saveMapReference(Request $request)
    {
        
        // step 0: validation
        $targetId = $request->hidTargetIdEatRef;
        if ($targetId == null)
        {
            Session::flash('message', 'Whoops! The target Id is not set');
            Session::flash('class', 'alert alert-danger');
            return back();
        }
        
        $target = User::where('id', '=', $targetId)->first();
        if ($target === null) {
            // user doesn't exist
            Session::flash('message', 'Whoops! User not exists in system.');
            Session::flash('class', 'alert alert-danger');
            return back();
        }
        
        // step 1: remove all the value of the map reference for the target 
        DB::beginTransaction();
        try {
            $existingReference = EatMapReferences::where("user_id", "=", $targetId)
            ->get();
            
            foreach($existingReference as $e)
            {
                $e->delete();
            }
            
        }
        catch(\Exception $ex)
        {
            DB::rollBack();
            Session::flash('message', 'Whoops! '.$ex->getMessage());
            Session::flash('class', 'alert alert-danger');
            return back();
        }        
        
        // step 2: retrieve all the reference category (as reference)
        $mymapreference = collect(DB::SELECT("SELECT CONCAT(cat.eat_detail_type_name, '_', cat.eat_detail_subtype_name, '_', cat.eat_detail_tertiertype_name)  AS thekey,
                                             cat.id As id
                                      FROM eat_detail_categories as cat"))->keyBy('thekey');
        
        // step 3: insert the value
        try {
            foreach($mymapreference as $r)
            {
                $key = $r->thekey;
                $column_value = $request->get("txt_".$key);
                
                $newMap = new EatMapReferences();
                $newMap->user_id = $targetId;
                $newMap->pdc_id = $r->id;
                
                if ($column_value != null)
                {                    
                    $newMap->value = $column_value;
                }
                else {
                    $newMap->value = 0;
                }
                $newMap->save();
            }
        }
        catch(\Exception $ex)
        {
            DB::rollBack();
            Session::flash('message', 'Whoops! '.$ex->getMessage());
            Session::flash('class', 'alert alert-danger');
            return back();
        }        
        
        // step 3.1: commit the transaction
        DB::commit();
        
        Session::flash('message', 'Reference Eat Limit saved successfully.');
        Session::flash('class', 'alert alert-success');
        // step 4: return to frontend
        return redirect()->route("memberSelectedDetail", [$request->hidUplineIdEatRef, $targetId]);
    }
    
    /*
     * Open up Edit Eat Map View
     */
    public function editEatMap()
    {
        // 1. Get the real me
        $me = self::getRealMe();
        
        // 2. Get my reference of limit
        $mymapreference = collect(DB::SELECT("SELECT CONCAT(cat.eat_detail_type_name, '_', cat.eat_detail_subtype_name, '_', cat.eat_detail_tertiertype_name)  AS thekey,
                                             map.value
                                      FROM eat_map_references as map
                                      INNER JOIN eat_detail_categories as cat
                                      ON map.pdc_id = cat.id
									  WHERE map.user_id =".$me->id))->keyBy('thekey');
        
        // 2.1 Try to get own eat limit
        $myeat = collect(DB::SELECT("SELECT CONCAT(cat.eat_detail_type_name, '_', cat.eat_detail_subtype_name, '_', 
                                              cat.eat_detail_tertiertype_name,'_', pool.shortname)  AS thekey,
                                             map.value,
                                             pool.images
                                      FROM eat_maps as map
                                      INNER JOIN eat_detail_categories as cat
                                      ON map.pdc_id = cat.id
                                      INNER JOIN pools as pool
                                      ON pool.id = map.pool_id
									  WHERE map.user_id =".$me->id))->keyBy('thekey');
        
        // 3. Get all the pools available from pools table
        $pools = Pool::all();
        
        // 2.2 Check if own limit is nothing?
        if (count($myeat) == 0)
        {
            foreach ($mymapreference as $r)
            {
                foreach ($pools as $pool)
                {
                    $myeat = array_add($myeat,
                                        $r->thekey."_".$pool->shortname,                        
                                        [
                                            "thekey" => $r->thekey."_".$pool->shortname,
                                            "value" => 0.00,
                                            "images" => $pool->images
                                        ]);
                }
            }            
        }
        else 
        {
            $myeat = json_decode($myeat, true);
        }
        
        // 4. Return to front end  
        $data = array('myMapReference' => $mymapreference,
                      'myMap' => $myeat,
                      'pools' => $pools,
                      'me'=>$me
                    );
        
        return view("editEatMap")->with('data', $data);
    }
    
    public function saveEditMap(Request $request)
    {
        $user = auth()->user();
        $me = User::find(User::find($user->id)->user_main_account);
        
        // step 0: Remove all my eat maps
        DB::beginTransaction();
        try {
            $existingMap = EatMap::where("user_id", "=", $me->id)
            ->get();
            
            foreach($existingMap as $e)
            {
                $e->delete();
            }
            
        }
        catch(\Exception $ex)
        {
            DB::rollBack();
            Session::flash('message', 'Whoops! '.$ex->getMessage());
            Session::flash('class', 'alert alert-danger');
            return back();
        }  
        
        // Step 1: Find all the reference of myself
        $mymapreference = collect(DB::SELECT("SELECT CONCAT(cat.eat_detail_type_name, '_', cat.eat_detail_subtype_name, '_', cat.eat_detail_tertiertype_name)  AS thekey,
                                             map.value,cat.id 
                                      FROM eat_map_references as map
                                      INNER JOIN eat_detail_categories as cat
                                      ON map.pdc_id = cat.id
									  WHERE map.user_id =".$me->id))->keyBy('thekey');
        
        // Step 2: Get all the pools from the database
        $pools = Pool::all();
        
        // Step 3: Loop Request and try to update based on the reference with pools        
        foreach ($mymapreference as $r)
        {
            foreach ($pools as $pool)
            {
                try {
                    // Step 3: Get the key from reference and take the Id
                    $key_part1 = $r->thekey;
                    
                    // Step 4: Get the key from pool and
                    $key_part2 = $pool->id;
                    $key_part3 = $pool->shortname;
                    
                    $thetarget = $request->get("txt_".$key_part1."_".$key_part3);
                    if ($thetarget != null)
                    {
                        $newMap = new EatMap();
                        $newMap->user_id = $user->id;
                        $newMap->pdc_id = $r->id;
                        $newMap->pool_id = $pool->id;
                        $newMap->value= $thetarget;
                        $newMap->save();
                    }
                    else
                    {
                        $newMap = new EatMap();
                        $newMap->user_id = $user->id;
                        $newMap->pdc_id = $r->id;
                        $newMap->pool_id = $pool->id;
                        $newMap->value= 0;
                        $newMap->save();
                    }
                }
                catch(\Exception $ex)
                {
                    dd($ex->getMessage());
                    DB::rollBack();
                    Session::flash('message', 'Whoops! '.$ex->getMessage());
                    Session::flash('class', 'alert alert-danger');
                    return back();
                }  
            }
        }
        
        // step 3.1 save the position
        $me->eat_type_3d4d = $request->get('selEatType3d4d');
        $me->eat_type_5d6d = $request->get('selEatType5d6d');
        $me->eat_type_4dspcl = $request->get('selEatType4dSpcl');
        $me->save();
        
        // step 3.1: commit the transaction
        DB::commit();
        
        Session::flash('message', 'Eat Map saved successfully.');
        Session::flash('class', 'alert alert-success');
        // step 4: return to frontend
        return redirect()->route("editEatMap");
    }
    
    /*
     * getRealMe()
     * To get the real identity of login user. If login person is main account, it will return the account itself.
     * If the login person is sub account, it will get his main account.
     */
    private function getRealMe()
    {
        $user = auth()->user();
        $me = User::find(User::find($user->id)->user_main_account);
        return $me;
    }
    
    /*
     * amIauthorizeToView($realMe, $targetId)
     * $realMe: real identity of the login person
     * $targetId: targer user id that will be accessed by myself 
     */
    private function amIAuthorizeToView($realMe, $targetId)
    {
        if ($realMe->role->role_name == "System")
        {
            return true;
        }
        else if ($realMe == $targetId)
        {
            return true;
        }
        else {
            // loop from targetId go upward until reaching system.
            // if found realMe id then return true immediately.
            // if at the end never see realMe id return false as it does not belongs to realMe hierarchy
            
            
            
            return true;
        }
    }
}
