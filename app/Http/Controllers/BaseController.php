<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Goutte\Client;
use Carbon\Carbon;
use Session;
use Redirect;

class BaseController extends Controller
{	
    //these hard coded value are refer to database id
	public static $package3D4DId = "1";
	public static $package5D6DId = "2";
	public static $activeState = "0";
	public static $inactiveState = "1";
	public static $suspendedState = "2";
	public static $companyRole = "8";

	public static $package3D4D = "1";
	public static $package5D6D = "2";
	public static $packageRed3D4D = "3";
	public static $packageRed5D6D = "4";
	public $lastPackageType = "5";

	public function php()
	{
		return \View("testphp");
	}

	public function login()
	{
		return \View("login");
	}

	public function addNewMember()
	{
		return View::make("resetPassword");
	}

	public function getUserById($id)
	{
		return collect(DB::SELECT("select a.*, b.role_name,b.role_id from users as a LEFT JOIN roles as b on b.role_id = a.user_role where a.id = '$id'"))->first();
	}

	public function performPackageCleanup()
	{
		DB::DELETE("DELETE from packages where package_id not in (select package_id from package_assignments)");
		DB::DELETE("DELETE from package_details where package_id not in (select package_id from package_assignments)");
		DB::DELETE("DELETE from package_assignments where package_id not in (select package_id from packages)");
	}

	public function countArrSize($obj)
	{
		if ($obj == null || !is_array($obj))
		{
			return 0;
		}
		else
		{
			return count($obj);
		}
	}

	public function checkIsIdParentChildRelation($parentId, $childId)
	{
		$childUser = self::getUserById($childId);
		return $childUser->user_upline == $parentId;
	}

	public function checkIsIDBelongsToLoginUserHierachy($id)
	{
		$childList= DB::SELECT("SELECT COALESCE(GROUP_CONCAT(lv SEPARATOR ','),0) AS child FROM (
			SELECT @pv:=(SELECT GROUP_CONCAT(id SEPARATOR ',') FROM users 
			WHERE FIND_IN_SET(user_upline, @pv)) AS lv FROM users 
			JOIN (SELECT @pv:='".Auth::User()->user_main_account."') tmp) a");
		$childArr = explode(",",$childList[0]->child);
		if (in_array($id, $childArr) || $id == Auth::User()->id || $id == Auth::User()->user_main_account)
		{
			return true;
		}
		else
		{
			return false;
		}
	}

	public function scraper($date){
		$client = new Client();

		//Check if entered date is today
		if($date == Carbon::now()->format("Y-m-d")){
			$url = "http://www.check4d.com";

			$crawler = $client->request('GET', $url);

			$date_html = collect();

			$crawler->filter('.resultdrawdate')->each(function ($node) use($date_html) {
				$date_html->push($node);
			});

			//Check if text is Date: dd-mm-yyyy
			$date_text = $date_html[0]->text();
			if(strpos($date_text, 'Date:') !== false){
				$date_len = strpos($date_text, '(') - 7;
				//retrieving the (dd-mm-yyyy) date
				$date_str = substr($date_html[0]->text(), 6, $date_len);
				if($date_str != Carbon::now()->format("d-m-Y")){
					return;
				}
			}
		} else {
			$url = 'http://www.check4d.com/past-results/' . $date;
		}

		$crawler = $client->request('GET', $url);

		$html = collect();

		$crawler->filter('.outerbox')->each(function ($node) use($html) {
			$html->push($node);
		});

		$date = Carbon::createFromFormat("Y-m-d", $date)->format("Ymd");

		$html->each(function($pool) use($date){
			$magnum = collect();
			$pool_name = substr($pool->text(), 0, strpos($pool->text(), 'Date'));
			switch($pool_name){
				case "Magnum 4D 萬能":
				$this->getTopResult($date, $pool, 1);
				break;
				case "Da Ma Cai 1+3D 大馬彩":
				$this->getTopResult($date, $pool, 2);
				break;
				case "SportsToto 4D 多多":
				$this->getTopResult($date, $pool, 3);
				break;
				case "Singapore 4D":
				$this->getTopResult($date, $pool, 4);
				break;
				case "Sabah 88 4D 沙巴萬字":
				$this->getTopResult($date, $pool, 5);
				break;
				case "Sandakan 4D山打根赛马会":
				$this->getTopResult($date, $pool, 6);
				break;
				case "Special CashSweep 砂勞越大萬":
				$this->getTopResult($date, $pool, 7);
				break;
				case "SportsToto 5D, 6D, Lotto多多六合彩":
				$this->getToto5D6D($date, $pool, 3);
				break;
			}
		});
	}

	public function scraperGD($date){
		$url = "https://www.4dking.com.my/past_result_json.php?draw_date=" . $date;

		$client = new \GuzzleHttp\Client();
		$res = $client->request('GET', $url);
		$json_body = json_decode($res->getBody());
		$list = collect($json_body);

		$gd_collection = collect();

		foreach($list AS $ea){
			if($ea->t == "GD" && strlen($ea->n) == 4 && $ea->n != "----"){
				$gd_collection->push($ea);
			}
		}

		$date = Carbon::createFromFormat("Y-m-d", $date)->format("Ymd");

		$starter = 1;
		$consolation = 1;

		foreach($gd_collection AS $num){

			$type = "";

			if($num->p == 4){
				$type = "s" . $starter;
				$starter++;
			} else if($num->p == 5){
				$type = "c" . $consolation;
				$consolation++;
			} else {
				$type = $num->p;
			}

			$result = \App\DrawResult::firstOrCreate(
				['drawdate' => $date, 'poolid' => '8', 'type' => $type, 'number' => $num->n]
			);
		}
	}

	function getTopResult($date, $pool, $poolid){
        //Top
		$pool->filter('.resulttop')->each(function ($node, $key) use($date, $poolid){
			if($key+1 <= 3){
				$result = \App\DrawResult::firstOrCreate(
					['drawdate' => $date, 'poolid' => $poolid, 'type' => $key+1, 'number' => $node->text()]
				);
			}
		});

		$specialNum = 0;
		$consolationNum = 0;

        //Special
		$pool->filter('.resultbottom')->each(function ($node, $key) use($date, $poolid, &$specialNum, &$consolationNum){
			if(strlen($node->text()) != 4 || is_numeric($node->text()) == false || $node->text() == ""){
				return;
			}
			if($specialNum < 10){
				$specialNum++;
				$type = "s" . $specialNum;
			} else {
				$consolationNum++;
				$type = "c" . $consolationNum;
			}

			$result = \App\DrawResult::firstOrCreate(
				['drawdate' => $date, 'poolid' => $poolid, 'type' => $type, 'number' => $node->text()]
			);
		});
	}

	function getToto5D6D($date, $pool){
		$counter = 0;
		$last = false;
		$collect = collect();
		$result = $pool->filter('.resultbottom')->each(function ($node, $key) use($date, &$counter, $last, $collect){
			$collect->push($node->text());
			if($counter <= 6){
				if(strlen($node->text()) == 5 && $counter < 3){
					$counter++;
					$type = "5d" . $counter;
					$result = \App\DrawResult::firstOrCreate(
						['drawdate' => $date, 'poolid' => 3, 'type' => $type, 'number' => $node->text()]
					);
				} else if(strlen($node->text()) == 6) {
					$type = "6d1";
					$last = true;
					$result = \App\DrawResult::firstOrCreate(
						['drawdate' => $date, 'poolid' => 3, 'type' => $type, 'number' => $node->text()]
					);
				}
			}

			if($last){
				return;
			}
		});
	}

	public function switch_language($lang) {
		if (array_key_exists($lang, \Config::get('languages'))) {
			Session::put('applocale', $lang);
		}

		return Redirect::back();
	}
}
