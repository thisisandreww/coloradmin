<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\User;
use App\Pool;
use App\BetStage;
use App\AutoTicketing;
use Session;
use Redirect;
use Carbon\Carbon;

class ReportController extends BaseController
{
    protected $type3d4d = "0";
    protected $type3d4dSpecial = "1";
    protected $type5d6d = "2";
    protected $typeAll = "3";
    
    
    public function getWinLossDetailReport(Request $request, $userId)
    {
        $record = null;
        if (!parent::checkIsIDBelongsToLoginUserHierachy($userId))
		{
		    return back()->with('error', "Invalid URL Access");
		}
        $user =  User::findOrFail($userId);
        if ($request->has('searchRange'))
        {
            $rangeArr = explode(' - ',$request->searchRange);
            foreach ($rangeArr as &$str) 
            {
                $str = str_replace('-', '', $str);
            }

            $record = $this->convertRecordToStandardForm($this->getWinLossInDownlineView($userId, 
                                                         $request->input('enable3D4D', false), 
                                                         $request->input('enable5D6D', false), 
                                                         $rangeArr[0], 
                                                         $rangeArr[1]));
        }
        return \View('winLossDetail', compact('record','user'));
    }
    
    public function getWinLossSimpleReport(Request $request, $userId)
    {
        $record = null;
        if (!parent::checkIsIDBelongsToLoginUserHierachy($userId))
		{
		    return back()->with('error', "Invalid URL Access");
		}
        $user =  User::findOrFail($userId);
        if ($request->has('searchRange'))
        {
            $rangeArr = explode(' - ',$request->searchRange);
            foreach ($rangeArr as &$str) 
            {
                $str = str_replace('-', '', $str);
            }
            $record = $this->convertRecordToStandardForm($this->getWinLossInDownlineView($userId, 
                                                         $request->input('enable3D4D', false), 
                                                         $request->input('enable5D6D', false), 
                                                         $rangeArr[0], 
                                                         $rangeArr[1]));
        }
        return \View('winLossSimple', compact('record','user'));
    }
    
    public function getTotalReport(Request $request, $userId)
    {
        $record = null;
        $user =  User::findOrFail($userId);
        if ($request->has('searchRange'))
        {
            $rangeArr = explode(' - ',$request->searchRange);
            foreach ($rangeArr as &$str) 
            {
                $str = str_replace('-', '', $str);
            }
            $record = $this->convertRecordToStandardForm($this->getWinLossInDownlineView($userId, 
                                                         $request->input('enable3D4D', false), 
                                                         $request->input('enable5D6D', false), 
                                                         $rangeArr[0], 
                                                         $rangeArr[1]));
        }
        return \View('totalReport', compact('record','user'));
    }
    
    private function convertRecordToStandardForm($record)
    {
        foreach($record as $ea)
        {
            $ea->totalBet = 0;
            if(isset($ea->total3d4dbet))
            {
                $ea->totalBet += round($ea->total3d4dbet * 100) / 100;
            }
            if(isset($ea->total5d6dbet))
            {
                $ea->totalBet += round($ea->total5d6dbet * 100) / 100;
            }
            
            $ea->downlineSales = 0;
            if(isset($ea->stock3d4d))
            {
                $ea->downlineSales += round($ea->stock3d4d * 100) / 100;
            }
            if(isset($ea->stock5d6d))
            {
                $ea->downlineSales += round($ea->stock5d6d * 100) / 100;
            }
            
            $ea->downlineComm = 0;
            if(isset($ea->commission3d4d))
            {
                $ea->downlineComm += round($ea->commission3d4d * 100) / 100;
            }
            if(isset($ea->commission5d6d))
            {
                $ea->downlineComm += round($ea->commission5d6d * 100) / 100;
            }
            
            $ea->selfEatTotal = 0;
            if(isset($ea->eat3d4d))
            {
                $ea->selfEatTotal += round($ea->eat3d4d * 100) / 100;
            }
            if(isset($ea->eat5d6d))
            {
                $ea->selfEatTotal += round($ea->eat5d6d * 100) / 100;
            }
            
            $ea->commEarn = 0;
            if(isset($ea->commission3d4dEarn))
            {
                $ea->commEarn += round($ea->commission3d4dEarn * 100) / 100;
            }
            if(isset($ea->commission5d6dEarn))
            {
                $ea->commEarn += round($ea->commission5d6dEarn * 100) / 100;
            }
            
            $ea->selfEatCommTotal = 0;
            if(isset($ea->selfEat3d4dComm))
            {
                $ea->selfEatCommTotal += round($ea->selfEat3d4dComm * 100) / 100;
            }
            if(isset($ea->selfEat5d6dComm))
            {
                $ea->selfEatCommTotal += round($ea->selfEat5d6dComm * 100) / 100;
            }
        }
        return $record;
    }
    
    private function getWinLossInDownlineView($userId, $include3D4D, $include5D6D, $from, $to)
    {
        $userList = null;
        $noDownLine = false;
        if (($userList = DB::SELECT("select id, username, name from users where user_upline='$userId'")) == null)
        {
            $noDownLine = true;
            $userList = DB::SELECT("select id, username, name from users where id ='$userId'");
        }
        
        foreach($userList as $user)
        {
            $stageList = collect(DB::SELECT("select GROUP_CONCAT(a.stageid) as stage_list from stage_details as a left join bet_stages as b on a.stageid = b.id 
                                             where a.userid='".$user->id."' and b.drawdate >= '$from' and b.drawdate <= '$to'"))->first()->stage_list;
            if ($include3D4D)
            {
                if ($stageList != null)
                {
                    $total3d4dBet = collect(DB::SELECT("select sum(stockbig+stocksmall+stock4a+stock3a+stock3abc+stock2a+stock2abc+stock4b+stock4c+stock4d+stock4e+stock4abc) as total_bet
                                                        from stage_details where downlinerecordid = 1 and stageid in (".$stageList.")"))->first();

                    $total3d4dStockAndCommEarn = collect(DB::SELECT("select sum(stockbig+stocksmall+stock4a+stock3a+stock3abc+stock2a+stock2abc+stock4b+stock4c+stock4d+stock4e+stock4abc) as stock3d4d,
                                                                     sum(eatbig+eatsmall+eat4a+eat3a+eat3abc+eat2a+eat2abc+eat4b+eat4c+eat4d+eat4e+eat4abc) as eat3d4d    
                                                                     from stage_details where userid='".$user->id."' and stageid in (".$stageList.")"))->first();
                                                                       
                    $total3d4dEat = collect(DB::SELECT("select sum(eatbig+eatsmall+eat4a+eat3a+eat3abc+eat2a+eat2abc+eat4b+eat4c+eat4d+eat4e+eat4abc) as eat3d4d,
                                                        sum(commbig+commsmall+comm4a+comm3a+comm3abc+comm2a+comm2abc+comm4b+comm4c+comm4e+comm4d+comm4abc) as comm3d4dEarn,
                                                        sum((commratebig*eatbig/100)+(commratesmall*eatsmall/100)+(commrate4a*eat4a/100)+(commrate3a*eat3a / 100)
                                                        +(commrate3abc*eat3abc/100)+(commrate2a*eat2a/100)+(commrate2abc*eat2abc/100)+(commrate4b*eat4b/100)+
                                                        (commrate4c*eat4c/100)+(commrate4e*eat4e/100)+(commrate4d*eat4d/100)+(commrate4abc*eat4abc/100)) as selfEatComm  
                                                        from stage_details where userid='".$userId."' and stageid in (".$stageList.")"))->first();
                    $total3d4dComm = collect(DB::SELECT("select sum(commbig+commsmall+comm4a+comm3a+comm3abc+comm2a+comm2abc+comm4b+comm4c+comm4e+comm4d+comm4abc) as commPaid
                                                         from stage_details where downlinerecordid = IF(downlinerecordid = 1,1, downlinerecordid - 1) and stageid in (".$stageList.")"))->first();

                    $user->total3d4dbet = $total3d4dBet->total_bet;
                    if (!$noDownLine)
                    {
                        $user->stock3d4d = $total3d4dStockAndCommEarn->stock3d4d - $total3d4dStockAndCommEarn->eat3d4d;
                    }
                    else
                    {
                        $user->stock3d4d = $total3d4dStockAndCommEarn->stock3d4d;
                    }
                    $user->eat3d4d = $total3d4dEat->eat3d4d;
                    $user->commission3d4dEarn = $total3d4dEat->comm3d4dEarn;
                    $user->selfEat3d4dComm = $total3d4dEat->selfEatComm;
                    $user->commission3d4d = ((!$noDownLine) ? $total3d4dComm->commPaid: 0);
                }
            }
            
            if ($include5D6D)
            {
                if ($stageList != null)
                {
                    $total5d6dBet = collect(DB::SELECT("select sum(stock5d+stock6d) as total_bet from stage_details where 
                                                        downlinerecordid = 1 and stageid in (".$stageList.")"))->first();
            
                    $total5d6dStockAndCommEarn = collect(DB::SELECT("select sum(stock5d+stock6d) as stock5d6d,
                                                                     sum(eat5d+eat6d) as eat5d6d
                                                                     from stage_details where userid='".$user->id."' and stageid in (".$stageList.")"))->first();

                    $total5d6dEat = collect(DB::SELECT("select sum(eat5d+eat6d) as eat5d6d,
                                                        sum(comm5d+comm6d) as comm5d6dEarn,
                                                        sum((commrate5d*eat5d/100)+(commrate6d*eat6d/100)) as selfEatComm
                                                        from stage_details where userid='".$userId."' and stageid in (".$stageList.")"))->first();
                    $total5d6dComm = collect(DB::SELECT("select sum(comm5d+comm6d) as commPaid
                                                         from stage_details where downlinerecordid = IF(downlinerecordid = 1,1, downlinerecordid - 1) and stageid in (".$stageList.")"))->first();

                    $user->total5d6dbet = $total5d6dBet->total_bet;
                    if (!$noDownLine)
                    {
                        $user->stock5d6d = $total5d6dStockAndCommEarn->stock5d6d - $total5d6dStockAndCommEarn->eat5d6d;
                    }
                    else
                    {
                        $user->stock5d6d = $total5d6dStockAndCommEarn->stock5d6d;
                    }
                    $user->eat5d6d = $total5d6dEat->eat5d6d;
                    $user->commission5d6dEarn = $total5d6dEat->comm5d6dEarn;
                    $user->selfEat5d6dComm = $total5d6dEat->selfEatComm;
                    $user->commission5d6d = ((!$noDownLine) ? $total5d6dComm->commPaid :0);
                }
            }
        }
        return $userList;
    }
    
    public function getEatMap(Request $request, $userId, $type)
    {
        $record = null;
        
        if (!parent::checkIsIDBelongsToLoginUserHierachy($userId))
		{
		    return back()->with('error', "Invalid URL Access");
		}
        $user =  User::findOrFail($userId);
        
        $arr = null;
        if ($request->has('searchRange'))
        {
            $arr = array();
            $rangeArr = explode(' - ',$request->searchRange);
            foreach ($rangeArr as &$str) 
            {
                $str = str_replace('-', '', $str);
            }
            $query = "";

            $record = DB::SELECT("select c.id, c.name, sum(eatbig) as eatbig, sum(eatsmall) as eatsmall, sum(eat4a) as eat4a, sum(eat3a) as eat3a, sum(eat3abc) as eat3abc, 
                    sum(eat2a) as eat2a, sum(eat2abc) as eat2abc, sum(eat4b) as eat4b, sum(eat4c) as eat4c, sum(eat4d) as eat4d ,sum(eat4e) as eat4e, 
                    sum(eat4abc) as eat4abc, sum(eat5d) as eat5d, sum(eat6d) as eat6d from stage_details as a left join bet_stages as b on a.stageid = b.id 
                    left join users as c on c.id = a.userid where (a.userid in (select id from users where user_upline = '$userId') or a.userid = '$userId') and
                    b.drawdate >= '".$rangeArr[0]."' and b.drawdate <= '".$rangeArr[1]."' group by a.userid");
            if ($type == $this->type3d4d || $type == $this->typeAll)
            {
                $arr['Big'] = 'eatbig'; 
                $arr['Small'] = 'eatsmall'; 
                $arr['4A'] = 'eat4a'; 
                $arr['3A'] = 'eat3a'; 
                $arr['3ABC'] = 'eat3abc'; 
            }
            if ($type == $this->type3d4dSpecial || $type == $this->typeAll)
            {
                $arr['2A'] = 'eat2a'; 
                $arr['2ABC'] = 'eat2abc'; 
                $arr['4B'] = 'eat4b'; 
                $arr['4C'] = 'eat4c'; 
                $arr['4D'] = 'eat4d'; 
                $arr['4E'] = 'eat4e'; 
                $arr['4ABC'] = 'eat4abc'; 
            }
            if ($type == $this->type5d6d || $type == $this->typeAll)
            {
                $arr['5D'] = 'eat5d';
                $arr['6D'] = 'eat6d';
            }
        }
        return \View('eatMapReport', compact('record','user', 'arr','type'));
    }
    
    public function getBetReport(Request $request, $type, $targetUserId = null)
    {
        $record = null;
        $arr = null;
        $userId = ($targetUserId == null) ? Session::get('loginUserId'): $targetUserId;
        if ($request->has('searchRange'))
        {
            $userId = ($targetUserId == null) ? $request->userId: $targetUserId;

            if (!parent::checkIsIDBelongsToLoginUserHierachy($userId))
		    {
			    return back()->with('error', "Invalid URL Access");
		    }
            $arr = array();
            $rangeArr = explode(' - ',$request->searchRange);
            foreach ($rangeArr as &$str) 
            {
                $str = str_replace('-', '', $str);
            }
            if ($type == $this->type3d4d || $type == $this->typeAll)
            {
                $arr['Big'] = 'stockbig'; 
                $arr['Small'] = 'stocksmall'; 
                $arr['4A'] = 'stock4a'; 
                $arr['3A'] = 'stock3a'; 
                $arr['3ABC'] = 'stock3abc'; 
            }
            if ($type == $this->type3d4dSpecial || $type == $this->typeAll)
            {
                $arr['2A'] = 'stock2a'; 
                $arr['2ABC'] = 'stock2abc'; 
                $arr['4B'] = 'stock4b'; 
                $arr['4C'] = 'stock4c'; 
                $arr['4D'] = 'stock4d'; 
                $arr['4E'] = 'stock4e'; 
                $arr['4ABC'] = 'stock4abc'; 
            }
            if ($type == $this->type5d6d || $type == $this->typeAll)
            {
                $arr['5D'] = 'stock5d';
                $arr['6D'] = 'stock6d';
            }

            $downline = DB::SELECT("select * from users where user_upline='$userId'");
            $record = DB::SELECT("select c.id, c.name, b.pool, sum(stockbig) as stockbig, sum(stocksmall) as stocksmall, sum(stock4a) as stock4a, sum(stock3a) as stock3a, 
                                  sum(stock3abc) as stock3abc, sum(stock2a) as stock2a, sum(stock2abc) as stock2abc, sum(stock4b) as stock4b, sum(stock4c) as stock4c, sum(stock4d) as stock4d ,
                                  sum(stock4e) as stock4e, sum(stock4abc) as stock4abc, sum(stock5d) as stock5d, sum(stock6d) as stock6d from stage_details as a left join 
                                  bet_stages as b on a.stageid = b.id left join users as c on c.id = a.userid where 
                                  a.userid = '$userId' and b.drawdate >= '".$rangeArr[0]."' and b.drawdate <= '".$rangeArr[1]."' group by a.userid, b.pool");
        }

        $user =  User::findOrFail($userId);
        $downline = DB::SELECT("select * from users where user_upline='$userId'");
        return \View('betReport', compact('record','user', 'arr', 'type', 'downline'));
    }

    public function bettingRecords(Request $request){
        try{
            $pools = Pool::get();

            $query_pool = $request->query("pool", "all");
            $query_number = $request->query("number", null);
            $query_user = $request->query("user", null);
            $query_page = $request->query("page", null);
            $query_startdate = $request->query("start_date", null);
            $query_enddate = $request->query("end_date", null);

            $bets = BetStage::query();

            if($query_startdate != null){
                $start_date = Carbon::createFromFormat("d/m/Y", $query_startdate, 'Asia/Kuala_Lumpur')->format('Y-m-d 00:00:00');
                if($query_enddate != null){
                    $end_date = Carbon::createFromFormat("d/m/Y", $query_enddate, 'Asia/Kuala_Lumpur')->format('Y-m-d 23:59:59');

                    $bets->where('created_at', '>=', $start_date)->where('created_at', '<=', $end_date);
                }
            }

            if($query_pool != "all"){
                $bets->join("pools", "pools.code", "=", "bet_stages.pool")
                ->where("bet_stages.pool", $query_pool);
            }

            if($query_number != null){
                $bets->where("bet_stages.number", $query_number);
            }

            if($query_user != null){
                $user = User::where("username", $query_user)->first();
                if(!empty($user)){
                    $bets->where("bet_stages.userid", $user->id);
                }
            }

            if($query_page != null){
                $bets->join("tickets", "tickets.id", "=", "bet_stages.ticketid")
                ->where("tickets.pageid", $query_page);
            }

            $bets = $bets->select(
                "bet_stages.id", "ticketid", "bet_stages.pool", "bet_stages.userid", "number", "drawdate", 
                "actualbig", "actualsmall", "actual4a", "actual4b", 
                "actual4c", "actual4d", "actual4e", "actual4abc", 
                "actual3a", "actual3abc", "actual2a", "actual2abc", 
                "bet_stages.created_at")
                ->get();
            
            return view("bettingRecords", compact("pools", "bets", "query_pool", "query_number", "query_user", "query_page", "query_startdate", "query_enddate"));
        } catch (\Exception $e) {
            Session::flash("msg", "Unable to filter betting records.");
            Session::flash("class", "alert-danger");
            return redirect::action("ReportController@bettingRecords");
        }
    }

    public function autoTicketing(Request $request) {
         try{
            $autotickets = AutoTicketing::with('getTicket')->where("userid", Auth()->user()->id)->get();

            return view("autoTicketing", compact("autotickets"));
        } catch (\Exception $e) {
            Session::flash("msg", $e->getMessage());
            Session::flash("class", "alert-danger");

            return view("autoTicketing");
        }
    }

    public function getBetStages(Request $request, $ticketid) {
         try{
            $betstages = BetStage::with('getType')->where("ticketid", $ticketid)->orderBy("pool", "asc")->get();

            return ['success' => $betstages];
        } catch (\Exception $e) {
            Session::flash("msg", $e->getMessage());
            Session::flash("class", "alert-danger");

            return view("autoTicketing");
        }
    } 

    public function changeAutoTicketing(Request $request, $id, $status) {
        try
        {
            $autoTicketing = AutoTicketing::findOrFail($id);
            $autoTicketing->status = $status;
            $autoTicketing->save();

            return ['success' => $status];
        }
        catch (\Exception $e)
        {
            Session::flash("msg", $e->getMessage());
            Session::flash("class", "alert-danger");
            Session::flash("error", $e->getMessage());
            return "error";
        }
    }

    public function deleteAutoTicketing(Request $request, $id) {
        try
        {
            $autoTicketing = AutoTicketing::findOrFail($id);
            $autoTicketing->delete();

            $autotickets = AutoTicketing::with('getTicket')->where("userid", Auth()->user()->id)->get();

            return ['success' => $autotickets];
        }
        catch (\Exception $e)
        {
            Session::flash("msg", $e->getMessage());
            Session::flash("class", "alert-danger");
            Session::flash("error", $e->getMessage());
            return "error";
        }
    }

}
