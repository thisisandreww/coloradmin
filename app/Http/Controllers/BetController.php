<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

use App\EatMapReferences;
use App\Pool;
use App\User;
use App\EatMap;
use App\Schedule;
use App\BetCutOffSetting;
use App\AgentDistribution;
use App\Ticket;
use App\Arrangement;
use App\SystemSetting;
use App\AutoTicketing;

class BetController extends Controller
{   

    /*
     * Open up Bet 3d4d form
     */
    public function bet3d4d()
    {             
        // 1. Get the real me
        $me = self::getRealMe();
        $myeat = self::getAllMyDirectDownlines($me);
        
        // 2. Get all the pools available from pools table
        $pools = Pool::all();
        
        // 3. Get the bettings schedule that available
        date_default_timezone_set("Asia/Kuala_Lumpur");
        $date_str = Carbon::now()->format('Ymd');        
        
        // 4. If current time >= 2000 then 
        if (date('H') >= 20) {
            $date_str = Carbon::now()->addDays(1)->format('Ymd');            
        }
        
        // 4. Get all cutoff
        $schedules = Schedule::where('drawdate', '>=', $date_str)->take(4)->get();
        
        $data = array('loginUser'  => Auth::User(),
            'me'  => $me, 
            'downlines' => $myeat,
            'pools' => $pools,
            'schedules'=>$schedules
        );
        
        return view("bet3d4d")->with('data', $data);
    }
    
    /**
     * Request to make betting for 3D/4D
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function makeBet3d4d(Request $request)
    {
        // 0. Set the DateTime to Asia/Kuala Lumpur
        date_default_timezone_set("Asia/Kuala_Lumpur");
        $date_str = Carbon::now()->format('d/m');
        $date_str_yyMMdd = Carbon::now()->format('Ymd');
        $time_str = Carbon::now()->format('H:i');
        $date_str = $date_str.("/99")." ".$time_str;
        $usr_str = $request->input('betAgent');
        //dd($date_str);
        
        // 1. Get the bettings schedule that available
        $date_str = Carbon::now()->format('Ymd');
        $initial_total = $request->input('hidTotal');
        
        // 2. If current time >= 2000 then
        if (date('H') >= 20) {
            $date_str = Carbon::now()->addDays(1)->format('Ymd');
        }
        
        try {            
            // get the betting agent
            $agent = \App\User::where('username', '=', $usr_str)->get()->first();
            
            // check if agent valid
            if ($agent == null)
            {
                // if agent cannot be found in the DB
                return response()->json(['success'=>false, 'message'=> 'The betting agent: '.$usr_str.' is not valid.','data'=>$agent]);
            }
            
            // check if package valid?
            if ($agent->activated_package_3D4D_id == 0)
            {
                // there is no activated package thus reject the betting with error message
                return response()->json(['success'=>false, 'message'=> 'The betting agent: '.$usr_str.' has no betting 3D/4D packages activated.','data'=>$agent]);
            }
            
            // extract good bet entries from the request            
            $data = self::extractBet3d4dData($request, $agent);

            // bet data extraction
            $str_bet = self::getBetString($data);

            // verify if the schedule is valid
            // 1. check if all the date having all the pools of all the bets
            $all_pools = \App\Pool::all()->keyBy("code");
            $cutOff = \App\BetCutOffSetting::get()->keyBy('pool_id');
            foreach($data->date as $d)
            {
                // get the available pools for the date
                $schedule = \App\Schedule::where("drawdate", "=", $d)->get()->first();

                foreach($data->number as $n)
                {
                    foreach($n->pools as $p)
                    {
                        $pool_shortname = $all_pools[$p]->shortname;
                        $pool_id = $all_pools[$p]->id;

                        if ($schedule->$pool_shortname == 0 && $p != 'G')
                        {
                            return response()->json(['success'=>false, 'message'=> 'The date: '.$d.' has no draw for: '.$all_pools[$p]->name,'data'=>$agent]);
                        }
                        
                        // if same date happen
                        if ($d == $date_str_yyMMdd)
                        {
                            // if same date then need to make sure there is no bets after the cutoff date
                            $cutOffTime = $cutOff[$pool_id]->hours . $cutOff[$pool_id]->minutes;
                            $timeNow = date("Hi");
                            if($timeNow >= $cutOffTime){
                                return response()->json(['success'=>false, 'message'=> 'Cut off time passed for pool: '.$all_pools[$p]->name." for date: ".$d,'data'=>$agent]);
                            }
                        }
                    }
                }               
            }

            // now all the betting records shall be valid
            // expand the bet records
            $out = new \App\StructuredNumber();
            $output = $out->structurizeBetList($data);
            
            $str = json_encode($output);   
            $returned_eat_info =  DB::SELECT('CALL prepare_3d4d(:str)', ['str' => $str]);            
            $we_array = [];
            $bet_agent = 0;
            $bet_package = 0;
            $bet_type = '3D/4D';

            // nice, oh then we now start to distribute - do one round of massaging of the data first
            // foreach starts
            foreach($returned_eat_info as $eat_info)
            {
                if ($bet_agent == 0)
                {
                    $bet_agent = $eat_info->userid;
                    $bet_package = $eat_info->package;
                }
                // define
                $we = new \App\WorkingElement();
                
                $we->number = $eat_info->number;
                $we->pool = $eat_info->pool;
                $we->drawdate = $eat_info->drawdate;
                $we->position = $eat_info->position_3d4d;
                $we->amount_Big = $eat_info->amount_Big;
                $we->amount_Small = $eat_info->amount_Small;
                $we->amount_3A = $eat_info->amount_3A;
                $we->amount_3ABC = $eat_info->amount_3ABC;
                $we->amount_4A = $eat_info->amount_4A;
                $we->payrate = $eat_info->payrate;
                $we->eat_type_3d4d = $eat_info->eat_type_3d4d;
                $we->uniqueid = $eat_info->uniqueid;
                $we->rowid = $eat_info->rowid;
                $we->userid = $eat_info->userid;
                $we->hierarchy = $eat_info->hierarchy;

                //Get bet agent bet method, bet format, arrangement
                $bet_user = User::find($eat_info->userid);

                $we->arrangement = Arrangement::getCode($bet_user->arrangement_id);
                // for total
                
                // extract the commission out
                if ($eat_info->commission != null)
                {
                    if ($eat_info->commission != "")
                    {
                        // commission is valid
                        $comm_array = json_decode($eat_info->commission, true);
                        
                        foreach($comm_array as $c)
                        {
                            if ($c['type'] == 'Big')
                            {
                                $we->commission_Big = $c['commission'];
                            }
                            else if ($c['type'] == 'Small')
                            {
                                $we->commission_Small = $c['commission'];
                            }
                            else if ($c['type'] == '3A')
                            {
                                $we->commission_3A = $c['commission'];
                            }
                            else if ($c['type'] == '3ABC')
                            {
                                $we->commission_3ABC = $c['commission'];
                            }
                            else if ($c['type'] == '4A')
                            {
                                $we->commission_4A = $c['commission'];
                            }                            
                        }
                    }
                }            

                if ($eat_info->payrate != null)
                {
                    if ($eat_info->payrate != "")
                    {

                        // commission is valid
                        $payrate_array = json_decode($eat_info->payrate, true);
                        
                        foreach($payrate_array as $p)
                        {
                            if ($p['type'] == 'Big')
                            {
                                $payrate_big_array = json_decode($p['value']);
                                foreach($payrate_big_array as $p1)
                                {
                                    if ($p1->type == 'first_prize')
                                    {
                                        $we->pay_Big = $p1->rate;
                                        break;
                                    }
                                }
                            }
                            else if ($p['type'] == 'Small')
                            {
                                $payrate_small_array = json_decode($p['value']);
                                foreach($payrate_small_array as $p1)
                                {
                                    if ($p1->type == 'first_prize')
                                    {
                                        $we->pay_Small = $p1->rate;
                                        break;
                                    }
                                }
                            }
                            else if ($p['type'] == '3A')
                            {
                                $payrate_3a_array = json_decode($p['value']);
                                foreach($payrate_3a_array as $p1)
                                {
                                    if ($p1->type == 'first_prize')
                                    {
                                        $we->pay_3A = $p1->rate;
                                        break;
                                    }
                                }
                            }
                            else if ($p['type'] == '3ABC')
                            {
                                $payrate_3abc_array = json_decode($p['value']);
                                foreach($payrate_3abc_array as $p1)
                                {
                                    if ($p1->type == 'first_prize')
                                    {
                                        $we->pay_3ABC = $p1->rate;
                                        break;
                                    }
                                }
                            }
                            else if ($p['type'] == '4A')
                            {
                                $payrate_4a_array = json_decode($p['value']);
                                foreach($payrate_4a_array as $p1)
                                {
                                    if ($p1->type == 'first_prize')
                                    {
                                        $we->pay_4A = $p1->rate;
                                        break;
                                    }
                                }
                            }
                            
                        }
                    }   
                }


                if ($eat_info->thelimit != null)
                {
                    if ($eat_info->thelimit != "")
                    {
                        // the limit is valid
                        $limit_array = json_decode($eat_info->thelimit, true);
                        
                        foreach($limit_array as $c)
                        {
                            if ($c['type'] == 'Big')
                            {
                                $we->limit_Big = $c['value'] - $eat_info->eat_Big;
                            }
                            else if ($c['type'] == 'Small')
                            {
                                $we->limit_Small = $c['value'] - $eat_info->eat_Small;
                            }
                            else if ($c['type'] == '3A')
                            {
                                $we->limit_3A = $c['value'] - $eat_info->eat_3A;
                            }
                            else if ($c['type'] == '3ABC')
                            {
                                $we->limit_3ABC = $c['value'] - $eat_info->eat_3ABC;
                            }
                            else if ($c['type'] == '4A')
                            {
                                $we->limit_4A = $c['value'] - $eat_info->eat_4A;
                            }
                            else if ($c['type'] == 'total')
                            {
                                $we->limit_Big = $eat_info->eat_Big;
                                $we->limit_Small = $eat_info->eat_Small;
                                $we->limit_3A = $eat_info->eat_3A;
                                $we->limit_3ABC = $eat_info->eat_3ABC;
                                $we->limit_4A = $eat_info->eat_4A;

                                //$we->limit_3A = $c['value'] - $eat_info->eat_3A;
                                $we->limit_Total = $c['value'] - ($eat_info->eat_Big*$we->pay_Big + $eat_info->eat_Small*$we->pay_Small
                                    + $eat_info->eat_3A*$we->pay_3A + $eat_info->eat_3ABC*$we->pay_3ABC + $eat_info->eat_4A*$we->pay_4A);
                            }
                            else if ($c['type'] == 'group')
                            {
                                // Set a limit for group
                                // See if the current number drop into which group
                                $range = $c['sub_type'];
                                $range = explode('-', $range);
                                $startNum = $range[0];
                                $endNum = $range[1];

                                if ($c['tertier_type'] == 'Big')
                                {
                                    if ($we->number >= $startNum && $we->number <= $endNum) {
                                        $we->limit_Big = $c['value'] - $eat_info->eat_Big;
                                    }
                                }
                                else if ($c['tertier_type'] == 'Small')
                                {
                                    if ($we->number >= $startNum && $we->number <= $endNum) {
                                        $we->limit_Small = $c['value'] - $eat_info->eat_Small;
                                    }
                                }
                                else if ($c['tertier_type'] == '3A')
                                {
                                    $lastThreeNumbers = substr($we->number, -3);
                                    if ($lastThreeNumbers >= $startNum && $lastThreeNumbers <= $endNum) {
                                        $we->limit_3A = $c['value'] - $eat_info->eat_3A;
                                    }
                                }
                                else if ($c['tertier_type'] == '3ABC')
                                {
                                    $lastThreeNumbers = substr($we->number, -3);
                                    if ($lastThreeNumbers >= $startNum && $lastThreeNumbers <= $endNum) {
                                        $we->limit_3ABC = $c['value'] - $eat_info->eat_3ABC;
                                    }
                                }
                                else if ($c['tertier_type'] == '4A')
                                {
                                    if ($we->number >= $startNum && $we->number <= $endNum) {
                                        $we->limit_4A = $c['value'] - $eat_info->eat_4A;
                                    }
                                }
                            }                            
                        }
                    }
                }
                
                $we_array = array_add($we_array,
                    $we->rowid.'_'.$we->uniqueid,
                    $we
                );
            }
            
            // foreach ends

            $rowId = 0;
            
            $wD_array = [];
            $final_array = [];
            $stock = new \App\BetType();

            $max_id = 0;
            foreach ($we_array as $w)
            {
                if ($w->hierarchy > $max_id)
                {
                    $max_id = $w->hierarchy;
                    
                }
                else if ($w->hierarchy < $max_id) break; // When level id goes smaller than max_id, that means one iteration has ended...                
            }

            $bet_stages = [];
            // foreach start distribution
            foreach($we_array as &$w)
            {                
                $wD = new \App\AgentDistribution();
                $wD->uid = $w->userid;
                $wD->rid = $w->rowid;
                $wD->did = $w->uniqueid;
                $wD->hid = $w->hierarchy;
                $wD->pos = $w->position;
                
                $wD->cr_Big = $w->commission_Big;
                $wD->cr_Small = $w->commission_Small;
                $wD->cr_3A = $w->commission_3A;
                $wD->cr_3ABC = $w->commission_3ABC;
                $wD->cr_4A = $w->commission_4A;

                if ($rowId == 0)
                {
                    $rowId = $w->rowid;
                    //set the stock
                    $stock->amount_Big = $w->amount_Big;
                    $stock->amount_Small = $w->amount_Small;
                    $stock->amount_3A = $w->amount_3A;
                    $stock->amount_3ABC = $w->amount_3ABC;
                    $stock->amount_4A = $w->amount_4A;
                    
                    // consider new stage
                    $bet_stage = new \App\BetStage();
                    $bet_stage->userid = $bet_agent;
                    $bet_stage->packageid = $bet_package;
                    $bet_stage->number = $w->number;
                    $bet_stage->drawdate = $w->drawdate;
                    $bet_stage->pool = $w->pool;
                    $bet_stage->betamountbig = number_format($w->amount_Big, 4);
                    $bet_stage->betamountsmall = number_format($w->amount_Small, 4);
                    $bet_stage->betamount3a = number_format($w->amount_3A, 4);
                    $bet_stage->betamount3abc = number_format($w->amount_3ABC, 4);
                    $bet_stage->betamount4a = number_format($w->amount_4A, 4);
                    $bet_stage->rowid = $w->rowid;   
                    $bet_stage->created_at = date('Y-m-d H:i:s');
                    $bet_stages[] = $bet_stage->attributesToArray();
                }
                else if ($rowId != $w->rowid)
                {
                    $wD_array = [];
                    $rowId = $w->rowid;
                    
                    $stock->amount_Big = $w->amount_Big;
                    $stock->amount_Small = $w->amount_Small;
                    $stock->amount_3A = $w->amount_3A;
                    $stock->amount_3ABC = $w->amount_3ABC;
                    $stock->amount_4A = $w->amount_4A;   
                    
                    // consider new stage
                    $bet_stage = new \App\BetStage();
                    $bet_stage->userid = $bet_agent;
                    $bet_stage->packageid = $bet_package;
                    $bet_stage->number = $w->number;
                    $bet_stage->drawdate = $w->drawdate;
                    $bet_stage->pool = $w->pool;
                    $bet_stage->betamountbig = number_format($w->amount_Big, 4);
                    $bet_stage->betamountsmall = number_format($w->amount_Small, 4);
                    $bet_stage->betamount3a = number_format($w->amount_3A, 4);
                    $bet_stage->betamount3abc = number_format($w->amount_3ABC, 4);
                    $bet_stage->betamount4a = number_format($w->amount_4A, 4);
                    $bet_stage->rowid = $w->rowid;
                    $bet_stage->created_at = date('Y-m-d H:i:s');
                    // check all the 
                    $bet_stages[] = $bet_stage->attributesToArray();
                }
                
                $perc_to_be_left = (100 - $w->position) / 100;

                // go through the position taking first
                if ($w->eat_type_3d4d == "Amount" || $w->eat_type_3d4d == "Group")
                {
                    $actualEat = self::tryEat($w->limit_Big, $stock->amount_Big, $stock->amount_Big - $w->amount_Big * $perc_to_be_left);
                    $w->limit_Big -= $actualEat;
                    $stock->amount_Big -= $actualEat;
                    $wD->e_Big = $actualEat;
                    
                    $actualEat = self::tryEat($w->limit_Small, $stock->amount_Small, $stock->amount_Small - $w->amount_Small * $perc_to_be_left);
                    $w->limit_Small -= $actualEat;
                    $stock->amount_Small -= $actualEat;
                    $wD->e_Small = $actualEat;
                    
                    $actualEat = self::tryEat($w->limit_3A, $stock->amount_3A, $stock->amount_3A - $w->amount_3A * $perc_to_be_left);
                    $w->limit_3A -= $actualEat;
                    $stock->amount_3A -= $actualEat;
                    $wD->e_3A = $actualEat;
                    
                    $actualEat = self::tryEat($w->limit_3ABC, $stock->amount_3ABC, $stock->amount_3ABC - $w->amount_3ABC * $perc_to_be_left);
                    $w->limit_3ABC -= $actualEat;
                    $stock->amount_3ABC -= $actualEat;
                    $wD->e_3ABC = $actualEat;
                    
                    $actualEat = self::tryEat($w->limit_4A, $stock->amount_4A, $stock->amount_4A - $w->amount_4A * $perc_to_be_left);
                    $w->limit_4A -= $actualEat;
                    $stock->amount_4A -= $actualEat;
                    $wD->e_4A = $actualEat;  
                }
                else if ($w->eat_type_3d4d == "Total Payout")
                {
                    $arrangement = explode("-", $w->arrangement);

                    foreach ($arrangement as $arr) {
                        if ($w->limit_Total < 0) {
                            $w->limit_Total = 0;
                        }

                        if ($arr == "B") {
                            $actualEat = self::tryEat($stock->amount_Big, $stock->amount_Big, $stock->amount_Big - $w->amount_Big * $perc_to_be_left);
                            if ($w->limit_Total < $actualEat*$w->pay_Big) {
                                $actualEat = $w->limit_Total/$w->pay_Big; 
                            }

                            $w->limit_Total -= ($actualEat*$w->pay_Big);
                            $stock->amount_Big -= $actualEat;
                            $wD->e_Big = $actualEat;
                        }
                        elseif ($arr == "S") {
                            $actualEat = self::tryEat($stock->amount_Small, $stock->amount_Small, $stock->amount_Small - $w->amount_Small * $perc_to_be_left);

                            if ($w->limit_Total < $actualEat*$w->pay_Small) {
                                $actualEat = $w->limit_Total/$w->pay_Small; 
                            }

                            $w->limit_Total -= ($actualEat*$w->pay_Small);
                            $stock->amount_Small -= $actualEat;
                            $wD->e_Small = $actualEat;
                        }
                        elseif ($arr == "4A") {
                            $actualEat = self::tryEat($stock->amount_4A, $stock->amount_4A, $stock->amount_4A - $w->amount_4A * $perc_to_be_left);

                            if ($w->limit_Total < $actualEat*$w->pay_4A) {
                                $actualEat = $w->limit_Total/$w->pay_4A; 
                            }

                            $w->limit_Total -= ($actualEat*$w->pay_4A);
                            $stock->amount_4A -= $actualEat;
                            $wD->e_4A = $actualEat;
                        }
                        elseif ($arr == "C") {
                            $actualEat = self::tryEat($stock->amount_3ABC, $stock->amount_3ABC, $stock->amount_3ABC - $w->amount_3ABC * $perc_to_be_left);

                            if ($w->limit_Total < $actualEat*$w->pay_3ABC) {
                                $actualEat = $w->limit_Total/$w->pay_3ABC; 
                            }

                            $w->limit_Total -= ($actualEat*$w->pay_3ABC);
                            $stock->amount_3ABC -= $actualEat;
                            $wD->e_3ABC = $actualEat;
                        }
                        elseif ($arr == "A" || $arr == "3A") {
                            $actualEat = self::tryEat($stock->amount_3A, $stock->amount_3A, $stock->amount_3A - $w->amount_3A * $perc_to_be_left);

                            if ($w->limit_Total < $actualEat*$w->pay_3A) {
                                $actualEat = $w->limit_Total/$w->pay_3A; 
                            }

                            $w->limit_Total -= ($actualEat*$w->pay_3A);
                            $stock->amount_3A -= $actualEat;
                            $wD->e_3A = $actualEat;
                        }
                    }
                    /*$totalBasedLimitEaten = $w->limit_Big * $w->pay_Big + $w->limit_Small * $w->pay_Small + $w->limit_4A * $w->pay_4A + $w->limit_3ABC * $w->pay_3ABC 
                                            + $w->limit_3A * $w->pay_3A;
                    $remainTotalBasedQuota = $w->limit_Total - $totalBasedLimitEaten;
                    
                    // 4D uses TryTotalEat
                    $actualEat = TryTotalEat(agent.RemainTotalBasedQuota, stock._big, stock._big - agent.BetAmount._big * perc_to_be_left_total, agent.FirstPrize._big);
                    agent.RemainTotalBasedQuota -= actualEat * agent.FirstPrize._big;
                    agent.EatCurrent._big = actualEat;
                    stock._big -= actualEat;
                    
                    // 4D uses TryTotalEat
                    actualEat = TryTotalEat(agent.RemainTotalBasedQuota, stock._small, stock._small - agent.BetAmount._small * perc_to_be_left_total, agent.FirstPrize._small);
                    agent.RemainTotalBasedQuota -= actualEat * agent.FirstPrize._small;
                    agent.EatCurrent._small = actualEat;
                    stock._small -= actualEat;
                    
                    // 4D uses TryTotalEat
                    actualEat = TryTotalEat(agent.RemainTotalBasedQuota, stock._4a, stock._4a - agent.BetAmount._4a * perc_to_be_left_total, agent.FirstPrize._4a);
                    agent.RemainTotalBasedQuota -= actualEat * agent.FirstPrize._4a;
                    agent.EatCurrent._4a = actualEat;
                    stock._4a -= actualEat;
                    
                    // 3D uses TryEat
                    actualEat = TryEat(agent.RemainQuota._3a, stock._3a, stock._3a - agent.BetAmount._3a * perc_to_be_left_amount);
                    agent.RemainQuota._3a -= actualEat;
                    stock._3a -= actualEat;
                    agent.EatCurrent._3a = actualEat;
                    
                    // 3D uses TryEat
                    actualEat = TryEat(agent.RemainQuota._3c, stock._3c, stock._3c - agent.BetAmount._3c * perc_to_be_left_amount);
                    agent.RemainQuota._3c -= actualEat;
                    stock._3c -= actualEat;
                    agent.EatCurrent._3c = actualEat;*/
                }

                // add in working arrays
                $wD_array = array_add($wD_array,
                    $wD->rid.'_'.$wD->did,
                    $wD
                );
                
                // position done
                // now handling if that come to the last agent
                if ($w->hierarchy == $max_id)
                {

                    // hiararchy = 1 means the most top agent normally means
                    // company or houses/investors
                    // loop the working arrays to pump in the eat
                    // and finally calculate the commission for himself                    
                    $restock = new \App\BetType();
                    foreach($wD_array as &$reeat_wD)
                    {
                        $w1 = $we_array[$reeat_wD->rid.'_'.$reeat_wD->did];
                        if ($w1->eat_type_3d4d == "Amount" || $w1->eat_type_3d4d == "Group")
                        {
                            $actualEat = self::tryEat($w1->limit_Big, $stock->amount_Big, $stock->amount_Big);
                            $w1->limit_Big -= $actualEat;
                            $reeat_wD->e_Big += $actualEat;
                            $stock->amount_Big -= $actualEat;
                            $restock->amount_Big += $reeat_wD->e_Big;
                            
                            $actualEat = self::tryEat($w1->limit_Small, $stock->amount_Small, $stock->amount_Small);
                            $w1->limit_Small -= $actualEat;
                            $reeat_wD->e_Small += $actualEat;
                            $stock->amount_Small -= $actualEat;
                            $restock->amount_Small += $reeat_wD->e_Small;
                            
                            $actualEat = self::tryEat($w1->limit_3A, $stock->amount_3A, $stock->amount_3A);
                            $w1->limit_3A -= $actualEat;
                            $reeat_wD->e_3A += $actualEat;
                            $stock->amount_3A -= $actualEat;
                            $restock->amount_3A += $reeat_wD->e_3A;
                            
                            $actualEat = self::tryEat($w1->limit_3ABC, $stock->amount_3ABC, $stock->amount_3ABC);
                            $w1->limit_3ABC -= $actualEat;
                            $reeat_wD->e_3ABC += $actualEat;
                            $stock->amount_3ABC -= $actualEat;
                            $restock->amount_3ABC += $reeat_wD->e_3ABC;
                            
                            $actualEat = self::tryEat($w1->limit_4A, $stock->amount_4A, $stock->amount_4A);
                            $w1->limit_4A -= $actualEat;
                            $reeat_wD->e_4A += $actualEat;
                            $stock->amount_4A -= $actualEat;
                            $restock->amount_4A += $reeat_wD->e_4A;
                        }
                        else if ($w1->eat_type_3d4d == "Total Payout")
                        {
                            $arrangement = explode("-", $w1->arrangement);

                            foreach ($arrangement as $arr) {
                                if ($w1->limit_Total < 0) {
                                    $w1->limit_Total = 0;
                                }

                                if ($arr == "B") {
                                    $actualEat = $stock->amount_Big;

                                    if ($w1->limit_Total < $actualEat*$w1->pay_Big) {
                                        $actualEat = $w1->limit_Total/$w1->pay_Big; 
                                    }

                                    $w1->limit_Total -= ($actualEat*$w1->pay_Big);
                                    $reeat_wD->e_Big += $actualEat;
                                    $stock->amount_Big -= $actualEat;
                                    $restock->amount_Big += $reeat_wD->e_Big;
                                }
                                elseif ($arr == "S") {
                                    $actualEat = $stock->amount_Small;

                                    if ($w1->limit_Total < $actualEat*$w1->pay_Small) {
                                        $actualEat = $w1->limit_Total/$w1->pay_Small; 
                                    }

                                    $w1->limit_Total -= ($actualEat*$w1->pay_Small);
                                    $reeat_wD->e_Small += $actualEat;
                                    $stock->amount_Small -= $actualEat;
                                    $restock->amount_Small += $reeat_wD->e_Small;
                                }
                                elseif ($arr == "4A") {
                                    $actualEat = $stock->amount_4A;

                                    if ($w1->limit_Total < $actualEat*$w1->pay_4A) {
                                        $actualEat = $w1->limit_Total/$w1->pay_4A; 
                                    }

                                    $w1->limit_Total -= ($actualEat*$w1->pay_4A);
                                    $reeat_wD->e_4A += $actualEat;
                                    $stock->amount_4A -= $actualEat;
                                    $restock->amount_4A += $reeat_wD->e_4A;
                                }
                                elseif ($arr == "C") {
                                    $actualEat = $stock->amount_3ABC;

                                    if ($w1->limit_Total < $actualEat*$w1->pay_3ABC) {
                                        $actualEat = $w1->limit_Total/$w1->pay_3ABC; 
                                    }

                                    $w1->limit_Total -= ($actualEat*$w1->pay_3ABC);
                                    $reeat_wD->e_3ABC += $actualEat;
                                    $stock->amount_3ABC -= $actualEat;
                                    $restock->amount_3ABC += $reeat_wD->e_3ABC;
                                }
                                elseif ($arr == "A" || $arr == "3A") {
                                    $actualEat = $stock->amount_3A;

                                    if ($w1->limit_Total < $actualEat*$w1->pay_3A) {
                                        $actualEat = $w1->limit_Total/$w1->pay_3A; 
                                    }

                                    $w1->limit_Total -= ($actualEat*$w1->pay_3A);
                                    $reeat_wD->e_3A += $actualEat;
                                    $stock->amount_3A -= $actualEat;
                                    $restock->amount_3A += $reeat_wD->e_3A;
                                }
                            }

                        }
                    }

                    foreach($wD_array as $recount_wD)
                    {
                        $w1 = $we_array[$recount_wD->rid.'_'.$recount_wD->did];
                        $final_dist = new \App\AgentDistribution();
                        $final_dist->did = $recount_wD->did;
                        $final_dist->uid = $recount_wD->uid;
                        $final_dist->rid = $recount_wD->rid;
                        $final_dist->pos = $recount_wD->pos;
                        $final_dist->hid = $recount_wD->hid;
                        
                        $final_dist->cr_Big = $recount_wD->cr_Big;
                        $final_dist->cr_Small = $recount_wD->cr_Small;
                        $final_dist->cr_3A = $recount_wD->cr_3A;
                        $final_dist->cr_3ABC = $recount_wD->cr_3ABC;
                        $final_dist->cr_4A = $recount_wD->cr_4A;

                        
                        if ($recount_wD->hid == $max_id)
                        {
                            // this is the last person in the hierarchy
                            $final_dist->s_Big = $restock->amount_Big;
                            $final_dist->e_Big = $recount_wD->e_Big;
                            
                            $final_dist->s_Small = $restock->amount_Small;
                            $final_dist->e_Small = $recount_wD->e_Small;
                            
                            $final_dist->s_3A = $restock->amount_3A;
                            $final_dist->e_3A = $recount_wD->e_3A; 
                            
                            $final_dist->s_3ABC = $restock->amount_3ABC;
                            $final_dist->e_3ABC = $recount_wD->e_3ABC;

                            $final_dist->s_4A = $restock->amount_4A;
                            $final_dist->e_4A = $recount_wD->e_4A;
                        }
                        else
                        {
                            $stockUp = 0;
                            $currentComm = 0;
                            
                            $stockUp = $restock->amount_Big - $recount_wD->e_Big;
                            //$currentComm = $stockUp * ($w->commission_Big + wa.Discount) / 100;
                            $currentComm = $stockUp * ($w1->commission_Big) / 100;
                            $final_dist->s_Big = $restock->amount_Big;
                            $final_dist->c_Big = $currentComm;
                            $final_dist->e_Big = $recount_wD->e_Big;
                            
                            $stockUp = $restock->amount_Small - $recount_wD->e_Small;
                            $currentComm = $stockUp * ($w1->commission_Small) / 100;
                            $final_dist->s_Small = $restock->amount_Small;
                            $final_dist->c_Small = $currentComm;
                            $final_dist->e_Small = $recount_wD->e_Small;
                            
                            $stockUp = $restock->amount_3A - $recount_wD->e_3A;
                            $currentComm = $stockUp * ($w1->commission_3A) / 100;
                            $final_dist->s_3A = $restock->amount_3A;
                            $final_dist->c_3A = $currentComm;
                            $final_dist->e_3A = $recount_wD->e_3A;                            
                            
                            $stockUp = $restock->amount_3ABC - $recount_wD->e_3ABC;
                            $currentComm = $stockUp * ($w1->commission_3ABC) / 100;
                            $final_dist->s_3ABC = $restock->amount_3ABC;
                            $final_dist->c_3ABC = $currentComm;
                            $final_dist->e_3ABC = $recount_wD->e_3ABC;                            
                            
                            $stockUp = $restock->amount_4A - $recount_wD->e_4A;
                            $currentComm = $stockUp * ($w1->commission_4A) / 100;
                            $final_dist->s_4A = $restock->amount_4A;
                            $final_dist->c_4A = $currentComm;
                            $final_dist->e_4A = $recount_wD->e_4A;
                            
                            $restock->amount_Big -= $final_dist->e_Big;
                            $restock->amount_Small -= $final_dist->e_Small;
                            $restock->amount_3A -= $final_dist->e_3A;
                            $restock->amount_3ABC -= $final_dist->e_3ABC;
                            $restock->amount_4A -= $final_dist->e_4A;
                        }
                        
                        $final_array = array_add($final_array,
                            $final_dist->rid.'_'.$final_dist->did,
                            $final_dist
                        );
                    }
                    
                }
                // finish last agent
            }            
            // End foreach Distribution

            //Get bet agent bet method, bet format, arrangement
            $bet_user = User::find($bet_agent);

            $bet_format = Arrangement::getCode($bet_user->arrangement_id);

            // now start to save the bet records in the database
            // should loop final array
            // for every loop item
            // using $out 
            $submitHeaderRecordSql = "CALL submit_header_record(
            :currency, 
            :betAgent,
            :betPackage,
            :betType,
            :betMethod,
            :inbox,
            :betString,
            :initialAmount,
            :actualAmount,
            :betFormat,
            :drawFormat,
            :boxIbox
        )";

        $ticket_id = DB::SELECT($submitHeaderRecordSql, [
            'currency' => 'MYR',
            'betAgent' => $bet_agent,
            'betPackage' => $bet_package,
            'betType' => $bet_type,
            'betMethod' => $bet_user->getBetMethod(),
            'inbox' => '',
            'betString' => $str_bet,
            'initialAmount' => $initial_total,
            'actualAmount' => $initial_total,
            'betFormat' => $bet_format,
            'drawFormat' => $bet_user->pool_shortcode,
            'boxIbox' => $bet_user->getBoxIBox->box_format
        ])[0]->id;

        if ($request->autoTicketStatus != NULL) {
            $autoTicketing = new AutoTicketing();
            $autoTicketing->ticketid = $ticket_id;
            $autoTicketing->userid = Auth()->user()->id;
            $autoTicketing->status = 1; 
            $autoTicketing->save();
        }

            //Generate bet slip
        $str_inbox = self::getInboxStr($usr_str, $data, $ticket_id, $bet_format);

            // now start to save the bet stages
            // Bet Stages Array
            //$bet_stages = [];
        foreach($bet_stages as $key=> $o)
        {
            $bet_stages[$key]['ticketid'] = $ticket_id;
            $actual_big = 0;
            $actual_small = 0;
            $actual_3a = 0;
            $actual_3abc = 0;
            $actual_4a = 0;

            foreach($final_array as $final)
            {
                if ($final->rid == $o['rowid'])
                {
                    $actual_big += $final->e_Big;
                    $actual_small += $final->e_Small;
                    $actual_3a += $final->e_3A;
                    $actual_3abc += $final->e_3ABC;
                    $actual_4a += $final->e_4A;
                }
            }

            $bet_stages[$key]['actualbig'] = number_format($actual_big, 4);
            $bet_stages[$key]['actualsmall'] = number_format($actual_small, 4);
            $bet_stages[$key]['actual3a'] = number_format($actual_3a, 4);
            $bet_stages[$key]['actual3abc'] = number_format($actual_3abc, 4);
            $bet_stages[$key]['actual4a'] = number_format($actual_4a, 4);


        }

            // print_r($bet_stages); echo "\n";
        \App\BetStage::insert($bet_stages);

        $stages_inserted = \App\BetStage::where('ticketid', '=', $ticket_id)->get();
        $stages_details = [];
            // now start to save the bet stages details
        foreach($stages_inserted as $stage)
        {
                // get the stage id
            $stage_id = $stage -> id; 

            foreach($final_array as $final)
            {
                if ($final->rid == $stage->rowid)
                {
                    $key_now = $final->rid.'_'.$final->did;
                    $target_we = $we_array[$key_now];

                    $detail = new \App\StageDetail();
                    $detail->stageid = $stage_id;
                    $detail->userid = $target_we->userid;
                    $detail->downlinerecordid = $target_we->hierarchy;
                    $detail->status = 0;
                    $detail->position = $target_we -> position;
                    $detail->stockbig = number_format($final -> s_Big, 4);
                    $detail->stocksmall = number_format($final -> s_Small, 4);
                    $detail->stock3a = number_format($final -> s_3A, 4);
                    $detail->stock3abc = number_format($final -> s_3ABC, 4);
                    $detail->stock4a = number_format($final -> s_4A, 4);
                    $detail->eatbig = number_format($final->e_Big, 4);
                    $detail->eatsmall = number_format($final -> e_Small, 4);
                    $detail->eat3a = number_format($final -> e_3A, 4);
                    $detail->eat3abc = number_format($final -> e_3ABC, 4);
                    $detail->eat4a = number_format($final -> e_4A, 4);
                    $detail->commbig = number_format($final -> c_Big, 4);
                    $detail->commsmall = number_format($final -> c_Small, 4);
                    $detail->comm3a = number_format($final -> c_3A, 4);
                    $detail->comm3abc = number_format($final -> c_3ABC, 4);
                    $detail->comm4a = number_format($final -> c_4A, 4);
                    $detail->created_at = date('Y-m-d H:i:s');
                    $detail->commratebig = number_format($final -> cr_Big, 4);
                    $detail->commratesmall = number_format($final -> cr_Small, 4);
                    $detail->commrate3a = number_format($final -> cr_3A, 4);
                    $detail->commrate3abc = number_format($final -> cr_3ABC, 4);
                    $detail->commrate4a = number_format($final -> cr_4A, 4);

                    $stages_details[] = $detail->attributesToArray();
                }                    
            }
        }
            // print_r($stages_details); echo "\n";
        \App\StageDetail::insert($stages_details);

        return response()->json(['success'=>true, 'message'=> $we_array, 'tix_id'=> $ticket_id, 'data'=>$agent, 'ticket' => $str_inbox]);
    }
    catch(\Exception $ex)
    {
        return response()->json(['success'=>false, 'message'=> $ex->__toString(), 'data'=>$agent]);
    }
    return response()->json(['success'=>false, 'data'=>$agent]);
}

public function bet5d6d()
{
        // 1. Get the real me
    $me = self::getRealMe();
    $myeat = self::getAllMyDirectDownlines($me);

        // 2. Get all the pools available from pools table
    $pools = Pool::all();

        // 3. Get the bettings schedule that available
    date_default_timezone_set("Asia/Kuala_Lumpur");
    $date_str = Carbon::now()->format('Ymd');        

        // 4. If current time >= 2000 then 
    if (date('H') >= 20) {
        $date_str = Carbon::now()->addDays(1)->format('Ymd');            
    }

        // 4. Get all cutoff
    $schedules = Schedule::where('drawdate', '>=', $date_str)->take(4)->get();

    $data = array('loginUser'  => Auth::User(),
        'me'  => $me, 
        'downlines' => $myeat,
        'pools' => $pools,
        'schedules'=>$schedules
    );

    return view("bet5d6d", compact("data"));
}

public function makeBet5d6d(Request $request)
{
        // 0. Set the DateTime to Asia/Kuala Lumpur
    date_default_timezone_set("Asia/Kuala_Lumpur");
    $date_str = Carbon::now()->format('d/m');
    $date_str_yyMMdd = Carbon::now()->format('Ymd');
    $time_str = Carbon::now()->format('H:i');
    $date_str = $date_str.("/99")." ".$time_str;
    $usr_str = $request->input('betAgent');
        //dd($date_str);

        // 1. Get the bettings schedule that available
    $date_str = Carbon::now()->format('Ymd');
    $initial_total = $request->input('hidTotal');

        // 2. If current time >= 2000 then
    if (date('H') >= 20) {
        $date_str = Carbon::now()->addDays(1)->format('Ymd');
    }

    try {            
            // get the betting agent
        $agent = \App\User::where('username', '=', $usr_str)->get()->first();

            // check if agent valid
        if ($agent == null)
        {
                // if agent cannot be found in the DB
            return response()->json(['success'=>false, 'message'=> 'The betting agent: '.$usr_str.' is not valid.','data'=>$agent]);
        }

            // check if package valid?
        if ($agent->activated_package_5D6D_id == 0)
        {
                // there is no activated package thus reject the betting with error message
            return response()->json(['success'=>false, 'message'=> 'The betting agent: '.$usr_str.' has no betting 5D/6D packages activated.','data'=>$agent]);
        }

            // extract good bet entries from the request            
        $data = self::extractBet5d6dData($request, $agent);

        // bet data extraction
        $str_bet = self::get5d6dBetString($data);

        // verify if the schedule is valid
        // 1. check if all the date having all the pools of all the bets
        $all_pools = \App\Pool::all()->keyBy("code");
        $cutOff = \App\BetCutOffSetting::get()->keyBy('pool_id');
        foreach($data->date as $d)
        {
                // get the available pools for the date
            $schedule = \App\Schedule::where("drawdate", "=", $d)->get()->first();

            foreach($data->number as $n)
            {
                foreach($n->pools as $p)
                {
                    $pool_shortname = $all_pools[$p]->shortname;
                    $pool_id = $all_pools[$p]->id;

                    if ($schedule->$pool_shortname == 0 && $p != 'G')
                    {
                        return response()->json(['success'=>false, 'message'=> 'The date: '.$d.' has no draw for: '.$all_pools[$p]->name,'data'=>$agent]);
                    }

                        // if same date happen
                    if ($d == $date_str_yyMMdd)
                    {
                            // if same date then need to make sure there is no bets after the cutoff date
                        $cutOffTime = $cutOff[$pool_id]->hours . $cutOff[$pool_id]->minutes;
                        $timeNow = date("Hi");
                        if($timeNow >= $cutOffTime){
                            return response()->json(['success'=>false, 'message'=> 'Cut off time passed for pool: '.$all_pools[$p]->name." for date: ".$d,'data'=>$agent]);
                        }
                    }
                }
            }               
        }

        // now all the betting records shall be valid
        // expand the bet records
        $out = new \App\StructuredNumber();
        $output = $out->structurizeBetList($data);

        $str = json_encode($output);
        $returned_eat_info =  DB::SELECT('CALL prepare_5d6d(:str)', ['str' => $str]);            
        $we_array = [];
        $bet_agent = 0;
        $bet_package = 0;
        $bet_type = '5D/6D';

        // nice, oh then we now start to distribute - do one round of massaging of the data first
        // foreach starts
        foreach($returned_eat_info as $eat_info)
        {
            if ($bet_agent == 0)
            {
                $bet_agent = $eat_info->userid;
                $bet_package = $eat_info->package;
            }
                // define
            $we = new \App\WorkingElement();

            $we->number = $eat_info->number;
            $we->pool = $eat_info->pool;
            $we->drawdate = $eat_info->drawdate;
            $we->position = $eat_info->position_5d6d;
            $we->amount_5D = $eat_info->amount_5D;
            $we->amount_6D = $eat_info->amount_6D;
            $we->payrate = $eat_info->payrate;
            $we->eat_type_5d6d = $eat_info->eat_type_5d6d;
            $we->uniqueid = $eat_info->uniqueid;
            $we->rowid = $eat_info->rowid;
            $we->userid = $eat_info->userid;
            $we->hierarchy = $eat_info->hierarchy;

                //Get bet agent bet method, bet format, arrangement
            $bet_user = User::find($eat_info->userid);

            $we->arrangement = Arrangement::getCode($bet_user->arrangement_id);
                // for total

                // extract the commission out
            if ($eat_info->commission != null)
            {
                if ($eat_info->commission != "")
                {
                        // commission is valid
                    $comm_array = json_decode($eat_info->commission, true);

                    foreach($comm_array as $c)
                    {
                        if ($c['type'] == '5D')
                        {
                            $we->commission_5D = $c['commission'];
                        }
                        else if ($c['type'] == '6D')
                        {
                            $we->commission_6D = $c['commission'];
                        }                        
                    }
                }
            }            

            if ($eat_info->payrate != null)
            {
                if ($eat_info->payrate != "")
                {

                        // commission is valid
                    $payrate_array = json_decode($eat_info->payrate, true);

                    foreach($payrate_array as $p)
                    {
                        if ($p['type'] == '5D')
                        {
                            $payrate_5d_array = json_decode($p['value']);
                            foreach($payrate_5d_array as $p1)
                            {
                                if ($p1->type == 'first_prize')
                                {
                                    $we->pay_5D = $p1->rate;
                                    break;
                                }
                            }
                        }
                        else if ($p['type'] == '6D')
                        {
                            $payrate_6d_array = json_decode($p['value']);
                            foreach($payrate_6d_array as $p1)
                            {
                                if ($p1->type == 'first_prize')
                                {
                                    $we->pay_6D = $p1->rate;
                                    break;
                                }
                            }
                        }
                    }
                }   
            }


            if ($eat_info->thelimit != null)
            {
                if ($eat_info->thelimit != "")
                {
                        // the limit is valid
                    $limit_array = json_decode($eat_info->thelimit, true);

                    foreach($limit_array as $c)
                    {
                        if ($c['type'] == '5D')
                        {
                            $we->limit_5D = $c['value'] - $eat_info->eat_5D;
                        }
                        else if ($c['type'] == '6D')
                        {
                            $we->limit_6D = $c['value'] - $eat_info->eat_6D;
                        }
                        else if ($c['type'] == 'total')
                        {
                            $we->limit_5D = $eat_info->eat_5D;
                            $we->limit_6D = $eat_info->eat_6D;

                                //$we->limit_3A = $c['value'] - $eat_info->eat_3A;
                            $we->limit_Total = $c['value'] - ($eat_info->eat_5D*$we->pay_5D + $eat_info->eat_6D*$we->pay_6D);
                        }
                        else if ($c['type'] == 'group')
                        {
                                // Set a limit for group
                                // See if the current number drop into which group
                            $range = $c['sub_type'];
                            $range = explode('-', $range);
                            $startNum = $range[0];
                            $endNum = $range[1];

                            if ($c['tertier_type'] == '5D')
                            {
                                if ($we->number >= $startNum && $we->number <= $endNum) {
                                    $we->limit_5D = $c['value'] - $eat_info->eat_5D;
                                }
                            }
                            else if ($c['tertier_type'] == '6D')
                            {
                                if ($we->number >= $startNum && $we->number <= $endNum) {
                                    $we->limit_6D = $c['value'] - $eat_info->eat_6D;
                                }
                            }
                        }                            
                    }
                }
            }

            $we_array = array_add($we_array,
                $we->rowid.'_'.$we->uniqueid,
                $we
            );
        }

            // foreach ends

        $rowId = 0;

        $wD_array = [];
        $final_array = [];
        $stock = new \App\BetType();

        $max_id = 0;
        foreach ($we_array as $w)
        {
            if ($w->hierarchy > $max_id)
            {
                $max_id = $w->hierarchy;

            }
                else if ($w->hierarchy < $max_id) break; // When level id goes smaller than max_id, that means one iteration has ended...                
            }

            $bet_stages = [];
            // foreach start distribution
            foreach($we_array as &$w)
            {                
                $wD = new \App\AgentDistribution();
                $wD->uid = $w->userid;
                $wD->rid = $w->rowid;
                $wD->did = $w->uniqueid;
                $wD->hid = $w->hierarchy;
                $wD->pos = $w->position;
                
                $wD->cr_5D = $w->commission_5D;
                $wD->cr_6D = $w->commission_6D;

                if ($rowId == 0)
                {
                    $rowId = $w->rowid;
                    //set the stock
                    $stock->amount_5D = $w->amount_5D;
                    $stock->amount_6D = $w->amount_6D;
                    
                    // consider new stage
                    $bet_stage = new \App\BetStage();
                    $bet_stage->userid = $bet_agent;
                    $bet_stage->packageid = $bet_package;
                    $bet_stage->number = $w->number;
                    $bet_stage->drawdate = $w->drawdate;
                    $bet_stage->pool = $w->pool;
                    $bet_stage->betamount5d = number_format($w->amount_5D, 4);
                    $bet_stage->betamount6d = number_format($w->amount_6D, 4);
                    $bet_stage->rowid = $w->rowid;   
                    $bet_stage->created_at = date('Y-m-d H:i:s');
                    $bet_stages[] = $bet_stage->attributesToArray();
                }
                else if ($rowId != $w->rowid)
                {
                    $wD_array = [];
                    $rowId = $w->rowid;
                    
                    $stock->amount_5D = $w->amount_5D;
                    $stock->amount_6D = $w->amount_6D;   
                    
                    // consider new stage
                    $bet_stage = new \App\BetStage();
                    $bet_stage->userid = $bet_agent;
                    $bet_stage->packageid = $bet_package;
                    $bet_stage->number = $w->number;
                    $bet_stage->drawdate = $w->drawdate;
                    $bet_stage->pool = $w->pool;
                    $bet_stage->betamount5d = number_format($w->amount_5D, 4);
                    $bet_stage->betamount6d = number_format($w->amount_6D, 4);
                    $bet_stage->rowid = $w->rowid;
                    $bet_stage->created_at = date('Y-m-d H:i:s');
                    // check all the 
                    $bet_stages[] = $bet_stage->attributesToArray();
                }
                
                $perc_to_be_left = (100 - $w->position) / 100;

                // go through the position taking first
                if ($w->eat_type_5d6d == "Amount" || $w->eat_type_5d6d == "Group")
                {
                    
                    $actualEat = self::tryEat($w->limit_5D, $stock->amount_5D, $stock->amount_5D - $w->amount_5D * $perc_to_be_left);
                    $w->limit_5D -= $actualEat;
                    $stock->amount_5D -= $actualEat;
                    $wD->e_5D = $actualEat;
                    
                    $actualEat = self::tryEat($w->limit_6D, $stock->amount_6D, $stock->amount_6D - $w->amount_6D * $perc_to_be_left);
                    $w->limit_6D -= $actualEat;
                    $stock->amount_6D -= $actualEat;
                    $wD->e_6D = $actualEat;  
                }
                else if ($w->eat_type_5d6d == "Total Payout")
                {
                    $actualEat = self::tryEat($stock->amount_5D, $stock->amount_5D, $stock->amount_5D - $w->amount_5D * $perc_to_be_left);
                    if ($w->limit_Total < $actualEat*$w->pay_5D) {
                        $actualEat = $w->limit_Total/$w->pay_5D; 
                    }

                    $w->limit_Total -= ($actualEat*$w->pay_5D);
                    $stock->amount_5D -= $actualEat;
                    $wD->e_5D = $actualEat;
         
                    $actualEat = self::tryEat($stock->amount_6D, $stock->amount_6D, $stock->amount_6D - $w->amount_6D * $perc_to_be_left);

                    if ($w->limit_Total < $actualEat*$w->pay_6D) {
                        $actualEat = $w->limit_Total/$w->pay_6D; 
                    }

                    $w->limit_Total -= ($actualEat*$w->pay_6D);
                    $stock->amount_6D -= $actualEat;
                    $wD->e_6D = $actualEat;
                }

                // add in working arrays
                $wD_array = array_add($wD_array,
                    $wD->rid.'_'.$wD->did,
                    $wD
                );
                
                // position done
                // now handling if that come to the last agent
                if ($w->hierarchy == $max_id)
                {

                    // hiararchy = 1 means the most top agent normally means
                    // company or houses/investors
                    // loop the working arrays to pump in the eat
                    // and finally calculate the commission for himself                    
                    $restock = new \App\BetType();
                    foreach($wD_array as &$reeat_wD)
                    {
                        $w1 = $we_array[$reeat_wD->rid.'_'.$reeat_wD->did];
                        if ($w1->eat_type_5d6d == "Amount" || $w1->eat_type_5d6d == "Group")
                        {
                            $actualEat = self::tryEat($w1->limit_5D, $stock->amount_5D, $stock->amount_5D);
                            $w1->limit_5D -= $actualEat;
                            $reeat_wD->e_5D += $actualEat;
                            $stock->amount_5D -= $actualEat;
                            $restock->amount_5D += $reeat_wD->e_5D;
                            
                            $actualEat = self::tryEat($w1->limit_6D, $stock->amount_6D, $stock->amount_6D);
                            $w1->limit_6D -= $actualEat;
                            $reeat_wD->e_6D += $actualEat;
                            $stock->amount_6D -= $actualEat;
                            $restock->amount_6D += $reeat_wD->e_6D;
                        }
                        else if ($w1->eat_type_5d6d == "Total Payout")
                        {
                            if ($w1->limit_Total < 0) {
                                $w1->limit_Total = 0;
                            }

                            $actualEat = $stock->amount_5D;

                            if ($w1->limit_Total < $actualEat*$w1->pay_5D) {
                                $actualEat = $w1->limit_Total/$w1->pay_5D; 
                            }

                            $w1->limit_Total -= ($actualEat*$w1->pay_5D);
                            $reeat_wD->e_5D += $actualEat;
                            $stock->amount_5D -= $actualEat;
                            $restock->amount_5D += $reeat_wD->e_5D;
                          
                            $actualEat = $stock->amount_6D;

                            if ($w1->limit_Total < $actualEat*$w1->pay_6D) {
                                $actualEat = $w1->limit_Total/$w1->pay_6D; 
                            }

                            $w1->limit_Total -= ($actualEat*$w1->pay_6D);
                            $reeat_wD->e_6D += $actualEat;
                            $stock->amount_6D -= $actualEat;
                            $restock->amount_6D += $reeat_wD->e_6D;
                        }
                    }

                    foreach($wD_array as $recount_wD)
                    {
                        $w1 = $we_array[$recount_wD->rid.'_'.$recount_wD->did];
                        $final_dist = new \App\AgentDistribution();
                        $final_dist->did = $recount_wD->did;
                        $final_dist->uid = $recount_wD->uid;
                        $final_dist->rid = $recount_wD->rid;
                        $final_dist->pos = $recount_wD->pos;
                        $final_dist->hid = $recount_wD->hid;
                        
                        $final_dist->cr_5D = $recount_wD->cr_5D;
                        $final_dist->cr_6D = $recount_wD->cr_6D;
                        
                        if ($recount_wD->hid == $max_id)
                        {
                            // this is the last person in the hierarchy
                            $final_dist->s_5D = $restock->amount_5D;
                            $final_dist->e_5D = $recount_wD->e_5D;

                            $final_dist->s_6D = $restock->amount_6D;
                            $final_dist->e_6D = $recount_wD->e_6D;
                        }
                        else
                        {
                            $stockUp = 0;
                            $currentComm = 0;                          
                            
                            $stockUp = $restock->amount_5D - $recount_wD->e_5D;
                            $currentComm = $stockUp * ($w1->commission_5D) / 100;
                            $final_dist->s_5D = $restock->amount_5D;
                            $final_dist->c_5D = $currentComm;
                            $final_dist->e_5D = $recount_wD->e_5D;                            
                            
                            $stockUp = $restock->amount_6D - $recount_wD->e_6D;
                            $currentComm = $stockUp * ($w1->commission_6D) / 100;
                            $final_dist->s_6D = $restock->amount_6D;
                            $final_dist->c_6D = $currentComm;
                            $final_dist->e_6D = $recount_wD->e_6D;
                            
                            $restock->amount_5D -= $final_dist->e_5D;
                            $restock->amount_6D -= $final_dist->e_6D;
                        }
                        
                        $final_array = array_add($final_array,
                            $final_dist->rid.'_'.$final_dist->did,
                            $final_dist
                        );
                    }
                    
                }
                // finish last agent
            }            
            // End foreach Distribution

            //Get bet agent bet method, bet format, arrangement
            $bet_user = User::find($bet_agent);

            $bet_format = Arrangement::getCode($bet_user->arrangement_id);

            // now start to save the bet records in the database
            // should loop final array
            // for every loop item
            // using $out 
            $submitHeaderRecordSql = "CALL submit_header_record(
            :currency, 
            :betAgent,
            :betPackage,
            :betType,
            :betMethod,
            :inbox,
            :betString,
            :initialAmount,
            :actualAmount,
            :betFormat,
            :drawFormat,
            :boxIbox
        )";

        $ticket_id = DB::SELECT($submitHeaderRecordSql, [
            'currency' => 'MYR',
            'betAgent' => $bet_agent,
            'betPackage' => $bet_package,
            'betType' => $bet_type,
            'betMethod' => $bet_user->getBetMethod(),
            'inbox' => '',
            'betString' => $str_bet,
            'initialAmount' => $initial_total,
            'actualAmount' => $initial_total,
            'betFormat' => $bet_format,
            'drawFormat' => $bet_user->pool_shortcode,
            'boxIbox' => $bet_user->getBoxIBox->box_format
        ])[0]->id;

        if ($request->autoTicketStatus != NULL) {
            $autoTicketing = new AutoTicketing();
            $autoTicketing->ticketid = $ticket_id;
            $autoTicketing->userid = Auth()->user()->id;
            $autoTicketing->status = 1; 
            $autoTicketing->save();
        }

            //Generate bet slip
        $str_inbox = self::getInboxStr($usr_str, $data, $ticket_id, $bet_format);

            // now start to save the bet stages
            // Bet Stages Array
            //$bet_stages = [];
        foreach($bet_stages as $key=> $o)
        {
            $bet_stages[$key]['ticketid'] = $ticket_id;
            $actual_5d = 0;
            $actual_6d = 0;

            foreach($final_array as $final)
            {
                if ($final->rid == $o['rowid'])
                {
                    $actual_5d += $final->e_5D;
                    $actual_6d += $final->e_6D;
                }
            }

            $bet_stages[$key]['actual5d'] = number_format($actual_5d, 4);
            $bet_stages[$key]['actual6d'] = number_format($actual_6d, 4);
        }

            // print_r($bet_stages); echo "\n";
        \App\BetStage::insert($bet_stages);

        $stages_inserted = \App\BetStage::where('ticketid', '=', $ticket_id)->get();
        $stages_details = [];
            // now start to save the bet stages details
        foreach($stages_inserted as $stage)
        {
                // get the stage id
            $stage_id = $stage -> id; 

            foreach($final_array as $final)
            {
                if ($final->rid == $stage->rowid)
                {
                    $key_now = $final->rid.'_'.$final->did;
                    $target_we = $we_array[$key_now];

                    $detail = new \App\StageDetail();
                    $detail->stageid = $stage_id;
                    $detail->userid = $target_we->userid;
                    $detail->downlinerecordid = $target_we->hierarchy;
                    $detail->status = 0;
                    $detail->position = $target_we -> position;
                    $detail->stock5d = number_format($final -> s_5D, 4);
                    $detail->stock6d = number_format($final -> s_6D, 4);
                    $detail->eat5d = number_format($final -> e_5D, 4);
                    $detail->eat6d = number_format($final -> e_6D, 4);
                    $detail->comm5d = number_format($final -> c_5D, 4);
                    $detail->comm6d = number_format($final -> c_6D, 4);
                    $detail->created_at = date('Y-m-d H:i:s');
                    $detail->commrate5d = number_format($final -> cr_5D, 4);
                    $detail->commrate6d = number_format($final -> cr_6D, 4);

                    $stages_details[] = $detail->attributesToArray();
                }                    
            }
        }
            // print_r($stages_details); echo "\n";
        \App\StageDetail::insert($stages_details);

        return response()->json(['success'=>true, 'message'=> $we_array, 'tix_id'=> $ticket_id, 'data'=>$agent, 'ticket' => $str_inbox]);
    }
    catch(\Exception $ex)
    {
        return response()->json(['success'=>false, 'message'=> $ex->__toString(), 'data'=>$agent]);
    }
    return response()->json(['success'=>false, 'data'=>$agent]);
}

private function getBetString($array)
{
    $str = "#";
    $all_pools = \App\Pool::all()->keyBy("code");

    foreach($array->number as $a)
    {
        foreach($a->pools as $p)
        {
            $str = $str.$all_pools[$p]->id;
        }
        $str = $str.'<br />';  
        if ($a->amount_Big > 0)
        {
            $str = $str.'#'.$a->amount_Big;
        }
        else
        {
            $str = $str.'#';
        }
        if ($a->amount_Small > 0)
        {
            $str = $str.'#'.$a->amount_Small;
        }
        else
        {
            $str = $str.'#';
        }
        if ($a->amount_4A > 0)
        {
            $str = $str.'#'.$a->amount_4A;
        }
        else {
            $str = $str.'#';
        }
        if ($a->amount_3ABC > 0)
        {
            $str = $str.'#'.$a->amount_3ABC;
        }
        else {
            $str = $str.'#';
        }
        if ($a->amount_3A > 0)
        {
            $str = $str.'#'.$a->amount_3A;
        }
        else
        {
            $str = $str.'#';
        }
        if ($a->amount_4B > 0)
        {
            $str = $str.'#'.$a->amount_4B;
        }
        else
        {
            $str = $str.'#';
        }
        if ($a->amount_4C > 0)
        {
            $str = $str.'#'.$a->amount_4C;
        }
        else
        {
            $str = $str.'#';
        }
        if ($a->amount_4D > 0)
        {
            $str = $str.'#'.$a->amount_4D;
        }
        else
        {
            $str = $str.'#';
        }
        if ($a->amount_4E > 0)
        {
            $str = $str.'#'.$a->amount_4E;
        }
        else
        {
            $str = $str.'#';
        }
        if ($a->amount_4ABC > 0)
        {
            $str = $str.'#'.$a->amount_4ABC;
        }
        else
        {
            $str = $str.'#';
        }
        if ($a->amount_2A > 0)
        {
            $str = $str.'#'.$a->amount_2A;
        }
        else
        {
            $str = $str.'#';
        }
        if ($a->amount_2ABC > 0)
        {
            $str = $str.'#'.$a->amount_2ABC;
        }
        else
        {
            $str = $str.'#';
        }
    }

    return rtrim($str, '#');
}

private function get5d6dBetString($array)
{
    $str = "#";

    foreach($array->number as $a)
    {
        $str = $str."3";

        $str = $str.'<br />';  

        if ($a->amount_5D > 0)
        {
            $str = $str.'#'.$a->amount_5D;
        }
        else
        {
            $str = $str.'#';
        }

        if ($a->amount_6D > 0)
        {
            $str = $str.'#'.$a->amount_6D;
        }
        else
        {
            $str = $str.'#';
        }
    }

    return rtrim($str, '#');
}

private function getInboxStr($agent, $array, $ticket_id, $bet_format)
{
        // first one is Date of the ticket creation
    $date_str = Carbon::now()->format('d/m');
    $date_str_yyMMdd = Carbon::now()->format('Ymd');
    $time_str = Carbon::now()->format('H:i');
    $fake_date_str = $date_str.("/99")." ".$time_str;
    $week = self::getWeeks(Carbon::now(), 'Monday');

        // Ticket string starts here
    $str = "D:" . $fake_date_str . "W" . $week;
    $str .= "<br />" . $date_str;
    $str .= "<br />-" . $agent;
        // $str = $str."<br />-"."XB-S-4A-C-A";

        // dd($array);

    foreach ($array->number as $key => $ea) {
        $betNum = $ea->number;

        $str .= "<br />" . implode("", $ea->pools);

        if($key == 0){
            $str .= " X" . $bet_format;
        }

        $betArr = array();

        foreach ($ea as $property => $value) {
            if(fnmatch("amount_*", $property)){
                if($value > 0){
                    if($property == "amount_Big" || $property == "amount_Small"){
                        array_push($betArr, substr($property, strpos($property, "_")+1, 1) . $value);
                    } else {
                        array_push($betArr, substr($property, strpos($property, "_")+1) . $value);
                    }
                }
            }
        }

        $str .= "<br />" . $betNum . "=" . implode("-", $betArr);
    }

    $ticket = Ticket::find($ticket_id);

    $str .= "<br />" . "Total(" . number_format($ticket->actual_amount,2) . ")OK" . "<br />" . $ticket_id . "@";

    $ticket->inbox = $str;
    $ticket->save();

    return $str;
}

private function getWeeks($date, $rollover)
{
    $cut = substr($date, 0, 8);
    $daylen = 86400;

    $timestamp = strtotime($date);
    $first = strtotime($cut . "00");
    $elapsed = ($timestamp - $first) / $daylen;

    $weeks = 1;

    for ($i = 1; $i <= $elapsed; $i++)
    {
        $dayfind = $cut . (strlen($i) < 2 ? '0' . $i : $i);
        $daytimestamp = strtotime($dayfind);

        $day = strtolower(date("l", $daytimestamp));

        if($day == strtolower($rollover))  $weeks ++;
    }

    return $weeks;
}

private function tryEat($remaining_quota, $remaining_stock, $allocated_amount)
{
    if ($remaining_quota <= 0 || $remaining_stock <= 0 || $allocated_amount <= 0) return 0;
    $max_amount = self::GetSmallerValue($remaining_quota, $remaining_stock);
    return self::GetSmallerValue($max_amount, $allocated_amount);
}

private function getSmallerValue($a, $b) { if ($a < $b) return $a; else return $b; }



public function permuteNumber($number){
    $arr = collect();
    $this->permute($arr, $number,0,strlen($number));
    return response()->json([$arr->unique()->all()]);
}

    // function to generate and print all N! permutations of $str. (N = strlen($str)).
private function permute($arr, $str, $i, $n) {
    if ($i == $n)
        $arr->push($str);
    else {
        for ($j = $i; $j < $n; $j++) {
            $this->swap($str,$i,$j);
            $this->permute($arr, $str, $i+1, $n);
            $this->swap($str,$i,$j);
        }
    }
}

    // function to swap the char at pos $i and $j of $str.
private function swap(&$str,$i,$j) {
    $temp = $str[$i];
    $str[$i] = $str[$j];
    $str[$j] = $temp;
}

    /**
     * Extracts the bet data object from the request
     * @param Request $request gotten from front end form
     * @return array of the bet object
     */
    private function extractBet3d4dData(Request $request, \App\User $agent)
    {
        // Create a Token object
        $tokenObj = new \App\Token();
        $date_arr = [];
        $bet_entries = [];
        
        // Get all the pools available from the database
        $all_pools = \App\Pool::all();                

        // 1. Loop through the Request
        // 2. Get the pools available for today, with drawdate, pumped to bet_entries
        // 2. Extract date out
        // 3. Extract number with the pools and the betting type with value
        foreach($request->all() as $key => $value)
        {
            if (strpos($key, 'txt_number_') !== false )
            {
                // this is dates, check if the value is ON
                if ($value != null)
                {
                    if ($value !== "")
                    {
                        // Get the row number that is valid, which having the bet number filled
                        // Get the rest of the param
                        $no = str_replace('txt_number_', '', $key);
                        $bet_entry = new \App\Number();  
                        
                        // Put in the number of the betting
                        $bet_entry->number =  $value;
                        $bet_entry->agent = $agent->username;
                        $bet_entry->package = $agent->activated_package_3D4D_id;
                        
                        foreach ($bet_entry as $property => $value) {
                            if(fnmatch("amount_*", $property)){

                                // after getting the amount extract the common name
                                $common_name = str_replace("amount_", "", $property);
                                $target_value = $request->input("txt_".$common_name."_".$no);
                                if ($target_value != null)
                                {
                                    $bet_entry->$property = $target_value;
                                }
                                else
                                {
                                    $bet_entry->$property = 0;
                                }
                            }
                        } 
                        
                        // getting the pools and assign into pool array
                        foreach($all_pools as $pool)
                        {
                            if ($request->input("select_".$pool->shortname."_".$no)=='on')
                            {
                                array_push($bet_entry->pools, $pool->code);
                            }
                        }
                        
                        if ($request->input('selBetType'.$no) == null)
                        {
                            $bet_entry->type = "Normal";
                        }
                        else {
                            $bet_entry->type = $request->input('selBetType'.$no);
                        }

                        array_push($bet_entries, $bet_entry);

                        if ($bet_entry->type != "IBox" && $bet_entry->type != "Normal") {
                            if ($bet_entry->type == "Box") {
                                $numberSet = str_split($bet_entry->number);
                                $values = $this->computePermutations($numberSet);
                                $results = [];
                                foreach ($values as $val) {
                                    $numberString = "";
                                    foreach ($val as $v) {
                                        $numberString = $numberString.$v;
                                    }
                                    array_push($results, $numberString);
                                }

                                $numbers = array_unique($results);
                                $betType = "Box";
                            } //End Box
                            elseif ($bet_entry->type == 'P-H') {
                                $numberStr = substr($bet_entry->number, 1);
                                $numbers = [];
                                for($i = 0; $i<10; $i++) {
                                    $numberString = $i.$numberStr;
                                    array_push($numbers, $numberString);
                                }

                                $betType = 'P-H';
                            }
                            elseif ($bet_entry->type == 'P-B') {
                                $numberStr = substr($bet_entry->number, 0, -1);
                                $numbers = [];
                                for($i = 0; $i<10; $i++) {
                                    $numberString = $numberStr.$i;
                                    array_push($numbers, $numberString);
                                }

                                $betType = 'P-B';
                            }

                            foreach ($numbers as $n) {
                                if ($n != $bet_entry->number) {
                                    $box_bet_entry = new \App\Number();  

                                    // Put in the number of the betting
                                    $box_bet_entry->number =  $n;
                                    $box_bet_entry->agent = $agent->username;
                                    $box_bet_entry->package = $agent->activated_package_3D4D_id;
                                    $box_bet_entry->type = $betType;

                                    foreach ($box_bet_entry as $property => $value) {
                                        if(fnmatch("amount_*", $property)){

                                            // after getting the amount extract the common name
                                            $common_name = str_replace("amount_", "", $property);
                                            $target_value = $request->input("txt_".$common_name."_".$no);
                                            if ($target_value != null)
                                            {
                                                $box_bet_entry->$property = $target_value;
                                            }
                                            else
                                            {
                                                $box_bet_entry->$property = 0;
                                            }
                                        }
                                    } 

                                    // getting the pools and assign into pool array
                                    foreach($all_pools as $pool)
                                    {
                                        if ($request->input("select_".$pool->shortname."_".$no)=='on')
                                        {
                                            array_push($box_bet_entry->pools, $pool->code);
                                        }
                                    }

                                    array_push($bet_entries, $box_bet_entry);
                                }                    
                            }
                        } //Not iBox 
                    }
                }                
            }
            
            if (strpos($key, 'dd_') !== false )
            {
                // this is dates, check if the value is ON
                if ($value == "on")
                {
                    array_push($tokenObj->date, str_replace('dd_', '', $key));
                }
                
            }
            
        }
        
        $tokenObj->number = $bet_entries;
        return($tokenObj);
    }

    private function extractBet5d6dData(Request $request, \App\User $agent)
    {
        // Create a Token object
        $tokenObj = new \App\Token();
        $date_arr = [];
        $bet_entries = [];           

        // 1. Loop through the Request
        // 2. Get the pools available for today, with drawdate, pumped to bet_entries
        // 2. Extract date out
        // 3. Extract number with the pools and the betting type with value
        foreach($request->all() as $key => $value)
        {
            if (strpos($key, 'txt_number_') !== false )
            {
                // this is dates, check if the value is ON
                if ($value != null)
                {
                    if ($value !== "")
                    {
                        // Get the row number that is valid, which having the bet number filled
                        // Get the rest of the param
                        $no = str_replace('txt_number_', '', $key);
                        $bet_entry = new \App\Number();  
                        
                        // Put in the number of the betting
                        $bet_entry->number =  $value;
                        $bet_entry->agent = $agent->username;
                        $bet_entry->package = $agent->activated_package_5D6D_id;
                        
                        foreach ($bet_entry as $property => $value) {
                            if(fnmatch("amount_*", $property)){

                                // after getting the amount extract the common name
                                $common_name = str_replace("amount_", "", $property);
                                $target_value = $request->input("txt_".$common_name."_".$no);
       
                                if ($target_value != null)
                                {
                                    $bet_entry->$property = $target_value;
                                }
                                else
                                {
                                    $bet_entry->$property = 0;
                                }
                            }
                        } 

                        array_push($bet_entry->pools, "T");
                        array_push($bet_entries, $bet_entry);
                    }
                }                
            }
      
            if (strpos($key, 'dd_') !== false )
            {
                // this is dates, check if the value is ON
                if ($value == "on")
                {
                    array_push($tokenObj->date, str_replace('dd_', '', $key));
                }
                
            }
            
        }
        
        $tokenObj->number = $bet_entries;
        return($tokenObj);
    }

    function computePermutations($array) {
        $result = [];

        $recurse = function($array, $start_i = 0) use (&$result, &$recurse) {
            if ($start_i === count($array)-1) {
                array_push($result, $array);
            }

            for ($i = $start_i; $i < count($array); $i++) {
                //Swap array value at $i and $start_i
                $t = $array[$i]; $array[$i] = $array[$start_i]; $array[$start_i] = $t;

                //Recurse
                $recurse($array, $start_i + 1);

                //Restore old order
                $t = $array[$i]; $array[$i] = $array[$start_i]; $array[$start_i] = $t;
            }
        };

        $recurse($array);

        return $result;
    }

    private function deserializeBet3d4dData($bet_entries)
    {

        return "";
    }
    
    /**
     * Get into Bet Cut Off view
     * @return \Illuminate\View\View|\Illuminate\Contracts\View\Factory
     */
    public function cuttoff()
    {
        // 1. Get all the pools available from pools table
        $pools = Pool::all();
        
        // 2. Get all the bet cut off that has been stored
        $cutoffs = BetCutOffSetting::all()->keyBy('pool_id');
        
        // 3. Get void timeout setting
        $void = SystemSetting::where("setting_name", "void_timeout")->first();

        // 4. Return to front end
        $data = array(
            'pools' => $pools,
            'cutoffs' => $cutoffs,
            'void' => $void
        );

        return view("betcutoff")->with('data', $data);
    }
    
    public function saveCutOff(Request $request)
    {
        // 1. Get all the pools available from pools table
        $pools = Pool::all()->keyBy('id');
        
        // 2. Get all the bet cut off that has been stored
        $cutoffs = BetCutOffSetting::all();
        
        try {
            foreach($cutoffs as $cutoff)
            {
                $cutoff->hours = $request->get('ddl_hour_'.$pools[$cutoff->pool_id]->shortname);
                $cutoff->minutes = $request->get('ddl_minute_'.$pools[$cutoff->pool_id]->shortname);
                $cutoff->save();
            }
            
            
            Session::flash('message', 'Cut off saved successfully.');
            Session::flash('class', 'alert alert-success');
            return back();
        }
        catch(\Exception $ex)
        {
            DB::rollBack();
            Session::flash('message', 'Whoops! '.$ex->getMessage());
            Session::flash('class', 'alert alert-danger');
            return back();
        }  
    }
    
    
    /**
     * Get all the downline under myself
     * @param User $me me as user class
     * @return NULL[]
     */
    private function getAllMyDirectDownlines(User $me)
    {
        $myeat = array();
        $myeat[] = $me->username;
        $users = User::where("user_upline", "=", $me->id)->get();
        foreach($users as $u)
        {
            $myeat[] = $u->username;
        }
        return $myeat;
    }
    
    /*
     * getRealMe()
     * To get the real identity of login user. If login person is main account, it will return the account itself.
     * If the login person is sub account, it will get his main account.
     */
    private function getRealMe()
    {
        $user = auth()->user();
        $me = User::find(User::find($user->id)->user_main_account);
        return $me;
    }

    public function bet4dspecial()
    {             
        // 1. Get the real me
        $me = self::getRealMe();
        $myeat = self::getAllMyDirectDownlines($me);
        
        // 2. Get all the pools available from pools table
        $pools = Pool::all();
        
        // 3. Get the bettings schedule that available
        date_default_timezone_set("Asia/Kuala_Lumpur");
        $date_str = Carbon::now()->format('Ymd');        
        
        // 4. If current time >= 2000 then 
        if (date('H') >= 20) {
            $date_str = Carbon::now()->addDays(1)->format('Ymd');            
        }
        
        // 4. Get all cutoff
        $schedules = Schedule::where('drawdate', '>=', $date_str)->take(4)->get();
        
        $data = array('loginUser'  => Auth::User(),
            'me'  => $me, 
            'downlines' => $myeat,
            'pools' => $pools,
            'schedules'=>$schedules
        );
        
        return view("bet4dspecial")->with('data', $data);
    }

    public function makeBet4dSpecial(Request $request)
    {
        // 0. Set the DateTime to Asia/Kuala Lumpur
        date_default_timezone_set("Asia/Kuala_Lumpur");

        $date_str = Carbon::now()->format('d/m');
        $date_str_yyMMdd = Carbon::now()->format('Ymd');
        $time_str = Carbon::now()->format('H:i');
        $date_str = $date_str.("/99")." ".$time_str;
        $usr_str = $request->input('betAgent');
        //dd($date_str);
        
        // 1. Get the bettings schedule that available
        $date_str = Carbon::now()->format('Ymd');
        $initial_total = $request->input('hidTotal');

        // 2. If current time >= 2000 then
        if (date('H') >= 20) {
            $date_str = Carbon::now()->addDays(1)->format('Ymd');
        }
        
        try {            
            // get the betting agent
            $agent = \App\User::where('username', '=', $usr_str)->get()->first();
            
            // check if agent valid
            if ($agent == null)
            {
                // if agent cannot be found in the DB
                return response()->json(['success'=>false, 'message'=> 'The betting agent: '.$usr_str.' is not valid.','data'=>$agent]);
            }
            
            // check if package valid?
            if ($agent->activated_package_3D4D_id == 0)
            {
                // there is no activated package thus reject the betting with error message
                return response()->json(['success'=>false, 'message'=> 'The betting agent: '.$usr_str.' has no betting 3D/4D packages activated.','data'=>$agent]);
            }
            
            // extract good bet entries from the request            
            $data = self::extractBet3d4dData($request, $agent);

            // bet data extraction
            $str_bet = self::getBetString($data);

            // verify if the schedule is valid
            // 1. check if all the date having all the pools of all the bets
            $all_pools = \App\Pool::all()->keyBy("code");
            $cutOff = \App\BetCutOffSetting::get()->keyBy('pool_id');

            foreach($data->date as $d)
            {   
                // get the available pools for the date
                $schedule = \App\Schedule::where("drawdate", "=", $d)->get()->first();

                foreach($data->number as $n)
                {
                    foreach($n->pools as $p)
                    {
                        $pool_shortname = $all_pools[$p]->shortname;
                        $pool_id = $all_pools[$p]->id;

                        if ($schedule->$pool_shortname == 0 && $p != 'G')
                        {
                            return response()->json(['success'=>false, 'message'=> 'The date: '.$d.' has no draw for: '.$all_pools[$p]->name,'data'=>$agent]);
                        }
                        
                        // if same date happen
                        if ($d == $date_str_yyMMdd)
                        {
                            // if same date then need to make sure there is no bets after the cutoff date
                            $cutOffTime = $cutOff[$pool_id]->hours . $cutOff[$pool_id]->minutes;
                            $timeNow = date("Hi");
                            if($timeNow >= $cutOffTime){
                                return response()->json(['success'=>false, 'message'=> 'Cut off time passed for pool: '.$all_pools[$p]->name." for date: ".$d,'data'=>$agent]);
                            }
                        }
                    }
                }               
            }

            // now all the betting records shall be valid
            // expand the bet records
            $out = new \App\StructuredNumber();
            $output = $out->structurizeBetList($data);            
            $str = json_encode($output);   
            $returned_eat_info =  DB::SELECT('CALL prepare_4dspecial(:str)', ['str' => $str]); 

            $we_array = [];
            $bet_agent = 0;
            $bet_package = 0;
            $bet_type = '4D Special';

            // nice, oh then we now start to distribute - do one round of massaging of the data first
            // foreach starts
            foreach($returned_eat_info as $eat_info)
            {
                if ($bet_agent == 0)
                {
                    $bet_agent = $eat_info->userid;
                    $bet_package = $eat_info->package;
                }
                // define
                $we = new \App\WorkingElement();
                
                $we->number = $eat_info->number;
                $we->pool = $eat_info->pool;
                $we->drawdate = $eat_info->drawdate;
                $we->position = $eat_info->position_3d4d;
                $we->amount_4B = $eat_info->amount_4B;
                $we->amount_4C = $eat_info->amount_4C;
                $we->amount_4D = $eat_info->amount_4D;
                $we->amount_4E = $eat_info->amount_4E;
                $we->amount_4ABC = $eat_info->amount_4ABC;
                $we->amount_2A = $eat_info->amount_2A;
                $we->amount_2ABC = $eat_info->amount_2ABC;
                $we->payrate = $eat_info->payrate;
                $we->eat_type_3d4d = $eat_info->eat_type_3d4d;
                $we->uniqueid = $eat_info->uniqueid;
                $we->rowid = $eat_info->rowid;
                $we->userid = $eat_info->userid;
                $we->hierarchy = $eat_info->hierarchy;
                
                // for total
                
                // extract the commission out
                if ($eat_info->commission != null)
                {
                    if ($eat_info->commission != "")
                    {
                        // commission is valid
                        $comm_array = json_decode($eat_info->commission, true);
                        
                        foreach($comm_array as $c)
                        {
                            if ($c['type'] == '4B')
                            {
                                $we->commission_4B = $c['commission'];
                            }
                            else if ($c['type'] == '4C')
                            {
                                $we->commission_4C = $c['commission'];
                            }
                            else if ($c['type'] == '4D')
                            {
                                $we->commission_4D = $c['commission'];
                            }
                            else if ($c['type'] == '4E')
                            {
                                $we->commission_4E = $c['commission'];
                            }
                            else if ($c['type'] == '4ABC')
                            {
                                $we->commission_4ABC = $c['commission'];
                            }  
                            else if ($c['type'] == '2A')
                            {
                                $we->commission_2A = $c['commission'];
                            }  
                            else if ($c['type'] == '2ABC')
                            {
                                $we->commission_2ABC = $c['commission'];
                            }                            
                        }
                    }
                }            
                
                if ($eat_info->payrate != null)
                {
                    if ($eat_info->payrate != "")
                    {

                        // commission is valid
                        $payrate_array = json_decode($eat_info->payrate, true);
                        
                        foreach($payrate_array as $p)
                        {
                            if ($p['type'] == '4B')
                            {
                                $payrate_4b_array = json_decode($p['value']);
                                foreach($payrate_4b_array as $p1)
                                {
                                    if ($p1->type == 'first_prize')
                                    {
                                        $we->pay_4AB = $p1->rate;
                                        break;
                                    }
                                }
                            }
                            else if ($p['type'] == '4C')
                            {
                                $payrate_4c_array = json_decode($p['value']);
                                foreach($payrate_4c_array as $p1)
                                {
                                    if ($p1->type == 'first_prize')
                                    {
                                        $we->pay_4C = $p1->rate;
                                        break;
                                    }
                                }
                            }
                            else if ($p['type'] == '4D')
                            {
                                $payrate_4d_array = json_decode($p['value']);
                                foreach($payrate_4d_array as $p1)
                                {
                                    if ($p1->type == 'first_prize')
                                    {
                                        $we->pay_4D = $p1->rate;
                                        break;
                                    }
                                }
                            }
                            else if ($p['type'] == '4E')
                            {
                                $payrate_4e_array = json_decode($p['value']);
                                foreach($payrate_4e_array as $p1)
                                {
                                    if ($p1->type == 'first_prize')
                                    {
                                        $we->pay_4E = $p1->rate;
                                        break;
                                    }
                                }
                            }
                            else if ($p['type'] == '4ABC')
                            {
                                $payrate_4abc_array = json_decode($p['value']);
                                foreach($payrate_4abc_array as $p1)
                                {
                                    if ($p1->type == 'first_prize')
                                    {
                                        $we->pay_4ABC = $p1->rate;
                                        break;
                                    }
                                }
                            }
                            else if ($p['type'] == '2A')
                            {
                                $payrate_2a_array = json_decode($p['value']);
                                foreach($payrate_2a_array as $p1)
                                {
                                    if ($p1->type == 'first_prize')
                                    {
                                        $we->pay_2A = $p1->rate;
                                        break;
                                    }
                                }
                            }
                            else if ($p['type'] == '2ABC')
                            {
                                $payrate_2abc_array = json_decode($p['value']);
                                foreach($payrate_2abc_array as $p1)
                                {
                                    if ($p1->type == 'first_prize')
                                    {
                                        $we->pay_2ABC = $p1->rate;
                                        break;
                                    }
                                }
                            }
                            
                        }
                    }   
                }

                if ($eat_info->thelimit != null)
                {
                    if ($eat_info->thelimit != "")
                    {
                        // the limit is valid
                        $limit_array = json_decode($eat_info->thelimit, true);
                        
                        foreach($limit_array as $c)
                        {
                            if ($c['type'] == '4B')
                            {
                                $we->limit_4B = $c['value'] - $eat_info->eat_4B;
                            }
                            else if ($c['type'] == '4C')
                            {
                                $we->limit_4C = $c['value'] - $eat_info->eat_4C;
                            }
                            else if ($c['type'] == '4D')
                            {
                                $we->limit_4D = $c['value'] - $eat_info->eat_4D;
                            }
                            else if ($c['type'] == '4E')
                            {
                                $we->limit_4E = $c['value'] - $eat_info->eat_4E;
                            }
                            else if ($c['type'] == '4ABC')
                            {
                                $we->limit_4ABC = $c['value'] - $eat_info->eat_4ABC;
                            }
                            else if ($c['type'] == '2A')
                            {
                                $we->limit_2A = $c['value'] - $eat_info->eat_2A;
                            }
                            else if ($c['type'] == '2ABC')
                            {
                                $we->limit_2ABC = $c['value'] - $eat_info->eat_2ABC;
                            }
                            else if ($c['type'] == 'group')
                            {
                                // Set a limit for group
                                // See if the current number drop into which group
                                $range = $c['sub_type'];
                                $range = explode('-', $range);
                                $startNum = $range[0];
                                $endNum = $range[1];

                                if ($c['tertier_type'] == '4B')
                                {
                                    if ($we->number >= $startNum && $we->number <= $endNum) {
                                        $we->limit_4B = $c['value'] - $eat_info->eat_4B;
                                    }
                                }
                                else if ($c['tertier_type'] == '4C')
                                {
                                    if ($we->number >= $startNum && $we->number <= $endNum) {
                                        $we->limit_4C = $c['value'] - $eat_info->eat_4C;
                                    }
                                }
                                else if ($c['tertier_type'] == '4D')
                                {
                                    if ($we->number >= $startNum && $we->number <= $endNum) {
                                        $we->limit_4C = $c['value'] - $eat_info->eat_4C;
                                    }
                                }
                                else if ($c['tertier_type'] == '4E')
                                {
                                    if ($we->number >= $startNum && $we->number <= $endNum) {
                                        $we->limit_4E = $c['value'] - $eat_info->eat_4E;
                                    }
                                }
                                else if ($c['tertier_type'] == '4ABC')
                                {
                                    if ($we->number >= $startNum && $we->number <= $endNum) {
                                        $we->limit_4ABC = $c['value'] - $eat_info->eat_4ABC;
                                    }
                                }
                                else if ($c['tertier_type'] == '2A')
                                {
                                    if ($we->number >= $startNum && $we->number <= $endNum) {
                                        $we->limit_2A = $c['value'] - $eat_info->eat_2A;
                                    }
                                }
                                else if ($c['tertier_type'] == '2ABC')
                                {
                                    if ($we->number >= $startNum && $we->number <= $endNum) {
                                        $we->limit_2ABC = $c['value'] - $eat_info->eat_2ABC;
                                    }
                                }
                            }
                        }
                    }
                }
                
                $we_array = array_add($we_array,
                    $we->rowid.'_'.$we->uniqueid,
                    $we
                );
            }
            // foreach ends
            
            $rowId = 0;
            
            $wD_array = [];
            $final_array = [];
            $stock = new \App\BetType();
            $max_id = 0;
            foreach ($we_array as $w)
            {
                if ($w->hierarchy > $max_id)
                {
                    $max_id = $w->hierarchy;
                    
                }
                else if ($w->hierarchy < $max_id) break; // When level id goes smaller than max_id, that means one iteration has ended...                
            }
            
            $bet_stages = [];
            // foreach start distribution
            foreach($we_array as &$w)
            {
                $wD = new \App\AgentDistribution();
                $wD->uid = $w->userid;
                $wD->rid = $w->rowid;
                $wD->did = $w->uniqueid;
                $wD->hid = $w->hierarchy;
                $wD->pos = $w->position;
                
                $wD->cr_4B = $w->commission_4B;
                $wD->cr_4C = $w->commission_4C;
                $wD->cr_4D = $w->commission_4D;
                $wD->cr_4E = $w->commission_4E;
                $wD->cr_4ABC = $w->commission_4ABC;
                $wD->cr_2A = $w->commission_2A;
                $wD->cr_2ABC = $w->commission_2ABC;

                if ($rowId == 0)
                {
                    $rowId = $w->rowid;
                    //set the stock                    
                    $stock->amount_4B = $w->amount_4B;
                    $stock->amount_4C = $w->amount_4C;
                    $stock->amount_4D = $w->amount_4D;
                    $stock->amount_4E = $w->amount_4E;
                    $stock->amount_4ABC = $w->amount_4ABC;
                    $stock->amount_2A = $w->amount_2A;
                    $stock->amount_2ABC = $w->amount_2ABC;

                    // consider new stage
                    $bet_stage = new \App\BetStage();
                    $bet_stage->userid = $bet_agent;
                    $bet_stage->packageid = $bet_package;
                    $bet_stage->number = $w->number;
                    $bet_stage->drawdate = $w->drawdate;
                    $bet_stage->pool = $w->pool;
                    $bet_stage->betamount4b = number_format($w->amount_4B, 4);
                    $bet_stage->betamount4c = number_format($w->amount_4C, 4);
                    $bet_stage->betamount4d = number_format($w->amount_4D, 4);
                    $bet_stage->betamount4e = number_format($w->amount_4E, 4);
                    $bet_stage->betamount4abc = number_format($w->amount_4ABC, 4);
                    $bet_stage->betamount2a = number_format($w->amount_2A, 4);
                    $bet_stage->betamount2abc = number_format($w->amount_2ABC, 4);
                    $bet_stage->rowid = $w->rowid;   
                    $bet_stage->created_at = date('Y-m-d H:i:s');
                    $bet_stages[] = $bet_stage->attributesToArray();
                }
                else if ($rowId != $w->rowid)
                {
                    $wD_array = [];
                    $rowId = $w->rowid;
                    
                    $stock->amount_4B = $w->amount_4B;
                    $stock->amount_4C = $w->amount_4C;
                    $stock->amount_4D = $w->amount_4D;
                    $stock->amount_4E = $w->amount_4E;
                    $stock->amount_4ABC = $w->amount_4ABC;
                    $stock->amount_2A = $w->amount_2A;
                    $stock->amount_2ABC = $w->amount_2ABC; 
                    
                    // consider new stage
                    $bet_stage = new \App\BetStage();
                    $bet_stage->userid = $bet_agent;
                    $bet_stage->packageid = $bet_package;
                    $bet_stage->number = $w->number;
                    $bet_stage->drawdate = $w->drawdate;
                    $bet_stage->pool = $w->pool;
                    $bet_stage->betamount4b = number_format($w->amount_4B, 4);
                    $bet_stage->betamount4c = number_format($w->amount_4C, 4);
                    $bet_stage->betamount4d = number_format($w->amount_4D, 4);
                    $bet_stage->betamount4e = number_format($w->amount_4E, 4);
                    $bet_stage->betamount4abc = number_format($w->amount_4ABC, 4);
                    $bet_stage->betamount2a = number_format($w->amount_2A, 4);
                    $bet_stage->betamount2abc = number_format($w->amount_2ABC, 4);
                    $bet_stage->rowid = $w->rowid;
                    $bet_stage->created_at = date('Y-m-d H:i:s');
                    // check all the 
                    $bet_stages[] = $bet_stage->attributesToArray();
                }
                
                $perc_to_be_left = (100 - $w->position) / 100;
                
                // go through the position taking first
                if ($w->eat_type_3d4d == "Amount" || $w->eat_type_3d4d == "Group")
                {                    
                    $actualEat = self::tryEat($w->limit_4B, $stock->amount_4B, $stock->amount_4B - $w->amount_4B * $perc_to_be_left);
                    $w->limit_4B -= $actualEat;
                    $stock->amount_4B -= $actualEat;
                    $wD->e_4B = $actualEat;
                    
                    $actualEat = self::tryEat($w->limit_4C, $stock->amount_4C, $stock->amount_4C - $w->amount_4C * $perc_to_be_left);
                    $w->limit_4C -= $actualEat;
                    $stock->amount_4C -= $actualEat;
                    $wD->e_4C = $actualEat;
                    
                    $actualEat = self::tryEat($w->limit_4D, $stock->amount_4D, $stock->amount_4D - $w->amount_4D * $perc_to_be_left);
                    $w->limit_4D -= $actualEat;
                    $stock->amount_4D -= $actualEat;
                    $wD->e_4D = $actualEat;
                    
                    $actualEat = self::tryEat($w->limit_4E, $stock->amount_4E, $stock->amount_4E - $w->amount_4E * $perc_to_be_left);
                    $w->limit_4E -= $actualEat;
                    $stock->amount_4E -= $actualEat;
                    $wD->e_4E = $actualEat;

                    $actualEat = self::tryEat($w->limit_2A, $stock->amount_2A, $stock->amount_2A - $w->amount_2A * $perc_to_be_left);
                    $w->limit_2A -= $actualEat;
                    $stock->amount_2A -= $actualEat;
                    $wD->e_2A = $actualEat;

                    $actualEat = self::tryEat($w->limit_2ABC, $stock->amount_2ABC, $stock->amount_2ABC - $w->amount_2ABC * $perc_to_be_left);
                    $w->limit_2ABC -= $actualEat;
                    $stock->amount_2ABC -= $actualEat;
                    $wD->e_2ABC = $actualEat;
                }
                
                // add in working arrays
                $wD_array = array_add($wD_array,
                    $wD->rid.'_'.$wD->did,
                    $wD
                );
                
                // position done
                // now handling if that come to the last agent
                if ($w->hierarchy == $max_id)
                {
                    // hiararchy = 1 means the most top agent normally means
                    // company or houses/investors
                    // loop the working arrays to pump in the eat
                    // and finally calculate the commission for himself                    
                    $restock = new \App\BetType();
                    foreach($wD_array as &$reeat_wD)
                    {
                        $w1 = $we_array[$reeat_wD->rid.'_'.$reeat_wD->did];
                        if ($w1->eat_type_3d4d == "Amount" || $w1->eat_type_3d4d == "Group")
                        {
                            $actualEat = self::tryEat($w1->limit_4B, $stock->amount_4B, $stock->amount_4B);
                            $w1->limit_4B -= $actualEat;
                            $reeat_wD->e_4B += $actualEat;
                            $stock->amount_4B -= $actualEat;
                            $restock->amount_4B += $reeat_wD->e_4B;
                            
                            $actualEat = self::tryEat($w1->limit_4C, $stock->amount_4C, $stock->amount_4C);
                            $w1->limit_4C -= $actualEat;
                            $reeat_wD->e_4C += $actualEat;
                            $stock->amount_4C -= $actualEat;
                            $restock->amount_4C += $reeat_wD->e_4C;
                            
                            $actualEat = self::tryEat($w1->limit_4D, $stock->amount_4D, $stock->amount_4D);
                            $w1->limit_4D -= $actualEat;
                            $reeat_wD->e_4D += $actualEat;
                            $stock->amount_4D -= $actualEat;
                            $restock->amount_4D += $reeat_wD->e_4D;
                            
                            $actualEat = self::tryEat($w1->limit_4E, $stock->amount_4E, $stock->amount_4E);
                            $w1->limit_4E -= $actualEat;
                            $reeat_wD->e_4E += $actualEat;
                            $stock->amount_4E -= $actualEat;
                            $restock->amount_4E += $reeat_wD->e_4E;
                            
                            $actualEat = self::tryEat($w1->limit_4ABC, $stock->amount_4ABC, $stock->amount_4ABC);
                            $w1->limit_4ABC -= $actualEat;
                            $reeat_wD->e_4ABC += $actualEat;
                            $stock->amount_4ABC -= $actualEat;
                            $restock->amount_4ABC += $reeat_wD->e_4ABC;

                            $actualEat = self::tryEat($w1->limit_2A, $stock->amount_2A, $stock->amount_2A);
                            $w1->limit_2A -= $actualEat;
                            $reeat_wD->e_2A += $actualEat;
                            $stock->amount_2A -= $actualEat;
                            $restock->amount_2A += $reeat_wD->e_2A;

                            $actualEat = self::tryEat($w1->limit_2ABC, $stock->amount_2ABC, $stock->amount_2ABC);
                            $w1->limit_2ABC -= $actualEat;
                            $reeat_wD->e_2ABC += $actualEat;
                            $stock->amount_2ABC -= $actualEat;
                            $restock->amount_2ABC += $reeat_wD->e_2ABC;
                        }
                    }
                    
                    foreach($wD_array as $recount_wD)
                    {
                        $w1 = $we_array[$recount_wD->rid.'_'.$recount_wD->did];
                        $final_dist = new \App\AgentDistribution();
                        $final_dist->did = $recount_wD->did;
                        $final_dist->uid = $recount_wD->uid;
                        $final_dist->rid = $recount_wD->rid;
                        $final_dist->pos = $recount_wD->pos;
                        $final_dist->hid = $recount_wD->hid;
                        
                        $final_dist->cr_4B = $recount_wD->cr_4B;
                        $final_dist->cr_4C = $recount_wD->cr_4C;
                        $final_dist->cr_4D = $recount_wD->cr_4D;
                        $final_dist->cr_4E = $recount_wD->cr_4E;
                        $final_dist->cr_4ABC = $recount_wD->cr_4ABC;
                        $final_dist->cr_2A = $recount_wD->cr_2A;
                        $final_dist->cr_2ABC = $recount_wD->cr_2ABC;
                        
                        if ($recount_wD->hid == $max_id)
                        {
                            // this is the last person in the hierarchy
                            $final_dist->s_4B = $restock->amount_4B;
                            $final_dist->e_4B = $recount_wD->e_4B;
                            
                            $final_dist->s_4C = $restock->amount_4C;
                            $final_dist->e_4C = $recount_wD->e_4C;
                            
                            $final_dist->s_4D = $restock->amount_4D;
                            $final_dist->e_4D = $recount_wD->e_4D; 
                            
                            $final_dist->s_4E = $restock->amount_4E;
                            $final_dist->e_4E = $recount_wD->e_4E;

                            $final_dist->s_4ABC = $restock->amount_4ABC;
                            $final_dist->e_4ABC = $recount_wD->e_4ABC;

                            $final_dist->s_2A = $restock->amount_2A;
                            $final_dist->e_2A = $recount_wD->e_2A;

                            $final_dist->s_2ABC = $restock->amount_2ABC;
                            $final_dist->e_2ABC = $recount_wD->e_2ABC;
                        }
                        else
                        {
                            $stockUp = 0;
                            $currentComm = 0;
                            
                            $stockUp = $restock->amount_4B - $recount_wD->e_4B;
                            //$currentComm = $stockUp * ($w->commission_Big + wa.Discount) / 100;
                            $currentComm = $stockUp * ($w1->commission_4B) / 100;
                            $final_dist->s_4B = $restock->amount_4B;
                            $final_dist->c_4B = $currentComm;
                            $final_dist->e_4B = $recount_wD->e_4B;
                            
                            $stockUp = $restock->amount_4C - $recount_wD->e_4C;
                            $currentComm = $stockUp * ($w1->commission_4C) / 100;
                            $final_dist->s_4C = $restock->amount_4C;
                            $final_dist->c_4C = $currentComm;
                            $final_dist->e_4C = $recount_wD->e_4C;
                            
                            $stockUp = $restock->amount_4D - $recount_wD->e_4D;
                            $currentComm = $stockUp * ($w1->commission_4D) / 100;
                            $final_dist->s_4D = $restock->amount_4D;
                            $final_dist->c_4D = $currentComm;
                            $final_dist->e_4D = $recount_wD->e_4D;                            
                            
                            $stockUp = $restock->amount_4E - $recount_wD->e_4E;
                            $currentComm = $stockUp * ($w1->commission_4E) / 100;
                            $final_dist->s_4E = $restock->amount_4E;
                            $final_dist->c_4E = $currentComm;
                            $final_dist->e_4E = $recount_wD->e_4E;                            
                            
                            $stockUp = $restock->amount_4ABC - $recount_wD->e_4ABC;
                            $currentComm = $stockUp * ($w1->commission_4ABC) / 100;
                            $final_dist->s_4ABC = $restock->amount_4ABC;
                            $final_dist->c_4ABC = $currentComm;
                            $final_dist->e_4ABC = $recount_wD->e_4ABC;

                            $stockUp = $restock->amount_2A - $recount_wD->e_2A;
                            $currentComm = $stockUp * ($w1->commission_2A) / 100;
                            $final_dist->s_2A = $restock->amount_2A;
                            $final_dist->c_2A = $currentComm;
                            $final_dist->e_2A = $recount_wD->e_2A;

                            $stockUp = $restock->amount_2ABC - $recount_wD->e_2ABC;
                            $currentComm = $stockUp * ($w1->commission_2ABC) / 100;
                            $final_dist->s_2ABC = $restock->amount_2ABC;
                            $final_dist->c_2ABC = $currentComm;
                            $final_dist->e_2ABC = $recount_wD->e_2ABC;
                            
                            $restock->amount_4B -= $final_dist->e_4B;
                            $restock->amount_4C -= $final_dist->e_4C;
                            $restock->amount_4D -= $final_dist->e_4D;
                            $restock->amount_4E -= $final_dist->e_4E;
                            $restock->amount_4ABC -= $final_dist->e_4ABC;
                            $restock->amount_2A -= $final_dist->e_2A;
                            $restock->amount_2ABC -= $final_dist->e_2ABC;
                        }
                        
                        $final_array = array_add($final_array,
                            $final_dist->rid.'_'.$final_dist->did,
                            $final_dist
                        );
                    }
                    
                }
                // finish last agent
                
            }            
            // End foreach Distribution

            //Get bet agent bet method, bet format, arrangement
            $bet_user = User::find($bet_agent);
            $bet_format = Arrangement::getCode($bet_user->arrangement_id);

            // now start to save the bet records in the database
            // should loop final array
            // for every loop item
            // using $out 
            $submitHeaderRecordSql = "CALL submit_header_record(
            :currency, 
            :betAgent,
            :betPackage,
            :betType,
            :betMethod,
            :inbox,
            :betString,
            :initialAmount,
            :actualAmount,
            :betFormat,
            :drawFormat,
            :boxIbox
        )";

        $ticket_id = DB::SELECT($submitHeaderRecordSql, [
            'currency' => 'MYR',
            'betAgent' => $bet_agent,
            'betPackage' => $bet_package,
            'betType' => $bet_type,
            'betMethod' => $bet_user->getBetMethod(),
            'inbox' => '',
            'betString' => $str_bet,
            'initialAmount' => $initial_total,
            'actualAmount' => $initial_total,
            'betFormat' => $bet_format,
            'drawFormat' => $bet_user->pool_shortcode,
            'boxIbox' => $bet_user->getBoxIBox->box_format
        ])[0]->id;


        if ($request->autoTicketStatus != NULL) {
            $autoTicketing = new AutoTicketing();
            $autoTicketing->ticketid = $ticket_id;
            $autoTicketing->userid = Auth()->user()->id;
            $autoTicketing->status = 1; 
            $autoTicketing->save();
        }

            //Generate bet slip
        $str_inbox = self::getInboxStr($usr_str, $data, $ticket_id, $bet_format);

            // now start to save the bet stages
            // Bet Stages Array
            //$bet_stages = [];
        foreach($bet_stages as $key=> $o)
        {
            $bet_stages[$key]['ticketid'] = $ticket_id;
            $actual_4b = 0;
            $actual_4c = 0;
            $actual_4d = 0;
            $actual_4e = 0;
            $actual_4abc = 0;
            $actual_2a = 0;
            $actual_2abc = 0;

            foreach($final_array as $final)
            {
                if ($final->rid == $o['rowid'])
                {
                    $actual_4b += $final->e_4B;
                    $actual_4c += $final->e_4C;
                    $actual_4d += $final->e_4D;
                    $actual_4e += $final->e_4E;
                    $actual_4abc += $final->e_4ABC;
                    $actual_2a += $final->e_2A;
                    $actual_2abc += $final->e_2ABC;
                }
            }

            $bet_stages[$key]['actual4b'] = number_format($actual_4b, 4);
            $bet_stages[$key]['actual4c'] = number_format($actual_4c, 4);
            $bet_stages[$key]['actual4d'] = number_format($actual_4d, 4);
            $bet_stages[$key]['actual4e'] = number_format($actual_4e, 4);
            $bet_stages[$key]['actual4abc'] = number_format($actual_4abc, 4);
            $bet_stages[$key]['actual2a'] = number_format($actual_2a, 4);
            $bet_stages[$key]['actual2abc'] = number_format($actual_2abc, 4);    
        }

        \App\BetStage::insert($bet_stages);

        $stages_inserted = \App\BetStage::where('ticketid', '=', $ticket_id)->get();
        $stages_details = [];
            // now start to save the bet stages details
        foreach($stages_inserted as $stage)
        {
                // get the stage id
            $stage_id = $stage -> id; 

            foreach($final_array as $final)
            {
                if ($final->rid == $stage->rowid)
                {
                    $key_now = $final->rid.'_'.$final->did;
                    $target_we = $we_array[$key_now];

                    $detail = new \App\StageDetail();
                    $detail->stageid = $stage_id;
                    $detail->userid = $target_we->userid;
                    $detail->downlinerecordid = $target_we->hierarchy;
                    $detail->status = 0;
                    $detail->position = $target_we -> position;
                    $detail->stock4b = number_format($final -> s_4B, 4);
                    $detail->stock4c = number_format($final -> s_4C, 4);
                    $detail->stock4d = number_format($final -> s_4D, 4);
                    $detail->stock4e = number_format($final -> s_4E, 4);
                    $detail->stock4abc = number_format($final -> s_4ABC, 4);
                    $detail->stock2a = number_format($final -> s_2A, 4);
                    $detail->stock2abc = number_format($final -> s_2ABC, 4);
                    $detail->eat4b = number_format($final-> e_4B, 4);
                    $detail->eat4c = number_format($final -> e_4C, 4);
                    $detail->eat4d = number_format($final -> e_4D, 4);
                    $detail->eat4e = number_format($final -> e_4E, 4);
                    $detail->eat4abc = number_format($final -> e_4ABC, 4);
                    $detail->eat2a = number_format($final -> e_2A, 4);
                    $detail->eat2abc = number_format($final -> e_2ABC, 4);
                    $detail->comm4b = number_format($final -> c_4B, 4);
                    $detail->comm4c = number_format($final -> c_4C, 4);
                    $detail->comm4d = number_format($final -> c_4D, 4);
                    $detail->comm4e = number_format($final -> c_4E, 4);
                    $detail->comm4abc = number_format($final -> c_4ABC, 4);
                    $detail->comm2a = number_format($final -> c_2A, 4);
                    $detail->comm2abc = number_format($final -> c_2ABC, 4);
                    $detail->created_at = date('Y-m-d H:i:s');
                    $detail->commrate4b = number_format($final -> cr_4B, 4);
                    $detail->commrate4c = number_format($final -> cr_4C, 4);
                    $detail->commrate4d = number_format($final -> cr_4D, 4);
                    $detail->commrate4e = number_format($final -> cr_4E, 4);
                    $detail->commrate4abc = number_format($final -> cr_4ABC, 4);
                    $detail->commrate2a = number_format($final -> cr_2A, 4);
                    $detail->commrate2abc = number_format($final -> cr_2ABC, 4);

                    $stages_details[] = $detail->attributesToArray();
                }                    
            }
        }
        \App\StageDetail::insert($stages_details);

        return response()->json(['success'=>true, 'message'=> $we_array, 'tix_id'=> $ticket_id, 'data'=>$agent, 'ticket' => $str_inbox]);
    }
    catch(\Exception $ex)
    {
        return response()->json(['success'=>false, 'message'=> $ex->__toString(), 'data'=>$agent]);
    }
    return response()->json(['success'=>false, 'data'=>$agent]);
}
}
