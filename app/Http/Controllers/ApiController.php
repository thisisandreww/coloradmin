<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use DateTime;

class ApiController extends Controller
{
	public function index(){
		return response()->json(['success' => 'ok']);
	}

	public function pool(){
		$pools = \App\Pool::get();
		return response()->json(['pools' => $pools]);
	}

    //Get draw result based on Date & Pool ID ( 0 = All )
	public function result($date, $pool){
		$result = \App\DrawResult::where("drawdate", $date);

		if($pool == 0){
			$selected_pool = "All";
		} else {
			$selected_pool = \App\Pool::find($pool);
			$result = $result->where("poolid", $selected_pool->id);
		}

		$result = $result->get();

		$keyed = $result->groupBy('poolid');

		//If pool is Toto
		if(isset($keyed['3'])){
			$count5d = 1;
			for($i = 4; $i <= 6; $i++){
				$num = $keyed['3']->where('type', '5d1')->first();
				$draw = new \App\DrawResult();
				$draw->type = '5d' . $i;
				$draw->number = substr_replace($num->number, str_repeat("_", $count5d), 0, $count5d);
				$keyed['3']->push($draw);
				$count5d++;
			}

			$count6d = 1;
			for($i = 2; $i <= 5; $i++){
				$num = $keyed['3']->where('type', '6d1')->first();
				$draw = new \App\DrawResult();
				$draw->type = '6d' . $i;
				$draw->number = substr_replace($num->number, str_repeat("_", $count6d), 0, $count6d);
				$keyed['3']->push($draw);

				$num = $keyed['3']->where('type', '6d1')->first();
				$draw = new \App\DrawResult();
				$draw->type = '6d' . $i;
				$draw->number = substr_replace($num->number, str_repeat("_", $count6d), -$count6d, $count6d);
				$keyed['3']->push($draw);

				$count6d++;
			}
		}

		//Group pool result based on type
		$data = $keyed->map(function($item, $key){
			$item = $item->mapToGroups(function($item2, $key2){
				if(str_is('s*', $item2['type'])){
					return ['starter' => $item2];
				}

				if(str_is('c*', $item2['type'])){
					return ['consolation' => $item2];
				}

				if(str_is('5d*', $item2['type'])){
					return ['5d' => $item2];
				}

				if(str_is('6d*', $item2['type'])){
					return ['6d' => $item2];
				}

				return ['top' => $item2];
			});
			return $item;
		});

		return response()->json(['pool' => $selected_pool, 'data' => $data]);
	}

	public function permuteNumber($number){
		$arr = collect();
		$this->permute($arr, $number,0,strlen($number));
		return response()->json([$arr->unique()->all()]);
	}

	// function to generate and print all N! permutations of $str. (N = strlen($str)).
	function permute($arr, $str, $i, $n) {
		if ($i == $n)
			$arr->push($str);
		else {
			for ($j = $i; $j < $n; $j++) {
				$this->swap($str,$i,$j);
				$this->permute($arr, $str, $i+1, $n);
				$this->swap($str,$i,$j); 
			}
		}
	}

	// function to swap the char at pos $i and $j of $str.
	function swap(&$str,$i,$j) {
		$temp = $str[$i];
		$str[$i] = $str[$j];
		$str[$j] = $temp;
	}

	// parse betting token
	public function parse(Request $request){
		$tokenObj = new \App\Token();
		$tokenObj->status = true;
		$tokenStr = $request->text . "\n";

		$this->findToken($tokenObj, $tokenStr);

		if(count($tokenObj->number) == 0 && $tokenObj->error == false){
			$tokenObj->error = "No betting number found.";
			$tokenObj->status = false;
		}

		return response()->json(['data' => $tokenObj]);
	}

	function validateDraw($token){
		$valid = preg_match('/^((D[1-4])|(D[0-1][0-9][0-3][0-9]))$/', $token);
		
		//Token has date
		if(strlen($token) == 5){
			try {
				return Carbon::createSafe((int)date("Y"), (int)ltrim(substr($token, 1, 2), '0'), (int)ltrim(substr($token, 3), '0'), 0, 0, 0);
			} catch (\Carbon\Exceptions\InvalidDateException $exp) {
				return $valid = false;
			}
		} else {
			return $valid;
		}
	}

	function validatePool($token){
		$valid = preg_match('/^\+[1-7]{1,7}$/', $token);
		if($valid){
			$uniquePool = str_split(substr($token, 1));
			if(count($uniquePool) != count(array_unique($uniquePool))){
				return $valid = false;
			}
		}
		return $valid;
	}

	function validateBet($token){
		$valid = preg_match('/^\*{0,2}\d{3,4}(#\d+){1,5}$/', $token);
		return $valid;
	}

	function findToken($tokenObj, $string){
		$this->findDrawToken($tokenObj, $string);
	}

	function findDrawToken($tokenObj, $string){
		$endLinePos = strpos($string, "\n");
		$drawToken = substr($string, 0, $endLinePos);
		$validToken = $this->validateDraw($drawToken);
		if($validToken){

			//Remove draw token from parse string
			$string = substr($string, $endLinePos+1);

			$this->findPoolToken($tokenObj, $string);

			//Condition for D1,D2,D3,D4
			if(strlen($drawToken) == 2){
				//Get number of draws
				$numOfDraw = substr($drawToken, 1, 2);

				$cutOff = \App\BetCutOffSetting::get()->keyBy('pool_id');

				//Get time now in format of 2400 (HourMinute)
				$timeNow = date("Hi");

				//Date string to retrieve next draw schedule
				$dateStr = date("Ymd");

				foreach($tokenObj->pool AS $pool){
					$cutOffTime = $cutOff[$pool]->hours . $cutOff[$pool]->minutes;
					if($timeNow >= $cutOffTime){
						$tomorrow = new DateTime('tomorrow');
						$dateStr = $tomorrow->format("Ymd");
					}
				}

				$schedule = \App\Schedule::where("drawdate", ">=", $dateStr)->limit($numOfDraw)->get();
				$dateArr = [];
				foreach ($schedule as $value) {
					array_push($dateArr, $value->drawdate);
				}
				$tokenObj->date = $dateArr;
			} else {
				$tokenObj->date = [date("Y") . substr($drawToken, 1)];
			}
		} else {
			$tokenObj->error = "Error found in draw token. " . $drawToken;
			$tokenObj->status = false;
		}
	}

	function findPoolToken($tokenObj, $string){
		$endLinePos = strpos($string, "\n");
		$poolToken = substr($string, 0, $endLinePos);
		$validToken = $this->validatePool($poolToken);
		if($validToken){
			$poolArr = str_split(substr($poolToken, 1));

			foreach($poolArr AS $pool){
				if(!in_array($pool, $tokenObj->pool)){
					array_push($tokenObj->pool, $pool);
				}
			}

			$string = substr($string, $endLinePos+1);
			$remain = $this->findBetToken($tokenObj, $poolArr, $string);

			//If error found in bet token, do not proceed to search for next pool token
			if($tokenObj->status == false){
				return $string;
			}
			
			$this->findPoolToken($tokenObj, $remain);
			return $string;
		} else {
			if($poolToken != ""){
				$tokenObj->error = "Error found in pool token. " . $poolToken;
				$tokenObj->status = false;
			}
			return $string;
		}
	}

	function findBetToken($tokenObj, $poolArr, $string){
		$endLinePos = strpos($string, "\n");
		$betToken = substr($string, 0, $endLinePos);
		$validToken = $this->validateBet($betToken);
		if($validToken){
			if(strpos($betToken, "*") === 0){
				if(substr_count($betToken, "*") == 1){
					$type = "IBox";
				} else {
					$type = "Box";
				}

				$betNumber = str_replace("*", "", substr($betToken, 0, strpos($betToken, "#")));
			} else {
				$type = "Normal";
				$betNumber = substr($betToken, 0, strpos($betToken, "#"));
			}

			$numberObj = new \App\Number();
			$numberObj->number = $betNumber;
			$numberObj->pools = $poolArr;
			$numberObj->type = $type;

			$betSide = substr($betToken, strpos($betToken, "#"));

			$sideArr = explode("#", $betSide);

			$count = 1;
			foreach ($numberObj as $property => $value) {
				if(fnmatch("amount_*", $property)){
					if(array_key_exists($count, $sideArr)){
						$numberObj->$property = $sideArr[$count];
					}
					$count++;
				}
			}

			array_push($tokenObj->number, $numberObj);

			$string = substr($string, $endLinePos+1);
			$string = $this->findBetToken($tokenObj, $poolArr, $string);
			return $string;
		} else {
			//Set error message if an error found in bet token, not pool token
			if($betToken != "" && $this->validatePool($betToken) == false){
				$tokenObj->error = "Error found in bet token. " . $betToken;
			}
			return $string;
		}
	}
}
