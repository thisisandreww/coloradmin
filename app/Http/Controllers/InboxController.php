<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;
use Redirect;
use Session;
use Auth;
use DB;

use \App\Ticket;
use \App\BetStage;
use \App\StageDetail;
use \App\SystemSetting;

class InboxController extends Controller
{
	public function viewVoidInvoice(){
		$tickets = Ticket::orderBy("id", "desc")->get();
		$timeout = SystemSetting::where("setting_name", "void_timeout")->first()->setting_value;

		return view("viewVoidInvoice", compact("tickets", "timeout"));
	}

	public function voidInvoice(Request $request){
		try {
			DB::beginTransaction();

			$ticket = Ticket::findOrFail($request->txt_ticket);

			$void_time = SystemSetting::where("setting_name", "void_timeout")->first()->setting_value;

			$void_valid = true;

			//Admin role can ignore void timeout setting
			if(session('loginUserRoleName') != "admin") {
				if($ticket->created_at->diffInMinutes() > $void_time){
					$void_valid = false;
				}
			}

			if($void_valid){
				$ticket->status = -1;
				$ticket->voidby = Auth::user()->id;
				$ticket->voidTime = Carbon::now();
				$ticket->save();

				//Retrieve bet stages for selected ticket
				$bet_stages = BetStage::where("ticketid", $ticket->id)->get();

				foreach($bet_stages AS $bet){
					$bet->status = -1;
					$bet->save();

					//Void stage details
					$stage_detail = StageDetail::where("stageid", $bet->id)->get();

					foreach($stage_detail AS $det){
						$det->status = -1;
						$det->save();
					}
				}

				DB::commit();
				Session::flash("msg", "Invoice voided.");
				Session::flash("class", "alert-success");
			} else {

				DB::rollBack();
				Session::flash("msg", "Invoice can only be voided in the first " . $void_time . " minutes.");
				Session::flash("class", "alert-danger");		
			}
			
			return redirect::back();
		} catch (\Exception $e) {
			DB::rollBack();
			Session::flash("msg", "Unable to void invoice. " . $e->getMessage());
			Session::flash("class", "alert-danger");
			return redirect::back();
		}
	}
}
