<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;

class Controller extends BaseController
{
	use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

	// public function scraper($date){
	// 	$client = new Client();
	// 	$crawler = $client->request('GET', 'http://www.check4d.com/past-results/' . $date);

	// 	$html = collect();

	// 	$crawler->filter('.outerbox')->each(function ($node) use($html) {
	// 		$html->push($node);
	// 	});

	// 	$html->each(function($pool){
	// 		$magnum = collect();
	// 		$pool_name = substr($pool->text(), 0, strpos($pool->text(), 'Date'));
	// 		switch($pool_name){
	// 			case "Magnum 4D 萬能":
	// 			$this->getTopResult($date, $pool, 1);
	// 			break;
	// 			case "Da Ma Cai 1+3D 大馬彩":
	// 			$this->getTopResult($date, $pool, 2);
	// 			break;
	// 			case "SportsToto 4D 多多":
	// 			$this->getTopResult($date, $pool, 3);
	// 			break;
	// 			case "Singapore 4D":
	// 			$this->getTopResult($date, $pool, 4);
	// 			break;
	// 			case "Sabah 88 4D 沙巴萬字":
	// 			$this->getTopResult($date, $pool, 5);
	// 			break;
	// 			case "Sandakan 4D山打根赛马会":
	// 			$this->getTopResult($date, $pool, 6);
	// 			break;
	// 			case "Special CashSweep 砂勞越大萬":
	// 			$this->getTopResult($date, $pool, 7);
	// 			break;
	// 			case "SportsToto 5D, 6D, Lotto多多六合彩":
	// 			$this->getToto5D6D($date, $pool, 3);
	// 			break;
	// 		}
	// 	});
	// }

	// function getTopResult($date, $pool, $poolid){
	// 	//Top
	// 	$pool->filter('.resulttop')->each(function ($node, $key) use($poolid){
	// 		if($key+1 <= 3){
	// 			$result = \App\DrawResult::firstOrCreate(
	// 				['drawdate' => $date, 'poolid' => $poolid, 'type' => $key+1, 'number' => $node->text()]
	// 			);
	// 		}
	// 	});

	// 	$specialNum = 0;
	// 	$consolationNum = 0;

	// 	//Special
	// 	$pool->filter('.resultbottom')->each(function ($node, $key) use($poolid, &$specialNum, &$consolationNum){
	// 		if(strlen($node->text()) != 4 || is_numeric($node->text()) == false || $node->text() == ""){
	// 			return;
	// 		}
	// 		if($specialNum < 10){
	// 			$specialNum++;
	// 			$type = "s" . $specialNum;
	// 		} else {
	// 			$consolationNum++;
	// 			$type = "c" . $consolationNum;
	// 		}

	// 		$result = \App\DrawResult::firstOrCreate(
	// 			['drawdate' => $date, 'poolid' => $poolid, 'type' => $type, 'number' => $node->text()]
	// 		);
	// 	});
	// }

	// function getToto5D6D($date, $pool){
	// 	$counter = 0;
	// 	$last = false;
	// 	$collect = collect();
	// 	$result = $pool->filter('.resultbottom')->each(function ($node, $key) use(&$counter, $last, $collect){
	// 		$collect->push($node->text());
	// 		if($counter <= 6){
	// 			if(strlen($node->text()) == 5 && $counter < 3){
	// 				$counter++;
	// 				$type = "5d" . $counter;
	// 				$result = \App\DrawResult::firstOrCreate(
	// 					['drawdate' => $date, 'poolid' => 3, 'type' => $type, 'number' => $node->text()]
	// 				);
	// 			} else if(strlen($node->text()) == 6) {
	// 				$type = "6d1";
	// 				$last = true;
	// 				$result = \App\DrawResult::firstOrCreate(
	// 					['drawdate' => $date, 'poolid' => 3, 'type' => $type, 'number' => $node->text()]
	// 				);
	// 			}
	// 		}

	// 		if($last){
	// 			return;
	// 		}
	// 	});
	// }

}
