<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Hash;

use App\DrawResult;
use App\User;
use App\PackageAssignment;
use App\Schedule;
use App\Pool;
use App\Ticket;
use App\EatMapReferences;
use App\PackageDetailCategories;
use App\EatDetailCategory;
use App\BetLimit;
use App\BetStage;
use App\StageDetail;
use App\PackageDetail;
use App\RedNumberRestriction;
use App\SystemSetting;

use App\Jobs\ProcessResults;

use Redirect;

class MemberController extends BaseController
{
	private function getAvailableRoleAssignmentById($role_id)
	{
		return DB::SELECT("SELECT b.role_id,b.role_name FROM role_assignment AS a JOIN roles AS b ON b.role_id = a.role_assignment_id WHERE a.role_id = '$role_id'");
	}
	
	private function getPackageOption($id, $packageTypeId)
	{
		return DB::SELECT("SELECT b.package_id, b.package_name, b.package_type_id, b.red_package FROM package_assignments AS a LEFT JOIN packages AS b ON 
			b.package_id = a.package_id WHERE user_id = '$id' and b.package_type_id = '$packageTypeId'");
	}
	
	private function getAllPackageWithAssignedOption($uplineId, $id, $packageTypeId)
	{
		return DB::SELECT("SELECT distinct b.*,(select count(*) 
			from package_assignments where user_id = '$id' and package_id = b.package_id 
			group by package_id, user_id) as assigned, (select count(*) from package_assignments as f LEFT JOIN users as g on g.id = f.user_id left join packages as h on h.package_id = f.package_id
			where g.user_upline = '$id' and (f.package_id = b.package_id OR h.reference_package_id = b.package_id)) as reference_count from 
			package_assignments as a left join packages as b 
			on b.package_id = a.package_id where a.user_id = '$uplineId' and b.package_type_id = '$packageTypeId' and b.package_id not 
			in (select b.reference_package_id from package_assignments 
			as a LEFT JOIN packages as b on b.package_id = a.package_id where a.user_id = '$id')
			UNION
			SELECT distinct b.*, (select count(*) from package_assignments where user_id = '$id' and package_id = b.package_id 
			group by package_id, user_id) as assigned, (select count(*) from package_assignments as f LEFT JOIN users as g on g.id = f.user_id left join packages as h on h.package_id = f.package_id
			where g.user_upline = '$id' and (f.package_id = b.package_id OR h.reference_package_id = b.package_id)) as reference_count from package_assignments as a left join packages 
			as b on b.package_id = a.package_id where a.user_id = '$id' and b.package_type_id = '$packageTypeId'");
	}

	private function getDownlineMemberByParentId($id)
	{
		return DB::SELECT("select a.*,d.role_name, (select count(*) from users where user_upline = a.id) as downline_count , 
			(select b.package_name from packages as b left join package_assignments as c on c.package_id = 
			b.package_id where c.user_id = a.id and c.package_id = a.activated_package_5D6D_id) as activated_5D6D_package , 
			(select b.package_name from packages as b left join package_assignments as c on c.package_id = b.package_id where
			c.user_id = a.id and c.package_id = a.activated_package_3D4D_id) as activated_3D4D_package,
			(select count(*) from role_assignment where role_id = a.user_role) as downline_create from users as a left join roles as d on 
			d.role_id = a.user_role where a.user_upline = '$id' AND a.id != '$id'");
	}

	private function getCreateDownlineCreditLimit($id)
	{
		return DB::SELECT("select (credit_limit - (select  COALESCE(SUM(credit_limit),0) as credit from users where user_upline = '$id' and id !='$id')) 
			as credit_balance from users where id = '$id'");
	}

	private function getAllChildByParent($id)
	{
		return DB::SELECT("SELECT COALESCE(GROUP_CONCAT(lv SEPARATOR ','),0) AS child FROM (
			SELECT @pv:=(SELECT GROUP_CONCAT(id SEPARATOR ',') FROM users 
			WHERE FIND_IN_SET(user_upline, @pv)) AS lv FROM users 
			JOIN (SELECT @pv:='$id') tmp) a");
	}

	private function getUserInfo($id)
	{
		$userDetail = parent::getUserById(parent::getUserById($id)->user_main_account);
		$childMember = self::getDownlineMemberByParentId($userDetail->id);
		$roleOption = self::getAvailableRoleAssignmentById($userDetail->user_role);
		$data = array('loginUser'  => Auth::User(),'user'  => $userDetail,'childMember'   => $childMember, 'roleOption' => $roleOption);
		return $data;
	}

	public function showUserView($id)
	{
		if (!parent::checkIsIDBelongsToLoginUserHierachy($id))
		{
			return back()->with('error', "Invalid URL Access");
		}
		$data = self::getUserInfo($id);
		return \view('memberlist')->with('data', $data);
	}

	public function showSubAccountView()
	{
		$id = Auth::User()->id;
		$subAccount = collect(DB::SELECT("SELECT * FROM users where user_main_account = '$id' and id != '$id'"))->keyby('id');
		$user = parent::getUserById($id);
		$mainAcc = parent::getUserById($user->user_main_account);
		$data = array('subAccountList'  => $subAccount,
			'loginUser' => $user,
			'mainAcc' => $mainAcc);
		return \view('subAccount')->with('data', $data);
	}

	public function addSubAccountView(Request $request, $id)
	{
		if (DB::table('users')->WHERE('username', '=', $request->get('username'))->count())
		{
			return back()->with('error', 'Username Used');
		}
		else
		{
			$nextId = DB::table('users')->max('id') + 1;
			$user = parent::getUserById($id);
			$newMember = new User;
			$newMember->password = Hash::make($request->get('password'));
			$newMember->name = $request->get('username');
			$newMember->username = $request->get('username');
			$newMember->id = $nextId;
			$newMember->user_role = $user->user_role;
			$newMember->state = 0;
			$newMember->user_main_account = $id;
			if (!empty($request->get('downline_control')))
				$newMember->downline_create = $request->get('downline_control');
			if (!empty($request->get('betting_capability')))
				$newMember->betting_capability = $request->get('betting_capability');
			if (!empty($request->get('package_control')))
				$newMember->package_control = $request->get('package_control');
			if (!empty($request->get('report_view')))
				$newMember->report_view = $request->get('report_view');
			DB::beginTransaction();
			try
			{
				$newMember->push();
				DB::commit();
			}
			catch (\Exception $e)
			{
				DB::rollback();
				return back()->with('error',"System error. Please try again later. (DB Connection Failure)");
			}
			return back()->with('success',"New Sub Account Added");
		}
	}

	public function editSubAccountView(Request $request, $subId)
	{
		$user = parent::getUserById($subId);
		$name = $request->get("username");
		$username = $request->get("username");
		if ($user->password == $request->get("password"))
			$password = $request->get("password");
		else
			$password = Hash::make($request->get("password"));

		DB::beginTransaction();
		try
		{
			DB::STATEMENT("update users set username='$username', 
				name='$name', 
				password='$password'
				".((!empty($request->get('betting_capability'))) ? ",betting_capability='".$request->get("betting_capability")."'":"")."
				".((!empty($request->get('report_view'))) ? ",report_view='".$request->get("report_view")."'":"")."
				".((!empty($request->get('downline_control'))) ? ",downline_create='".$request->get("downline_control")."'":"")."
				".((!empty($request->get('package_control'))) ? ",package_control='".$request->get("package_control")."'":"")."  
				where id='$subId'");
			DB::commit();
		}
		catch (\Exception $e)
		{
			DB::rollback();
			return back()->with('error',"System error. Please try again later. (DB Connection Failure)");
		}
		return back()->with('success',"Sub Account Data Edited");
	}

	private function redirectToUserView($id)
	{
		return redirect()->route("memberlist",$id)->with('data', self::getUserInfo($id));
	}

	public function openResetPasswordView()
	{
		$user = parent::getUserById(Auth::User()->id);
		return \view('resetPassword')->with('user', $user);
	}

	private function getRoleOption($id)
	{
		return DB::SELECT("select b.role_name,b.role_id from role_assignment as a left join roles as b on b.role_id = a.role_assignment_id where a.role_id = '$id'");
	}

	public function openEditMemberView($uplineId, $selectedId)
	{
		if ($uplineId != $selectedId && 
			(!parent::checkIsIDBelongsToLoginUserHierachy($uplineId) ||
				!parent::checkIsIDBelongsToLoginUserHierachy($selectedId) || 
				!parent::checkIsIdParentChildRelation($uplineId, $selectedId)))
		{
			return back()->with('error',"Invalid URL Access");
		}
		else if ($uplineId == $selectedId && $selectedId != Auth::User()->id)
		{
			return back()->with('error',"Invalid URL Access");
		}

		$user = parent::getUserById($uplineId);
		$userToEdit = parent::getUserById($selectedId);
		$creditBalance = self::getCreateDownlineCreditLimit($uplineId);
		$roleOption = self::getRoleOption($user->user_role);
		$package3D4DOption = self::getAllPackageWithAssignedOption($uplineId, $selectedId, parent::$package3D4DId);
		$package5D6DOption = self::getAllPackageWithAssignedOption($uplineId, $selectedId, parent::$package5D6DId);
		$arrangement = DB::SELECT("select * from arrangements");
		$boxes = DB::SELECT("select * from boxes");
		$assigned5D6DPackageList = DB::SELECT("select b.* from package_assignments as a LEFT JOIN packages as b on a.package_id = b.package_id 
			where a.user_id = '$selectedId' and b.package_type_id = '".parent::$package5D6DId."'");
		$assigned3D4DPackageList = DB::SELECT("select b.* from package_assignments as a LEFT JOIN packages as b on a.package_id = b.package_id 
			where a.user_id = '$selectedId' and b.package_type_id = '".parent::$package3D4DId."'");
		$setting = collect(DB::SELECT("select * from system_settings"))->keyby('setting_name');  
		$pools = Pool::all();


		$mymapreference = collect(DB::SELECT("SELECT CONCAT(cat.eat_detail_type_name, '_', cat.eat_detail_subtype_name, '_', cat.eat_detail_tertiertype_name)  AS thekey,
			map.value
			FROM eat_map_references as map
			INNER JOIN eat_detail_categories as cat
			ON map.pdc_id = cat.id
			WHERE map.user_id =".$selectedId))->keyBy('thekey');

        // 2.1 Try to get own eat limit
		$eat = collect(DB::SELECT("SELECT CONCAT(cat.eat_detail_type_name, '_', cat.eat_detail_subtype_name, '_',
			cat.eat_detail_tertiertype_name,'_', pool.shortname)  AS thekey,
			map.value,
			pool.images
			FROM eat_maps as map
			INNER JOIN eat_detail_categories as cat
			ON map.pdc_id = cat.id
			INNER JOIN pools as pool
			ON pool.id = map.pool_id
			WHERE map.user_id =".$selectedId))->keyBy('thekey');

        // 3. Get all the pools available from pools table
		$pools = Pool::all();

        // 2.2 Check if own limit is nothing?
		if (count($eat) == 0)
		{
			foreach ($mymapreference as $r)
			{
				foreach ($pools as $pool)
				{
					$eat = array_add($eat,
						$r->thekey."_".$pool->shortname,
						[
							"thekey" => $r->thekey."_".$pool->shortname,
							"value" => 0.00,
							"images" => $pool->images
						]);
				}
			}
		}
		else
		{
			$eat = json_decode($eat, true);
		}

		$data = array('loginUser'  => Auth::User(),
			'user'  => $user,
			'userToEdit'  => $userToEdit,
			'creditBalance'   => $creditBalance[0]->credit_balance,
			'package3D4DOption' => $package3D4DOption,
			'package5D6DOption' => $package5D6DOption,
			'arrangement' => $arrangement,
			'boxes' => $boxes,
			'roleOption' =>$roleOption,
			'assigned5D6DPackageList' => $assigned5D6DPackageList,
			'assigned3D4DPackageList' => $assigned3D4DPackageList,
			'setting' => $setting,
			'pools' => $pools,
			'myMapReference' => $mymapreference,
			'myMap' => $eat
		);

		return \view('editMemberDetail')->with('data', $data);
	}


	private function getAssigned3D4DRedPackage($id)
	{
		$companyUser = parent::getUserById(2);
		return collect(DB::select("select * from package_assignments as a LEFT JOIN packages as b on b.package_id = a.package_id where 
			b.package_id = '".$companyUser->assigned_red_package_3D4D_id."' and a.user_id = '$id'"))->first();
	}

	private function getAssigned5D6DRedPackage($id)
	{
		$companyUser = parent::getUserById(2);
		return collect(DB::select("select * from package_assignments as a LEFT JOIN packages as b on b.package_id = a.package_id where 
			b.package_id = '".$companyUser->assigned_red_package_5D6D_id."' and a.user_id = '$id'"))->first();
	}

	public function openAddMemberView($id)
	{
		$me = self::getRealMe();
		$loginUser = Auth::User();
		$user = parent::getUserById($id);
		if (!parent::checkIsIDBelongsToLoginUserHierachy($id) ||
			!$loginUser->downline_create ||
			$loginUser->state != 0 ||
			!$user->downline_create ||
			$user->role_name=="System" || 
			$user->role_id <= 1)
		{
			return back()->with('error', "Invalid URL Access");
		}
		$creditBalance = self::getCreateDownlineCreditLimit($id);
		$package3D4DOption = self::getPackageOption($id, parent::$package3D4DId);
		$package5D6DOption = self::getPackageOption($id, parent::$package5D6DId);
		$packageRed3D4D = self::getAssigned3D4DRedPackage($id);

		$packageRed5D6D = self::getAssigned5D6DRedPackage($id);
		$roleOption = self::getRoleOption($user->user_role);
		$arrangement = DB::SELECT("select * from arrangements");
		$boxes = DB::SELECT("select * from boxes");
		$setting = collect(DB::SELECT("select * from system_settings"))->keyby('setting_name');      
		$data = array('user' => $user,
			'creditBalance' => $creditBalance[0]->credit_balance,
			'package3D4DOption' => $package3D4DOption,
			'package5D6DOption' => $package5D6DOption,
			'packageRed3D4D' => $packageRed3D4D,
			'packageRed5D6D' => $packageRed5D6D,
			'roleOption' => $roleOption,
			'arrangement' => $arrangement, 
			'boxes' => $boxes,
			'setting'=>$setting,
			'me'=>$me
		);
		return \view('addMemberDetail')->with('data', $data);
	}


	/*
	 * getRealMe()
	 * To get the real identity of login user. If login person is main account, it will return the account itself.
	 * If the login person is sub account, it will get his main account.
	 */
	private function getRealMe()
	{
		$user = auth()->user();
		$me = User::find(User::find($user->id)->user_main_account);
		return $me;
	}

	public function validateResetPassword(Request $request, $activeUserId)
	{
		$activeUserId = Auth::user()->id;
		$user = parent::getUserById($activeUserId);

		if (!Hash::check($request->input('oldPassword'), $user->password))
		{
			return redirect()->route("resetPassword",$activeUserId)->with('error', 'Incorrect password, please retry');
		}
		else if ($request->input('oldPassword') == $request->input('newPassword'))
		{
			return redirect()->route("resetPassword",$activeUserId)->with('error', 'Old password and new password is same, Please change password');
		}
		else if($request->input('retypePassword') != $request->input('newPassword'))
		{
			return redirect()->route("resetPassword",$activeUserId)->with('error', 'New password and retype password are not match, please retry');
		}
		else
		{
			DB::table('users')->WHERE('id', $activeUserId)->update(['password' =>  Hash::make($request->input('newPassword'))]);
			return redirect()->route("resetPassword",$activeUserId)->with('success', $user);
		}
	}

	public function deleteMember($id)
	{
		DB::table('users')->WHERE('id', '=', $id)->delete();
		DB::table('package_Assignments')->WHERE('user_id', '=', $id)->delete();
		$childList = self::getAllChildByParent($id)[0]->child;
		DB::statement("UPDATE users SET state = 2 WHERE id in (".$childList.")");
		DB::delete("delete from package_assignments where user_id in (".$childList.")");
		parent::performPackageCleanup();
		return back();
	}

	private function hasDownlineAccount($id)
	{
		$childList = self::getAllChildByParent($id)[0]->child;
		return (strlen($childList) > 0) ? 1 : 0;
	}

	private function hasSubAccount($id)
	{
		$subAccountList = DB::SELECT("SELECT GROUP_CONCAT(id SEPARATOR ',') as sub_account_list FROM users where user_main_account = '$id' and id != '$id'");
		return (strlen($subAccountList[0]->sub_account_list) > 0) ? 1 : 0;
	}

	public function editDownlineDetail(Request $request, $id)
	{
		$user = parent::getUserById($id);
		$username = $request->get('username');
		$role = $request->get('role');
		$arrangement = $request->get('arrangement');
		$boxes = $request->get('boxes');
		$mode = $request->get('mode');
		$name = $request->get('name');
		$position_3D4D = $request->get('selPosition3d4d');
		$position_5D6D = $request->get('selPosition5d6d');
		$unassignedPackage = $request->get('unassignedPackage');

		$activated_3D4D_package = $request->get('activated_3D4D_package');
		$activated_5D6D_package = $request->get('activated_5D6D_package');

		$credit_limit = $request->get('credit_limit');

		if ($request->get('password') == $user->password)
			$password  = $user->password;
		else
			$password =  Hash::make($request->get('password'));

		
		if ($request->has('state'))
			$state = $request->get('state');
		else
			$state = $user->state;

		if ($request->has('member_create'))
			$createMember  = $request->get('member_create');
		else
			$createMember = 0;

		if ($request->has('float'))
			$float  = $request->get('float');
		else
			$float = 0;

		if ($request->has('locked'))
			$locked = $request->get('locked');
		else
			$locked = $user->locked;

		if ($request->has('betting_capability'))
			$bettingCapability = $request->get('betting_capability');
		else
			$bettingCapability = $user->betting_capability;

		if ($request->has('report_view'))
			$reportView = $request->get('report_view');
		else
			$reportView = $user->report_view;

		if ($request->has('package_control'))
			$packageControl = $request->get('package_control');
		else
			$packageControl = $user->package_control;
		DB::beginTransaction();
		try
		{
            //Update Red Package with tree struct
			if ($request->has('red_3D4D_package') && $user->assigned_red_package_3D4D_id != $request->get('red_3D4D_package'))
			{
				if (empty($request->get('red_3D4D_package')))
				{
					$newRed3D4DPackage = '0';
					DB::delete("delete from package_assignments where package_id = '".$user->assigned_red_package_3D4D_id."' and user_id != '1'");
				}
				else
				{
					$newRed3D4DPackage =$request->get('red_3D4D_package');
					DB::STATEMENT("update package_assignments set package_id = '".$newRed3D4DPackage."' where user_id != '1' and package_id = '".$user->assigned_red_package_3D4D_id."'");
				}

				DB::STATEMENT("update users set assigned_red_package_3D4D_id = '".$newRed3D4DPackage."', activated_package_3D4D_id = '".$newRed3D4DPackage."'
					where assigned_red_package_3D4D_id= '".$user->assigned_red_package_3D4D_id."' OR activated_package_3D4D_id  = '".$user->assigned_red_package_3D4D_id."'");
				DB::STATEMENT("update package_assignments set package_id = '".$newRed3D4DPackage."' where user_id != '1' and package_id = '".$user->assigned_red_package_3D4D_id."'");
			}

			if ($request->has('red_5D6D_package') && $user->assigned_red_package_5D6D_id != $request->get('red_5D6D_package'))
			{
				if (empty($request->get('red_5D6D_package')))
				{
					$newRed5D6DPackage = '0';
					DB::delete("delete from package_assignments where package_id = '".$user->assigned_red_package_5D6D_id."' and user_id != '1'");
				}
				else
				{
					$newRed5D6DPackage =$request->get('red_5D6D_package');
					DB::STATEMENT("update package_assignments set package_id = '".$newRed5D6DPackage."' where user_id != '1' and package_id = '".$user->assigned_red_package_5D6D_id."'");
				}
				DB::STATEMENT("update users set assigned_red_package_5D6D_id = '".$newRed5D6DPackage."', activated_package_5D6D_id = '".$newRed5D6DPackage."'
					where assigned_red_package_5D6D_id= '".$user->assigned_red_package_5D6D_id."' OR activated_package_5D6D_id  = '".$user->assigned_red_package_5D6D_id."'");
			}

			if ($request->has('red_package_control'))
			{
				DB::STATEMENT("update system_settings set setting_value='".$request->get('red_package_control')."' where setting_name='allow_red_package_control'");
			}

            //check account permission
			if (self::hasDownlineAccount($id))
			{
				$childList = self::getAllChildByParent($id)[0]->child;
				DB::statement("UPDATE users SET state = '$state' WHERE id in (".$childList.") and state < '$state'");
				$updateSubAccountQuery = "UPDATE users SET "; 
				if ($packageControl == 0)
					DB::statement("UPDATE users set package_control = 0 where id IN (".$childList.")");
				if ($bettingCapability == 0)
					DB::statement("UPDATE users set betting_capability = 0 where id IN (".$childList.")");
				if ($reportView == 0)
					DB::statement("UPDATE users set report_view = 0 where id IN (".$childList.")");
				if ($createMember == 0)
					DB::statement("UPDATE users set downline_create = 0 where id IN (".$childList.")");
			}

			if (self::hasSubAccount($id))
			{
				$subAccount = DB::SELECT("SELECT COALESCE(GROUP_CONCAT(id SEPARATOR ','),0) as sub_account_list FROM users where user_main_account = '$id' and id != '$id'");
				DB::statement("UPDATE users SET state = '$state' WHERE id in (".$subAccount[0]->sub_account_list.") and state < '$state'");
			}
			$updateQuery = "UPDATE users SET username='$username', 
			password='$password', 
			user_role='$role', 
			credit_limit='$credit_limit',
			state='$state',
			name ='$name',
			box_id = '$boxes',
			arrangement_id = '$arrangement',
			locked = '$locked',
			mode = '$mode',
			betting_capability = '$bettingCapability',
			package_control = '$packageControl',
			report_view = '$reportView',
			position_3D4D = '$position_3D4D',
			position_5D6D = '$position_5D6D',
			float_val = '$float',
			downline_create = '$createMember',";
			if (!empty($activated_3D4D_package))
				$updateQuery = $updateQuery . "activated_package_3D4D_id ='$activated_3D4D_package', ";
			else
				$updateQuery = $updateQuery . "activated_package_3D4D_id ='0', ";
			if (!empty($activated_5D6D_package))
				$updateQuery = $updateQuery . "activated_package_5D6D_id ='$activated_5D6D_package', ";
			else
				$updateQuery = $updateQuery . "activated_package_5D6D_id ='0', ";
			$updateQuery = $updateQuery . "downline_create = '$createMember' WHERE id='$id'";   
			DB::statement($updateQuery);

            //need check login user because normally user can't edit own assigned package
			if ($id != Session::get('loginUserId'))
			{
				DB::DELETE("DELETE from package_assignments where user_id = '$id'");
				$package3D4DAssignedArr = $request->get('package5D6DAssigned');
				$package5D6DAssignedArr = $request->get('package3D4DAssigned');
				for($i = 0; $i< parent::countArrSize($package3D4DAssignedArr);$i++)
				{
					$Assignment = new PackageAssignment;
					$Assignment->user_id = $id;
					$Assignment->package_id = $package3D4DAssignedArr[$i];
					$Assignment->push();
				}

				for($i = 0; $i<parent::countArrSize($package5D6DAssignedArr);$i++)
				{
					$Assignment = new PackageAssignment;
					$Assignment->user_id = $id;
					$Assignment->package_id = $package5D6DAssignedArr[$i];
					$Assignment->push();
				}

				if (!empty($request->get('red_3D4D_package')))
				{
					$Assignment = new PackageAssignment;
					$Assignment->user_id = $id;
					$Assignment->package_id = $request->get('red_3D4D_package');
					$Assignment->push();
				}
				if (!empty($request->get('red_5D6D_package')))
				{
					$Assignment = new PackageAssignment;
					$Assignment->user_id = $id;
					$Assignment->package_id = $request->get('red_5D6D_package');
					$Assignment->push();
				}

				if (!empty($unassignedPackage))
					self::unassignPackageInDownline($unassignedPackage, $id);
				parent::performPackageCleanup();
			}

			if ($user->user_upline == 0)
				$reference_id = $id;
			else
				$reference_id = $user->user_upline;

			DB::commit();
		}
		catch (\Exception $e)
		{
			DB::rollback();
			return back()->with('error', $e->getMessage())->withInput();
		}
		return back()->with('success',"Member Data Updated");
	}

	private function unassignPackageInDownline($packageIdList, $userId)
	{
		$package = explode(",",$packageIdList);
		$child = DB::SELECT("(SELECT COALESCE(GROUP_CONCAT(lv SEPARATOR ','),0) AS 
			child FROM ( SELECT @pv:=(SELECT GROUP_CONCAT(id SEPARATOR ',') FROM users WHERE FIND_IN_SET(user_upline, @pv))
			AS lv FROM users JOIN (SELECT @pv:='$userId') tmp) a)");
		$var = ((String)$child[0]->child);

		for($i = 0; $i < parent::countArrSize($package); $i++)
		{
			$childPackageList= DB::SELECT("SELECT COALESCE(GROUP_CONCAT(lv SEPARATOR ','),0) AS child FROM (
				SELECT @pv:=(SELECT GROUP_CONCAT(package_id SEPARATOR ',') FROM packages 
				WHERE FIND_IN_SET(reference_package_id, @pv)) AS lv FROM packages
				JOIN (SELECT @pv:='".$package[$i]."') tmp) a");

			DB::delete("DELETE from package_assignments where package_id in (".$package[$i].",".$childPackageList[0]->child.") AND user_id in (".$var.")");
			DB::statement("UPDATE users set activated_package_3D4D_id = '0' where activated_package_3D4D_id in (".$package[$i].",".$childPackageList[0]->child.") and id in (".$var.")");
			DB::statement("UPDATE users set activated_package_5D6D_id = '0' where activated_package_5D6D_id in (".$package[$i].",".$childPackageList[0]->child.") and id in (".$var.")");
		}
	}

	public function addNewDownline(Request $request, $id)
	{
		$mainAccId = parent::getUserById($id)->user_main_account;
		$package3D4DAssignedArr = $request->get('package3D4DAssigned');
		$package5D6DAssignedArr = $request->get('package5D6DAssigned');

		$eat_cats = EatDetailCategory::all();

		if (DB::table('users')->WHERE('username', '=', $request->get('username'))->count())
		{
			return back()->with('error', 'username used');
		}
		else
		{
			\DB::beginTransaction();
			try
			{
				$nextId = DB::table('users')->max('id') + 1;
				$newMember = new User;
				$newMember->user_upline = $mainAccId;
				$newMember->state = $request->get('state');
				$newMember->id = $nextId;
				$newMember->user_main_account = $nextId;
				$newMember->mode = $request->get('mode');
				$newMember->box_id = $request->get('boxes');
				$newMember->arrangement_id = $request->get('arrangement');
				$newMember->pool_shortcode = $request->get('short_code');
				$newMember->locked = $request->get('locked', false);
				$newMember->downline_create = $request->get('member_create', false);
				$newMember->float_val = $request->get('float', false);
				$newMember->username = $request->get('username');
				$newMember->name = $request->get('name');
				$newMember->user_role = $request->get('role');
				$newMember->password = Hash::make($request->get('password'));
				$newMember->credit_limit = $request->get('credit_limit');
				$newMember->position_3d4d = $request->get('selPosition3d4d');
				$newMember->position_5d6d = $request->get('selPosition5d6d');
				if ($request->has('package_control'))
					$newMember->package_control = $request->get('package_control');
				else
					$newMember->package_control = "1";

				if ($request->has('betting_capability'))
					$newMember->betting_capability = $request->get('betting_capability');
				else
					$newMember->betting_capability = "1";

				if ($request->has('report_view'))
					$newMember->report_view = $request->get('report_view');
				else
					$newMember->report_view = "1";

				$activated3D4DPackage = $request->get('activated_3D4D_package');
				if (!empty($activated3D4DPackage))
					$newMember->activated_package_3D4D_id = $activated3D4DPackage;
				$activated5D6DPackage = $request->get('activated_5D6D_package');
				if (!empty($activated5D6DPackage))
					$newMember->activated_package_5D6D_id = $activated5D6DPackage;


				$red3D4D = $request->get('red_3D4D_package');
				if ($red3D4D > 0)
				{
					$newMember->assigned_red_package_3D4D_id = $red3D4D;
					$Assignment = new PackageAssignment;
					$Assignment->user_id = $nextId;
					$Assignment->package_id = $red3D4D;
					$Assignment->push();
				}
				$red5D6D = $request->get('red_5D6D_package');
				if ($red5D6D > 0)
				{
					$newMember->assigned_red_package_5D6D_id = $red5D6D;
					$Assignment = new PackageAssignment;
					$Assignment->user_id = $nextId;
					$Assignment->package_id = $red5D6D;
					$Assignment->push();
				}
				$newMember->save();
				for($i = 0; $i<parent::countArrSize($package3D4DAssignedArr);$i++)
				{
					$Assignment = new PackageAssignment;
					$Assignment->user_id = $nextId;
					$Assignment->package_id = $package3D4DAssignedArr[$i];
					$Assignment->push();
				}
				for($i = 0; $i<parent::countArrSize($package5D6DAssignedArr);$i++)
				{
					$Assignment = new PackageAssignment;
					$Assignment->user_id = $nextId;
					$Assignment->package_id = $package5D6DAssignedArr[$i];
					$Assignment->push();
				}

				foreach($eat_cats as $eat_cat)
				{
					$eat_ref = new EatMapReferences();
					$eat_ref->user_id = $nextId;
					$eat_ref->pdc_id = $eat_cat->id;
					$eat_ref->value = 0;
					$eat_ref->save();                    
				}

				DB::commit();
			}
			catch (\Exception $e)
			{
				\DB::rollback();
				return back()->with('error',"System error. Please try again later. (DB Connection Failure)")->withInput();
			}
			return self::redirectToUserView($mainAccId);
		}
	}

	public function updateAccountState($id, $state)
	{
		if (!parent::checkIsIDBelongsToLoginUserHierachy($id))
		{
			return back()->with('error', "Invalid URL Access");
		}
		$mainAccId = parent::getUserById($id)->user_upline;

		DB::beginTransaction();
		try
		{
			DB::statement("UPDATE users SET state = '$state' WHERE id = '".$id."'");
			if (self::hasDownlineAccount($id))
			{
				$childList = self::getAllChildByParent($id)[0]->child;
				DB::statement("UPDATE users SET state = '$state' WHERE id in (".$childList.") and state < '$state'");
			}

			if (self::hasSubAccount($id))
			{
				$subAccount = DB::SELECT("SELECT COALESCE(GROUP_CONCAT(id SEPARATOR ','),0) as sub_account_list FROM users where user_main_account = '$id' and id != '$id'");
				DB::statement("UPDATE users SET state = '$state' WHERE id in (".$subAccount[0]->sub_account_list.") and state < '$state'");
			}
			DB::commit();
		}
		catch (\Exception $e)
		{
			\DB::rollback();
			return back()->with('error',"System error. Please try again later. (DB Connection Failure)");
		}
		return self::redirectToUserView($mainAccId);
	}

	public function index()
	{
		$todayDate = date("Ymd");
		$schedules = \App\Schedule::where("drawdate", ">=", $todayDate)->limit(4)->get()->keyBy('drawdate');

		$latestDate = \App\DrawResult::orderBy("id", "desc")->first();
		if(!empty($latestDate)){
			$poolResult = \App\DrawResult::where("drawdate", $latestDate->drawdate)
			->join("pools", "pools.id", "poolid")
			->orderBy("poolid", "ASC")
			->get()
			->groupBy('poolid');

			foreach($poolResult AS $key => $pool){
				$poolResult[$key]->pool = $pool[0];
				$poolResult[$key]->data = $pool->mapToGroups(function($item2, $key2){
					if(str_is('s*', $item2['type'])){
						return ['special' => $item2];
					}

					if(str_is('c*', $item2['type'])){
						return ['consolation' => $item2];
					}

					if(str_is('5d*', $item2['type'])){
						return ['5d' => $item2];
					}

					if(str_is('6d*', $item2['type'])){
						return ['6d' => $item2];
					}

					return ['top' => $item2];
				});
			}
		} else {
			$poolResult = collect();
		}

		return \view('index', compact("schedules", "poolResult"));
	}

    // Region of Bet Schedule
	public function betschedule()
	{
	    // 1. Retrieve all the schedules from the database
	    // 2. Send the data over to the View
		return \view('betschedule', ['schedules' => self::getAllSchedule()]);
	}

	public function loadprimary(Request $request)
	{
	    // Load all the possible primary bet dates (Wednesday, Saturday and Sunday) of a year
	    // Save the dates into database

		$year = $request->ddlyears;
		$fromDateStr = "01-JAN-".$year;
		$toDateStr = "31-DEC-".$year;

	    // parse the dates string to correct date format
		$startDateRaw = Carbon::parse($fromDateStr);
		$endDate = Carbon::parse($toDateStr);

	    // check the first date is what date
		$dayOfWeek = date('w', strtotime($startDateRaw));
		$searchCriteria = Carbon::MONDAY;

		$betDays = [];
		$searchCriteria = [];
		$dayArray = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
		switch($dayOfWeek)
		{
			case 0:
			$betDays[] = ['date'=> $startDateRaw->format('Ymd'), 'day'=> $dayArray[date('w', strtotime($startDateRaw))]];
			$searchCriteria = array(\Carbon\Carbon::WEDNESDAY, \Carbon\Carbon::SATURDAY, \Carbon\Carbon::SUNDAY);
			break;
			case 1:
			case 2:
			$startDateRaw = $startDateRaw->next(Carbon::WEDNESDAY);
			case 3:
			$betDays[] = ['date'=> $startDateRaw->format('Ymd'), 'day'=> $dayArray[date('w', strtotime($startDateRaw))]];
			$searchCriteria = array(\Carbon\Carbon::SATURDAY, \Carbon\Carbon::SUNDAY, \Carbon\Carbon::WEDNESDAY);
			break;
			case 4:
			case 5:
			$startDateRaw = $startDateRaw->next(Carbon::SATURDAY);
			case 6:
			$betDays[] = ['date'=> $startDateRaw->format('Ymd'), 'day'=> $dayArray[date('w', strtotime($startDateRaw))]];
			$searchCriteria = array(\Carbon\Carbon::SUNDAY, \Carbon\Carbon::WEDNESDAY, \Carbon\Carbon::SATURDAY);
			break;	          
		} 

	    // Start looping based on the search criteria	    
		while ($startDateRaw->lte($endDate))
		{
			foreach ($searchCriteria as $s)
			{
				$startDateRaw->next($s);
				if (!($startDateRaw->lte($endDate)))
				{
					break;
				}

				$betDays[] = ['date'=> $startDateRaw->format('Ymd'), 'day'=> $dayArray[date('w', strtotime($startDateRaw))]];	            
			}	        
		}

	    // now loop the array and save in the database
		try {
			foreach($betDays as $b)
			{
				$newSchedule = new Schedule();
				$newSchedule->drawdate = $b['date'];
				$newSchedule->day = $b['day'];
				$newSchedule->mg = true;
				$newSchedule->kd = true;
				$newSchedule->tt = true;
				$newSchedule->sg = true;
				$newSchedule->sb = true;
				$newSchedule->stc = true;
				$newSchedule->sw = true;
				$newSchedule->isDeleted = false;
				$newSchedule->save();
			}

			Session::flash('message', 'Primary schedule saved successfully.');
			Session::flash('class', 'alert alert-success');
			return redirect()->route("betschedule");
		}
		catch(\Exception $ex)
		{
			Session::flash('message', 'Whoops! '.$ex->getMessage());
			Session::flash('class', 'alert alert-danger');
			return redirect()->route("betschedule");
		}	    
	}

	public function writeADate(Request $request)
	{
	    // 1. get new schedule and write into database
		$newSchedule = new Schedule();
		$newSchedule->drawdate = $request->txtNewDate;
		$newSchedule->mg = $request->chkmg=='on'? true: false;
		$newSchedule->kd = $request->chkkd=='on'? true: false;
		$newSchedule->tt = $request->chktt=='on'? true: false;
		$newSchedule->sg = $request->chksg=='on'? true: false;
		$newSchedule->sb = $request->chksb=='on'? true: false;
		$newSchedule->sw = $request->chksw=='on'? true: false;
		$newSchedule->stc = $request->chkstc=='on'? true: false;
		$newSchedule->isdeleted = false;

		$thedate = date("w", strtotime($newSchedule->drawdate));
		$dayArray = array('Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday');
		$day = $dayArray[$thedate];
		$newSchedule->day = $day;

		try {
			$newSchedule->save();
			Session::flash('message', 'New schedule inserted successfully.');
			Session::flash('class', 'alert alert-success'); 
			return redirect()->route("betschedule");
		}
		catch(\Exception $ex)
		{
			Session::flash('message', 'Whoops! '.$ex->getMessage());
			Session::flash('class', 'alert alert-danger'); 
			return redirect()->route("betschedule");
		}
	}

	public function index_betlimit(){
		$pools = Pool::get();
		$limits = BetLimit::where("isdeleted", 0)->get();
		return view("betlimit", compact("pools", "limits"));
	}

	public function store_betlimit(Request $request){
		$pools = Pool::get();
		$limit = new BetLimit();
		
		//loop through pools based on shortname
		foreach($pools AS $pool){
			$property = $pool->shortname;
			$limit->$property = ($request->input("chk_" . $property) == "on") ? 1 : 0;
		}

		$limit->box = ($request->input("chk_box") == "on") ? 1 : 0;
		$limit->number = $request->txt_number;

		$limit->bet_big = ($request->txt_big == null) ? 0 : $request->txt_big;
		$limit->bet_small = ($request->txt_small == null) ? 0 : $request->txt_small;
		$limit->bet_big_small = ($request->txt_big_small == null) ? 0 : $request->txt_big_small;
		$limit->bet_a = ($request->txt_a == null) ? 0 : $request->txt_a;
		$limit->bet_abc = ($request->txt_abc == null) ? 0 : $request->txt_abc;
		$limit->bet_a_abc = ($request->txt_a_abc == null) ? 0 : $request->txt_a_abc;
		$limit->bet_4a = ($request->txt_4a == null) ? 0 : $request->txt_4a;
		$limit->bet_4b = ($request->txt_4b == null) ? 0 : $request->txt_4b;
		$limit->bet_4c = ($request->txt_4c == null) ? 0 : $request->txt_4c;
		$limit->bet_4d = ($request->txt_4d == null) ? 0 : $request->txt_4d;
		$limit->bet_4e = ($request->txt_4e == null) ? 0 : $request->txt_4e;
		$limit->bet_4abc = ($request->txt_4abc == null) ? 0 : $request->txt_4abc;
		$limit->bet_3a = ($request->txt_3a == null) ? 0 : $request->txt_3a;
		$limit->bet_3abc = ($request->txt_3abc == null) ? 0 : $request->txt_3abc;
		$limit->bet_2a = ($request->txt_2a == null) ? 0 : $request->txt_2a;
		$limit->bet_2abc = ($request->txt_2abc == null) ? 0 : $request->txt_2abc;

		$limit->user_id = Auth::user()->id;

		$limit->save();

		return redirect::action("MemberController@store_betlimit");
	}

	public function getBetLimit($id){
		$limit = BetLimit::find($id);
		$limit->checkedArr = $limit->getCheckedPools();
		return response()->json(['success' => true, 'limit' => $limit]);
	}

	public function update_betlimit($id, Request $request){

		$limit = BetLimit::find($id);

		$pools = Pool::get();
		foreach($pools AS $pool){
			$property = $pool->shortname;
			$limit->$property = ($request->input("edit_chk_" . $property) == "on") ? 1 : 0;
		}

		$limit->box = ($request->input("edit_chk_box") == "on") ? 1 : 0;

		$limit->bet_big = ($request->edit_txt_big == null) ? 0 : $request->edit_txt_big;
		$limit->bet_small = ($request->edit_txt_small == null) ? 0 : $request->edit_txt_small;
		$limit->bet_big_small = ($request->edit_txt_big_small == null) ? 0 : $request->edit_txt_big_small;
		$limit->bet_a = ($request->edit_txt_a == null) ? 0 : $request->edit_txt_a;
		$limit->bet_abc = ($request->edit_txt_abc == null) ? 0 : $request->edit_txt_abc;
		$limit->bet_a_abc = ($request->edit_txt_a_abc == null) ? 0 : $request->edit_txt_a_abc;
		$limit->bet_4a = ($request->edit_txt_4a == null) ? 0 : $request->edit_txt_4a;
		$limit->bet_4b = ($request->edit_txt_4b == null) ? 0 : $request->edit_txt_4b;
		$limit->bet_4c = ($request->edit_txt_4c == null) ? 0 : $request->edit_txt_4c;
		$limit->bet_4d = ($request->edit_txt_4d == null) ? 0 : $request->edit_txt_4d;
		$limit->bet_4e = ($request->edit_txt_4e == null) ? 0 : $request->edit_txt_4e;
		$limit->bet_4abc = ($request->edit_txt_4abc == null) ? 0 : $request->edit_txt_4abc;
		$limit->bet_3a = ($request->edit_txt_3a == null) ? 0 : $request->edit_txt_3a;
		$limit->bet_3abc = ($request->edit_txt_3abc == null) ? 0 : $request->edit_txt_3abc;
		$limit->bet_2a = ($request->edit_txt_2a == null) ? 0 : $request->edit_txt_2a;
		$limit->bet_2abc = ($request->edit_txt_2abc == null) ? 0 : $request->edit_txt_2abc;

		$limit->number = $request->edit_txt_number;
		$limit->save();

		Session::flash("msg", "Successfully updated bet limit");
		Session::flash("class", "alert-success");
		return redirect::action("MemberController@index_betlimit");
	}

	public function checkBetLimitNum($number, $id){
		$limit = BetLimit::where("number", $number)
		->where("user_id", Auth::user()->id)
		->where("isdeleted", 0)
		->where("id", "!=", $id)
		->first();

		if(empty($limit)){
			//Return true if number doesn't exist in DB
			return response()->json(['success' => true]);
		} else {
			return response()->json(['success' => false]);
		}
	}

	public function delete_betlimit($id){
		DB::table('bet_limits')
		->where('id', $id)
		->update(['isdeleted' => 1]);

		Session::flash("msg", "Successfully deleted selected bet limit");
		Session::flash("class", "alert-success");
		return redirect::action("MemberController@index_betlimit");
	}

	public function index_rednumberrestriction(){
		$pools = Pool::get();
		$numbers = RedNumberRestriction::where("isdeleted", 0)->get();
		return view("redNumberRestriction", compact("pools", "numbers"));
	}

	public function store_rednumberrestriction(Request $request){
		try {

			$red = new RedNumberRestriction();

			$pools = Pool::get();

			$selectedPool = [];

			//loop through pools based on shortname
			foreach($pools AS $pool){
				$property = $pool->shortname;

				if($request->input("chk_" . $property) == "on"){
					$red->$property = 1;
					array_push($selectedPool, $property);
				} else {
					$red->$property = 0;
				}
			}

			$red->type = $request->ddl_type;


			//Check if restriction with selected type(P4, P6, P12) with any of the selected pool (M,K,T..) is added
			$exist = RedNumberRestriction::where("type", $red->type)
			->where("isdeleted", "0")
			->where(function ($query) use ($selectedPool){
				foreach($selectedPool AS $sel){
					$query->orWhere($sel, "1");
				}
			})
			->first();

			//Stop user from adding restriction
			if(!empty($exist)){
				Session::flash("msg", "Unable to add red number restriction. One of the selected pool has already been added into the restriction list.");
				Session::flash("class", "alert-danger");
				return redirect::back()->withInput();
			}

			$red->bet_big = ($request->txt_big == null) ? 0 : $request->txt_big;
			$red->bet_small = ($request->txt_small == null) ? 0 : $request->txt_small;
			$red->bet_big_small = ($request->txt_big_small == null) ? 0 : $request->txt_big_small;
			$red->bet_a = ($request->txt_a == null) ? 0 : $request->txt_a;
			$red->bet_abc = ($request->txt_abc == null) ? 0 : $request->txt_abc;
			$red->bet_a_abc = ($request->txt_a_abc == null) ? 0 : $request->txt_a_abc;
			$red->bet_4a = ($request->txt_4a == null) ? 0 : $request->txt_4a;
			$red->bet_4b = ($request->txt_4b == null) ? 0 : $request->txt_4b;
			$red->bet_4c = ($request->txt_4c == null) ? 0 : $request->txt_4c;
			$red->bet_4d = ($request->txt_4d == null) ? 0 : $request->txt_4d;
			$red->bet_4e = ($request->txt_4e == null) ? 0 : $request->txt_4e;
			$red->bet_4abc = ($request->txt_4abc == null) ? 0 : $request->txt_4abc;
			$red->bet_3a = ($request->txt_3a == null) ? 0 : $request->txt_3a;
			$red->bet_3abc = ($request->txt_3abc == null) ? 0 : $request->txt_3abc;
			$red->bet_2a = ($request->txt_2a == null) ? 0 : $request->txt_2a;
			$red->bet_2abc = ($request->txt_2abc == null) ? 0 : $request->txt_2abc;

			$red->user_id = Auth::user()->id;

			$red->save();

			Session::flash("msg", "Red number restriction added.");
			Session::flash("class", "alert-success");
			return redirect::back();
		} catch (\Exception $e) {
			Session::flash("msg", "Unable to add red number restriction. " . $e->getMessage());
			Session::flash("class", "alert-danger");
			return redirect::back();
		}
	}

	public function delete_rednumberrestriction(Request $request){
		try {

			$delete_arr = $request->txt_id;

			if(!empty($delete_arr)){
				$delete = DB::table('red_number_restrictions')
				->whereIn('id', $delete_arr)
				->update(['isdeleted' => 1]);

				Session::flash("msg", "Successfully deleted selected red number restriction(s).");
				Session::flash("class", "alert-success");
			}

			return redirect::back();
		} catch (\Exception $e) {
			Session::flash("msg", "Unable to delete selected red number restriction(s). " . $e->getMessage());
			Session::flash("class", "alert-danger");
			return redirect::back();
		}
	}

	public function getRedNumberRestriction($id){
		$red = RedNumberRestriction::find($id);

		$checkPool = $red->getCheckedPools();

		$red->checkedArr = $checkPool[0];
		$red->disabledArr = $checkPool[1];

		return response()->json(['success' => true, 'red' => $red]);
	}

	public function update_rednumberrestriction($id, Request $request){
		try {
			$red = RedNumberRestriction::find($id);

			$pools = Pool::get();
			foreach($pools AS $pool){
				$property = $pool->shortname;

				//Check if selected pool is already added for this type
				$check = RedNumberRestriction::where("type", $red->type)
				->where($property, 1)
				->where("isdeleted", 0)
				->where("id", "!=", $red->id)
				->first();

				if(empty($check)){
					$red->$property = ($request->input("edit_chk_" . $property) == "on") ? 1 : 0;
				}
			}

			$red->bet_big = ($request->edit_txt_big == null) ? 0 : $request->edit_txt_big;
			$red->bet_small = ($request->edit_txt_small == null) ? 0 : $request->edit_txt_small;
			$red->bet_big_small = ($request->edit_txt_big_small == null) ? 0 : $request->edit_txt_big_small;
			$red->bet_a = ($request->edit_txt_a == null) ? 0 : $request->edit_txt_a;
			$red->bet_abc = ($request->edit_txt_abc == null) ? 0 : $request->edit_txt_abc;
			$red->bet_a_abc = ($request->edit_txt_a_abc == null) ? 0 : $request->edit_txt_a_abc;
			$red->bet_4a = ($request->edit_txt_4a == null) ? 0 : $request->edit_txt_4a;
			$red->bet_4b = ($request->edit_txt_4b == null) ? 0 : $request->edit_txt_4b;
			$red->bet_4c = ($request->edit_txt_4c == null) ? 0 : $request->edit_txt_4c;
			$red->bet_4d = ($request->edit_txt_4d == null) ? 0 : $request->edit_txt_4d;
			$red->bet_4e = ($request->edit_txt_4e == null) ? 0 : $request->edit_txt_4e;
			$red->bet_4abc = ($request->edit_txt_4abc == null) ? 0 : $request->edit_txt_4abc;
			$red->bet_3a = ($request->edit_txt_3a == null) ? 0 : $request->edit_txt_3a;
			$red->bet_3abc = ($request->edit_txt_3abc == null) ? 0 : $request->edit_txt_3abc;
			$red->bet_2a = ($request->edit_txt_2a == null) ? 0 : $request->edit_txt_2a;
			$red->bet_2abc = ($request->edit_txt_2abc == null) ? 0 : $request->edit_txt_2abc;

			$red->save();

			Session::flash("msg", "Successfully updated selected red number restriction.");
			Session::flash("class", "alert-success");
			return redirect::back();
		} catch (\Exception $e) {
			Session::flash("msg", "Unable to update selected red number restriction. " . $e->getMessage());
			Session::flash("class", "alert-danger");
			return redirect::back();
		}
	}

	public function store_voidtimeout(Request $request){
		try {

			$setting = SystemSetting::updateOrCreate(
				['setting_name' => 'void_timeout'],
				['setting_value' => $request->void_timeout]
			);

			Session::flash("msg", "Successfully updated void timeout setting.");
			Session::flash("class1", "alert-success");
			return redirect::back();
		} catch (\Exception $e) {
			Session::flash("msg", "Unable to update void timeout setting. " . $e->getMessage());
			Session::flash("class1", "alert-danger");
			return redirect::back();
		}
	}

	public function updatesShedule(Request $request)
	{
	    //dd($request);
		$idarray = $request->id;
		foreach($idarray as $i)
		{
	        // search for the record in database
			$schedule = new Schedule();
			$schedule = Schedule::find($i);
			if ($request->get('chk'.$i) == '0')
			{
				$schedule->isdeleted = true;
			}
			else
			{
				$schedule->isdeleted = false;
			}

			$schedule->mg = $request->get('chkmg'.$i) == 'on'? true: false;
			$schedule->kd = $request->get('chkkd'.$i) == 'on'? true: false;
			$schedule->sg = $request->get('chksg'.$i) == 'on'? true: false;
			$schedule->tt = $request->get('chktt'.$i) == 'on'? true: false;
			$schedule->sb = $request->get('chksb'.$i) == 'on'? true: false;
			$schedule->stc = $request->get('chkstc'.$i) == 'on'? true: false;
			$schedule->sw = $request->get('chksw'.$i) == 'on'? true: false;

			try {
				$schedule->save();	            
			}
			catch(\Exception $ex)
			{
				Session::flash('message', 'Whoops! '.$ex->getMessage());
				Session::flash('class', 'alert alert-danger');
				return redirect()->route("betschedule");
			}
		}   

		Session::flash('message', 'Schedule saved successfully.');
		Session::flash('class', 'alert alert-success');
		return redirect()->route("betschedule");

	}

	private function getAllSchedule()
	{
		return 
		Schedule::orderBy('drawdate')->get();	    
	}

	// Region of Draw Result
	public function drawResult()
	{
		return \view('drawresult');
	}

	public function drawResultPostback(Request $request)
	{
		$tab = 'mg';
		$pp = '1';
		if ($request->btnSubmit == "Save Result")
		{
	        // Get the current tab that need to save result
			$tab = $request->update_id;
			if ($tab == 'mg') $pp = 1;
			else if ($tab == 'kd') $pp = 2;
			else if ($tab == 'tt') $pp = 3;
			else if ($tab == 'sg') $pp = 4;
			else if ($tab == 'sb') $pp = 5;
			else if ($tab == 'stc') $pp = 6;
			else if ($tab == 'gd') $pp = 8;
			else $pp = 7;

	        // remove all the same pool, same date
			try {
				$existingresults = DrawResult::where("poolid", "=", $pp)
				->where("drawdate", "=", $request->txtNewDate)
				->get();

				foreach($existingresults as $e)
				{
					$e->delete();
				}

			}
			catch(\Exception $ex)
			{
				Session::flash('message', 'Whoops! '.$ex->getMessage());
				Session::flash('class', 'alert alert-danger');
				return redirect()->route("drawresult");
			}

	        // first 3 prizes
			for ($i=1; $i<4 ; $i++)
			{
				$result = new DrawResult();
				$result->drawdate = $request->txtNewDate;
				$result->type = $i;
				$result->number = $request->get('txt'.$i.$tab) == null? '': $request->get('txt'.$i.$tab);
				$result->poolid = $pp;
				$result->save();
			}  

			$section = array("s", "c");
			foreach($section as $s)
			{
				for ($i=1; $i<11 ; $i++)
				{
					$result = new DrawResult();
					$result->drawdate = $request->txtNewDate;
					$result->type = $s.$i;
					$result->number = $request->get('txt'.$s.$i.$tab) == null? '': $request->get('txt'.$s.$i.$tab);
					$result->poolid = $pp;
					$result->save();
				} 
			}        

			if ($tab == 'tt')
			{
				$result = new DrawResult();
				$result->drawdate = $request->txtNewDate;
				$result->type = '5d1';
				$result->number = $request->get('txt5d1tt') == null? '': $request->get('txt5d1tt');
				$result->poolid = $pp;
				$result->save();

				$result = new DrawResult();
				$result->drawdate = $request->txtNewDate;
				$result->type = '5d2';
				$result->number = $request->get('txt5d2tt') == null? '': $request->get('txt5d2tt');
				$result->poolid = $pp;
				$result->save();

				$result = new DrawResult();
				$result->drawdate = $request->txtNewDate;
				$result->type = '5d3';
				$result->number = $request->get('txt5d3tt') == null? '': $request->get('txt5d3tt');
				$result->poolid = $pp;
				$result->save();

				$result = new DrawResult();
				$result->drawdate = $request->txtNewDate;
				$result->type = '6d1';
				$result->number = $request->get('txt6d1tt') == null? '': $request->get('txt6d1tt');
				$result->poolid = $pp;
				$result->save();
			}

			ProcessResults::dispatch($request->txtNewDate);
			//$this->processResults($request->txtNewDate);

			Session::flash('message', 'Result saved successfully.');
			Session::flash('class', 'alert alert-success');
			return redirect()->route("drawresult");
		}
		else
		{
	        // Load the primary out
			$dateInput = Carbon::createFromFormat("Ymd", $request->txtNewDate)->format("Y-m-d");

			$this->scraper($dateInput);

			$this->scraperGD($dateInput);

			Session::flash("date", $request->txtNewDate);

			return Redirect::back();
		}
	}

	public function getAllResult($drawdate)
	{
		$existingresults = DrawResult::where("drawdate", "=", $drawdate)
		->get();    
		return $existingresults;
	}

	public function getAvailablePosition($userid)
	{
		$user = \App\User()::find($userid);
	}
}

