<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Auth;

use Illuminate\Support\Facades\DB;

class LoginController extends BaseController
{
    public function loginValidate(Request $request)
    {
        request()->validate([
            'captcha' => 'required|captcha'
        ],
        ['captcha.captcha'=>'Invalid captcha code.']);
        
        if (Auth::attempt(['username' => $request->input('username'), 'password' => $request->input('user_password')], $request->input('remember_me_checkbox')))
        {
            if (auth()->user()->state == parent::$suspendedState)
            {
        		return redirect('/')->with('error', 'Account is suspended');
            }
            else
            {
                $role = collect(DB::SELECT("select role_name from roles where role_id = '".auth()->user()->user_role."'"))->first();
        		Session::put('loginUserId', auth()->user()->id);
        		Session::put('loginUserMainAccountId', auth()->user()->user_main_account);
                Session::put('loginUsername', auth()->user()->username);
        		Session::put('loginUserRole', auth()->user()->user_role);
                Session::put('loginUserRoleName', $role->role_name);
        		return redirect()->route("index",auth()->user()->id);
            }
        }
        else
        {
            return redirect('/')->with('error', 'Incorrect Username or Password');
        }
    }
    
    public function refreshCaptcha()
    {
        return response()->json(['captcha'=> captcha_img()]);
    }
}
