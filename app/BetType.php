<?php
namespace app;

class BetType
{
    public $amount_Big;
    public $amount_Small;
    public $amount_3A;
    public $amount_4A;
    public $amount_3ABC;
    public $amount_4B;
    public $amount_4C;
    public $amount_4D;
    public $amount_4E;
    public $amount_4ABC;
    public $amount_2A;
    public $amount_2ABC;
    public $amount_5D;
    public $amount_6D;
}

