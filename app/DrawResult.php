<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DrawResult extends Model
{
    protected $fillable = ['drawdate', 'poolid', 'type', 'number'];
}
