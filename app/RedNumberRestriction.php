<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RedNumberRestriction extends Model
{
	public function getCheckedPools()
	{
		$checkedArr = [];
		$disabledArr = [];

		$pools = \App\Pool::get();
		foreach($pools AS $pool){
			$property = $pool->shortname;
			if($this->$property == 1){
				array_push($checkedArr, $property);
			} else {
				//Check if pool already added
				$exist = \App\RedNumberRestriction::where("id", "!=", $this->id)
				->where($property, 1)
				->where("isdeleted", 0)
				->where("type", $this->type)
				->first();

				if(!empty($exist)){
					array_push($disabledArr, $property);
				}
			}
		}
		return [$checkedArr, $disabledArr];
	}
}
