<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Carbon\Carbon;
use \App\Ticket;

class BetStage extends Model
{
	const actual_bets = [
		"actualbig",
		"actualsmall",
		"actual4a",
		"actual4b",
		"actual4c",
		"actual4d",
		"actual4e",
		"actual4abc",
		"actual3a",
		"actual3abc",
		"actual2a",
		"actual2abc",
		"actual5d",
		"actual6d"
	];

	public function getDrawDate(){
		return Carbon::createFromFormat("Ymd", $this->drawdate)->format("d/m/Y");
	}

	public function getUser(){
		return $this->hasOne('App\User', 'id', 'userid');
	}

	public function getType(){
		return $this->hasOne('App\Pool', 'code', 'pool');
	}

	public function getPage(){
		$ticket = Ticket::find($this->ticketid);
		return $ticket->pageid;
	}

	public function getTotal(){
		$total = 0;
		foreach(BetStage::actual_bets AS $property){
			$total += $this->$property;
		}

		$this->total = $total;

		return number_format($total, 2);
	}

	public static function getPoolId($code){
		$poolId;
    	switch($code){
    		case "M":
    		$poolId = 1;
    		break;

    		case "P":
    		$poolId = 2;
    		break;

    		case "T":
    		$poolId = 3;
    		break;

    		case "S":
    		$poolId = 4;
    		break;

    		case "B":
    		$poolId = 5;
    		break;

    		case "K":
    		$poolId = 6;
    		break;

    		case "W":
    		$poolId = 7;
    		break;

    		case "G":
    		$poolId = 8;
    		break;
    	}
    	return $poolId;
    }
}
