<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ticket extends Model
{

	protected $dates = ["betTime"];

    public function getUser(){
		return $this->hasOne('App\User', 'id', 'userid');
	}
}
