<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;

use App\DrawResult;
use App\EatMapReferences;
use App\Pool;
use App\User;
use App\EatMap;
use App\Schedule;
use App\BetCutOffSetting;
use App\AgentDistribution;
use App\Ticket;
use App\Arrangement;
use App\SystemSetting;
use App\BetStage;
use App\StageDetail;
use App\PackageDetail;

class ProcessResults implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $drawdate;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($drawdate)
    {
        //
        $this->drawdate = $drawdate;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        //
         try {
            DB::beginTransaction();
            $bet_stages = BetStage::where('drawdate', $this->drawdate)
            ->where('status', 0)->get();
            $group_tic = $bet_stages->groupBy('ticketid');
            //Loop tickets
            foreach ($group_tic as $tickets) {
                //Loop bet stages
                foreach ($tickets as $bets) {
                    //4D or 2D
                    $draw_results = DrawResult::where('number', $bets->number)
                    ->where('poolid', $bets->getPoolId($bets->pool))
                    ->where('drawdate', $this->drawdate)->get();

                    //if wins
                    if (count($draw_results) > 0) {
                        foreach ($draw_results as $dr) {
                            if ($dr->type == "1") {
                                if ($bets->actualbig > 0 || $bets->actualsmall > 0 || $bets->actual4abc > 0 || $bets->actual3a > 0 || $bets->actual3abc > 0 || $bets->actual2a > 0 || $bets->actual2abc) {
                                    $stage_details = StageDetail::where("stageid", $bets->id)
                                    ->where('status', 0)->get();

                                    foreach ($stage_details as $sd) {
                                        $packageStr = "SELECT * FROM package_details WHERE package_id = '".$bets->packageid."' AND package_detail_category_id IN (SELECT package_detail_category_id FROM package_detail_categories WHERE package_detail_subcategory_name = ";

                                        if ($sd->eatbig > 0) {
                                            $package = DB::SELECT($packageStr."'Big') LIMIT 1");
                                            $sd->paybig = $sd->eatbig * $package[0]->first_prize;
                                        }

                                        if ($sd->eatsmall > 0) {
                                            $package = DB::SELECT($packageStr."'Small') LIMIT 1");
                                            $sd->paysmall = $sd->eatsmall * $package[0]->first_prize;
                                        }

                                        if ($sd->eat4abc > 0) {
                                            $package = DB::SELECT($packageStr."'4ABC') LIMIT 1");
                                            $sd->pay4abc = $sd->eat4abc * $package[0]->first_prize;
                                        }

                                        if ($sd->eat3a > 0) {
                                            $package = DB::SELECT($packageStr."'3A') LIMIT 1");
                                            $sd->pay3a = $sd->eat3a * $package[0]->first_prize;
                                        }

                                        if ($sd->eat3abc > 0) {
                                            $package = DB::SELECT($packageStr."'3ABC') LIMIT 1");
                                            $sd->pay3abc = $sd->eat3abc * $package[0]->first_prize;
                                        }

                                        if ($sd->eat2a > 0) {
                                            $package = DB::SELECT($packageStr."'2A') LIMIT 1");
                                            $sd->pay2a = $sd->eat2a * $package[0]->first_prize;
                                        }

                                        if ($sd->eat2abc > 0) {
                                            $package = DB::SELECT($packageStr."'2ABC') LIMIT 1");
                                            $sd->pay2abc = $sd->eat2abc * $package[0]->first_prize;
                                        }

                                        $sd->status = 1;
                                        $sd->save();
                                    } // end forloop stage details
                                }
                            } 
                            else if ($dr->type == "2") {
                                if ($bets->actualbig > 0 || $bets->actualsmall > 0 || $bets->actual4abc > 0 || $bets->actual4b > 0 || $bets->actual3abc > 0 || $bets->actual2abc) {
                                    $stage_details = StageDetail::where("stageid", $bets->id)
                                    ->where('status', 0)->get();

                                    foreach ($stage_details as $sd) {
                                        $packageStr = "SELECT * FROM package_details WHERE package_id = '".$bets->packageid."' AND package_detail_category_id IN (SELECT package_detail_category_id FROM package_detail_categories WHERE package_detail_subcategory_name = ";

                                        if ($sd->eatbig > 0) {
                                            $package = DB::SELECT($packageStr."'Big') LIMIT 1");
                                            $sd->paybig = $sd->eatbig * $package[0]->second_prize;
                                        }

                                        if ($sd->eatsmall > 0) {
                                            $package = DB::SELECT($packageStr."'Small') LIMIT 1");
                                            $sd->paysmall = $sd->eatsmall * $package[0]->second_prize;
                                        }

                                        if ($sd->eat4abc > 0) {
                                            $package = DB::SELECT($packageStr."'4ABC') LIMIT 1");
                                            $sd->pay4abc = $sd->eat4abc * $package[0]->second_prize;
                                        }

                                        if ($sd->eat4b > 0) {
                                            $package = DB::SELECT($packageStr."'4B') LIMIT 1");
                                            $sd->pay4b = $sd->eat4b * $package[0]->second_prize;
                                        }

                                        if ($sd->eat3abc > 0) {
                                            $package = DB::SELECT($packageStr."'3ABC') LIMIT 1");
                                            $sd->pay3abc = $sd->eat3abc * $package[0]->second_prize;
                                        }

                                        if ($sd->eat2abc > 0) {
                                            $package = DB::SELECT($packageStr."'2ABC') LIMIT 1");
                                            $sd->pay2abc = $sd->eat2abc * $package[0]->second_prize;
                                        }

                                        $sd->status = 1;
                                        $sd->save();
                                    } // end forloop stage details
                                }
                            }
                            else if ($dr->type == "3") {
                                if ($bets->actualbig > 0 || $bets->actualsmall > 0 || $bets->actual4abc > 0 || $bets->actual4c > 0 || $bets->actual3abc > 0 || $bets->actual2abc) {
                                    $stage_details = StageDetail::where("stageid", $bets->id)
                                    ->where('status', 0)->get();

                                    foreach ($stage_details as $sd) {
                                        $packageStr = "SELECT * FROM package_details WHERE package_id = '".$bets->packageid."' AND package_detail_category_id IN (SELECT package_detail_category_id FROM package_detail_categories WHERE package_detail_subcategory_name = ";

                                        if ($sd->eatbig > 0) {
                                            $package = DB::SELECT($packageStr."'Big') LIMIT 1");
                                            $sd->paybig = $sd->eatbig * $package[0]->third_prize;
                                        }

                                        if ($sd->eatsmall > 0) {
                                            $package = DB::SELECT($packageStr."'Small') LIMIT 1");
                                            $sd->paysmall = $sd->eatsmall * $package[0]->third_prize;
                                        }

                                        if ($sd->eat4abc > 0) {
                                            $package = DB::SELECT($packageStr."'4ABC') LIMIT 1");
                                            $sd->pay4abc = $sd->eat4abc * $package[0]->third_prize;
                                        }

                                        if ($sd->eat4c > 0) {
                                            $package = DB::SELECT($packageStr."'4C') LIMIT 1");
                                            $sd->pay4c = $sd->eat4c * $package[0]->third_prize;
                                        }

                                        if ($sd->eat3abc > 0) {
                                            $package = DB::SELECT($packageStr."'3ABC') LIMIT 1");
                                            $sd->pay3abc = $sd->eat3abc * $package[0]->third_prize;
                                        }

                                        if ($sd->eat2abc > 0) {
                                            $package = DB::SELECT($packageStr."'2ABC') LIMIT 1");
                                            $sd->pay2abc = $sd->eat2abc * $package[0]->third_prize;
                                        }

                                        $sd->status = 1;
                                        $sd->save();
                                    } // end forloop stage details
                                }
                            }
                            else if (substr($dr->type, 0, 1) == "s") {
                                if ($bets->actualbig > 0 || $bets->actual4d > 0) {
                                    $stage_details = StageDetail::where("stageid", $bets->id)
                                    ->where('status', 0)->get();

                                    foreach ($stage_details as $sd) {
                                        $packageStr = "SELECT * FROM package_details WHERE package_id = '".$bets->packageid."' AND package_detail_category_id IN (SELECT package_detail_category_id FROM package_detail_categories WHERE package_detail_subcategory_name = ";

                                        if ($sd->eatbig > 0) {
                                            $package = DB::SELECT($packageStr."'Big') LIMIT 1");
                                            $sd->paybig = $sd->eatbig * $package[0]->special_prize;
                                        }

                                        if ($sd->eat4d > 0) {
                                            $package = DB::SELECT($packageStr."'4D') LIMIT 1");
                                            $sd->pay4d = $sd->eat4d * $package[0]->special_prize;
                                        }

                                        $sd->status = 1;
                                        $sd->save();
                                    } // end forloop stage details
                                }
                            }
                            else if (substr($dr->type, 0, 1) == "c") {
                                if ($bets->actualbig > 0 || $bets->actual4e > 0) {
                                    $stage_details = StageDetail::where("stageid", $bets->id)
                                    ->where('status', 0)->get();

                                    foreach ($stage_details as $sd) {
                                        $packageStr = "SELECT * FROM package_details WHERE package_id = '".$bets->packageid."' AND package_detail_category_id IN (SELECT package_detail_category_id FROM package_detail_categories WHERE package_detail_subcategory_name = ";

                                        if ($sd->eatbig > 0) {
                                            $package = DB::SELECT($packageStr."'Big') LIMIT 1");
                                            $sd->paybig = $sd->eatbig * $package[0]->consolation_prize;
                                        }

                                        if ($sd->eat4e > 0) {
                                            $package = DB::SELECT($packageStr."'4E') LIMIT 1");
                                            $sd->pay4e = $sd->eat4e * $package[0]->consolation_prize;
                                        }

                                        $sd->status = 1;
                                        $sd->save();
                                    } // end forloop stage details
                                }
                            }
                        } //end forloop draw result

                        $bets->status = 1;
                        $bets->save();
                    }
                    else {
                        //3D
                        $draw_results_3d = DB::SELECT("SELECT * FROM draw_results WHERE poolid = '".$bets->getPoolId($bets->pool)."' AND drawdate = '".$this->drawdate."' AND number LIKE '_".(substr($bets->number, 1))."'");

                        if (empty($draw_results_3d)) {
                            $bets->status = 1;
                            $bets->save();

                            DB::table('stage_details')
                            ->where('stageid', $bets->id)
                            ->update(['status' => 1]);
                        }
                        else {
                            foreach ($draw_results_3d as $dr) {
                                if ($dr->type == "1") {
                                    if ($bets->actual3a > 0 || $bets->actual3abc > 0) {
                                        $stage_details = StageDetail::where("stageid", $bets->id)
                                        ->where('status', 0)->get();

                                        foreach ($stage_details as $sd) {
                                            $packageStr = "SELECT * FROM package_details WHERE package_id = '".$bets->packageid."' AND package_detail_category_id IN (SELECT    package_detail_category_id FROM package_detail_categories WHERE package_detail_subcategory_name = ";

                                            if ($sd->eat3a > 0) {
                                                $package = DB::SELECT($packageStr."'3A') LIMIT 1");
                                                $sd->pay3a = $sd->eat3a * $package[0]->first_prize;
                                            }

                                            if ($sd->eat3abc > 0) {
                                                $package = DB::SELECT($packageStr."'3ABC') LIMIT 1");
                                                $sd->pay3abc = $sd->eat3abc * $package[0]->first_prize;
                                            }

                                            $sd->status = 1;
                                            $sd->save();
                                        } // end forloop stage details
                                    }
                                } 
                                else if ($dr->type == "2") {
                                    if ($bets->actual3abc > 0) {
                                        $stage_details = StageDetail::where("stageid", $bets->id)
                                        ->where('status', 0)->get();

                                        foreach ($stage_details as $sd) {
                                            $packageStr = "SELECT * FROM package_details WHERE package_id = '".$bets->packageid."' AND package_detail_category_id IN (SELECT    package_detail_category_id FROM package_detail_categories WHERE package_detail_subcategory_name = ";


                                            if ($sd->eat3abc > 0) {
                                                $package = DB::SELECT($packageStr."'3ABC') LIMIT 1");
                                                $sd->pay3abc = $sd->eat3abc * $package[0]->second_prize;
                                            }

                                            $sd->status = 1;
                                            $sd->save();
                                        } // end forloop stage details
                                    }
                                }
                                else if ($dr->type == "3") {
                                    if ($bets->actual3abc > 0) {
                                        $stage_details = StageDetail::where("stageid", $bets->id)
                                        ->where('status', 0)->get();

                                        foreach ($stage_details as $sd) {
                                            $packageStr = "SELECT * FROM package_details WHERE package_id = '".$bets->packageid."' AND package_detail_category_id IN (SELECT    package_detail_category_id FROM package_detail_categories WHERE package_detail_subcategory_name = ";


                                            if ($sd->eat3abc > 0) {
                                                $package = DB::SELECT($packageStr."'3ABC') LIMIT 1");
                                                $sd->pay3abc = $sd->eat3abc * $package[0]->third_prize;
                                            }

                                            $sd->status = 1;
                                            $sd->save();
                                        } // end forloop stage details
                                    }
                                }
                            } //end forloop
                            $bets->status = 1;
                            $bets->save();
                        }
                    }
                }

                DB::table('tickets')
                ->where('id', $tickets[0]->ticketid)
                ->update(['status' => 1]);
            }

            DB::commit();
        }
        catch (\Exception $e) {
            DB::rollBack();
        }
    }
}

