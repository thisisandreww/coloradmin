<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SystemSetting extends Model
{
    protected $table = "system_settings";

    protected $fillable = ["setting_value"];

    protected $primaryKey = "setting_id";
}
