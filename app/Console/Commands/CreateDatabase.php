<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use Illuminate\Support\Facades\DB;
use Symfony\Component\Console\Input\InputArgument;

class CreateDatabase extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'make:database {dbname} {connection?}';
	protected $name = "make:database";

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

	protected function getArguments()
{
    return [
        ['name', InputArgument::REQUIRED, 'The name of the database'],
    ];
}
public function fire()
{
    DB::getConnection()->statement('CREATE DATABASE :schema', ['schema' => $this->argument('name')]);
} 

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
		DB::getConnection()->statement('CREATE DATABASE :schema', ['schema' => $this->argument('name')]);
    }
}
