// this is to populate the drop dowlist with an array
function populateDropdownList(ddl, theArray) {
    ddl.empty();
    var vItems = [];
    for (var i = 0; i < theArray.length; ++i) {
        elem = theArray[i];
        var found = false;
        vItems.push('<option value="' + elem.value + '">' + elem.text + '</option>');
    }
    ddl.append(vItems.join(''));
}