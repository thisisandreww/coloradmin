## BotDetect Captcha jQuery Plugin (BotDetect Captcha Simple API integration for all of the jQuery versions)

### Quick guide:

##### 1) Captcha jQuery Plugin Installation
```sh
npm install jquery-captcha --save
```
##### 2) Include Captcha jQuery Plugin in Your Web App
```html
<script src="node_modules/jquery-captcha/dist/jquery-captcha.min.js"></script>
```
##### 3) Load Captcha jQuery Plugin in Your App
- Java Captcha endpoint:
```js
$(document).ready(function() {
    // DOM ready
    var captcha = $('#botdetect-captcha').captcha({
      captchaEndpoint: '/botdetect-java-captcha-api-path-at-server-side/botdetectcaptcha'
    });
});
```

- PHP Captcha endpoint:
```js
$(document).ready(function() {
    // DOM ready
    var captcha = $('#botdetect-captcha').captcha({
      captchaEndpoint: '/botdetect-php-captcha-api-path-at-server-side/simple-botdetect.php'
    });
});
```

##### 4) Display Captcha In Your View
```html
<div id="botdetect-captcha" data-stylename="exampleCaptcha"></div>
```

##### 5) Validate Captcha on the Client-side
```html
<input 
  type="text" 
  name="captchaCode"
  id="captchaCode"
  data-correct-captcha
>
```

```js
$('#captchaCode').on('validatecaptcha', function(event, isCorrect) {
  if (isCorrect) {
    // UI Captcha validation passed
  } else {
    // UI Captcha validation failed
  }
});
```

##### 6) Validate Captcha on the Server-side
The client-side Captcha validation is just a usability improvement. Captcha protects some action on a server-side, and therefore Captcha validation has to be there as well.

- Server-side Captcha validation with [Java Captcha](https://captcha.com/java-captcha.html#simple-api) looks in this way:
```java
SimpleCaptcha captcha = SimpleCaptcha.load(request);
boolean isHuman = captcha.validate(captchaCode, captchaId);
```

- Server-side Captcha validation with [PHP Captcha](https://captcha.com/php-captcha.html#simple-api) looks in this way:
```php
$captcha = new SimpleCaptcha();
$isHuman = $captcha->Validate($captchaCode, $captchaId);
```

### Docs:
 
[jQuery CAPTCHA Integration Guide](https://captcha.com/jquery-captcha.html)

### Code Examples: 
1. [Basic jQuery CAPTCHA Example](https://captcha.com/doc/jquery/examples/jquery-basic-captcha-example.html)

2. [jQuery CAPTCHA Form Example](https://captcha.com/doc/jquery/examples/jquery-form-captcha-example.html)


### Dependencies:
Captcha jQuery plugin uses Captcha library for Captcha image and Captcha sound generation. At the moment challenges are generated in Java backend with [BotDetect Java Captcha library](https://captcha.com/java-captcha.html#simple-api) and PHP backend with [BotDetect PHP Captcha library](https://captcha.com/php-captcha.html#simple-api), but BotDetect ASP.NET is going to work with Captcha jQuery plugin soon as well.


### Technical Support:

Through [contact form on captcha.com](https://captcha.com/contact.html).
