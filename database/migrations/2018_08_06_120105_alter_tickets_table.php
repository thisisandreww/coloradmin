<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('tickets', function (Blueprint $table) {
            $table->dateTime('betTime')->nullable()->change();
            $table->dateTime('voidTime')->nullable()->change();
            $table->integer('voidby')->nullable()->change();
            $table->integer('packageid_3d4d')->nullable()->change();
            $table->integer('packageid_5d6d')->nullable()->change();
            $table->string('type')->nullable()->change();
            $table->string('bet_method')->nullable()->change();
            $table->string('bet_format')->nullable()->change();
            $table->string('draw_format')->nullable()->change();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
