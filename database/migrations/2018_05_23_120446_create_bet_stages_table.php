<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBetStagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bet_stages', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('ticketid');
            $table->integer('status');
            $table->integer('userid');
            $table->integer('packageid');
            $table->string('number');
            $table->string('drawdate');
            $table->float('betamountbig');
            $table->float('betamountsmall');
            $table->float('betamount4a');
            $table->float('betamount3a');
            $table->float('betamount3abc');
            $table->float('betamount5d');
            $table->float('betamount6d');
            $table->float('betamount2a');
            $table->float('betamount2abc');
            $table->float('betamount4b');
            $table->float('betamount4c');
            $table->float('betamount4d');
            $table->float('betamount4e');
            $table->float('betamount4abc');
            $table->float('actualbig');
            $table->float('actualsmall');
            $table->float('actual4a');
            $table->float('actual3a');
            $table->float('actual3abc');
            $table->float('actual5d');
            $table->float('actual6d');
            $table->float('actual2a');
            $table->float('actual2abc');
            $table->float('actual4b');
            $table->float('actual4c');
            $table->float('actual4d');
            $table->float('actual4e');
            $table->float('actual4abc');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bet_stages');
    }
}
