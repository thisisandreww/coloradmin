<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBetLimitsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bet_limits', function (Blueprint $table) {
            $table->increments('id');

            $table->boolean('mg');
            $table->boolean('kd');
            $table->boolean('tt');
            $table->boolean('sg');
            $table->boolean('sb');
            $table->boolean('stc');
            $table->boolean('sw');
            $table->boolean('gd');
            $table->boolean('pd');

            $table->boolean('box');
            $table->integer('number');

            $table->float('bet_big');
            $table->float('bet_small');
            $table->float('bet_big_small');
            $table->float('bet_a');
            $table->float('bet_abc');
            $table->float('bet_a_abc');
            $table->float('bet_4a');
            $table->float('bet_4b');
            $table->float('bet_4c');
            $table->float('bet_4d');
            $table->float('bet_4e');
            $table->float('bet_4abc');
            $table->float('bet_3a');
            $table->float('bet_3abc');
            $table->float('bet_2a');
            $table->float('bet_2abc');

            $table->boolean('isdeleted');

            $table->unsignedInteger('user_id');
            $table->foreign('user_id')->references('id')->on('users');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bet_limits');
    }
}
