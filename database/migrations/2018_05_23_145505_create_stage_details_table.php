<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStageDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stage_details', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('stageid');
            $table->integer('userid');
            $table->integer('downlinerecordid');
            $table->integer('status');
            $table->integer('position');
            $table->float('stockbig');
            $table->float('stocksmall');
            $table->float('stock4a');
            $table->float('stock3a');
            $table->float('stock3abc');
            $table->float('stock5d');
            $table->float('stock6d');
            $table->float('stock2a');
            $table->float('stock2abc');
            $table->float('stock4b');
            $table->float('stock4c');
            $table->float('stock4d');
            $table->float('stock4e');
            $table->float('stock4abc');
            $table->float('eatbig');
            $table->float('eatsmall');
            $table->float('eat4a');
            $table->float('eat3a');
            $table->float('eat3abc');
            $table->float('eat5d');
            $table->float('eat6d');
            $table->float('eat2a');
            $table->float('eat2abc');
            $table->float('eat4b');
            $table->float('eat4c');
            $table->float('eat4d');
            $table->float('eat4e');
            $table->float('eat4abc');
            $table->float('commbig');
            $table->float('commsmall');
            $table->float('comm4a');
            $table->float('comm3a');
            $table->float('comm3abc');
            $table->float('comm5d');
            $table->float('comm6d');
            $table->float('comm2a');
            $table->float('comm2abc');
            $table->float('comm4b');
            $table->float('comm4c');
            $table->float('comm4d');
            $table->float('comm4e');
            $table->float('comm4abc');
            $table->float('paybig');
            $table->float('paysmall');
            $table->float('pay4a');
            $table->float('pay3a');
            $table->float('pay3abc');
            $table->float('pay5d');
            $table->float('pay6d');
            $table->float('pay2a');
            $table->float('pay2abc');
            $table->float('pay4b');
            $table->float('pay4c');
            $table->float('pay4d');
            $table->float('pay4e');
            $table->float('pay4abc');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('stage_details');
    }
}
