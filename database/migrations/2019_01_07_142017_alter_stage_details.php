<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterStageDetails extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('stage_details', function (Blueprint $table) {
            $table->decimal('commratebig', 12, 4);
            $table->decimal('commratesmall', 12, 4);
            $table->decimal('commrate3a', 12, 4);
            $table->decimal('commrate3abc', 12, 4);
            $table->decimal('commrate4a', 12, 4);
            $table->decimal('commrate4b', 12, 4);
            $table->decimal('commrate4c', 12, 4);
            $table->decimal('commrate4d', 12, 4);
            $table->decimal('commrate4e', 12, 4);
            $table->decimal('commrate4abc', 12, 4);
            $table->decimal('commrate2a', 12, 4);
            $table->decimal('commrate2abc', 12, 4);
            $table->decimal('commrate5d', 12, 4);
            $table->decimal('commrate6d', 12, 4);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return voidddd
     */
    public function down()
    {
        //
    }
}
