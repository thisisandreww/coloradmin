<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageDetailCategoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('package_detail_categories', function (Blueprint $table) {
            $table->increments('package_detail_category_id');
            $table->engine = 'InnoDB';
			$table->string('package_detail_category_name', 50);
			$table->string('package_detail_subcategory_name', 50)->unique();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('package_detail_categories');
    }
}
