<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePackageDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('package_details', function (Blueprint $table) {
            $table->increments('package_detail_id');
            $table->engine = 'InnoDB';
            $table->integer('package_id');
			$table->integer('package_detail_category_id');
			$table->unique(array('package_id', 'package_detail_category_id'));
			$table->integer('ticket_prize');
			$table->decimal('commission');
			$table->decimal('first_prize');
			$table->decimal('second_prize');
			$table->decimal('third_prize');
			$table->decimal('special_prize');
			$table->decimal('consolation_prize');
			$table->decimal('sixth_prize');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('package_details');
    }
}
