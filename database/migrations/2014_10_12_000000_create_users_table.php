<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->increments('id');
            $table->engine = 'InnoDB';
            $table->string('username')->unique();
            $table->string('name');
            $table->string('password');
			$table->integer('credit_limit');
			$table->boolean('state');
			$table->boolean('user_upline');
            $table->boolean('user_main_account');
			$table->integer('user_role');
			$table->integer('activated_package_3D4D_id');
            $table->integer('activated_package_5D6D_id');
            $table->integer('assigned_red_package_3D4D_id');
            $table->integer('assigned_red_package_5D6D_id');
			$table->integer('mode');
			$table->integer('arrangement_id');
			$table->integer('box_id');
            $table->integer('locked');
            $table->integer('downline_create');
            $table->integer('betting_capability');
            $table->integer('package_control');
            $table->integer('report_view');
            $table->string('eat_type_3d4d');
            $table->string('eat_type_5d6d');
            $table->string('eat_type_4dspcl');
            $table->integer('position_3d4d');
            $table->integer('position_5d6d');
            $table->integer('float_val');
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
