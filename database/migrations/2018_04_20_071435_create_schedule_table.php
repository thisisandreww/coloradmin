<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateScheduleTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('schedules', function (Blueprint $table) {
            $table->increments('id');
            $table->string('drawdate')->unique();
            $table->string('day');
            $table->boolean('mg');
            $table->boolean('kd');
            $table->boolean('tt');
            $table->boolean('sg');
            $table->boolean('sb');
            $table->boolean('stc');
            $table->boolean('sw');
            $table->boolean('isdeleted');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('schedules');
    }
}
