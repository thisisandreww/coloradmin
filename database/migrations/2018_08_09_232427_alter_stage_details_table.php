<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterStageDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::table('stage_details', function (Blueprint $table) {
            $table->decimal('stockbig', 12, 4)->change();
            $table->decimal('stocksmall', 12, 4)->change();
            $table->decimal('stock4a', 12, 4)->change();
            $table->decimal('stock3a', 12, 4)->change();
            $table->decimal('stock3abc', 12, 4)->change();
            $table->decimal('stock5d', 12, 4)->change();
            $table->decimal('stock6d', 12, 4)->change();
            $table->decimal('stock2a', 12, 4)->change();
            $table->decimal('stock2abc', 12, 4)->change();
            $table->decimal('stock4b', 12, 4)->change();
            $table->decimal('stock4c', 12, 4)->change();
            $table->decimal('stock4d', 12, 4)->change();
            $table->decimal('stock4e', 12, 4)->change();
            $table->decimal('stock4abc', 12, 4)->change();
            $table->decimal('eatbig', 12, 4)->change();
            $table->decimal('eatsmall', 12, 4)->change();
            $table->decimal('eat4a', 12, 4)->change();
            $table->decimal('eat3a', 12, 4)->change();
            $table->decimal('eat3abc', 12, 4)->change();
            $table->decimal('eat5d', 12, 4)->change();
            $table->decimal('eat6d', 12, 4)->change();
            $table->decimal('eat2a', 12, 4)->change();
            $table->decimal('eat2abc', 12, 4)->change();
            $table->decimal('eat4b', 12, 4)->change();
            $table->decimal('eat4c', 12, 4)->change();
            $table->decimal('eat4d', 12, 4)->change();
            $table->decimal('eat4e', 12, 4)->change();
            $table->decimal('eat4abc', 12, 4)->change();
            $table->decimal('commbig', 12, 4)->change();
            $table->decimal('commsmall', 12, 4)->change();
            $table->decimal('comm4a', 12, 4)->change();
            $table->decimal('comm3a', 12, 4)->change();
            $table->decimal('comm3abc', 12, 4)->change();
            $table->decimal('comm5d', 12, 4)->change();
            $table->decimal('comm6d', 12, 4)->change();
            $table->decimal('comm2a', 12, 4)->change();
            $table->decimal('comm2abc', 12, 4)->change();
            $table->decimal('comm4b', 12, 4)->change();
            $table->decimal('comm4c', 12, 4)->change();
            $table->decimal('comm4d', 12, 4)->change();
            $table->decimal('comm4e', 12, 4)->change();
            $table->decimal('comm4abc', 12, 4)->change();
            $table->decimal('paybig', 12, 4)->change();
            $table->decimal('paysmall', 12, 4)->change();
            $table->decimal('pay4a', 12, 4)->change();
            $table->decimal('pay3a', 12, 4)->change();
            $table->decimal('pay3abc', 12, 4)->change();
            $table->decimal('pay5d', 12, 4)->change();
            $table->decimal('pay6d', 12, 4)->change();
            $table->decimal('pay2a', 12, 4)->change();
            $table->decimal('pay2abc', 12, 4)->change();
            $table->decimal('pay4b', 12, 4)->change();
            $table->decimal('pay4c', 12, 4)->change();
            $table->decimal('pay4d', 12, 4)->change();
            $table->decimal('pay4e', 12, 4)->change();
            $table->decimal('pay4abc', 12, 4)->change();
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
