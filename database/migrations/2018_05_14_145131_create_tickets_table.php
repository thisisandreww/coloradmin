<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTicketsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tickets', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('pageid');
            $table->string('currency');
            $table->integer('userid');
            $table->integer('packageid_3d4d');
            $table->integer('packageid_5d6d');
            $table->string('type');
            $table->string('bet_method');
            $table->string('bet_format');
            $table->string('draw_format');
            $table->string('box_ibox');
            $table->longText('inbox');
            $table->longText('betstring');
            $table->float('initial_amount');
            $table->float('actual_amount');
            $table->boolean('status');
            $table->integer('voidby');
            $table->dateTime('betTime');
            $table->dateTime('voidTime');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tickets');
    }
}
