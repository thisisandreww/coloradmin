<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB as DB;
class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(UserSeeder::class);
         $this->call(RoleAssignmentSeeder::class);
         $this->call(ArrangementSeeder::class);
         $this->call(BoxSeeder::class);
         $this->call(PackageDetailCategorySeeder::class);
         $this->call(PackageTypeSeeder::class);
         $this->call(RoleSeeder::class);
         $this->call(SystemSettingSeeder::class);
         $this->call(PoolSeeder::class);
         $this->call(EatDetailCategorySeeder::class);
         $this->call(EatMapReferenceSeeder::class);
         $this->call(CutOffSeeder::class);
    }
}

class UserSeeder extends Seeder
{
    public function run()
    {
        DB::table('users')->delete();
        DB::table('users')->insert([
            'id' => "1",
            'username' => "system",
            'name' => "system",
            'password' => Hash::make(111),
            'user_role' => '9',
            'user_main_account'=> '1',
            'state'=>'0',
            'credit_limit'=>'10000',
            'user_upline'=>'0',
            'downline_create'=>'1',
            'betting_capability'=>'1',
            'report_view'=>'1',
            'package_control'=>'1',
            'eat_type_3d4d'=>'',
            'eat_type_5d6d'=>'',
            'eat_type_4dspcl'=>'',
            'position_3d4d'=>100,
            'position_5d6d'=>100,
            ]);
        DB::table('users')->insert([
            'id' => "2",
            'username' => "Company",
            'name' => "Company",
            'password' => Hash::make(111),
            'user_role' => '8',
            'user_main_account'=> '2',
            'state'=>'0',
            'credit_limit'=>'0',
            'user_upline'=>'1',
            'downline_create'=>'1',
            'betting_capability'=>'1',
            'report_view'=>'1',
            'package_control'=>'1',
            'eat_type_3d4d'=>'',
            'eat_type_5d6d'=>'',
            'eat_type_4dspcl'=>'',
            'position_3d4d'=>100,
            'position_5d6d'=>100,
            ]);
    }
}

class ArrangementSeeder extends Seeder
{
    public function run()
    {
        DB::table('arrangements')->delete();
        DB::table('arrangements')->insert(['arrangement_id' => "1",'arrangement_name'=>'Big-Small-4A-ABC-A',]);
        DB::table('arrangements')->insert(['arrangement_id' => "2",'arrangement_name'=>'Big-Small-4A-ABC-4A',]);
        DB::table('arrangements')->insert(['arrangement_id' => "3",'arrangement_name'=>'Big-Small-3A-ABC-4A',]);
        DB::table('arrangements')->insert(['arrangement_id' => "4",'arrangement_name'=>'Small-Big-A-ABC-4A',]);
        DB::table('arrangements')->insert(['arrangement_id' => "5",'arrangement_name'=>'Small-Big-ABC-A-4A',]);
        DB::table('arrangements')->insert(['arrangement_id' => "6",'arrangement_name'=>'Big-Small-4A-A-ABC',]);
       }
}

class BoxSeeder extends Seeder
{
    public function run()
    {
        DB::table('boxes')->delete();
        DB::table('boxes')->insert(['box_id' => "1",'box_format'=>'* / **',]);
        DB::table('boxes')->insert(['box_id' => "2",'box_format'=>'** / *',]);
       }
}

class PackageDetailCategorySeeder extends Seeder
{
    public function run()
    {
        DB::table('package_detail_categories')->delete();
        DB::table('package_detail_categories')->insert(['package_detail_category_id' => "1",'package_detail_category_name'=>'4D','package_detail_subcategory_name'=>'Big',]);
        DB::table('package_detail_categories')->insert(['package_detail_category_id' => "2",'package_detail_category_name'=>'4D','package_detail_subcategory_name'=>'Small',]);
        DB::table('package_detail_categories')->insert(['package_detail_category_id' => "3",'package_detail_category_name'=>'4D','package_detail_subcategory_name'=>'4A',]);
        DB::table('package_detail_categories')->insert(['package_detail_category_id' => "4",'package_detail_category_name'=>'3D','package_detail_subcategory_name'=>'3ABC',]);
        DB::table('package_detail_categories')->insert(['package_detail_category_id' => "5",'package_detail_category_name'=>'3D','package_detail_subcategory_name'=>'3A',]);
        DB::table('package_detail_categories')->insert(['package_detail_category_id' => "6",'package_detail_category_name'=>'5D/6D','package_detail_subcategory_name'=>'5D',]);
        DB::table('package_detail_categories')->insert(['package_detail_category_id' => "7",'package_detail_category_name'=>'5D/6D','package_detail_subcategory_name'=>'6D',]);
        DB::table('package_detail_categories')->insert(['package_detail_category_id' => "8",'package_detail_category_name'=>'4D Special','package_detail_subcategory_name'=>'4B',]);
        DB::table('package_detail_categories')->insert(['package_detail_category_id' => "9",'package_detail_category_name'=>'4D Special','package_detail_subcategory_name'=>'4C',]);
        DB::table('package_detail_categories')->insert(['package_detail_category_id' => "10",'package_detail_category_name'=>'4D Special','package_detail_subcategory_name'=>'4D',]);
        DB::table('package_detail_categories')->insert(['package_detail_category_id' => "11",'package_detail_category_name'=>'4D Special','package_detail_subcategory_name'=>'4E',]);
        DB::table('package_detail_categories')->insert(['package_detail_category_id' => "12",'package_detail_category_name'=>'4D Special','package_detail_subcategory_name'=>'4ABC',]);
        DB::table('package_detail_categories')->insert(['package_detail_category_id' => "13",'package_detail_category_name'=>'4D Special','package_detail_subcategory_name'=>'2ABC',]);
        DB::table('package_detail_categories')->insert(['package_detail_category_id' => "14",'package_detail_category_name'=>'4D Special','package_detail_subcategory_name'=>'2A',]);
    }
}

class PackageTypeSeeder extends Seeder
{
    public function run()
    {
        DB::table('package_types')->delete();
        DB::table('package_types')->insert(['package_type_id' => "1",'package_type_name'=>'3D/4D',]);
        DB::table('package_types')->insert(['package_type_id' => "2",'package_type_name'=>'5D/6D',]);
    }
}

class SystemSettingSeeder extends Seeder
{
    public function run()
    {
        DB::table('system_settings')->delete();
        DB::table('system_settings')->insert(['setting_id'=> "1",'setting_name'=>'allow_red_package_control','setting_value'=>"0",]);
    }
}

class RoleSeeder extends Seeder
{
    public function run()
    {
        DB::table('roles')->delete();
        DB::table('roles')->insert(['role_id' => "1",'role_name'=>'Player']);
        DB::table('roles')->insert(['role_id' => "2",'role_name'=>'Agent']);
        DB::table('roles')->insert(['role_id' => "3",'role_name'=>'Master']);
        DB::table('roles')->insert(['role_id' => "4",'role_name'=>'Senior']);
        DB::table('roles')->insert(['role_id' => "5",'role_name'=>'Super Senior']);
        DB::table('roles')->insert(['role_id' => "6",'role_name'=>'Admin']);
        DB::table('roles')->insert(['role_id' => "7",'role_name'=>'Executive']);
        DB::table('roles')->insert(['role_id' => "8",'role_name'=>'Company']);
        DB::table('roles')->insert(['role_id' => "9",'role_name'=>'System']);
    }
}

class RoleAssignmentSeeder extends Seeder
{
    public function run()
    {
DB::table('role_assignment')->delete();
    DB::table('role_assignment')->insert(['role_id' => "2",'role_assignment_id'=>1,]);
    DB::table('role_assignment')->insert(['role_id' => "3",'role_assignment_id'=>1,]);
    DB::table('role_assignment')->insert(['role_id' => "3",'role_assignment_id'=>2,]);
    DB::table('role_assignment')->insert(['role_id' => "4",'role_assignment_id'=>1,]);
    DB::table('role_assignment')->insert(['role_id' => "4",'role_assignment_id'=>2,]);
    DB::table('role_assignment')->insert(['role_id' => "4",'role_assignment_id'=>3,]);
    DB::table('role_assignment')->insert(['role_id' => "5",'role_assignment_id'=>1,]);
    DB::table('role_assignment')->insert(['role_id' => "5",'role_assignment_id'=>2,]);
    DB::table('role_assignment')->insert(['role_id' => "5",'role_assignment_id'=>3,]);
    DB::table('role_assignment')->insert(['role_id' => "5",'role_assignment_id'=>4,]);
    DB::table('role_assignment')->insert(['role_id' => "6",'role_assignment_id'=>1,]);
    DB::table('role_assignment')->insert(['role_id' => "6",'role_assignment_id'=>2,]);
    DB::table('role_assignment')->insert(['role_id' => "6",'role_assignment_id'=>3,]);
    DB::table('role_assignment')->insert(['role_id' => "6",'role_assignment_id'=>4,]);
    DB::table('role_assignment')->insert(['role_id' => "6",'role_assignment_id'=>5,]);
    DB::table('role_assignment')->insert(['role_id' => "7",'role_assignment_id'=>1,]);
    DB::table('role_assignment')->insert(['role_id' => "7",'role_assignment_id'=>2,]);
    DB::table('role_assignment')->insert(['role_id' => "7",'role_assignment_id'=>3,]);
    DB::table('role_assignment')->insert(['role_id' => "7",'role_assignment_id'=>4,]);
    DB::table('role_assignment')->insert(['role_id' => "7",'role_assignment_id'=>5,]);
    DB::table('role_assignment')->insert(['role_id' => "7",'role_assignment_id'=>6,]);
    DB::table('role_assignment')->insert(['role_id' => "7",'role_assignment_id'=>7,]);
    DB::table('role_assignment')->insert(['role_id' => "8",'role_assignment_id'=>1,]);
    DB::table('role_assignment')->insert(['role_id' => "8",'role_assignment_id'=>2,]);
    DB::table('role_assignment')->insert(['role_id' => "8",'role_assignment_id'=>3,]);
    DB::table('role_assignment')->insert(['role_id' => "8",'role_assignment_id'=>4,]);
    DB::table('role_assignment')->insert(['role_id' => "8",'role_assignment_id'=>5,]);
    DB::table('role_assignment')->insert(['role_id' => "8",'role_assignment_id'=>6,]);
    DB::table('role_assignment')->insert(['role_id' => "8",'role_assignment_id'=>7,]);
    DB::table('role_assignment')->insert(['role_id' => "9",'role_assignment_id'=>8,]);
    }
}

class PoolSeeder extends Seeder
{
    public function run()
    {
        DB::table('pools')->delete();
        DB::table('pools')->insert(['id' => "1",'name'=>"Magnum", 'shortname'=>'mg', 'code'=>'M', 'images'=>"/assets/img/lotto/bet-logo-magnum.png"]);
        DB::table('pools')->insert(['id' => "2",'name'=>"PMP",'shortname'=>'kd', 'code'=>'P', 'images'=>"/assets/img/lotto/bet-logo-damacai-inversed.png"]);
        DB::table('pools')->insert(['id' => "3",'name'=>"Toto",'shortname'=>'tt', 'code'=>'T', 'images'=>"/assets/img/lotto/bet-logo-toto-inversed.png"]);
        DB::table('pools')->insert(['id' => "4",'name'=>"Singapore",'shortname'=>'sg', 'code'=>'S', 'images'=>"/assets/img/lotto/bet-logo-singapore-pools.png"]);
        DB::table('pools')->insert(['id' => "5",'name'=>"Sabah",'shortname'=>'sb', 'code'=>'B', 'images'=>"/assets/img/lotto/bet-logo-big-cash.png"]);
        DB::table('pools')->insert(['id' => "6",'name'=>"STC",'shortname'=>'stc', 'code'=>'K', 'images'=>"/assets/img/lotto/bet-logo-sabah88.png"]);
        DB::table('pools')->insert(['id' => "7",'name'=>"Sarawak",'shortname'=>'sw', 'code'=>'W', 'images'=>"/assets/img/lotto/bet-logo-stc.png"]);
        DB::table('pools')->insert(['id' => "8",'name'=>"GrandDragon",'shortname'=>'gd', 'code'=>'G', 'images'=>"/assets/img/lotto/bet-logo-gdlotto.png"]);
    }
}

class CutOffSeeder extends Seeder
{
    public function run()
    {
        DB::table('bet_cut_off_settings')->delete();
        DB::table('bet_cut_off_settings')->insert(['id' => "1",'pool_id'=>"1", 'hours'=>'18', 'minutes'=>"50"]);
        DB::table('bet_cut_off_settings')->insert(['id' => "2",'pool_id'=>"2",'hours'=>'18', 'minutes'=>"50"]);
        DB::table('bet_cut_off_settings')->insert(['id' => "3",'pool_id'=>"3",'hours'=>'18', 'minutes'=>"50"]);
        DB::table('bet_cut_off_settings')->insert(['id' => "4",'pool_id'=>"4",'hours'=>'18', 'minutes'=>"50"]);
        DB::table('bet_cut_off_settings')->insert(['id' => "5",'pool_id'=>"5",'hours'=>'18', 'minutes'=>"50"]);
        DB::table('bet_cut_off_settings')->insert(['id' => "6",'pool_id'=>"6",'hours'=>'18', 'minutes'=>"50"]);
        DB::table('bet_cut_off_settings')->insert(['id' => "7",'pool_id'=>"7",'hours'=>'18', 'minutes'=>"50"]);
        DB::table('bet_cut_off_settings')->insert(['id' => "8",'pool_id'=>"8",'hours'=>'18', 'minutes'=>"50"]);
    }
}

class EatDetailCategorySeeder extends Seeder
{
    public function run()
    {
        DB::table('eat_detail_categories')->delete();
        DB::table('eat_detail_categories')->insert(['id' => "1",'eat_detail_type_name'=>'Total',]);
        DB::table('eat_detail_categories')->insert(['id' => "2",'eat_detail_type_name'=>'Amount','eat_detail_subtype_name'=>'Big',]);
        DB::table('eat_detail_categories')->insert(['id' => "3",'eat_detail_type_name'=>'Amount','eat_detail_subtype_name'=>'Small',]);
        DB::table('eat_detail_categories')->insert(['id' => "4",'eat_detail_type_name'=>'Amount','eat_detail_subtype_name'=>'4A',]);
        DB::table('eat_detail_categories')->insert(['id' => "5",'eat_detail_type_name'=>'Amount','eat_detail_subtype_name'=>'3ABC',]);
        DB::table('eat_detail_categories')->insert(['id' => "6",'eat_detail_type_name'=>'Amount','eat_detail_subtype_name'=>'3A',]);
        DB::table('eat_detail_categories')->insert(['id' => "7",'eat_detail_type_name'=>'Amount','eat_detail_subtype_name'=>'5D',]);
        DB::table('eat_detail_categories')->insert(['id' => "8",'eat_detail_type_name'=>'Amount','eat_detail_subtype_name'=>'6D',]);
        DB::table('eat_detail_categories')->insert(['id' => "9",'eat_detail_type_name'=>'Amount','eat_detail_subtype_name'=>'4B',]);
        DB::table('eat_detail_categories')->insert(['id' => "10",'eat_detail_type_name'=>'Amount','eat_detail_subtype_name'=>'4C',]);
        DB::table('eat_detail_categories')->insert(['id' => "11",'eat_detail_type_name'=>'Amount','eat_detail_subtype_name'=>'4D',]);
        DB::table('eat_detail_categories')->insert(['id' => "12",'eat_detail_type_name'=>'Amount','eat_detail_subtype_name'=>'4E',]);
        DB::table('eat_detail_categories')->insert(['id' => "13",'eat_detail_type_name'=>'Amount','eat_detail_subtype_name'=>'4ABC',]);
        DB::table('eat_detail_categories')->insert(['id' => "14",'eat_detail_type_name'=>'Amount','eat_detail_subtype_name'=>'2ABC',]);
        DB::table('eat_detail_categories')->insert(['id' => "15",'eat_detail_type_name'=>'Amount','eat_detail_subtype_name'=>'2A',]);
        DB::table('eat_detail_categories')->insert(['id' => "16",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'0000-0999','eat_detail_tertiertype_name'=>'Big',]);
        DB::table('eat_detail_categories')->insert(['id' => "17",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'0000-0999','eat_detail_tertiertype_name'=>'Small',]);
        DB::table('eat_detail_categories')->insert(['id' => "18",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'0000-0999','eat_detail_tertiertype_name'=>'4A',]);
        DB::table('eat_detail_categories')->insert(['id' => "19",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'000-099','eat_detail_tertiertype_name'=>'3ABC',]);
        DB::table('eat_detail_categories')->insert(['id' => "20",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'000-099','eat_detail_tertiertype_name'=>'3A',]);       
        DB::table('eat_detail_categories')->insert(['id' => "21",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'1000-1999','eat_detail_tertiertype_name'=>'Big',]);
        DB::table('eat_detail_categories')->insert(['id' => "22",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'1000-1999','eat_detail_tertiertype_name'=>'Small',]);
        DB::table('eat_detail_categories')->insert(['id' => "23",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'1000-1999','eat_detail_tertiertype_name'=>'4A',]);
        DB::table('eat_detail_categories')->insert(['id' => "24",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'100-199','eat_detail_tertiertype_name'=>'3ABC',]);
        DB::table('eat_detail_categories')->insert(['id' => "25",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'100-199','eat_detail_tertiertype_name'=>'3A',]);
        DB::table('eat_detail_categories')->insert(['id' => "26",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'2000-2999','eat_detail_tertiertype_name'=>'Big',]);
        DB::table('eat_detail_categories')->insert(['id' => "27",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'2000-2999','eat_detail_tertiertype_name'=>'Small',]);
        DB::table('eat_detail_categories')->insert(['id' => "28",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'2000-2999','eat_detail_tertiertype_name'=>'4A',]);
        DB::table('eat_detail_categories')->insert(['id' => "29",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'200-299','eat_detail_tertiertype_name'=>'3ABC',]);
        DB::table('eat_detail_categories')->insert(['id' => "30",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'200-299','eat_detail_tertiertype_name'=>'3A',]);
        DB::table('eat_detail_categories')->insert(['id' => "31",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'3000-3999','eat_detail_tertiertype_name'=>'Big',]);
        DB::table('eat_detail_categories')->insert(['id' => "32",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'3000-3999','eat_detail_tertiertype_name'=>'Small',]);
        DB::table('eat_detail_categories')->insert(['id' => "33",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'3000-3999','eat_detail_tertiertype_name'=>'4A',]);
        DB::table('eat_detail_categories')->insert(['id' => "34",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'300-399','eat_detail_tertiertype_name'=>'3ABC',]);
        DB::table('eat_detail_categories')->insert(['id' => "35",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'300-399','eat_detail_tertiertype_name'=>'3A',]);
        DB::table('eat_detail_categories')->insert(['id' => "36",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'4000-4999','eat_detail_tertiertype_name'=>'Big',]);
        DB::table('eat_detail_categories')->insert(['id' => "37",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'4000-4999','eat_detail_tertiertype_name'=>'Small',]);
        DB::table('eat_detail_categories')->insert(['id' => "38",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'4000-4999','eat_detail_tertiertype_name'=>'4A',]);
        DB::table('eat_detail_categories')->insert(['id' => "39",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'400-499','eat_detail_tertiertype_name'=>'3ABC',]);
        DB::table('eat_detail_categories')->insert(['id' => "40",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'400-499','eat_detail_tertiertype_name'=>'3A',]);
        DB::table('eat_detail_categories')->insert(['id' => "41",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'5000-5999','eat_detail_tertiertype_name'=>'Big',]);
        DB::table('eat_detail_categories')->insert(['id' => "42",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'5000-5999','eat_detail_tertiertype_name'=>'Small',]);
        DB::table('eat_detail_categories')->insert(['id' => "43",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'5000-5999','eat_detail_tertiertype_name'=>'4A',]);
        DB::table('eat_detail_categories')->insert(['id' => "44",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'500-599','eat_detail_tertiertype_name'=>'3ABC',]);
        DB::table('eat_detail_categories')->insert(['id' => "45",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'500-599','eat_detail_tertiertype_name'=>'3A',]);
        DB::table('eat_detail_categories')->insert(['id' => "46",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'6000-6999','eat_detail_tertiertype_name'=>'Big',]);
        DB::table('eat_detail_categories')->insert(['id' => "47",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'6000-6999','eat_detail_tertiertype_name'=>'Small',]);
        DB::table('eat_detail_categories')->insert(['id' => "48",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'6000-6999','eat_detail_tertiertype_name'=>'4A',]);
        DB::table('eat_detail_categories')->insert(['id' => "49",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'600-699','eat_detail_tertiertype_name'=>'3ABC',]);
        DB::table('eat_detail_categories')->insert(['id' => "50",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'600-699','eat_detail_tertiertype_name'=>'3A',]);
        DB::table('eat_detail_categories')->insert(['id' => "51",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'7000-7999','eat_detail_tertiertype_name'=>'Big',]);
        DB::table('eat_detail_categories')->insert(['id' => "52",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'7000-7999','eat_detail_tertiertype_name'=>'Small',]);
        DB::table('eat_detail_categories')->insert(['id' => "53",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'7000-7999','eat_detail_tertiertype_name'=>'4A',]);
        DB::table('eat_detail_categories')->insert(['id' => "54",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'700-799','eat_detail_tertiertype_name'=>'3ABC',]);
        DB::table('eat_detail_categories')->insert(['id' => "55",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'700-799','eat_detail_tertiertype_name'=>'3A',]);
        DB::table('eat_detail_categories')->insert(['id' => "56",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'8000-8999','eat_detail_tertiertype_name'=>'Big',]);
        DB::table('eat_detail_categories')->insert(['id' => "57",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'8000-8999','eat_detail_tertiertype_name'=>'Small',]);
        DB::table('eat_detail_categories')->insert(['id' => "58",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'8000-8999','eat_detail_tertiertype_name'=>'4A',]);
        DB::table('eat_detail_categories')->insert(['id' => "59",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'800-899','eat_detail_tertiertype_name'=>'3ABC',]);
        DB::table('eat_detail_categories')->insert(['id' => "60",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'800-899','eat_detail_tertiertype_name'=>'3A',]);
        DB::table('eat_detail_categories')->insert(['id' => "61",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'9000-9999','eat_detail_tertiertype_name'=>'Big',]);
        DB::table('eat_detail_categories')->insert(['id' => "62",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'9000-9999','eat_detail_tertiertype_name'=>'Small',]);
        DB::table('eat_detail_categories')->insert(['id' => "63",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'9000-9999','eat_detail_tertiertype_name'=>'4A',]);
        DB::table('eat_detail_categories')->insert(['id' => "64",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'900-999','eat_detail_tertiertype_name'=>'3ABC',]);
        DB::table('eat_detail_categories')->insert(['id' => "65",'eat_detail_type_name'=>'Group','eat_detail_subtype_name'=>'900-999','eat_detail_tertiertype_name'=>'3A',]);
    }
}

class EatMapReferenceSeeder extends Seeder
{
    public function run()
    {
        DB::table('eat_map_references')->delete();
        DB::table('eat_map_references')->insert(['id' => "1",'user_id'=>"1",'pdc_id'=>"1",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "2",'user_id'=>"1",'pdc_id'=>"2",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "3",'user_id'=>"1",'pdc_id'=>"3",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "4",'user_id'=>"1",'pdc_id'=>"4",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "5",'user_id'=>"1",'pdc_id'=>"5",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "6",'user_id'=>"1",'pdc_id'=>"6",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "7",'user_id'=>"1",'pdc_id'=>"7",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "8",'user_id'=>"1",'pdc_id'=>"8",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "9",'user_id'=>"1",'pdc_id'=>"9",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "10",'user_id'=>"1",'pdc_id'=>"10",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "11",'user_id'=>"1",'pdc_id'=>"11",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "12",'user_id'=>"1",'pdc_id'=>"12",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "13",'user_id'=>"1",'pdc_id'=>"13",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "14",'user_id'=>"1",'pdc_id'=>"14",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "15",'user_id'=>"1",'pdc_id'=>"15",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "16",'user_id'=>"1",'pdc_id'=>"16",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "17",'user_id'=>"1",'pdc_id'=>"17",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "18",'user_id'=>"1",'pdc_id'=>"18",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "19",'user_id'=>"1",'pdc_id'=>"19",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "20",'user_id'=>"1",'pdc_id'=>"20",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "21",'user_id'=>"1",'pdc_id'=>"21",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "22",'user_id'=>"1",'pdc_id'=>"22",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "23",'user_id'=>"1",'pdc_id'=>"23",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "24",'user_id'=>"1",'pdc_id'=>"24",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "25",'user_id'=>"1",'pdc_id'=>"25",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "26",'user_id'=>"1",'pdc_id'=>"26",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "27",'user_id'=>"1",'pdc_id'=>"27",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "28",'user_id'=>"1",'pdc_id'=>"28",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "29",'user_id'=>"1",'pdc_id'=>"29",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "30",'user_id'=>"1",'pdc_id'=>"30",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "31",'user_id'=>"1",'pdc_id'=>"31",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "32",'user_id'=>"1",'pdc_id'=>"32",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "33",'user_id'=>"1",'pdc_id'=>"33",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "34",'user_id'=>"1",'pdc_id'=>"34",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "35",'user_id'=>"1",'pdc_id'=>"35",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "36",'user_id'=>"1",'pdc_id'=>"36",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "37",'user_id'=>"1",'pdc_id'=>"37",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "38",'user_id'=>"1",'pdc_id'=>"38",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "39",'user_id'=>"1",'pdc_id'=>"39",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "40",'user_id'=>"1",'pdc_id'=>"40",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "41",'user_id'=>"1",'pdc_id'=>"41",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "42",'user_id'=>"1",'pdc_id'=>"42",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "43",'user_id'=>"1",'pdc_id'=>"43",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "44",'user_id'=>"1",'pdc_id'=>"44",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "45",'user_id'=>"1",'pdc_id'=>"45",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "46",'user_id'=>"1",'pdc_id'=>"46",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "47",'user_id'=>"1",'pdc_id'=>"47",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "48",'user_id'=>"1",'pdc_id'=>"48",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "49",'user_id'=>"1",'pdc_id'=>"49",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "50",'user_id'=>"1",'pdc_id'=>"50",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "51",'user_id'=>"1",'pdc_id'=>"51",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "52",'user_id'=>"1",'pdc_id'=>"52",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "53",'user_id'=>"1",'pdc_id'=>"53",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "54",'user_id'=>"1",'pdc_id'=>"54",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "55",'user_id'=>"1",'pdc_id'=>"55",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "56",'user_id'=>"1",'pdc_id'=>"56",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "57",'user_id'=>"1",'pdc_id'=>"57",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "58",'user_id'=>"1",'pdc_id'=>"58",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "59",'user_id'=>"1",'pdc_id'=>"59",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "60",'user_id'=>"1",'pdc_id'=>"60",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "61",'user_id'=>"1",'pdc_id'=>"61",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "62",'user_id'=>"1",'pdc_id'=>"62",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "63",'user_id'=>"1",'pdc_id'=>"63",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "64",'user_id'=>"1",'pdc_id'=>"64",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "65",'user_id'=>"1",'pdc_id'=>"65",'value'=>"-1"]);
        DB::table('eat_map_references')->insert(['id' => "66",'user_id'=>"2",'pdc_id'=>"1",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "67",'user_id'=>"2",'pdc_id'=>"2",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "68",'user_id'=>"2",'pdc_id'=>"3",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "69",'user_id'=>"2",'pdc_id'=>"4",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "70",'user_id'=>"2",'pdc_id'=>"5",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "71",'user_id'=>"2",'pdc_id'=>"6",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "72",'user_id'=>"2",'pdc_id'=>"7",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "73",'user_id'=>"2",'pdc_id'=>"8",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "74",'user_id'=>"2",'pdc_id'=>"9",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "75",'user_id'=>"2",'pdc_id'=>"10",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "76",'user_id'=>"2",'pdc_id'=>"11",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "77",'user_id'=>"2",'pdc_id'=>"12",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "78",'user_id'=>"2",'pdc_id'=>"13",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "79",'user_id'=>"2",'pdc_id'=>"14",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "80",'user_id'=>"2",'pdc_id'=>"15",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "81",'user_id'=>"2",'pdc_id'=>"16",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "82",'user_id'=>"2",'pdc_id'=>"17",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "83",'user_id'=>"2",'pdc_id'=>"18",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "84",'user_id'=>"2",'pdc_id'=>"19",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "85",'user_id'=>"2",'pdc_id'=>"20",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "86",'user_id'=>"2",'pdc_id'=>"21",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "87",'user_id'=>"2",'pdc_id'=>"22",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "88",'user_id'=>"2",'pdc_id'=>"23",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "89",'user_id'=>"2",'pdc_id'=>"24",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "90",'user_id'=>"2",'pdc_id'=>"25",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "91",'user_id'=>"2",'pdc_id'=>"26",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "92",'user_id'=>"2",'pdc_id'=>"27",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "93",'user_id'=>"2",'pdc_id'=>"28",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "94",'user_id'=>"2",'pdc_id'=>"29",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "95",'user_id'=>"2",'pdc_id'=>"30",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "96",'user_id'=>"2",'pdc_id'=>"31",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "97",'user_id'=>"2",'pdc_id'=>"32",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "98",'user_id'=>"2",'pdc_id'=>"33",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "99",'user_id'=>"2",'pdc_id'=>"34",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "100",'user_id'=>"2",'pdc_id'=>"35",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "101",'user_id'=>"2",'pdc_id'=>"36",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "102",'user_id'=>"2",'pdc_id'=>"37",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "103",'user_id'=>"2",'pdc_id'=>"38",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "104",'user_id'=>"2",'pdc_id'=>"39",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "105",'user_id'=>"2",'pdc_id'=>"40",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "106",'user_id'=>"2",'pdc_id'=>"41",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "107",'user_id'=>"2",'pdc_id'=>"42",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "108",'user_id'=>"2",'pdc_id'=>"43",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "109",'user_id'=>"2",'pdc_id'=>"44",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "110",'user_id'=>"2",'pdc_id'=>"45",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "111",'user_id'=>"2",'pdc_id'=>"46",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "112",'user_id'=>"2",'pdc_id'=>"47",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "113",'user_id'=>"2",'pdc_id'=>"48",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "114",'user_id'=>"2",'pdc_id'=>"49",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "115",'user_id'=>"2",'pdc_id'=>"50",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "116",'user_id'=>"2",'pdc_id'=>"51",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "117",'user_id'=>"2",'pdc_id'=>"52",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "118",'user_id'=>"2",'pdc_id'=>"53",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "119",'user_id'=>"2",'pdc_id'=>"54",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "120",'user_id'=>"2",'pdc_id'=>"55",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "121",'user_id'=>"2",'pdc_id'=>"56",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "122",'user_id'=>"2",'pdc_id'=>"57",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "123",'user_id'=>"2",'pdc_id'=>"58",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "124",'user_id'=>"2",'pdc_id'=>"59",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "125",'user_id'=>"2",'pdc_id'=>"60",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "126",'user_id'=>"2",'pdc_id'=>"61",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "127",'user_id'=>"2",'pdc_id'=>"62",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "128",'user_id'=>"2",'pdc_id'=>"63",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "129",'user_id'=>"2",'pdc_id'=>"64",'value'=>"0"]);
        DB::table('eat_map_references')->insert(['id' => "130",'user_id'=>"2",'pdc_id'=>"65",'value'=>"0"]);
       
    }
}