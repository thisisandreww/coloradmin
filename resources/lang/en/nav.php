<?php

return [
	'HOME' => 'Home',

	'MASTERPACKAGES' => 'Master Packages',
	'BET_SCHEDULES' => 'Bet Schedules',
	'BET_CUT_OFF' => 'Bet Cut Off',
	'DRAW_RESULT' => 'Draw Result',

	'MEMBER_MANAGEMENT' => 'Member Management'
];

?>