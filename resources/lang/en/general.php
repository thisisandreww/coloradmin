<?php

return [
    'CLOSE'  => 'Close',
    'SAVE'  => 'Save',
    'ID'  => 'Id',
    'NEW'  => 'New',
    'CREATED_DATE'  => 'Created Date',
    'ACTION'  => 'Actions',
    'SUMMMARY'  => 'Summary',
];
