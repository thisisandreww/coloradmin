<?php

return [
    'PACKAGE_TITLE'  => 'Packages',
    'MASTER_PACKAGE_TITLE'  => 'Master Package Management',
    'PACKAGE_NAME'  => 'Package Name',
    'PACKAGE_3D4D'  => '3D/4D',
    'RED_PACKAGE'  => 'Red',
    'PACKAGE_5D6D'  => '5D/6D',
    'PACKAGE_RED_3D4D'  => 'Red 3D4D',
    'PACKAGE_RED_5D6D'  => 'Red 5D6D',
    'SELECT_ALL_PACKAGE'  => 'Select all packages',
    'UNASSIGNED_PACKAGE'  => 'Unassigned',
    'PACKAGE'  => 'package',
    'NO_PACKAGE'  => 'No Package Available',
    'EDIT_PACKAGE'  => 'Edit Package',
    'ALL_PACKAGE'  => 'All Normal Packages',
    'CREATE_PACKAGE'  => 'Create Package',
    'CREATE_MASTER_PACKAGE'  => 'Create Master Package',
    'PACKAGE_DETAIL'  => 'Package Details',
    
    'DEFAULT_VALUE'  => 'Load Default Value',
    'PACKAGE_BACK_TO_MASTER_LIST'  => 'Back To Master List',
    
    'TICKET_PRIZE'  => 'Ticket Prize',
    'COMMISSION'  => 'Commission',
    'FIRST_PRIZE'  => 'First Prize',
    'SECOND_PRIZE'  => 'Second Prize',
    'THIRD_PRIZE'  => 'Third Prize',
    'STARTER_PRIZE'  => 'Starter Prize',
    'CONSOLATION_PRIZE'  => 'Consolation Prize',
];
