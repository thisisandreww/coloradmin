@extends("layouts.userlayout")
@section("headerScript")
<link href="/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/parsley/src/parsley.css" rel="stylesheet" />
<link href="/assets/css/package.css" rel="stylesheet" />
@stop
@section("body")
@include("include.phpVar")

<div>
   <!-- begin page-header -->
   <h1 class="page-header">@lang('package.CREATE_MASTER_PACKAGE') <small>
   @if ($data['redPackage'])
       @lang('package.RED_PACKAGE') &nbsp;
   @endif
   @if ($data['packageType']->package_type_name =='3D/4D')
       @lang('package.PACKAGE_3D4D')&nbsp; 
   @else
       @lang('package.PACKAGE_5D6D') &nbsp;
   @endif
   @lang('package.PACKAGE') &nbsp;
   </small></h1>
   <!-- end page-header -->
   <!-- begin panel -->
   <div class="panel panel-inverse">
      <!-- begin panel-heading -->
      <div class="panel-heading">
         <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
         </div>
         <h4 class="panel-title">@lang('package.PACKAGE_DETAIL')</h4>
      </div>
      <!-- end panel-heading -->
      <!-- begin panel-body -->
      @if (session('error'))
      <div class="alert alert-danger">{{Session::get('error')}}</div>
      @endif
      <form class="form-horizontal" data-parsley-validate="true" name="demo-form" method="POST" action="\packageAdd\{{$data['packageType']->package_type_id}}">
         {{ csrf_field() }}
         <div class="panel-body">
            <div class="form-group m-r-10">
               <input type="text" class="form-control" data-parsley-pattern="^[_A-z0-9]*((-|\s)*[_A-z0-9])*$" data-parsley-required="true" name="package_name" id="package_name" placeholder="Enter Package Name">
            </div>
            @if($data['packageType']->package_type_name =='3D/4D')
            <div class="table-responsive">
               @include("include.table4D")
               <br/><br/><br/>
               @include("include.table4DS")
            </div>
            @elseif($data['packageType']->package_type_name =='5D/6D')
            <div class="table-responsive">
               @include("include.table5D6D")
            </div>
            @endif
         </div>
         <div class="row">&nbsp;</div>
         <!-- end table-responsive -->
         <div class="col-md-12">
            <button id="btnLoadPrimary" class="btn btn_submit btn-warning" style="margin: 5px"><i class="fa fa-share-square"></i> @lang('package.DEFAULT_VALUE')</button>
            <button type="submit" id="btnCreatePackage" class="btn btn_submit btn-primary" style="margin: 5px"><i class="fa fa-save"></i> &nbsp;@lang('general.SAVE')</button>
            <a href="/package/{{$data['packageType']->package_type_id}}" class="btn"><i class="fa fa-arrow-left "></i> @lang('package.PACKAGE_BACK_TO_MASTER_LIST')</a>						
         </div>
         <input type="hidden" name="red_package" value="{{$data['redPackage']}}" />
   </form>
   </div>
</div>
@stop

@section("page_script")
<script src="/assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
	<script src="/assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
	<script src="/assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
	<script src="/assets/plugins/bootstrap-sweetalert/sweetalert.min.js"></script>
	<script src="/assets/plugins/parsley/dist/parsley.js"></script>
	<script>
	
$(document).ready(function() {
	$(".default").css("display", "none");
	$(".reset_btn").click(function() {
        $("#package_3D4D_form :input").val(0);
		$("#package_5D6D_form :input").val(0);
    });
	
	$('#package_3D4D_form').on('submit', function (e)
	{
		var packageName = $("input[name=package_name]").val();
		if (packageName.indexOf("*") >= 0)
		{
			alert("Invalid Name");
			return false;
		}
    });
	
	$('#package_5D6D_form').on('submit', function (e)
	{
		var packageName = $("input[name=package_name]").val();
		if (packageName.indexOf("*") >= 0)
		{
			alert("Invalid Package Name");
			return false;
		}
    });
    
    $(document).on("click", "#btnLoadPrimary", function (e) {
				$('#ticket_prize_5D').val("1");
                $('#ticket_prize_6D').val("1");
                $('#ticket_prize_Big').val("1");
                $('#ticket_prize_Small').val("1");
                $('#ticket_prize_4A').val("1");
                $('#ticket_prize_3A').val("1");
                $('#ticket_prize_3ABC').val("1");
                $('#ticket_prize_4B').val("1");
                $('#ticket_prize_4C').val("1");
                $('#ticket_prize_4D').val("1");
                $('#ticket_prize_4E').val("1");
                $('#ticket_prize_4ABC').val("1");
                $('#ticket_prize_2A').val("1");
                $('#ticket_prize_2ABC').val("1");
				
                $('#commission_5D').val("11");
                $('#commission_6D').val("11");
                $('#commission_Big').val("23");
                $('#commission_Small').val("23");
                $('#commission_4A').val("23");
                $('#commission_3A').val("23");
                $('#commission_3ABC').val("23");
                $('#commission_4B').val("23");
                $('#commission_4C').val("23");
                $('#commission_4D').val("23");
                $('#commission_4E').val("23");
                $('#commission_4ABC').val("23");
                $('#commission_2A').val("23");
                $('#commission_2ABC').val("23");
                
                $('#first_prize_5D').val("16500");
                $('#first_prize_6D').val("110000");
                $('#first_prize_Big').val("2500");
                $('#first_prize_Small').val("3300");
                $('#first_prize_4A').val("6000");
                $('#first_prize_3A').val("600");
                $('#first_prize_3ABC').val("200");
                $('#first_prize_4ABC').val("2000");
                $('#first_prize_2A').val("67");
                $('#first_prize_2ABC').val("22.3");
                
                $('#second_prize_5D').val("5500");
                $('#second_prize_6D').val("3300");
                $('#second_prize_Big').val("1000");
                $('#second_prize_Small').val("2000");
                $('#second_prize_3ABC').val("200");
                $('#second_prize_4B').val("6000");
                $('#second_prize_4ABC').val("2000");
                $('#second_prize_2ABC').val("22.30");
                
                $('#third_prize_5D').val("3300");
                $('#third_prize_6D').val("3300");
                $('#third_prize_Big').val("1000");
                $('#third_prize_Small').val("2000");
                $('#third_prize_3ABC').val("200");
                $('#third_prize_4C').val("6000");
                $('#third_prize_4ABC').val("2000");
                $('#third_prize_2ABC').val("22.30");
                
                $('#special_prize_5D').val("550");
                $('#special_prize_6D').val("330");
                $('#special_prize_Big').val("220");
                $('#special_prize_4D').val("600");
                
                $('#consolation_prize_5D').val("22");
                $('#consolation_prize_6D').val("4.4");
				$('#consolation_prize_Big').val("66");
                $('#consolation_prize_4E').val("600");
                
                $('#sixth_prize_5D').val("5.5");
                
                e.preventDefault();
			});
});
	</script>
@stop