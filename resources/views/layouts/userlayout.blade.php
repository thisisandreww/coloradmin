<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<title>"水为财 | 发财网"</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />

	<script src="https://cdnjs.cloudflare.com/ajax/libs/pace/1.0.2/pace.min.js" integrity="sha256-EPrkNjGEmCWyazb3A/Epj+W7Qm2pB9vnfXw+X6LImPM=" crossorigin="anonymous"></script>

	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="http://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<link href="/assets/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" />
	<link href="/assets/plugins/bootstrap/4.0.0/css/bootstrap.min.css" rel="stylesheet" />
	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.13/css/all.css" integrity="sha384-DNOHZ68U8hZfKXOrtjWvjxusGo9WQnrNx2sqG0tfsghAvtVlRW3tvkXWZh58N9jp" crossorigin="anonymous">
	<link href="/assets/plugins/ionicons/css/ionicons.min.css" rel="stylesheet" />
	<link href="/assets/plugins/flag-icon/css/flag-icon.css" rel="stylesheet" />
	<link href="/assets/plugins/animate/animate.min.css" rel="stylesheet" />
	<link href="/assets/css/apple/style.min.css" rel="stylesheet" />
	<link href="/assets/css/apple/style-responsive.min.css" rel="stylesheet" />
	<link href="/assets/css/apple/theme/default.css" rel="stylesheet" id="theme" />
	<link href="/assets/css/custom.css" rel="stylesheet" id="theme" />
	@yield('headerScript')
	
	<!-- ================== BEGIN BASE JS ================== -->
	<!-- <script src="/assets/plugins/pace/pace.min.js"></script> -->
	<!-- ================== END BASE JS ================== -->
	
	<!-- ================== END BASE CSS STYLE ================== -->

	<style>
	.text-result
	{
		color: white;
	}
</style>
</head>
<body>


	<!-- begin #page-loader -->
	<div id="page-loader" class="fade show"><span class="spinner"></span></div>
	<!-- end #page-loader -->
	
	<!-- begin #page-container -->
	<div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
		<!-- begin #header -->
		<div id="header" class="header navbar-default">
			<!-- begin navbar-header -->
			<div class="navbar-header">
				<a href="/" class="navbar-brand"><span class="navbar-logo"><i class="ion-social-snapchat-outline"></i></span> <b>水为财</b> 发财网</a>
				<button type="button" class="navbar-toggle" data-click="sidebar-toggled">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>
			</div>
			<!-- end navbar-header -->
			
			<!-- begin header-nav -->
			<ul class="navbar-nav navbar-right">
				<li>
				</li>
				<li class="dropdown">
					<a href="javascript:;" data-toggle="dropdown" class="dropdown-toggle icon">
						<i class="ion-ios-bell"></i>
						<span class="label">5</span>
					</a>
					<ul class="dropdown-menu media-list dropdown-menu-right">
						<li class="dropdown-header">NOTIFICATIONS (5)</li>
						<li class="media">
							<a href="javascript:;">
								<div class="media-left">
									<i class="fa fa-bug media-object bg-silver-darker"></i>
								</div>
								<div class="media-body">
									<h6 class="media-heading">Server Error Reports <i class="fa fa-exclamation-circle text-danger"></i></h6>
									<div class="text-muted f-s-11">3 minutes ago</div>
								</div>
							</a>
						</li>
						<li class="media">
							<a href="javascript:;">
								<div class="media-left">
									<img src="../assets/img/user/user-1.jpg" class="media-object" alt="" />
									<i class="fab fa-facebook-messenger text-primary media-object-icon"></i>
								</div>
								<div class="media-body">
									<h6 class="media-heading">John Smith</h6>
									<p>Quisque pulvinar tellus sit amet sem scelerisque tincidunt.</p>
									<div class="text-muted f-s-11">25 minutes ago</div>
								</div>
							</a>
						</li>
						<li class="media">
							<a href="javascript:;">
								<div class="media-left">
									<img src="../assets/img/user/user-2.jpg" class="media-object" alt="" />
									<i class="fab fa-facebook-messenger text-primary media-object-icon"></i>
								</div>
								<div class="media-body">
									<h6 class="media-heading">Olivia</h6>
									<p>Quisque pulvinar tellus sit amet sem scelerisque tincidunt.</p>
									<div class="text-muted f-s-11">35 minutes ago</div>
								</div>
							</a>
						</li>
						<li class="media">
							<a href="javascript:;">
								<div class="media-left">
									<i class="fa fa-plus media-object bg-silver-darker"></i>
								</div>
								<div class="media-body">
									<h6 class="media-heading"> New User Registered</h6>
									<div class="text-muted f-s-11">1 hour ago</div>
								</div>
							</a>
						</li>
						<li class="media">
							<a href="javascript:;">
								<div class="media-left">
									<i class="fa fa-envelope media-object bg-silver-darker"></i>
									<i class="fab fa-google text-warning media-object-icon f-s-14"></i>
								</div>
								<div class="media-body">
									<h6 class="media-heading"> New Email From John</h6>
									<div class="text-muted f-s-11">2 hour ago</div>
								</div>
							</a>
						</li>
						<li class="dropdown-footer text-center">
							<a href="javascript:;">View more</a>
						</li>
					</ul>
				</li>
				<li class="dropdown navbar-language">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
						@if(App::getLocale() == "en")
						<span class="flag-icon flag-icon-us" title="us"></span>
						<span class="name">EN</span> <b class="caret"></b>
						@else
						<span class="flag-icon flag-icon-cn" title="us"></span>
						<span class="name">CN</span> <b class="caret"></b>
						@endif
					</a>
					<ul class="dropdown-menu p-b-0">
						<li class="arrow"></li>
						<li><a href="javascript:;" class="lang" data-lang="en"><span class="flag-icon flag-icon-us" title="us"></span> English</a></li>
						<li><a href="javascript:;" class="lang" data-lang="cn"><span class="flag-icon flag-icon-cn" title="cn"></span> Chinese</a></li>
					</ul>
				</li>
				<li class="dropdown navbar-user">
					<a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown">
						<span class="d-none d-md-inline">{{ Session::get('loginUsername') }}</span> <b class="caret"></b>
					</a>
					<div class="dropdown-menu dropdown-menu-right">
						<a href="\editMemberDetail\{{Session::get('loginUserId')}}\{{Session::get('loginUserId')}}" class="dropdown-item">Edit Profile</a>
						<div class="dropdown-divider"></div>
						<a href="\logout" class="dropdown-item">Log Out</a>
					</div>
				</li>
			</ul>
			<!-- end header navigation right -->
		</div>
		<!-- end #header -->
		
		<!-- begin #sidebar -->
		<div id="sidebar" class="sidebar">
			<!-- begin sidebar scrollbar -->
			<div data-scrollbar="true" data-height="100%">
				<!-- begin sidebar user -->
				<ul class="nav">
					<li class="nav-profile">
						<a href="javascript:;" data-toggle="nav-profile">
							<div class="cover with-shadow"></div>
							<div class="info">
								<b class="caret"></b>
								{{ Session::get('loginUsername') }}
								<small>{{ Session::get('loginUserRoleName') }}</small>
							</div>
						</a>
					</li>
					<li>
						<ul class="nav nav-profile">
							<li><a href="\editMemberDetail\{{Session::get('loginUserId')}}\{{Session::get('loginUserId')}}"><i class="ion-ios-cog-outline"></i> Edit Profile</a></li>
							<li><a href="\editEatMap"><i class="ion-ios-infinite-outline"></i> Edit Fight Limit</a></li>
							<li><a href="javascript:;"><i class="ion-ios-help-outline"></i> Helps</a></li>
						</ul>
					</li>
				</ul>
				<!-- end sidebar user -->
				<!-- begin sidebar nav -->
				<ul class="nav">
					<li class="nav-header">Navigation</li>
					<li class="active">
						<a href="/index">
							<i class="ion-ios-pulse-strong"></i>
							<span>@lang('nav.HOME')</span>
						</a>
					</li>
					<li class="nav-header">System Utilities</li>
					<li class="has-sub">
						<a href="javascript:;">
							<b class="caret"></b>
							<i class="fa fa-cubes bg-pink"></i> 
							<span>@lang('nav.MASTERPACKAGES')</span>
						</a>
						<ul class="sub-menu">
							<li><a href="\package\1">3D/4D/4D Special</a></li>
							<li><a href="\package\2">5D/6D</a></li>
							<li><a href="\package\3">Red 3D/4D/4D Special</a></li>
							<li><a href="\package\4">Red 5D/6D</a></li>
						</ul>						
					</li>

					<li><a href="/betschedule"><i class="fa fa-calendar bg-pink"></i> <span>Bet Schedules</span></a></li>
					<li><a href="/bet/cuttoff"><i class="fa fa-clock bg-pink"></i> <span>Bet / Void Cut Off</span></a></li>
					<li><a href="{{ action('MemberController@index_betlimit') }}"><i class="fas fa-tachometer-alt bg-pink"></i><span>Bet Limit</span></a></li>
					<li><a href="{{ action('MemberController@index_rednumberrestriction') }}"><i class="fa fa-sliders-h bg-pink"></i>Red Number Restriction</a></li>
					<li>
						<a href="/drawresult">
							<i class="fa fa-copy bg-pink"></i> <span>Draw Result</span></a></li>
						</a>
					</li>
					<li><a href="/autoTicketing"><i class="fa fa-ticket-alt bg-pink"></i> <span>Auto Ticketing</span></a></li>
					<li class="nav-header">Membership</li>
					<li class="has-sub">
						<a href="javascript:;">
							<b class="caret"></b>
							<i class="ion-ios-color-filter-outline bg-purple"></i>
							<span>Member Management</span> 
						</a>
						<ul class="sub-menu">
							<li><a href="\memberlist\{{ Session::get('loginUserId') }}">Member List</a></li>
							<li><a href="\subAccount">Sub Account</a></li>
							<li><a href="\resetPassword">Reset Password</a></li>
						</ul>
					</li>
					<li class="nav-header">Bettings</li>					
					<li class="has-sub">
						<a href="javascript:;">
							<b class="caret"></b>
							<i class="ion-ios-grid-view-outline bg-green"></i>
							<span>3D/4D Bettings</span></span> 
						</a>
						<ul class="sub-menu">
							<li><a href="\bet\bet3d4d">Bet 3D/4D</a></li>
							<li><a href="form_plugins.html">Express Bet 3D/4D</a></li>
							<li><a href="form_slider_switcher.html">Upload Bet 3D/4D</a></li>
						</ul>
					</li>
					<li class="has-sub">
						<a href="javascript:;">
							<b class="caret"></b>
							<i class="ion-ios-grid-view-outline bg-green"></i>
							<span>5D/6D Bettings</span></span> 
						</a>
						<ul class="sub-menu">
							<li><a href="\bet\bet5d6d">Bet 5D/6D</a></li>
							<li><a href="form_plugins.html">Express Bet 5D/6D</a></li>
							<li><a href="form_slider_switcher.html">Upload Bet 5D/6D</a></li>
						</ul>
					</li>
					<li class="has-sub">
						<a href="javascript:;">
							<b class="caret"></b>
							<i class="ion-ios-grid-view-outline bg-green"></i>
							<span>4D Special Bettings</span></span> 
						</a>
						<ul class="sub-menu">
							<li><a href="{{ action('BetController@bet4dspecial') }}">Bet 4D Special</a></li>
							<li><a href="form_plugins.html">Express Bet 4D Special</a></li>
							<li><a href="form_slider_switcher.html">Upload Bet 4D Special</a></li>
						</ul>
					</li>
					<li class="has-sub">
						<a href="javascript:;">
							<b class="caret"></b>
							<i class="ion-ios-grid-view-outline bg-green"></i>
							<span>Red Number Bettings</span></span> 
						</a>
						<ul class="sub-menu">
							<li><a href="form_elements.html">Bet Red Number</a></li>
							<li><a href="form_plugins.html">Express Bet Red Number</a></li>
							<li><a href="form_slider_switcher.html">Upload Bet Red Number</a></li>
						</ul>
					</li>
					<li class="nav-header">Inbox</li>
					<li>
						<a href="{{ action('ReportController@bettingRecords') }}">
							<i class="fa fa-clipboard bg-blue-darker"></i>
							<span>Betting Record</span>
						</a>
						<a href="{{ action('InboxController@viewVoidInvoice') }}">
							<i class="fa fa-receipt bg-blue-darker"></i>
							<span>View / Void Invoice</span>
						</a>
						<a href="javascript:;">
							<i class="fas fa-sync-alt bg-blue-darker"></i>
							<span>Manage Transfer</span>
						</a>
					</li>
					<li class="nav-header">Reports</li>
					<li class="has-sub">
						<a href="javascript:;">
							<b class="caret"></b>
							<i class="ion-ios-infinite-outline bg-gradient-aqua"></i> 
							<span>All Reports</span>
						</a>
						<ul class="sub-menu">
							<li><a href="/report/winLossSimple/{{Session::get('loginUserId')}}" target="_blank">Win Lose Report (Simple)</a></li>
							<li><a href="/report/winLossDetail/{{Session::get('loginUserId')}}" target="_blank">Win Lose Report (Details)</a></li>
							<li><a href="/report/totalReport/{{Session::get('loginUserId')}}" target="_blank">Total Report</a></li>
							<li><a href="/report/bet/3" target="_blank">Bet Report</a></li>
							<li class="has-sub">
								<a href="javascript:;">
									<b class="caret"></b>
									Eat Maps
								</a>
								<ul class="sub-menu">									
									<li><a href="/report/eatMap/{{Session::get('loginUserId')}}/0">3D/4D Eat Map</a></li>
									<li><a href="/report/eatMap/{{Session::get('loginUserId')}}/1">4D Special Eat Map</a></li>
									<li><a href="/report/eatMap/{{Session::get('loginUserId')}}/2">5D/6D Eat Map</a></li>
								</ul>
							</li>
						</ul>
					</li>

					<!-- begin sidebar minify button -->
					<li><a href="javascript:;" class="sidebar-minify-btn" data-click="sidebar-minify"><i class="ion-ios-arrow-left"></i> <span>Collapse</span></a></li>
					<!-- end sidebar minify button -->
				</ul>
				<!-- end sidebar nav -->
			</div>
			<!-- end sidebar scrollbar -->
		</div>
		<div class="sidebar-bg"></div>
		<!-- end #sidebar -->

		<!-- begin #content -->
		<div id="content" class="content">
			<!-- begin page-header -->
			<h1 class="page-header">@yield("title") <small>@yield("header")</small></h1>
			<!-- end page-header -->

			@yield("body")
		</div>
		<!-- end #content -->

		<!-- begin scroll to top btn -->
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
		<!-- end scroll to top btn -->
	</div>
	<!-- end page container -->


	<!-- ================== BEGIN BASE JS ================== -->

	<script src="/assets/plugins/jquery/jquery-3.2.1.min.js"></script>
	<script src="/assets/plugins/jquery-ui/jquery-ui.min.js"></script>
	<script src="/assets/plugins/bootstrap/4.0.0/js/bootstrap.bundle.min.js"></script>
	<!--[if lt IE 9]>
		<script src="/assets/crossbrowserjs/html5shiv.js"></script>
		<script src="/assets/crossbrowserjs/respond.min.js"></script>
		<script src="/assets/crossbrowserjs/excanvas.min.js"></script>
	<![endif]-->
	<script src="/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="/assets/plugins/js-cookie/js.cookie.js"></script>
	<script src="/assets/js/theme/apple.min.js"></script>
	<script src="/assets/js/apps.min.js"></script>
	<!-- ================== END BASE JS ================== -->

	@yield('page_script')

	<script>
		$(document).ready(function () {

			function activateClass(){
				var url = window.location.pathname;
				url = url.split('/');
				if(url[1] == ""){
					url[1] = "home";
				}
				$("." + url[1]).addClass("active");
			};

			var locale = "{{ App::getLocale() }}";
			$("#selected-lang").text(locale);

			$(".lang").click(function(){
				var lang = $(this).attr("data-lang");
				var url = "/lang/" + lang;
				$.get(url, function(){
					location.reload();
				});
			});

			activateClass();

			App.init();
			
		});
	</script>
</body>
</html>