@extends("layouts.userlayout")
@section("title", "Win Loss Report (Detail)")
@section("header", "Win Loss Report (Detail)")

@section('headerScript')
<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
<link href="/assets/plugins/bootstrap-eonasdan-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
<link href="/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/parsley/src/parsley.css" rel="stylesheet" />
@stop

@section("body")
<!-- begin panel -->
<div class="panel panel-inverse" >
    <!-- begin panel-heading -->
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        </div>
        <h4 class="panel-title">Win Loss</h4>
    </div>
    <!-- end panel-heading -->
    <!-- begin panel-body -->
    <div class="panel-body">
        @if (session('success'))
        <div class="alert alert-success" style="white-space:pre-wrap;">{{Session::get('success')}}</div>
        @endif
        @if (session('error'))
        <div class="alert alert-danger" style="white-space:pre-wrap;">{{Session::get('error')}}</div>
        @endif
        <form class="form-horizontal" id="winLossForm" method="get" action="\report\winLossDetail\{{$user->id}}" data-parsley-validate="true">
            <div class="form-group row m-b-15">
                <label class="col-md-2 col-form-label">Report Date Ranges</label>
                <div class="col-md-4">
                    <div class="input-group" id="searchRange">
                        <input type="text" name="searchRange" class="form-control" value="<?php if(app('request')->has('searchRange'))
                            {
                                echo app('request')->input('searchRange');
                            }
                            ?>" data-parsley-required="true" placeholder="click to select the date range" />
                        <span class="input-group-append">
                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-1">
                    <div class="checkbox checkbox-css m-b-20">
                        <label>3D/4D</label>
                        <input type="checkbox" id="enable3D4D" name="enable3D4D" value="1" <?php if(app('request')->has('enable3D4D')){echo "checked";} ?> />
                        <label for="enable3D4D"> </label>
                    </div>
                </div>
                <div class="col-md-1">
                    <div class="checkbox checkbox-css m-b-20">
                        <label>5D/6D</label>
                        <input type="checkbox" id="enable5D6D" name="enable5D6D" value="1" <?php if(app('request')->has('enable5D6D')){echo "checked";} ?> />
                        <label for="enable5D6D"> </label>
                    </div>
                </div>
            </div>
            <div class="form-group m-b-15 ro">
                <div class="col-md-3">
                    <button type="submit" id="searchBtn" class="btn btn-primary">Search</button>
                </div>
            </div>
            <br />
            <?php 
                $totalTurnOver = 0;
                $downlineTotalSales = 0;
                $downlineTotalComm = 0;
                $downlineTotalBal = 0;
                $totalSelfEat = 0;
                $totalSelfEatComm = 0;
                $totalAmountPassToUpline = 0;
                $totalCommEarn = 0;
                ?>
            <div class="form-group m-b-15">
                <div class="col-md-12">
                    <table id="reportTable" class="table table-striped table-bordered" width="100%">
                        <thead>
                            <tr>
                                <th class="text-nowrap" rowspan="2">Name</th>
                                <th class="text-nowrap" rowspan="2">Username</th>
                                <th class="text-nowrap" rowspan="2">Player Turnover</th>
                                <th class="text-nowrap" colspan="4">Downline</th>
                                <th class="text-nowrap" colspan="4">{{$user->username}}</th>
                            </tr>
                            <tr>
                                <th class="text-nowrap">Sales</th>
                                <th class="text-nowrap">Comm</th>
                                <th class="text-nowrap">Payout</th>
                                <th class="text-nowrap">Bal.</th>
                                <th class="text-nowrap">Sales</th>
                                <th class="text-nowrap">Comm</th>
                                <th class="text-nowrap">Payout</th>
                                <th class="text-nowrap">Profit</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if ($record != null)
                            @foreach($record as $ea)
                            <?php 
                                $totalTurnOver += $ea->totalBet;
                                $downlineTotalSales += $ea->downlineSales;
                                $downlineTotalComm += $ea->downlineComm;
                                $downlineTotalBal += ($downlineTotalSales - $downlineTotalComm);
                                $totalSelfEat +=$ea->selfEatTotal;
                                $totalSelfEatComm +=$ea->selfEatCommTotal;
                                $totalAmountPassToUpline += ($ea->downlineSales - $ea->selfEatTotal);
                                $totalCommEarn += $ea->commEarn;
                                ?>
                            <tr>
                                <td>{{$ea->name}}</td>
                                <td><a href="javascript:;" onclick="navigate({{$ea->id}})" >{{$ea->username}}</a></td>
                                <td>{{$ea->totalBet}}</td>
                                <td>{{$ea->downlineSales}}</td>
                                <td>{{$ea->downlineComm}}</td>
                                <td>0.00</td>
                                <td><?php echo $ea->downlineSales - $ea->downlineComm?></td>
                                <td>{{$ea->selfEatTotal}}</td>
                                <td>{{$ea->selfEatCommTotal}}</td>
                                <td>0.00</td>
                                <td><?php echo $ea->selfEatTotal - $ea->selfEatCommTotal; ?></td>
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                    </table>
                </div>
            </div>
            <div class="form-group m-b-15">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-bordered m-b-0">
                            <thead>
                                <tr>
                                    <th rowspan="2">Total bet</th>
                                    <th colspan="3">Downline</th>
                                    <th rowspan="2">Downline Total Collect</th>
                                    <th colspan="3">{{$user->username}}</th>
                                    <th colspan="4">Upline</th>
                                    <th rowspan="4">Profit</th>
                                </tr>
                                <tr>
                                    <th>Sales</th>
                                    <th>Comm</th>
                                    <th>Payout</th>
                                    <th>Sales</th>
                                    <th>Comm</th>
                                    <th>Payout</th>
                                    x
                                    <th>Sales</th>
                                    <th>Comm</th>
                                    <th>Payout</th>
                                    <th>Total</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>{{$totalTurnOver}}</td>
                                    <td>{{$downlineTotalSales}}</td>
                                    <td>{{$downlineTotalComm}}</td>
                                    <td>0.00</td>
                                    <td>{{$downlineTotalBal}}</td>
                                    <td>{{$totalSelfEat}}</td>
                                    <td>{{$totalSelfEatComm}}</td>
                                    <td>0.00</td>
                                    <td>{{$totalAmountPassToUpline}}</td>
                                    <td>{{$totalCommEarn}}</td>
                                    <td>0,.00</td>
                                    <td><?php echo $totalAmountPassToUpline - $totalCommEarn; ?></td>
                                    <td><?php echo $downlineTotalBal - $totalAmountPassToUpline + $totalCommEarn; ?></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <!-- end panel-body -->
</div>
<!-- end panel -->
@stop

@section("page_script")
<script src="/assets/plugins/moment/moment.min.js"></script>
<script src="/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="/assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
	<script src="/assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
	<script src="/assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script src="/assets/plugins/parsley/dist/parsley.js"></script>
    <script>
    $(document).ready(function() 
    {
        $('#reportTable').DataTable({
            dom: 'lBfrtip',
            buttons: [
                { extend: 'csv', className: 'btn-sm' },
                { extend: 'excel', className: 'btn-sm' },
                { extend: 'pdf', className: 'btn-sm' },
                { extend: 'print', className: 'btn-sm' }
            ],
            responsive: true,
            autoFill: true,
            colReorder: true,
            keys: true,
            rowReorder: true,
            select: true
        });
        
        $('#searchRange').daterangepicker({
        opens: 'right',
        format: 'YYYY/MM/DD',
        separator: ' to ',
        startDate: <?php if (isset($start)){ echo "'".$start."'";}else{echo "moment()";} ?>,
        endDate: <?php if (isset($end)){echo "'".$end."'";}else{echo "moment()";} ?>,
        minDate: '01/01/2012',
        maxDate: '12/31/2018',
    },
    function (start, end) {
        $('#searchRange input').val(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
    });
    });
    function navigate($id)
    {
        window.location.href = "/report/winLossDetail/"+$id+"?"+window.location.search.substring(1);
    }
    </script>
@stop