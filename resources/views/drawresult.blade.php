@extends("layouts.userlayout")
@section("title", "Draw Results")
@section("header", "Update draw results and process the results")

@section('headerScript')
<link href="/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css" rel="stylesheet" />
<link href="/assets/plugins/parsley/src/parsley.css" rel="stylesheet" />
@stop

@section("body")
<!-- begin panel -->
<div class="panel panel-inverse" >
	<!-- begin panel-heading -->
	<div class="panel-heading">
		<div class="panel-heading-btn">
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
		</div>
		<h4 class="panel-title">Draw Result Details</h4>
	</div>
	<!-- end panel-heading -->
	<!-- begin panel-body -->
	
	<div class="panel-body">
		@if (session('message'))
		<div class="{{session('class')}}">{{session('message')}}</div>
		@endif
		<!-- form-control-sm -->
		{!! Form::open(['url' => '/drawresult/manageresult', 'class' => 'form-horizontal', 'data-parsley-validate' => 'true']) !!}

		<div class="row">		
			<div class="col-md-4">
				<div class="form-group">
					<label>Enter Draw Date (yyyyMMdd)</label>
					<input type="text" autocomplete="off" class="form-control width-full m-b-10"  id="txtNewDate" name="txtNewDate" data-parsley-required='true' />
				</div>
			</div>
			<div class="col-md-4">
				<label>Load Result</label>
				<button type="button" class="btn btn-primary width-full m-b-10" name="btnLoad" id="btnLoad">Load Result</button>
			</div>
			<div class="col-md-4">
				<label>Retrieve Result From Website</label>
				<button type="submit" class="btn btn-primary width-full m-b-10" id="btnRetrieve">Retrieve Result From Website</button>
			</div>
		</div>
		<div class="row">&nbsp;</div>
		<div class="tab-overflow">
			<ul class="nav nav-tabs" style="width:100%;">
				<li class="nav-item prev-button"><a href="javascript:;" data-click="prev-tab" class="nav-link text-success"><i class="fa fa-arrow-left"></i></a></li>
				<li class="nav-item"><a href="#nav-tab-1" data-toggle="tab" onclick="clicktab('mg')" class="nav-link active"> <img src="../assets/img/lotto/bet-logo-magnum.png" style="width: 20px; height: 20px;"> &nbsp; Magnum</a></li>
				<li class="nav-item"><a href="#nav-tab-2" data-toggle="tab" onclick="clicktab('kd')" class="nav-link"><img src="../assets/img/lotto/bet-logo-damacai-inversed.png" style="width: 20px"> &nbsp;Kuda</a></li>
				<li class="nav-item"><a href="#nav-tab-3" data-toggle="tab" onclick="clicktab('tt')" class="nav-link"><img src="../assets/img/lotto/bet-logo-toto-inversed.png" style="width: 20px"> &nbsp; Toto</a></li>
				<li class="nav-item"><a href="#nav-tab-4" data-toggle="tab" onclick="clicktab('sg')" class="nav-link"><img src="../assets/img/lotto/bet-logo-singapore-pools.png" style="width: 20px"> &nbsp; Singapore</a></li>
				<li class="nav-item"><a href="#nav-tab-5" data-toggle="tab" onclick="clicktab('sb')" class="nav-link"><img src="../assets/img/lotto/bet-logo-big-cash.png" style="width: 20px"> &nbsp; Sandakan</a></li>
				<li class="nav-item"><a href="#nav-tab-6" data-toggle="tab" onclick="clicktab('stc')" class="nav-link"><img src="../assets/img/lotto/bet-logo-sabah88.png" style="width: 20px"> &nbsp; Sabah</a></li>
				<li class="nav-item"><a href="#nav-tab-7" data-toggle="tab" onclick="clicktab('sw')" class="nav-link"><img src="../assets/img/lotto/bet-logo-stc.png" style="width: 20px"> &nbsp; Sarawak</a></li>
				<li class="nav-item"><a href="#nav-tab-8" data-toggle="tab" onclick="clicktab('gd')" class="nav-link"><img src="../assets/img/lotto/bet-logo-gdlotto.png" style="width: 20px"> &nbsp; Grand Dragon</a></li>
				<!-- <li class="nav-item next-button"><a href="javascript:;" data-click="next-tab" class="nav-link text-success"><i class="fa fa-arrow-right"></i></a></li> -->
			</ul>
		</div>
		<div class="tab-content">
			<!-- begin tab-pane -->
			<div class="row">&nbsp;</div>
			<?php 
			$alltabs = array('mg','kd','tt','sg','sb','stc','sw','gd');
			$allimages
			= array(
				"../assets/img/lotto/bet-logo-magnum.png",
				"../assets/img/lotto/bet-logo-damacai-inversed.png",
				"../assets/img/lotto/bet-logo-toto-inversed.png",
				"../assets/img/lotto/bet-logo-singapore-pools.png",
				"../assets/img/lotto/bet-logo-big-cash.png",
				"../assets/img/lotto/bet-logo-sabah88.png",
				"../assets/img/lotto/bet-logo-stc.png",
				"../assets/img/lotto/bet-logo-gdlotto.png"
			);			         
			?>
			@foreach($alltabs as $key=>$a)
			<div class="tab-pane fade {{ $key == 0? 'active show': ''  }} " id="nav-tab-{{ $key+1 }}">
				<!--end spacing-->
				<table id="data-table-default" class="table table-striped table-bordered" style="margin-bottom:0px;">
					<thead>
						<tr>
							<th width="1%" colspan="5" style="text-align: center"><img src="{{ $allimages[$key] }}" width="100px"></th>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td align="center" colspan="2">First Prize</td>
							<td align="center" colspan="2">Second Prize</td>
							<td align="center" colspan="1">Third Prize</td>
						</tr>
						<tr style="margin-bottom:0px;">
							<td colspan="2"><input type="text" id="txt1{{ $a }}" name="txt1{{ $a }}" data-parsley-type="digits" class="form-control resulttext" maxlength="4" /></td>
							<td colspan="2"><input type="text" id="txt2{{ $a }}" name="txt2{{ $a }}" data-parsley-type="digits" class="form-control resulttext" maxlength="4"/></td>
							<td colspan="1"><input type="text" id="txt3{{ $a }}" name="txt3{{ $a }}" data-parsley-type="digits" class="form-control resulttext" maxlength="4"/></td>
						</tr>
					</table>
					<table id="data-table-default" class="table table-striped table-bordered">
						<tr>
							<td align="center" colspan="5">Starter</td>
						</tr>
						<tr>
							<td ><input type="text" id="txts1{{ $a }}" name="txts1{{ $a }}" data-parsley-type="digits" class="form-control resulttext" maxlength="4"/></td>
							<td><input type="text" id="txts2{{ $a }}" name="txts2{{ $a }}" data-parsley-type="digits" class="form-control resulttext" maxlength="4"/></td>
							<td><input type="text" id="txts3{{ $a }}" name="txts3{{ $a }}" data-parsley-type="digits" class="form-control resulttext" maxlength="4"/></td>
							<td><input type="text" id="txts4{{ $a }}" name="txts4{{ $a }}" data-parsley-type="digits" class="form-control resulttext" maxlength="4"/></td>
							<td><input type="text" id="txts5{{ $a }}" name="txts5{{ $a }}" data-parsley-type="digits" class="form-control resulttext" maxlength="4"/></td>
						</tr>
						<tr>
							<td><input type="text" id="txts6{{ $a }}" name="txts6{{ $a }}" data-parsley-type="digits" class="form-control resulttext" maxlength="4"/></td>
							<td><input type="text" id="txts7{{ $a }}" name="txts7{{ $a }}" data-parsley-type="digits" class="form-control resulttext" maxlength="4"/></td>
							<td><input type="text" id="txts8{{ $a }}" name="txts8{{ $a }}" data-parsley-type="digits" class="form-control resulttext" maxlength="4"/></td>
							<td><input type="text" id="txts9{{ $a }}" name="txts9{{ $a }}" data-parsley-type="digits" class="form-control resulttext" maxlength="4"/></td>
							<td><input type="text" id="txts10{{ $a }}" name="txts10{{ $a }}" data-parsley-type="digits" class="form-control resulttext" maxlength="4"/></td>
						</tr>
						<tr>
							<td align="center" colspan="5">Consolation</td>
						</tr>
						<tr>
							<td ><input type="text" id="txtc1{{ $a }}" name="txtc1{{ $a }}" data-parsley-type="digits" class="form-control resulttext" maxlength="4"/></td>
							<td><input type="text" id="txtc2{{ $a }}" name="txtc2{{ $a }}" data-parsley-type="digits" class="form-control resulttext" maxlength="4"/></td>
							<td><input type="text" id="txtc3{{ $a }}" name="txtc3{{ $a }}" data-parsley-type="digits" class="form-control resulttext" maxlength="4"/></td>
							<td><input type="text" id="txtc4{{ $a }}" name="txtc4{{ $a }}" data-parsley-type="digits" class="form-control resulttext" maxlength="4"/></td>
							<td><input type="text" id="txtc5{{ $a }}" name="txtc5{{ $a }}" data-parsley-type="digits" class="form-control resulttext" maxlength="4"/></td>
						</tr>
						<tr>
							<td><input type="text" id="txtc6{{ $a }}" name="txtc6{{ $a }}" data-parsley-type="digits" class="form-control resulttext" maxlength="4"/></td>
							<td><input type="text" id="txtc7{{ $a }}" name="txtc7{{ $a }}" data-parsley-type="digits" class="form-control resulttext" maxlength="4"/></td>
							<td><input type="text" id="txtc8{{ $a }}" name="txtc8{{ $a }}" data-parsley-type="digits" class="form-control resulttext" maxlength="4"/></td>
							<td><input type="text" id="txtc9{{ $a }}" name="txtc9{{ $a }}" data-parsley-type="digits" class="form-control resulttext" maxlength="4"/></td>
							<td><input type="text" id="txtc10{{ $a }}" name="txtc10{{ $a }}" data-parsley-type="digits" class="form-control resulttext" maxlength="4"/></td>
						</tr>
						@if($a == 'tt')
						<tr>
							<td align="center" colspan="5">5D</td>
						</tr>
						<tr>
							<td>1st</td>
							<td colspan="4"><input type="text" id="txt5d1{{ $a }}" name="txt5d1{{ $a }}" data-parsley-type="digits" class="form-control resulttext" maxlength="5"/></td>
						</tr>
						<tr>
							<td>2nd</td>
							<td colspan="4"><input type="text" id="txt5d2{{ $a }}" name="txt5d2{{ $a }}" data-parsley-type="digits" class="form-control resulttext" maxlength="5"/></td>
						</tr>
						<tr>
							<td>3rd</td>
							<td colspan="4"><input type="text" id="txt5d3{{ $a }}" name="txt5d3{{ $a }}" data-parsley-type="digits" class="form-control resulttext" maxlength="5"/></td>
						</tr>
						<tr>
							<td align="center" colspan="5">6D</td>
						</tr>
						<tr>
							<td>1st</td>
							<td colspan="4"><input type="text" id="txt6d1{{ $a }}" name="txt6d1{{ $a }}" data-parsley-type="digits" class="form-control resulttext" maxlength="6"/></td>
						</tr>
						@endif
					</table>
				</div>
				<!-- end tab-pane -->
				@endforeach

			</div>  
			<div class="col-md-12">
				<input type="text" hidden id="update_id" name="update_id" value="mg" />
				<input type="submit" id="btnSubmit" class="btn btn_submit btn btn-primary" name='btnSubmit' value="Save Result" />
			</div>
			{!! Form::close() !!}
		</div>
		<!-- end panel-body -->
	</div>
	<!-- end panel -->	

	@stop

	@section("page_script")
	<script src="/assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
	<script src="/assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
	<script src="/assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
	<script src="/assets/plugins/bootstrap-sweetalert/sweetalert.min.js"></script>
	<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script src="/assets/plugins/parsley/dist/parsley.js"></script>
	<script src="/js/ddlHelper.js"></script>
	<!-- ================== END PAGE LEVEL JS ================== -->

	<script>
		$(document).ready(function() {
			COLOR_GREEN = '#5AC8FA';
			$('#txtNewDate').datepicker({
				todayHighlight: true,
				orientation: 'bottom',
				weekStart: 1,
				format: 'yyyymmdd',
				endDate: new Date()
			});

			$("#btnLoad").click(function(){
				var date = $('#txtNewDate').val();
				if(date != ""){
					getResult(date);
				}
				$(this).blur();
			});

			$("#btnRetrieve").click(function(){
				$(this).closest("form").submit();
			});
		});

		function renderScreen(results)
		{
			var cars = ["mg", "kd", "tt", "sg", "sb", "stc", "sw", "gd"];
			var controls = $(".resulttext");
			for (i=1; i<8; i++)
			{
				var arrayLength = results.length;
				for (var i = 0; i < arrayLength; i++) {
					var controlTargeted = "txt" + results[i].type + cars[results[i].poolid - 1];
					console.log(controlTargeted);
					$('#'+controlTargeted).val(results[i].number);
				}
			}
		}

		function getResult(date)
		{
			$.ajax({
				url: "/getallresult/" + date,
				type: "get",
				success: function(response)
				{ 
					if(response.length != 0){
						renderScreen(response);	
					} else {
						alert("No result found!");
					}
				},
				error: function(response)
				{
					console.log(response);
				}
			});
		}


		function clicktab(star)
		{
			$('#update_id').val(star);
		}

		@if(session('date'))
		$("#txtNewDate").val("{{ session('date') }}");
		var date = "{{ session('date') }}";
		getResult(date);
		@endif
	</script>
	@stop