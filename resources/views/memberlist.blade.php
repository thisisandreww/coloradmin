@extends("layouts.userlayout")
@include("include.phpVar")
@section("title", "Member Management")
@section("header", "Member List")

@section("headerScript")
<link href="/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/parsley/src/parsley.css" rel="stylesheet" />
@stop
@section("body")

<!-- begin panel -->
<div class="panel panel-inverse">
	<!-- begin panel-heading -->
	<div class="panel-heading">
		<div class="panel-heading-btn">
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
		</div>
		<h4 class="panel-title">@lang('user.MEMBER_LIST_IND'): {{ $data['user']->username }}</h4>
	</div>
	<!-- end panel-heading -->
	<!-- begin panel-body -->
	<div class="panel-body">
        @if (session('error'))
        <div class="alert alert-danger">{{Session::get('error')}}</div>
        @endif
		<table id="data-table-default" class="table table-striped table-bordered">
			<thead>
				<tr>
					<th width="1%">No</th>
					<th class="text-nowrap">@lang('user.USERNAME')</th>
					<th class="text-nowrap">@lang('user.NICKNAME')</th>
					<th class="text-nowrap">@lang('user.LEVEL')</th>
					<th class="text-nowrap">@lang('user.CREDIT_LIMIT')</th>
					<th class="text-nowrap">@lang('user.BALANCE')</th>
					<th class="text-nowrap">@lang('user.STATUS')</th>
					<th class="text-nowrap all">@lang('general.ACTION')</th>
				</tr>
			</thead>
			<tbody>
            <?php $count = 0; ?>
                @if(count($data['childMember']) > 0)
                    @foreach($data['childMember'] as $child)
				<tr class="odd gradeX">
					<td width="1%" class="f-s-600 text-inverse"><?php echo ++$count; ?></td>
					<td>@if ($child->downline_create)
            <a href="\memberlist\{{$child->id}}" disabled>{{$child->username}}</a><input type="hidden" name="downlineId" value="{{ $child->id }}"/>
            @else
            {{$child->username}}
            @endif</td>
					<td>{{$child->name}}</td>
					<td>{{$child->role_name}}</td>
					<td>{{$child->credit_limit}}</td>
					<td>1000</td>
					<td>
                        @if ($child->state == 0)
                            Active
                        @elseif ($child->state == 1)
                            Inactive
                        @else
                            Suspended
                        @endif
                    </td>
					<td class="all"><button class='btn btn-primary btn-circle btn-icon edit' data-rowid="{{ $data['user']->id }}" data-rowchildid="{{$child->id}}"><i class='fa fa-edit'></i></button>&nbsp;
                        <button data-rowid="{{ $child->id }}" data-rowname="{{ $child->name }}" class='btn btn-warning btn-circle btn-icon delete'><i class='fa fa-minus-circle'></i></button>&nbsp;
                        <button data-rowid="{{ $child->id }}" data-rowname="{{ $child->name }}" class='btn btn-danger btn-circle btn-icon suspend'><i class='fa fa-lock'></i></button></td>
				</tr>
                @endforeach
                @endif
			</tbody>
		</table>
		<div class="col-md-12">
			<input type="text" hidden id="delete_id" name="delete_id" />
            @if ($data['user']->downline_create && 
                 $data['loginUser']->downline_create &&  
                 $data['loginUser']->state == 0 &&
                 $data['user']->role_name!="System" &&
                 count($data['roleOption']) > 0)
			<a id="btnCreateCategories" class="btn_submit btn btn-primary" style="margin: 10px" href="\addMemberDetail\{{ $data['user']->id }}"><i class="fa fa-plus-circle"></i> @lang('user.CREATE_MEMBER')</a>
		    @endif
            @if ($data['user']->id != $data['loginUser']->id)
            <a href="/memberlist/{{$data['user']->user_upline}}" class="btn"><i class="fa fa-arrow-left "></i> @lang('user.BACK_TO_PARENT')</a>
        @endif
        </div>
        <input type="hidden" value="{{$data['user']->id}}" id="id"/>
	</div>
	<!-- end panel-body -->
</div>
<!-- end panel -->			

		
		
@stop
@section("page_script")
<script src="/assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="/assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
<script src="/assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script src="/assets/plugins/bootstrap-sweetalert/sweetalert.min.js"></script>
<script src="/assets/plugins/parsley/dist/parsley.js"></script>
<script>
   $(document).ready(function() {
        $('#data-table-default').DataTable( {
           responsive: true
        });
         $(document).on("click", ".edit", function (e) {
            location.href = "/editMemberDetail/"+$(this).data("rowid")+"/"+$(this).data("rowchildid");
			e.preventDefault();
		});
    
			$(document).on("click", ".delete", function (e) {
				swal({
					title: 'Inactive Confirmation',
					text: 'Are you sure you want to Inactive this member?',
					icon: 'warning',
					buttons: {
						delete: {
							text: 'Cancel',
							value: false,
							visible: true,
							className: 'btn btn-primary',
							closeModal: true,
						},
						cancel: {
							text: 'Confirm',
							value: true,
							visible: true,
							className: 'btn btn-danger',
							closeModal: true,
						}
						}
				}).then((value) => {
                    if (value)
                    {
                        $.ajax({
                            url: "/updateMemberState/"+$(this).data("rowid")+"/1",
                            type: 'POST',
                            data: {_token: '{{csrf_token()}}'},
                            success: function (data) {
                                location.href = "/memberlist/{{$data['user']->id}}";
                            }
                        });
                    }
                });
				e.preventDefault();
			});
			
			$(document).on("click", ".suspend", function (e) {
				swal({
					title: 'Suspend Confirmation',
					text: 'Are you sure you want to suspend '+$(this).data("rowname")+'?',
					icon: 'warning',
					buttons: {
						delete: {
							text: 'Cancel',
							value: false,
							visible: true,
							className: 'btn btn-primary',
							closeModal: true,
						},
						cancel: {
							text: 'Confirm',
							value: true,
							visible: true,
							className: 'btn btn-danger',
							closeModal: true,
						}
						}
				}).then((value) => {
                     if (value)
                     {
                        $.ajax({
                            url: "/updateMemberState/"+$(this).data("rowid")+"/2",
                            type: 'POST',
                            data: {_token: '{{csrf_token()}}'},
                            success: function (data) {
                                location.href = "/memberlist/{{$data['user']->id}}";
                            }
                        });
                     }
                });
				e.preventDefault();
			});    
});
   	
</script>
@stop