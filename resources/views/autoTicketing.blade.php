@extends("layouts.userlayout")
@section("title", "Auto Ticketing Records")
@section("header", "Auto Ticketing Records")

@section('headerScript')
<link href="/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css" rel="stylesheet" />
<link href="/assets/plugins/parsley/src/parsley.css" rel="stylesheet" />
@stop

@section("body")
@if(session('msg'))
<div class="alert {{ session('class') }}">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{!! session('msg') !!}
</div>
@endif

<div class="panel panel-inverse">
	<div class="panel-heading">
		<div class="panel-heading-btn">
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
		</div>
		<h4 class="panel-title">Auto Ticketing Records List</h4>
	</div>

	<div id="divMsg" style="display: none" class="alert alert-danger"></div>
	
	<div class="panel-body">
		<div class="table-responsive">
			<table id="data-table-default" class="table table-bordered table-striped table-valign-middle f-s-14 m-b-20">
				<thead>
					<tr>
						<th></th>
						<th>Currency</th>
						<th>Type</th>
						<th>Bet Format</th>
						<th>Draw Format</th>
						<th>Amount</th>
						<th>Bets</th>
						<th>Action</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					@if($autotickets != NULL)
					@foreach($autotickets AS $index => $autoticket)
					<tr>
						<td>{{ $index+1 }}</th>
						<td>{{$autoticket->getTicket->currency}}</td>
						<td>{{$autoticket->getTicket->type}}</td>
						<td>{{$autoticket->getTicket->bet_method}}</td>
						<td>{{$autoticket->getTicket->draw_format}}</td>
						<td>{{$autoticket->getTicket->initial_amount}}</td>
						<td><a title="{{$autoticket->ticketid}}" data-rowname="{{$autoticket->ticketid}}" data-rownid="{{ $index+1 }}" class="viewBets btn btn-link" style="padding: 0; color: #007aff;"><i class="fas fa-eye"></i> View Bets</a></td>
						<td>
							<a href="javascript:;" class='btn btn-danger btn-icon btn-circle sweet_pop_delete_confirm' data-rowname="{{$autoticket->id}}" data-rownid="{{ $index+1 }}" ><i class='fa fa-trash'></i></a>
						</td>
						<td class="status">
							@if($autoticket->status == 0)
								<label class="switch">
									<input type="checkbox" class="cbStatus" data-rowname="{{$autoticket->id}}" data-rownid="{{ $index+1 }}">
									<span class="slider round"></span>
								</label>
							@else
							<label class="switch">
									<input type="checkbox" class="cbStatus" data-rowname="{{$autoticket->id}}" data-rownid="{{ $index+1 }}" checked>
									<span class="slider round"></span>
								</label>
							@endif
						</td>
					</tr>
					@endforeach
					@endif
				</tbody>
			</table>
		</div>
	</div>
</div>

<!-- The Modal -->
<div class="modal fade bd-example-modal-lg" id="betsModal" tabindex="-1" role="dialog" aria-labelledby="Bank Deposit" aria-hidden="true">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h3 class="modal-title" id="modalTicketID">Ticket: </h3>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body" style="text-align: center;">
				<div class="table-responsive">
					<table id="bets-table" class="table table-bordered table-striped table-valign-middle f-s-14 m-b-20">
						<thead>
							<th></th>
							<th>Pool</th>
							<th>Number</th>
							<th>Big</th>
							<th>Small</th>
							<th>4A</th>
							<th>4B</th>
							<th>4C</th>
							<th>4D</th>
							<th>4E</th>
							<th>4ABC</th>
							<th>3A</th>
							<th>3ABC</th>
							<th>2A</th>
							<th>2ABC</th>
							<th>5D</th>
							<th>6D</th>
							<th>Total</th>
						</thead>
						<tbody>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
@stop

@section("page_script")
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<script src="/assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="/assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
<script src="/assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script src="/assets/plugins/bootstrap-sweetalert/sweetalert.min.js"></script>
<script src="/assets/plugins/parsley/dist/parsley.js"></script>

<script type="text/javascript">
	$(function(){
		$('#data-table-default').DataTable( {

        });

        $('#bets-table').DataTable( {
     
        });

		$('#data-table-default').on('click', '.sweet_pop_delete_confirm', function(e)
		{	
			var row_id =  $(this).data("rownid");
			var ticketid =  $(this).data("rowname");
			var table = $("#data-table-default tbody");
			var table_content;
			swal({
				title: 'Delete Auto Ticketing',
				text: 'Delete Auto Ticketing ID: ' + $(this).data("rownid"),
				icon: 'warning',
				buttons: {
					confirm: {
						text: 'Delete',
						value: true,
						visible: true,
						className: 'btn btn-danger',
						closeModal: true,
					},
					cancel: {
						text: 'Cancel',
						value: false,
						visible: true,
						className: 'btn btn-default',
						closeModal: true,
					}
				}
			}).then((value) => {
				if (value)
				{
					$.ajax({
						url: "/autoTicketing/delete/"+$(this).data("rowname"),
						type: 'POST',
						data: {_token: '{{csrf_token()}}'},
						success: function (data) 
						{
							if (data.error != null)
							{
								swal("Error", data.error, "error");
								location.reload();
							}
							else
							{
								var response = data.success;
								table_content = "";
								swal("Success", "Auto Ticketing: " + row_id + " is deleted successfully!", "success");
								for (var i in response) {
									table_content += '<tr>';
									table_content += '<td>'+ (parseInt(i)+1) + '</td>';
									table_content += '<td>' + response[i].get_ticket.currency + '</td>';
									table_content += '<td>' + response[i].get_ticket.type + '</td>';
									table_content += '<td>' + response[i].get_ticket.bet_method+ '</td>';
									table_content += '<td>' + response[i].get_ticket.draw_format+ '</td>';
									table_content += '<td>' + response[i].get_ticket.initial_amount + '</td>';
									table_content += '<td><a title="' + response[i].id +  '" class="viewBets btn btn-link" style="padding: 0; color: #007aff;"><i class="fas fa-eye"></i> View Bets</a></td>';
									table_content += '<td><a href="javascript:;" class="btn btn-danger btn-icon btn-circle sweet_pop_delete_confirm" data-rowname="' + response[i].id +  '" data-rownid="' + i+1 +  '" ><i class="fa fa-trash"></i></a></td>';
										
									table_content += '<td class="status">';
									if (response[i].status == 0) {
										table_content += '<label class="switch">';
										table_content += '<input type="checkbox" class="cbStatus" data-rownid="' + response[i].id +  '">';
										table_content += '<span class="slider round"></span>';
										table_content += '</label>';
									}
									else {
										table_content += '<label class="switch">';
										table_content += '<input type="checkbox" class="cbStatus" data-rownid="' + response[i].id + '" checked>';
										table_content += '<span class="slider round"></span>';
										table_content += '</label>';
									}
									table_content += '</td>';
									table_content += '</tr>';
								}

								table.html(table_content);
							}
						}
					});
				}
			});
		}); //end delete function

		$('.cbStatus').change(function() {
			var status;
			var row_id =  $(this).data("rownid");

        	if(this.checked) {
        	    status = 1;
        	}
        	else {
        		status = 0;
        	}      

			$.ajax({
				url: "/autoTicketing/change_status/" + $(this).data("rowname") + "/" + status,
				type: 'POST',
				data: {_token: '{{csrf_token()}}'},
				success: function (data) 
				{
					if (data.error != null)
					{
						swal("Error", data.error, "error");
						location.reload();
					}
					else
					{
						var response = data.success;
						swal("Success", "Auto Ticketing: " + row_id + " status is updated successfully!", "success");  
					}
				}
			});
    	});
	}); //end change status

	// Get the modal
	$('#data-table-default').on('click', '.viewBets', function(e) {
		var tablebody = $("#bets-table tbody");
		var rowid = $(this).data("rownid");
		var ticketid = $(this).data("rowname");
		var body_content = "";
		var total_amount = 0;
		var total_big = 0;
		var total_small = 0;
		var total_4a = 0;
		var total_4b= 0;
		var total_4c = 0;
		var total_4d = 0;
		var total_4e = 0;
		var total_4abc= 0;
		var total_3a = 0;
		var total_3abc = 0;
		var total_2a = 0;
		var total_2abc = 0;
		var total_5d = 0;
		var total_6d = 0;

		$.ajax({
			url: "/getBetStages/" + ticketid,
			type: 'GET',
			success: function (data) 
			{
				if (data.error != null)
				{
					swal("Error", data.error, "error");
					location.reload();
				}
				else
				{
					var response = data.success;

					for (var i in response) {
						var row_total = parseInt(response[i].betamountbig) + parseInt(response[i].betamountsmall) + parseInt(response[i].betamount4a) +
						parseInt(response[i].betamount4b) + parseInt(response[i].betamount4c) + parseInt(response[i].betamount4d) + parseInt(response[i].betamount4e) +
						parseInt(response[i].betamount4abc) + parseInt(response[i].betamount3a) +parseInt(response[i].betamount3abc) + parseInt(response[i].betamount2a) +
						parseInt(response[i].betamount2abc +  parseInt(response[i].betamount5d) +  parseInt(response[i].betamount6d)); 

						body_content += '<tr>';
						body_content += '<td>' + (parseInt(i)+1) + '</td>';
						body_content += '<td>' + response[i].get_type.name + '</td>';
						body_content += '<td>' + response[i].number + '</td>';
						body_content += '<td>' + response[i].betamountbig + '</td>';
						body_content += '<td>' + response[i].betamountsmall + '</td>';
						body_content += '<td>' + response[i].betamount4a + '</td>';
						body_content += '<td>' + response[i].betamount4b + '</td>';
						body_content += '<td>' + response[i].betamount4c + '</td>';
						body_content += '<td>' + response[i].betamount4d + '</td>';
						body_content += '<td>' + response[i].betamount4e + '</td>';
						body_content += '<td>' + response[i].betamount4abc + '</td>';
						body_content += '<td>' + response[i].betamount3a + '</td>';
						body_content += '<td>' + response[i].betamount3abc + '</td>';
						body_content += '<td>' + response[i].betamount2a + '</td>';
						body_content += '<td>' + response[i].betamount2abc + '</td>';
						body_content += '<td>' + response[i].betamount5d + '</td>';
						body_content += '<td>' + response[i].betamount6d + '</td>';
						body_content += '<td>' + row_total.toFixed(2) + '</td>';
						body_content += '</tr>';
						
						total_amount += row_total ;
						total_big += parseInt(response[i].betamountbig);
						total_small += parseInt(response[i].betamountsmall);
						total_4a += parseInt(response[i].betamount4a);
						total_4b += parseInt(response[i].betamount4b);
						total_4c += parseInt(response[i].betamount4c);
						total_4d += parseInt(response[i].betamount4d);
						total_4e += parseInt(response[i].betamount4e);
						total_4abc += parseInt(response[i].betamount4abc);
						total_3a += parseInt(response[i].betamount3a);
						total_3abc += parseInt(response[i].betamount3abc);
						total_2a += parseInt(response[i].betamount2a);
						total_2abc += parseInt(response[i].betamount2abc);
						total_5d += parseInt(response[i].betamount5d);
						total_6d += parseInt(response[i].betamount6d);
					}

					body_content += '<tr>';
					body_content += '<td></td>';
					body_content += '<td></td>';
					body_content += '<td></td>';
					body_content += '<th>' + total_big.toFixed(2) + '</th>';
					body_content += '<th>' + total_small.toFixed(2) + '</th>';
					body_content += '<th>' + total_4a.toFixed(2) + '</th>';
					body_content += '<th>' + total_4b.toFixed(2) + '</th>';
					body_content += '<th>' + total_4c.toFixed(2) + '</th>';
					body_content += '<th>' + total_4d.toFixed(2) + '</th>';
					body_content += '<th>' + total_4e.toFixed(2) + '</th>';
					body_content += '<th>' + total_4abc.toFixed(2) + '</th>';
					body_content += '<th>' + total_3a.toFixed(2) + '</th>';
					body_content += '<th>' + total_3abc.toFixed(2) + '</th>';
					body_content += '<th>' + total_2a.toFixed(2) + '</th>';
					body_content += '<th>' + total_2abc.toFixed(2) + '</th>';
					body_content += '<th>' + total_5d.toFixed(2) + '</th>';
					body_content += '<th>' + total_6d.toFixed(2) + '</th>';
					body_content += '<th>' + total_amount.toFixed(2) + '</th>';
					body_content += '</tr>';

					tablebody.html(body_content);
				}
			}
		});

		$('#modalTicketID').html("Ticket: " + rowid);
		$('#betsModal').modal('show');
	});
</script>
@stop