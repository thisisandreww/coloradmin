@extends("layouts.userlayout")
@section("title", "Bet Form")
@section("header", "Bet 3D/4D")

@section('headerScript')
<link href="/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css" rel="stylesheet" />
<link href="/assets/plugins/parsley/src/parsley.css" rel="stylesheet" />
@stop

@section("body")

{!! Form::open(['url' => '/bet/makeBet3d4d', 'id' => 'betForm', 'name' => 'betForm', 'class' => 'form-horizontal']) !!}
<!-- begin panel -->
<div class="panel panel-inverse" >
	<!-- begin panel-heading -->
	<div class="panel-heading">
		<div class="panel-heading-btn">
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
		</div>
		<h4 class="panel-title">Bet Form 3D/4D</h4>
	</div>
	<!-- end panel-heading -->
	<!-- begin panel-body -->
	@if (session('message'))
	<div class="{{session('class')}}">{{session('message')}}</div>
	@endif
	
	<div id="divMsg"></div>

	<div id="divTicket" style="display: none" class="alert alert-success">
		<span id="spanTicket"></span>
	</div>

	<div class="panel-body" >
	<!-- 	@if (session('message_ticket'))
		<div class="{{session('class_ticket')}}">{!! session('message_ticket') !!}</div>
		@endif -->

		<div class="row row-space-10">
			<div class="col-md-4">
				<div class="form-group m-b-10" data-parsley-validate="true">
					<label for="exampleInputEmail1">Bet Agent</label>
					<select class="form-control" name="betAgent">
						@foreach($data['downlines'] as $d)
						<option>{{ $d }}</option>               		
						@endforeach
					</select>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group m-b-10">
					<label for="exampleInputPassword1">Credit Left</label>
					<input type="text" class="form-control" name="txtCreditLeft" value="{{ $data['me']->credit_limit }}" readonly /> 
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group m-b-10">
					<label for="exampleInputPassword1">Telephone (SMS)</label>
					<input type="text" class="form-control inputTelephone" name="txtTelephone" value="" placeholder="Enter Phone (0102345678)" />
				</div>
			</div>
		</div>
		<div class="row row-space-10">
			<div class="col-md-8">
				<div class="form-group m-b-10" data-parsley-validate="true">
					<label for="exampleInputEmail1">Betting for Draw Dates</label>
					<div class="checkbox checkbox-css m-b-20">
<!-- 						@foreach($data['schedules'] as $s)
						<div class="checkbox checkbox-css checkbox-inline">
							<input type="hidden" name={{ "dd_".$s->drawdate  }} value="0" />
							<input type="checkbox" class="dd" name={{ "dd_".$s->drawdate  }} id={{ "dd_".$s->drawdate  }} />
							<label for={{ "dd_".$s->drawdate  }}>{{ $s->day }}</label>
						</div>
						@endforeach   -->    

						<?php 
						$now = \Carbon\Carbon::now()->format('Y-m-d');
						$days = 7;
						$current_day = \Carbon\Carbon::createFromFormat("Y-m-d", $now)->format("l");

						switch ($current_day) {
							case "Monday":
							$days = $days + 2;
							break;
							case "Tuesday":
							$days = $days + 1;
							break;
							case "Thursday":
							$days = $days + 2;
							break;
							case "Friday":
							$days = $days + 1;
							break;
						}

						for ($x = 0; $x <= $days; $x++) {?>  
							<div class="checkbox checkbox-css checkbox-inline">
							<input type="hidden" name="dd_<?php echo str_replace("-", "",$now) ?>" value="0" />
							<input type="checkbox" class="dd" name="dd_<?php echo str_replace("-", "",$now) ?>" id="dd_<?php echo str_replace("-", "",$now) ?>" />
							<label for="dd_<?php echo str_replace("-", "",$now) ?>"><?php echo \Carbon\Carbon::createFromFormat("Y-m-d", $now)->format("l")?></label>
							</div>
						<?php $now = \Carbon\Carbon::parse($now)->addDays(1)->format("Y-m-d"); } ?>                            
					</div>
				</div>
			</div> 
			<div class="col-md-4">
				<div class="form-group m-b-10">
					<label for="exampleInputPassword1">Remark</label>
					<input type="text" class="form-control inputRemark" name="txtRemark" value=""  /> 
				</div>
			</div>        
		</div>
		<div class="row row-space-10">
			<div class="col-md-2">
				<div class="alert alert-success fade show m-b-10" id="divBig">
					<b>Big</b>&nbsp;:&nbsp;RM <span id="spanBig"></span>
				</div>			
			</div>
			<div class="col-md-2">
				<div class="alert alert-success fade show m-b-10">
					<b>Small</b>&nbsp;:&nbsp;RM <span id="spanSmall"></span>
				</div>				
			</div>
			<div class="col-md-2">
				<div class="alert alert-success fade show m-b-10">
					<b>4A</b>&nbsp;:&nbsp;RM <span id="span4A"></span>
				</div>					
			</div>
			<div class="col-md-2">
				<div class="alert alert-success fade show m-b-10">
					<b>3A</b>&nbsp;:&nbsp;RM <span id="span3A"></span>
				</div>				
			</div>
			<div class="col-md-2">
				<div class="alert alert-success fade show m-b-10">
					<b>3ABC</b>&nbsp;:&nbsp;RM <span id="span3ABC"></span>
				</div>				
			</div>
			<div class="col-md-2">
				<div class="alert alert-success fade show m-b-10">
					<b>Grand Total</b>&nbsp;:&nbsp;RM <span id="spanTotal"></span>
					<input type="hidden" name="hidTotal" id="hidTotal" />
				</div>				
			</div>
		</div>
		<div class="row row-space-10">
			<div class="col-md-12">
				<div class="checkbox checkbox-css" style="margin-bottom: 8px;">
					<input type="checkbox" id="cbAutoTicket" name="autoTicketStatus" />
  					<label for="cbAutoTicket">Auto Ticketing</label>
  				</div>
				<button id="btnSubmit" type="submit" class="btn_submit btn btn-primary" style="margin-bottom: 10px; margin-right: 5px;"><i class="fa fa-save"></i> Submit Bet</button> <button id="btnClear" type="button" class="btn_clear btn btn-primary" style="margin-bottom: 10px"><i class="fa fa-eraser"></i> Clear</button> <img src="/loader.gif" class="submitLoader" />
			</div>
		</div>
		<div class="table-responsive">
			<table id="data-table-default" class="table table-striped table-bordered table-width-fix table-valign-middle">
				<thead>
					<tr>
						<td width="5%">No</td>
						<td>Number</td>
						<td>Type</td>
						<td>Big</td>
						<td>Small</td>
						<td>4A</td>
						<td>3A</td>
						<td>3ABC</td>
						@foreach($data['pools'] as $pool)
						<td><img src="{{ $pool['images'] }}" class="allpools" id="{{ 'img_'.$pool['shortname'] }}" style="width: 20px; height: 20px;"></td>		
						@endforeach
						<td width="10%">&nbsp;</td>
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>
							<div class="checkbox checkbox-css">
								<input type="checkbox" id="selectAll_Big" class="selectAll" />
								<label for="selectAll_Big"> </label>
							</div>
						</td>
						<td>
							<div class="checkbox checkbox-css">
								<input type="checkbox" id="selectAll_Small" class="selectAll" />
								<label for="selectAll_Small"> </label>
							</div>
						</td>
						<td>
							<div class="checkbox checkbox-css">
								<input type="checkbox" id="selectAll_4A"  class="selectAll" />
								<label for="selectAll_4A"> </label>
							</div>
						</td>
						<td>
							<div class="checkbox checkbox-css">
								<input type="checkbox" id="selectAll_3A" class="selectAll" />
								<label for="selectAll_3A"> </label>
							</div>
						</td>
						<td>
							<div class="checkbox checkbox-css">
								<input type="checkbox" id="selectAll_3ABC" class="selectAll" />
								<label for="selectAll_3ABC"> </label>
							</div>
						</td>
						@foreach($data['pools'] as $pool)
						<td>
							<div class="checkbox checkbox-css">
								<input type="checkbox" id="{{ 'selectAll_'.$pool->shortname }}" class="selectPoolAll" />
								<label for="{{ 'selectAll_'.$pool->shortname }}"> </label>
							</div>
						</td>		
						@endforeach
						<td>&nbsp;</td>
					</tr>
					<?php 
					for($count = 1; $count <=30; $count++)
					{
						?>
						<tr>
							<td><input type="text" readonly id='txt_no_<?php echo $count ?>' name='txt_no_<?php echo $count ?>' data-parsley-type="number" class="form-control eatmap_input m_input" value="<?php echo old('txt_no_'.$count, $count) ?>"/></td>
							<td><input type="text" id='txt_number_<?php echo $count ?>' name='txt_number_<?php echo $count ?>' data-parsley-type="number" class="form-control drawnumber_input m_input" value="<?php echo old('txt_number_'.$count) ?>"/></td>
							<td><input type="text" readonly id='txt_type_<?php echo $count ?>' name='txt_type_<?php echo $count ?>' class="form-control eatmap_input" value="<?php echo old('txt_type_'.$count) ?>"/></td>
							<td><input type="text" id='txt_Big_<?php echo $count ?>' name='txt_Big_<?php echo $count ?>' data-parsley-type="number" class="form-control input_Big m_input" value="<?php echo old('txt_Big_'.$count) ?>"/></td>
							<td><input type="text" id='txt_Small_<?php echo $count ?>' name='txt_Small_<?php echo $count ?>' data-parsley-type="number" class="form-control input_Small m_input" value="<?php echo old('txt_Small_'.$count) ?>"/></td>
							<td><input type="text" id='txt_4A_<?php echo $count ?>' name='txt_4A_<?php echo $count ?>' data-parsley-type="number" class="form-control input_4A m_input" value="<?php echo old('txt_4A_'.$count) ?>"/></td>
							<td><input type="text" id='txt_3A_<?php echo $count ?>' name='txt_3A_<?php echo $count ?>' data-parsley-type="number" class="form-control input_3A m_input" value="<?php echo old('txt_3A_'.$count) ?>"/></td>
							<td><input type="text" id='txt_3ABC_<?php echo $count ?>' name='txt_3ABC_<?php echo $count ?>' data-parsley-type="number" class="form-control input_3ABC m_input" value="<?php echo old('txt_3ABC_'.$count) ?>"/></td>
							@foreach($data['pools'] as $pool)
							<td>
								<div class="checkbox checkbox-css">
									<input type="hidden" name="{{ 'select_'.$pool->shortname.'_'.$count }}" value="0" />
									<input type="checkbox" class="chkpools" name="{{ 'select_'.$pool->shortname.'_'.$count }}" id="{{ 'select_'.$pool->shortname.'_'.$count }}" />
									<label for="{{ 'select_'.$pool->shortname.'_'.$count }}"> </label>
								</div>
							</td>		
							@endforeach
							<td>
								<select class="form-control bt selBetType"  id="{{ 'selBetType'.$count }}" name="{{ 'selBetType'.$count }}">
									<option></option>
									<option>Box</option>
									<option>IBox</option>
									<option>P-H</option>
									<option>P-B</option>
								</select>
							</td>
						</tr>
						<?php 
					}
					?>
				</tbody>
			</table>	
		</div>	
		
		
		<!-- end panel-body -->
	</div>
	<!-- end panel -->
	{!! Form::close() !!}
	@stop

	@section("page_script")
	<script src="/assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
	<script src="/assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
	<script src="/assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
	<script src="/assets/plugins/bootstrap-sweetalert/sweetalert.min.js"></script>
	<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script src="/assets/plugins/parsley/dist/parsley.js"></script>
	<script src="/js/ddlHelper.js"></script>
	<!-- ================== END PAGE LEVEL JS ================== -->
	<script>
		$(".submitLoader").hide();
		$(document).ready(function() {
			$('.m_input').keypress(function (e) {
				var regex = new RegExp("^[0-9|.]+$");
				str = $(this).attr('id');

				if (str.indexOf("txt_number_") >= 0)
				{
					regex = new RegExp("^[0-9]+$");
				}
				
				var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
				if (regex.test(str)) {
					return true;
				}
				
				e.preventDefault();
				return false;
			});

			$(".dd").on('change', function () {  recalculate(); });
			$(".chkpools").on('change', function () {  recalculate(); });
			$(".m_input").on('change', function () {  recalculate(); });
			$(".bt").on('change', function () {  recalculate(); });

			$('#betForm').on('submit', function(e){
				recalculate();
				clearError();
				var entry_found = false;
		    var outstr = ""; // Blank if there is no error. Else, it's non-blank.

		    if (getDateCount() == 0)
		    {
		    	outstr = "Please select a date.";
		    }
		    
		    for (var i = 0; i < 30; ++i) {
		    	var o = isValidRow(i);
		    	if (o.is_valid && o._number != "") {
		    		entry_found = true;
		    	} else if (o.is_valid) {
		            // Do nothing, as bet number is blank. This entry would be ignored.
		        } else {
		        	outstr = "This betting line is incomplete / invalid: " + (i).toString();
		        	break;
		        }
		    }
		    if (outstr != "") {
		    	showError(outstr);
		    	e.preventDefault();
		    } else if (!entry_found) {
		    	showError("No bets found.");
		    	e.preventDefault();
		    }
		    else
		    {
		    	$("#divTicket").hide();
				// assuming all entries are valid now
				// try to use ajax to submit betting
				$.ajax({
					type: 'POST',
					data: $('#betForm').serialize(),
					url: '/bet/makeBet3d4d',
					beforeSend: function() {
                    	$("#btnSubmit").prop( "disabled", true ); //Disable
                    	$(".submitLoader").show();
                	},
					success: function(data) {
						$("#btnSubmit").prop( "disabled", false ); //Enable
                    	$(".submitLoader").hide();

						if (data.success == false)
						{
							showError(data.message);
						}
						else
						{
							console.log(data);
							$("#spanTicket").html(data.ticket);
							$("#divTicket").show();
							clearAllInputs();
						}						
					}
				});
				
				e.preventDefault();
			} 
			
		});
		});

		function getString(s) {
			if (!s) s = "";
			s = s.trim();
			return s;
		}


    // this is to check if the row is with correct entries
    function isValidRow(row_id) {
        /*
        The followings would return FALSE
        Number:
         - contains non-numeric characters.
         - length is neither 3 nor 4.
         - IF NUMBER'S LENGTH IS 0, IT'S CONSIDERED TRUE.
        Amount:
         - contains non-numeric characters.
         - fails float-parse (e.g., contains 2 dots).
         - blank amount is equivalent to 0. this does not fail.
         - if number's length is 3, any non-zero value for big, small and 4a would fail out.
         */
         var retobj = {};
         retobj.is_valid = true;
         retobj._number = "";
         retobj._amtstr_big = "";
         retobj._amtstr_small = "";
         retobj._amtstr_4a = "";
         retobj._amtstr_3a = "";
         retobj._amtstr_3c = "";
         retobj._amtfloat_big = 0;
         retobj._amtfloat_small = 0;
         retobj._amtfloat_4a = 0;
         retobj._amtfloat_3a = 0;
         retobj._amtfloat_3c = 0;

         var s = row_id.toString();
         var num = getString($("#txt_number_" + s).val());
         if (num == "") return retobj;

         retobj._number = num;

         if($("#txt_Big_" + s).is("[readonly]") == false){
         	retobj._amtstr_big = getString($("#txt_Big_" + s).val());
         }

         if($("#txt_Small_" + s).is("[readonly]") == false){
         	retobj._amtstr_small = getString($("#txt_Small_" + s).val());
         }

         if($("#txt_4A_" + s).is("[readonly]") == false){
         	retobj._amtstr_4a = getString($("#txt_4A_" + s).val());
         }


        // retobj._amtstr_big = getString($("#txt_Big_" + s).val());
        // retobj._amtstr_small = getString($("#txt_Small_" + s).val());
        // retobj._amtstr_4a = getString($("#txt_4A_" + s).val());
        retobj._amtstr_3a = getString($("#txt_3A_" + s).val());
        retobj._amtstr_3c = getString($("#txt_3ABC_" + s).val());

        if (!/^\d{3,4}$/.test(retobj._number)) retobj.is_valid = false;
        else if (!/^\d*(\.?\d+)?$/.test(retobj._amtstr_big)) retobj.is_valid = false;
        else if (!/^\d*(\.?\d+)?$/.test(retobj._amtstr_small)) retobj.is_valid = false;
        else if (!/^\d*(\.?\d+)?$/.test(retobj._amtstr_4a)) retobj.is_valid = false;
        else if (!/^\d*(\.?\d+)?$/.test(retobj._amtstr_3a)) retobj.is_valid = false;
        else if (!/^\d*(\.?\d+)?$/.test(retobj._amtstr_3c)) retobj.is_valid = false;

        if (retobj.is_valid) {
        	if (retobj._amtstr_big == "") retobj._amtstr_big = "0";
        	if (retobj._amtstr_small == "") retobj._amtstr_small = "0";
        	if (retobj._amtstr_4a == "") retobj._amtstr_4a = "0";
        	if (retobj._amtstr_3a == "") retobj._amtstr_3a = "0";
        	if (retobj._amtstr_3c == "") retobj._amtstr_3c = "0";

        	retobj._amtfloat_big = parseFloat(retobj._amtstr_big);
        	retobj._amtfloat_small = parseFloat(retobj._amtstr_small);
        	retobj._amtfloat_4a = parseFloat(retobj._amtstr_4a);
        	retobj._amtfloat_3a = parseFloat(retobj._amtstr_3a);
        	retobj._amtfloat_3c = parseFloat(retobj._amtstr_3c);

        	var total_4d = retobj._amtfloat_big + retobj._amtfloat_small + retobj._amtfloat_4a;
        	var total_3d = retobj._amtfloat_3a + retobj._amtfloat_3c;
        	
        	if (retobj._number.length == 3 && total_4d > 0) retobj.is_valid = false;
        	else if (retobj._number.length == 3 && total_3d == 0) retobj.is_valid == false;
        	else if (retobj._number.length == 4 && total_3d + total_4d == 0) retobj.is_valid = false;

        	if (total_4d + total_3d == 0)retobj.is_valid = false;
        }

        return retobj;
    }

    // Get how many days being selected
    function getDateCount()
    {
    	count = 0;
    	$(".dd").each(function() {
    		if ($(this).is(":checked"))
    		{
    			count = count + 1;
    		}
    	});
    	return count;
    }

    // Get how many pools being selected for the row
    function getPoolsSelectedCount(rowId)
    {
    	count = 0;
    	$(".allpools").each(function() {
    		id = $(this).attr('id');
    		targetid = id.replace('img', 'select') + '_' + rowId;

    		if ($('#'+targetid).is(":checked"))
    		{
    			count = count + 1;
    		}
    	});
    	return count;
    }

    // Recalculate the number amount of the betting forms
    function recalculate()
    {
    	var total_big = 0;
    	var total_small = 0;
    	var total_4a = 0;
    	var total_3a = 0;
    	var total_3abc = 0;
    	var total_amount = 0;
    	
    	for (var i = 0; i < 30; ++i) {

        	// change the row id to string
        	var s = i.toString();

            // get total how many pools being selected
            var pools = getPoolsSelectedCount(i);

            // get total how many days being selected
            var days = getDateCount();

            var o = isValidRow(i); 

            var timesfactors = 1;
            selected_type = $('#selBetType'+i).val();
            if (selected_type == '' || selected_type == 'IBox')
            {
            	timesfactors = pools * days;
            }
            else if (selected_type == "P-H" || selected_type == "P-B") {
            	timesfactors = pools * days * 10;
            }
            else if (selected_type == "Box") {
            	timesfactors = pools * days * getCombinationCount(o._number);
            }
            
			// start set the total of the betting form
			total_big = total_big + o._amtfloat_big * timesfactors;
			total_small = total_small + o._amtfloat_small * timesfactors;
			total_4a = total_4a + o._amtfloat_4a * timesfactors;
			total_3a = total_3a + o._amtfloat_3a * timesfactors;
			total_3abc = total_3abc + o._amtfloat_3c * timesfactors;
			
			total_amount = total_amount + o._amtfloat_big * timesfactors + 
			o._amtfloat_small * timesfactors + 
			o._amtfloat_4a * timesfactors + 
			o._amtfloat_3a * timesfactors + 
			o._amtfloat_3c * timesfactors;

			var combinations = 0;

			
            /*var pools_days = 1;
            
            if (muldiv_elem.val() == "MUL") pools_days = pools * days;
            if (type_elem.val() == "Normal" || type_elem.val() == "iBox") {
                $sub_total.val(pools_days * total_amount);
            } else if (type_elem.val() == "PH") {
                $sub_total.val(pools_days * total_amount * 10);
            } else if (type_elem.val() == "Box") {
                $sub_total.val(pools_days * total_amount * getCombinationCount(o._number));
            }*/
        }

        $('#spanBig').text(total_big);
        $('#spanSmall').text(total_small);
        $('#span4A').text(total_4a);
        $('#span3A').text(total_3a);
        $('#span3ABC').text(total_3abc);
        $("#spanTotal").text(total_amount);
        $('#hidTotal').val(total_amount);
    }

    // this is to get the total permutation of number
    function getCombinationCount(number) {
    	if (!/\d{3,4}/.test(number)) return 0;
    	var digit_counts = [];
    	for (var i = 0; i < 10; ++i) digit_counts.push(0);
    		var digits = [];
    	for (i = 0; i < number.length; ++i) {
    		var l = number.charAt(i);
    		if (digit_counts[l] == 0) digits.push(l);
    		++digit_counts[l];
    	}
    	var comb_count_4 = [0, 1, 4, 12, 24];
    	var comb_count_3 = [0, 1, 3, 6];
    	var numcode = "";
    	var retval = 0;
    	for (i = 0; i < digits.length; ++i) numcode = numcode + digit_counts[digits[i]].toString();

    		if (numcode == "22") {
    			retval = 6;
    		} else if (number.length == 3) {
    			retval = comb_count_3[digits.length];
    		} else if (number.length == 4) {
    			retval = comb_count_4[digits.length];
    		}
    		return retval;
    	}


    	$(document).on('change', '.drawnumber_input', function() {

    		$var = $(this).val();
    		$no = $(this).attr('id').replace("txt_number_", "");

    		if ($var.length == 3) {
    			$("#txt_type_"+$no).val("3D");
    			$(this).css('backgroundColor', 'white');

    			$("#txt_Big_"+$no).val("");
    			$("#txt_Big_"+$no).attr('readonly', true);

    			$("#txt_Small_"+$no).val("");
    			$("#txt_Small_"+$no).attr('readonly', true);

    			$("#txt_4A_"+$no).val("");
    			$("#txt_4A_"+$no).attr('readonly', true);
    		} else if ($var.length == 4) {
    			$("#txt_type_"+$no).val("4D");
    			$(this).css('backgroundColor', 'white');

    			$("#txt_Big_"+$no).val("");
    			$("#txt_Big_"+$no).attr('readonly', false);

    			$("#txt_Small_"+$no).val("");
    			$("#txt_Small_"+$no).attr('readonly', false);
    			
    			$("#txt_4A_"+$no).val("");
    			$("#txt_4A_"+$no).attr('readonly', false);
    		} else {
    			$("#txt_type_"+$no).val("Invalid");
    			$(this).css('backgroundColor', 'yellow');
    		}
    	});

    	$(document).on('change', '.selectAll', function() {

    		if(this.checked) {
            //Do stuff
            copyAllDown($(this));
        }
        else
        {
        	clearAllDown($(this))
        }
    });

    	$(document).on('change', '.selectPoolAll', function() {

    		if(this.checked) {
            //Do stuff
            checkAllDown($(this));
        }
        else
        {
        	uncheckAllDown($(this))
        }
    });

	// show error message
	function showError(msg)
	{
		$('#divMsg').addClass("alert alert-danger");
		$('#divMsg').text(msg);
	}

	function clearError()
	{
		$('#divMsg').removeClass("alert alert-danger");
		$('#divMsg').text('');
	}

    // copy all the textbox down
    function copyAllDown(control)
    {
    	functional_area = control.attr('id').replace("selectAll_", "txt_");
    	reference_value = document.getElementById(functional_area + "_" + 1).value;
    	for (count = 2; count <= 30; count++)
    	{
    		if($("#" + functional_area + "_" + count).is("[readonly]") == false){
    			document.getElementById(functional_area + "_" + count).value = (reference_value);
    		}
    	}   
    	recalculate();	    	
    }

    // clear all the textbox
    function clearAllDown(control)
    {
    	functional_area = control.attr('id').replace("selectAll_", "txt_");
    	for (count = 2; count <= 30; count++)
    	{
    		document.getElementById(functional_area + "_" + count).value = "";
    	}  
    	recalculate(); 	    	
    }

    // Make all the checkbox selected
    function checkAllDown(control)
    {
    	functional_area = control.attr('id').replace("selectAll_", "select_");
    	for (count = 1; count <= 30; count++)
    	{
    		document.getElementById(functional_area + "_" + count).checked = true;
    	}   
    	recalculate();	    	
    }

    // Make all the checkbox unselected
    function uncheckAllDown(control)
    {
    	functional_area = control.attr('id').replace("selectAll_", "select_");
    	for (count = 1; count <= 30; count++)
    	{
    		document.getElementById(functional_area + "_" + count).checked = false;
    	} 
    	recalculate();  	    	
    }

      $('#btnClear').click(function(){
    	clearAllInputs();
    });

    //Clear all inputs
    function clearAllInputs() {
    	$('.selectAll').prop('checked', false); // Unchecks it
    	$('.selectPoolAll').prop('checked', false); // Unchecks it
    	$('.dd').prop('checked', false); // Unchecks it
    	$('#cbAutoTicket').prop('checked', false); // Unchecks it
    	$('.chkpools').prop('checked', false); // Unchecks it
    	$('form').find("input[type=text].inputTelephone, input[type=text].inputRemark, td:nth-child(n+2) input[type=text]").val("");
    	$('.drawnumber_input').css('backgroundColor', 'white');
    	$('.selBetType').val(1);
    }
</script>
@stop