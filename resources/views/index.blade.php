@extends("layouts.userlayout")
@section("title", "Dashboard")
@section("header", "Dashboard")

@section('headerScript')
<link href="/assets/plugins/jquery-jvectormap/jquery-jvectormap.css" rel="stylesheet" />
<link href="/assets/plugins/bootstrap-calendar/css/bootstrap_calendar.css" rel="stylesheet" />
<link href="/assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" />
<link href="/assets/plugins/nvd3/build/nv.d3.css" rel="stylesheet" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.carousel.css" />
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/assets/owl.theme.default.css" />
@stop

@section("body")
<div class="row">
	<!-- begin col-3 -->
	<div class="col-lg-3 col-md-6">
		<div class="widget widget-stats bg-white text-inverse">
			<div class="stats-content">
				<div class="stats-title">CREDIT LIMIT (RM)</div>
				<div class="stats-number">500,000.00</div>
				<div class="stats-progress progress">
					<div class="progress-bar" style="width: 70.1%;"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- end col-3 -->
	<!-- begin col-3 -->
	<div class="col-lg-3 col-md-6">
		<div class="widget widget-stats bg-white text-inverse">
			<div class="stats-content">
				<div class="stats-title">BALANCE (RM)</div>
				<div class="stats-number">200,000.00</div>
				<div class="stats-progress progress">
					<div class="progress-bar" style="width: 40.5%;"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- end col-3 -->
	<!-- begin col-3 -->
	<div class="col-lg-3 col-md-6">
		<div class="widget widget-stats bg-white text-inverse">
			<div class="stats-content">
				<div class="stats-title">CASH BALANCE</div>
				<div class="stats-number">38,900.00</div>
				<div class="stats-progress progress">
					<div class="progress-bar" style="width: 76.3%;"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- end col-3 -->
	<!-- begin col-3 -->
	<div class="col-lg-3 col-md-6">
		<div class="widget widget-stats bg-white text-inverse">
			<div class="stats-content">
				<div class="stats-title">YESTERDAY CASH BALANCE</div>
				<div class="stats-number">3,988.00</div>
				<div class="stats-progress progress">
					<div class="progress-bar" style="width: 54.9%;"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- end col-3 -->
</div>
<!-- end row -->

<!-- begin row -->
<div class="row">
	<!-- begin col-3 -->
	<div class="col-lg-3 col-md-6">
		<div class="widget widget-stats bg-white text-inverse">
			<div class="stats-content">
				<div class="stats-title">TOTAL BALANCE (RM)</div>
				<div class="stats-number">500,000.00</div>
				<div class="stats-progress progress">
					<div class="progress-bar" style="width: 70.1%;"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- end col-3 -->
	<!-- begin col-3 -->
	<div class="col-lg-3 col-md-6">
		<div class="widget widget-stats bg-white text-inverse">
			<div class="stats-content">
				<div class="stats-title">YESTERDAY TOTAL BALANCE (RM)</div>
				<div class="stats-number">200,000.00</div>
				<div class="stats-progress progress">
					<div class="progress-bar" style="width: 40.5%;"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- end col-3 -->
	<!-- begin col-3 -->
	<div class="col-lg-3 col-md-6">
		<div class="widget widget-stats bg-white text-inverse">
			<div class="stats-content">
				<div class="stats-title">TOTAL OUTSTANDING BETS</div>
				<div class="stats-number">38,900.00</div>
				<div class="stats-progress progress">
					<div class="progress-bar" style="width: 76.3%;"></div>
				</div>
			</div>
		</div>
	</div>
	<!-- end col-3 -->
</div>
<!-- end row -->

<div class="row">
	<div class="col-lg-6">
		<div class="panel panel-inverse" data-sortable="false">
			<div class="panel-heading">
				<h4 class="panel-title">
					Upcoming Draws
				</h4>
			</div>
			<div>
				<table class="table table-bordered table-striped text-center table-valign-middle">
					<thead>
						<th>Pool</th>
						@foreach($schedules AS $item)
						<th>{{ \Carbon\Carbon::parse($item->drawdate)->format('d M (D)')}}</th>
						@endforeach
					</thead>
					<tbody>
						<tr>
							<td>
								<img src="../assets/img/lotto/bet-logo-magnum.png" class="pool-logo">
							</td>
							@foreach($schedules AS $item)
							<td>
								@if($item->mg)
								<i class="fa fa-check fa-lg" style="color: green"></i>
								@endif
							</td>
							@endforeach
						</tr>
						<tr>
							<td>
								<img src="../assets/img/lotto/bet-logo-damacai-inversed.png" class="pool-logo">
							</td>
							@foreach($schedules AS $item)
							<td>
								@if($item->kd)
								<i class="fa fa-check fa-lg" style="color: green"></i>
								@endif
							</td>
							@endforeach
						</tr>
						<tr>
							<td>
								<img src="../assets/img/lotto/bet-logo-toto-inversed.png" class="pool-logo">
							</td>
							@foreach($schedules AS $item)
							<td>
								@if($item->tt)
								<i class="fa fa-check fa-lg" style="color: green"></i>
								@endif
							</td>
							@endforeach
						</tr>
						<tr>
							<td>
								<img src="../assets/img/lotto/bet-logo-singapore-pools.png" class="pool-logo">
							</td>
							@foreach($schedules AS $item)
							<td>
								@if($item->sg)
								<i class="fa fa-check fa-lg" style="color: green"></i>
								@endif
							</td>
							@endforeach
						</tr>
						<tr>
							<td>
								<img src="../assets/img/lotto/bet-logo-big-cash.png" class="pool-logo">
							</td>
							@foreach($schedules AS $item)
							<td>
								@if($item->sw)
								<i class="fa fa-check fa-lg" style="color: green"></i>
								@endif
							</td>
							@endforeach
						</tr>
						<tr>
							<td>
								<img src="../assets/img/lotto/bet-logo-sabah88.png" class="pool-logo">
							</td>
							@foreach($schedules AS $item)
							<td>
								@if($item->sb)
								<i class="fa fa-check fa-lg" style="color: green"></i>
								@endif
							</td>
							@endforeach
						</tr>
						<tr>
							<td>
								<img src="../assets/img/lotto/bet-logo-stc.png" class="pool-logo">
							</td>
							@foreach($schedules AS $item)
							<td>
								@if($item->stc)
								<i class="fa fa-check fa-lg" style="color: green"></i>
								@endif
							</td>
							@endforeach
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
	<div class="col-lg-6">
		<div class="panel panel-inverse" data-sortable="false">
			<div class="panel-heading">
				<h4 class="panel-title">
					Latest Draw Result
				</h4>
			</div>
			<div class="panel-body">
				<div class="owl-carousel owl-theme">
					@foreach($poolResult AS $pool)
					<div>
						<table class="table table-bordered width-full table-pool-result {{ $pool['0']->shortname }}">
							<tbody>
								<tr>
									<td colspan="5" class="result-title">
										<img src="{{ $pool['0']->images }}" class="pool-logo">
										<strong class="text-uppercase f-s-15">{{ \Carbon\Carbon::createFromFormat("Ymd", $pool['0']->drawdate)->format("d M Y (D)") }}</strong>
									</td>
								</tr>
								<tr>
									<td colspan="2" class="prize-title">First Prize</td>
									<td colspan="3"><strong>{{ $pool->data['top'][0]->number }}</strong></td>
								</tr>
								<tr>
									<td colspan="2" class="prize-title">Second Prize</td>
									<td colspan="3"><strong>{{ $pool->data['top'][1]->number }}</strong></td>
								</tr>
								<tr>
									<td colspan="2" class="prize-title">Third Prize</td>
									<td colspan="3"><strong>{{ $pool->data['top'][2]->number }}</strong></td>
								</tr>
								<tr>
									<td colspan="5" class="result-title">
										Special
									</td>
								</tr>
								<tr>
									@for($i = 0; $i < 5; $i++)
									<td>{{ $pool->data['special'][$i]->number }}</td>
									@endfor
								</tr>
								<tr>
									@for($i = 5; $i < 10; $i++)
									<td>{{ $pool->data['special'][$i]->number }}</td>
									@endfor
								</tr>
								<tr>
									<td colspan="5" class="result-title">
										Consolation
									</td>
								</tr>
								<tr>
									@for($i = 0; $i < 5; $i++)
									<td>{{ $pool->data['consolation'][$i]->number }}</td>
									@endfor
								</tr>
								<tr>
									@for($i = 5; $i < 10; $i++)
									<td>{{ $pool->data['consolation'][$i]->number }}</td>
									@endfor
								</tr>
								@if($pool['0']->name == "Toto")
								<tr>
									<td colspan="5" class="result-title">
										5D
									</td>
								</tr>
								<tr>
									<td class="prize-title">1st</td>
									<td>{{ $pool->data['5d'][0]->number }}</td>
									<td class="prize-title">4th</td>
									<td colspan="3">{{ substr($pool->data['5d'][0]->number, -4) }}</td>
								</tr>
								<tr>
									<td class="prize-title">2nd</td>
									<td>{{ $pool->data['5d'][1]->number }}</td>
									<td class="prize-title">5th</td>
									<td colspan="3">{{ substr($pool->data['5d'][0]->number, -3) }}</td>
								</tr>
								<tr>
									<td class="prize-title">3rd</td>
									<td>{{ $pool->data['5d'][2]->number }}</td>
									<td class="prize-title">6th</td>
									<td colspan="3">{{ substr($pool->data['5d'][1]->number, -2) }}</td>
								</tr>
								<tr>
									<td colspan="5" class="result-title">
										6D
									</td>
								</tr>
								<tr>
									<td class="prize-title">1st</td>
									<td colspan="4">{{ $pool->data['6d'][0]->number }}</td>
								</tr>
								<tr>
									<td class="prize-title">2nd</td>
									<td>{{ substr($pool->data['6d'][0]->number, 0, 5) }}</td>
									<td class="prize-title">or</td>
									<td colspan="2">{{ substr($pool->data['6d'][0]->number, -5) }}</td>
								</tr>
								<tr>
									<td class="prize-title">3rd</td>
									<td>{{ substr($pool->data['6d'][0]->number, 0, 4) }}</td>
									<td class="prize-title">or</td>
									<td colspan="2">{{ substr($pool->data['6d'][0]->number, -4) }}</td>
								</tr>
								<tr>
									<td class="prize-title">4th</td>
									<td>{{ substr($pool->data['6d'][0]->number, 0, 3) }}</td>
									<td class="prize-title">or</td>
									<td colspan="2">{{ substr($pool->data['6d'][0]->number, -3) }}</td>
								</tr>
								<tr>
									<td class="prize-title">5th</td>
									<td>{{ substr($pool->data['6d'][0]->number, 0, 2) }}</td>
									<td class="prize-title">or</td>
									<td colspan="2">{{ substr($pool->data['6d'][0]->number, -2) }}</td>
								</tr>
								@endif
							</tr>
						</tbody>
					</table>
				</div>
				@endforeach
			</div>
		</div>
	</div>
</div>
@stop

@section("page_script")
<script src="https://cdnjs.cloudflare.com/ajax/libs/d3/3.5.2/d3.min.js"></script>
<script src="/assets/plugins/nvd3/build/nv.d3.js"></script>
<script src="/assets/plugins/jquery-jvectormap/jquery-jvectormap.min.js"></script>
<script src="/assets/plugins/jquery-jvectormap/jquery-jvectormap-world-merc-en.js"></script>
<script src="/assets/plugins/bootstrap-calendar/js/bootstrap_calendar.min.js"></script>
<script src="/assets/plugins/gritter/js/jquery.gritter.js"></script>
<script src="/assets/js/demo/dashboard-v2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/OwlCarousel2/2.3.4/owl.carousel.js"></script>

<!-- ================== END PAGE LEVEL JS ================== -->

<script>
	$(document).ready(function() {
		COLOR_GREEN = '#5AC8FA';
		DashboardV2.init();

		$('.owl-carousel').owlCarousel({
			items: 1,
			nav: false,
			autoHeight: true,
			margin: 10
		});
	});
</script>
@stop