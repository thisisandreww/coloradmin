@extends("layouts.userlayout")

@section("headerScript")
<link href="/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/parsley/src/parsley.css" rel="stylesheet" />
<link href="/assets/css/package.css" rel="stylesheet" />
@stop
@section("body")
@include("include.phpVar")
@include("include.phpVarEatMap")
<?php 
use Illuminate\Support\Facades\Config;

$isEditCompanyUser = 0;
$isLoginUser = 0;
$isLoginUserSystem = 0;
$isEditSystemUser = 0;
$isSubAccount = 0;
if ( $data['userToEdit']->id == $data['loginUser']->id)
    $isLoginUser = 1;

if ( $data['user']->role_name=="System")
    $isLoginUserSystem = 1;

if ($data['userToEdit']->role_name=="Company")
    $isEditCompanyUser = 1;

if ($data['userToEdit']->role_name=="System")
    $isEditSystemUser = 1;

if ($data['userToEdit']->id==$data['loginUser']->id && $data['loginUser']->id != $data['loginUser']->user_main_account)
    $isSubAccount = 1;
?>

<input type="hidden" id="hidUplineId" name="hidUplineId" value="{{ $data['user']->id }}" />
<input type="hidden" id="hidTargetId" name="hidTargetId" value="{{ $data['userToEdit']->id }}" />

<div>
   <!-- begin page-header -->
   <h1 class="page-header">
   @if ($isLoginUser)
        @lang('user.EDIT_PROFILE')
   @else
        @lang('user.EDIT_MEMBER')
   @endif
   </h1>
   <!-- end page-header -->
   @if (!$isLoginUser)
   <div class="tab-overflow">
      <ul class="nav nav-tabs" style="width:100%;">
         <li class="nav-item prev-button"><a href="javascript:;" data-click="prev-tab" class="nav-link text-success"><i class="fa fa-arrow-left"></i></a></li>
         <li class="nav-item"><a href="#nav-tab-1" data-toggle="tab" class="nav-link active" style="font-size:15px; font-style:bold;">Profile</a></li>
         <li class="nav-item"><a href="#nav-tab-2" data-toggle="tab" class="nav-link" style="font-size:15px; font-style:bold;">Eat Map</a></li>
         <li class="nav-item"><a href="#nav-tab-3" data-toggle="tab" class="nav-link" style="font-size:15px; font-style:bold;">Order Rate</a></li>
         <li class="nav-item"><a href="#nav-tab-4" data-toggle="tab" class="nav-link" style="font-size:15px; font-style:bold;">Account Setting</a></li>
         <li class="nav-item"><a href="#nav-tab-5" data-toggle="tab" class="nav-link" style="font-size:15px; font-style:bold;">Company Bet Code</a></li>
         
      </ul>
   </div>
   @endif
   <div class="tab-content">
      <!-- begin tab-pane -->
      <div class="tab-pane fade active show" id="nav-tab-1">
         <!-- Put Tab Here -->
         <!-- begin panel -->
         <div class="panel panel-inverse">
            <!-- begin panel-heading -->
            <div class="panel-heading">
               <div class="panel-heading-btn">
                  <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                  <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                  <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                  <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
               </div>
               <h4 class="panel-title">
               @if ($isLoginUser)
                   @lang('user.EDIT_PROFILE'): {{ $data['userToEdit']->username }}
               @else
                   @lang('user.EDIT_MEMBER'): {{ $data['userToEdit']->username }}
               @endif
               </h4>
            </div>
            <form  id="edit_form" action="/editMemberDetail/{{$data['userToEdit']->id}}" method="POST">
               {{ csrf_field() }}
               <!-- end panel-heading -->
               <!-- begin panel-body -->
               <div class="panel-body">
                   @if (session('success'))
                   <div class="alert alert-success">{{Session::get('success')}}</div>
                   @endif
                   @if (session('error'))
                   <div class="alert alert-danger">{{Session::get('error')}}</div>
                   @endif
                   @if (session('message'))
                        	<div class="{{session('class')}}">{{session('message')}}</div>
                  @endif
                  <div class="row row-space-10">
                     <div class="col-md-4">
                        <div class="form-group m-b-10" data-parsley-validate="true">
                           <label for="exampleInputEmail1">@lang('user.USERNAME')</label>
                           <input type="text" class="form-control" id="username" name="username" value="{{ old('username',$data['userToEdit']->username) }}" data-parsley-required placeholder="Enter username" />
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group m-b-10">
                           <label for="exampleInputPassword1">@lang('user.NICKNAME')</label>
                           <input type="text" class="form-control" id="name" name="name" value="{{ old('name',$data['userToEdit']->name) }}" data-parsley-required placeholder="Enter nick name" />
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group m-b-10">
                           <label for="exampleInputPassword1">@lang('user.PASSWORD')</label>
                           <input type="password" class="form-control" id="password" name="password" value="{{ old('password',$data['userToEdit']->password) }}" data-parsley-required placeholder="Enter password" />
                        </div>
                     </div>
                  </div>
                  <div class="row row-space-10">
                     <div class="col-md-4">
                        <div class="form-group m-b-10">
                           <label for="exampleInputPassword1">@lang('user.LEVEL')</label>
                           @if($isLoginUser)
                           <input type="text" readonly class="form-control" value="{{$data['userToEdit']->role_name}}" />
                           <input type="hidden" readonly  name="role" value="{{$data['userToEdit']->user_role}}" />
                           @else
                           <select class="form-control" name="role" id="role">
                           @if(count($data['roleOption']) > 0)
                           @foreach($data['roleOption'] as $role)
                           <option value="{{$role->role_id}}"
                           @if( old('role',$data['userToEdit']->user_role) == $role->role_id)
                           selected
                           @endif
                           >
                           {{ $role->role_name }}
                           </option> 
                           @endforeach
                           @endif
                           </select>
                           @endif
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group m-b-10">
                           <label for="exampleInputPassword1">@lang('user.CURRENCY')</label>
                           <select class="form-control" <?php if($isSubAccount){echo "disabled";} ?>  name="currency" id="currency">
                              <option value="myr" <?php if(old('currency') == "myr"){echo "selected";} ?>>@lang('user.CURRENCY_MYR')</option>
                              <option value="sgd" <?php if(old('currency') == "sgd"){echo "selected";} ?>>@lang('user.CURRENCY_SGD')</option>
                           </select>
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group m-b-10">
                           <label for="exampleInputPassword1">@lang('user.CREDIT_LIMIT')
                           <?php
                              $defaultCreditLimit = $data['creditBalance'] + $data['userToEdit']->credit_limit;
                                                 if ($isLoginUser == 0)
                                                     echo "(".$defaultCreditLimit.")";
                                                 else
                                                     echo "(".$data['userToEdit']->credit_limit.")";
                                             ?>
                           </label>
                           <?php 
                              if ($isLoginUser == 0)
                                  echo "<input type='number' class='form-control' name='credit_limit' id='credit_limit' min='0'
                                      value='" .old('credit_limit',$data['userToEdit']->credit_limit)."' max='".$defaultCreditLimit ."' placeholder='Maximum ".$defaultCreditLimit ."' required />";
                              else
                              {
                                  echo "<input type='number' class='form-control' name='credit_limit' id='credit_limit'";
                                  if (!$isEditSystemUser || $isSubAccount)
                                    echo "readonly ";
                                  echo "value='".old('credit_limit',$data['userToEdit']->credit_limit)."' placeholder='Maximum ".$data['userToEdit']->credit_limit."' required/>";
                              }
                              
                              ?>
                        </div>
                     </div>
                  </div>
                  <div class="row row-space-10">
                     <div class="col-md-4">
                        <div class="form-group m-b-10">
                           <label for="exampleInputPassword1">@lang('user.STATUS')</label>
                           <select class="form-control" name="state" id="state"
                           @if($isLoginUser)
                           disabled
                           @endif
                           >
                           <option 
                           @if(old('state',$data['userToEdit']->state) == 0) 
                           selected 
                           @endif 
                           value="0"> @lang('user.STATUS_ACTIVE') </option>
                           <option 
                           @if (old('state',$data['userToEdit']->state) == 1) 
                           selected 
                           @endif 
                           value="1"> 
                           @lang('user.STATUS_INACTIVE') 
                           </option>
                           <option 
                           @if (old('state',$data['userToEdit']->state) == 2) 
                           selected 
                           @endif 
                           value="2"> 
                           @lang('user.STATUS_SUSPEND') 
                           </option>
                           </select>
                        </div>
                     </div>
                     <?php 
                           $real_position_3d4d = $data['user']->position_3d4d;
                           $real_position_5d6d = $data['user']->position_5d6d;  
                           $selected_position_3d4d = $data['userToEdit']->position_3d4d;
                           $selected_position_5d6d = $data['userToEdit']->position_5d6d;
                       ?>
                     <div class="col-md-4"  <?php if($isSubAccount){ echo "style='display:none'";} ?>>
                        <div class="form-group m-b-10">
                           <label for="exampleInputPassword1">@lang('user.POSITION_3D4D')</label>
                           <select name="selPosition3d4d" class="form-control" @if($isLoginUser)
                           disabled
                           @endif>
                              <?php 
                                for ($curr = $real_position_3d4d; $curr >= 0; $curr--)
                                {
                                    if ($curr == $selected_position_3d4d)
                                    {
                             ?>
                                <option selected><?php echo $curr ?>%</option>
                             <?php }
                                    else 
                                    {
                             ?>
                             	<option><?php echo $curr ?>%</option>
                             <?php }} ?>

                           </select>
                        </div>
                     </div>
                     <div class="col-md-4"  <?php if($isSubAccount){ echo "style='display:none'";} ?>>
                        <div class="form-group m-b-10">
                           <label for="exampleInputPassword1">@lang('user.POSITION_5D6D')</label>
                           <select name="selPosition5d6d" class="form-control" @if($isLoginUser)
                           disabled
                           @endif>
                              <?php 
                                for ($curr = $real_position_5d6d; $curr >= 0; $curr--)
                                {
                                    if ($curr == $selected_position_5d6d)
                                    {
                             ?>
                                <option selected><?php echo $curr ?>%</option>
                             <?php }
                                    else 
                                    {
                             ?>
                             	<option><?php echo $curr ?>%</option>
                             <?php }} ?>
                           </select>
                        </div>
                     </div>
                  </div>
                  <div class="row row-space-10"  <?php if($isSubAccount){ echo "style='display:none'";} ?>>
                     <div class="col-md-4">
                        <div class="form-group m-b-10">
                           <label for="exampleInputPassword1">@lang('user.MEMBER_CREATE')</label>
                           <div class="checkbox checkbox-css m-b-20">
                              <input type="checkbox" id="nf_checkbox_css_1" name="member_create" value="1" <?php if($isLoginUser) echo "onclick='return false;'"; ?>
                              @if (old('member_create',$data['userToEdit']->downline_create) == 1)
                              checked
                              @endif
                              />
                              <label for="nf_checkbox_css_1"> </label>
                           </div>
                        </div>
                     </div>
                     <div class="col-md-4">
                        <div class="form-group m-b-10">
                           <label for="exampleInputPassword1">@lang('user.FLOAT')</label>
                           <div class="checkbox checkbox-css m-b-20">
                              <input type="checkbox" id="nf_checkbox_css_2" name="float" value="1" <?php if($isLoginUser) echo "disabled"; ?> 
                              @if (old('float',$data['userToEdit']->float_val) == 1)
                              checked
                              @endif
                              />
                              <label for="nf_checkbox_css_2"> </label>
                           </div>
                        </div>
                     </div>
                  </div>
                  <!-- end panel-body -->
               </div>
               <!-- end panel -->	
               <!-- begin panel -->
               <div class="panel panel-inverse" <?php if($isEditSystemUser) echo "style='display:none'" ?>>
                  <!-- begin panel-heading -->
                  <div class="panel-heading">
                     <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                     </div>
                     <h4 class="panel-title">@lang('package.PACKAGE_TITLE')</h4>
                  </div>
                  <!-- end panel-heading -->
                  <!-- begin panel-body -->
                  <div class="panel-body">
                     <!-- begin nav-tabs -->
                     <ul class="nav nav-tabs">
                        <li class="nav-items">
                           <a href="#default-tab-1" data-toggle="tab" class="nav-link active">
                           <span class="d-sm-none">@lang('package.PACKAGE_3D4D')</span>
                           <span class="d-sm-block d-none">@lang('package.PACKAGE_3D4D')</span>
                           </a>
                        </li>
                        <li class="nav-items">
                           <a href="#default-tab-2" data-toggle="tab" class="nav-link">
                           <span class="d-sm-none">@lang('package.PACKAGE_5D6D')</span>
                           <span class="d-sm-block d-none">@lang('package.PACKAGE_5D6D')</span>
                           </a>
                        </li>
                        <li class="">
                           <a href="#default-tab-3" data-toggle="tab" class="nav-link">
                           <span class="d-sm-none">@lang('package.PACKAGE_RED_3D4D')</span>
                           <span class="d-sm-block d-none">@lang('package.PACKAGE_RED_3D4D')</span>
                           </a>
                        </li>
                        <li class="">
                           <a href="#default-tab-4" data-toggle="tab" class="nav-link">
                           <span class="d-sm-none">@lang('package.PACKAGE_RED_5D6D')</span>
                           <span class="d-sm-block d-none">@lang('package.PACKAGE_RED_5D6D')</span>
                           </a>
                        </li>
                     </ul>
                     <!-- end nav-tabs -->
                     <!-- begin tab-content -->
                     <div class="tab-content">
                        <!-- begin tab-pane -->
                        <div class="tab-pane fade active show" id="default-tab-1">
                           <div class="row row-space-10">
                              <?php $package3D4DCount = 0; ?>
                              @if(!$isEditSystemUser && count($data['package3D4DOption']) > 0)
                              @foreach($data['package3D4DOption'] as $package)
                              <?php if ($package->red_package)
                                 continue;
                                 else
                                 $package3D4DCount++;
                                 ?>
                              <div class="col-md-2">
                                 <div class="form-group m-b-10">
                                    <div class="checkbox checkbox-css m-b-20">
                                       <input type="checkbox" name="package3D4DAssigned[]" 
                                       <?php 
                                          if ($isLoginUser)
                                              echo "onclick='showPermissionDeniedError()'";
                                          ?> 
                                       value="{{$package->package_id}}"
                                       id="package3D4D_checkbox_{{$package->package_id}}" data-rowref="{{$package->reference_count}}" data-rowname="{{ $package->package_name }}" class="checkbox_3D4D select_all_3D4D"
                                       <?php 
                                       if((old('package3D4DAssigned') != NULL && in_array($package->package_id, old('package3D4DAssigned'))) || $package->assigned != 0)
                                           echo "checked";
                                       ?>
                                       ></input>
                                       <label for="package3D4D_checkbox_{{$package->package_id}}"> <a href="#package_3D4D_dialog" id="package3D4D_{{$package->package_id}}" data-toggle="modal" class="package_3D4D_table" 
                                          data-rowid="{{$package->package_id}}" data-rowtypeid="{{$package->package_type_id}}" data-rowuserid="{{$data['userToEdit']->id}}"
                                          data-rowpriviledge="{{$package->assigned & $data['loginUser']->package_control & $data['user']->package_control & $data['loginUser']->state ==0}}" data-rowreferencedcount="{{$package->reference_count}}" data-rowredpack="0"  
                                          >{{$package->package_name}}</a></label>
                                    </div>
                                 </div>
                              </div>
                              @endforeach
                              @endif
                           </div>
                           @if(!$isLoginUser)
                           <p class="text-right m-b-0">
                           <div class="form-group m-b-10">
                              @if ($package3D4DCount > 0)
                              <div class="checkbox checkbox-css m-b-20">
                                 <input type="checkbox" id="selectAll3D4D"/>
                                 <label for="selectAll3D4D">@lang('package.SELECT_ALL_PACKAGE')</label>
                              </div>
                              @else
                              @lang('package.NO_PACKAGE')
                              @endif 
                           </div>
                           </p>
                           @endif
                        </div>
                        <!-- end tab-pane -->
                        <!-- begin tab-pane -->
                        <div class="tab-pane fade" id="default-tab-2">
                           <div class="row row-space-10">
                              <?php $package5D6DCount = 0; ?>
                              @if(!$isEditSystemUser && count($data['package5D6DOption']) > 0)
                              @foreach($data['package5D6DOption'] as $package)
                              <?php if ($package->red_package)
                                 continue;
                                 else
                                 $package5D6DCount++;
                                 ?>
                              <div class="col-md-2">
                                 <div class="form-group m-b-10">
                                    <div class="checkbox checkbox-css m-b-20">
                                       <input type="checkbox" name="package5D6DAssigned[]" 
                                       <?php 
                                          if ($isLoginUser)
                                              echo "onclick='showPermissionDeniedError()'";
                                          ?> 
                                       value="{{$package->package_id}}"
                                       id="package5D6D_checkbox_{{$package->package_id}}" data-rowref="{{$package->reference_count}}" data-rowname="{{ $package->package_name }}" class="checkbox_5D6D select_all_5D6D"
                                       <?php 
                                       if((old('package5D6DAssigned') != NULL && in_array($package->package_id, old('package5D6DAssigned'))) || $package->assigned != 0)
                                           echo "checked";
                                       ?>
                                       ></input>
                                       <label for="package5D6D_checkbox_{{$package->package_id}}"> <a href="#package_5D6D_dialog" id="package5D6D_{{$package->package_id}}" data-toggle="modal" class="package_5D6D_table" 
                                          data-rowid="{{$package->package_id}}" data-rowtypeid="{{$package->package_type_id}}" data-rowuserid="{{$data['userToEdit']->id}}"
                                          data-rowpriviledge="{{$package->assigned & $data['loginUser']->package_control & $data['user']->package_control & $data['loginUser']->state ==0}}" data-rowreferencedcount="{{$package->reference_count}}" data-rowredpack="0"  
                                          >{{$package->package_name}}</a></label>
                                    </div>
                                 </div>
                              </div>
                              @endforeach
                              @endif
                           </div>
                           @if (!$isLoginUser)
                           <p class="text-right m-b-0">
                           <div class="form-group m-b-10">
                              @if ($package5D6DCount>0)
                              <div class="checkbox checkbox-css m-b-20">
                                 <input type="checkbox" id="selectAll5D6D" />
                                 <label for="selectAll5D6D">@lang('package.SELECT_ALL_PACKAGE')</label>
                              </div>
                              @else
                              @lang('package.NO_PACKAGE')
                              @endif 
                           </div>
                           </p>
                           @endif
                        </div>
                        <!-- end tab-pane -->
                        <!-- begin tab-pane -->
                        <div class="tab-pane fade" id="default-tab-3">
                           <div class="row row-space-10">
                              <?php $packageRed3D4DCount = 0; ?>
                              @if(!$isEditSystemUser && count($data['package3D4DOption']) > 0)
                              @foreach($data['package3D4DOption'] as $assignedPackageList)
                              <?php if (!$assignedPackageList->red_package)
                                 continue;
                                 else
                                   $packageRed3D4DCount++;
                                 ?>
                              <div class="col-md-2">
                                 <div class="form-group m-b-10">
                                    <div class="radio radio-css m-b-20">
                                       <input type="radio" name="red_3D4D_package" id="package3D4D_checkbox_{{$assignedPackageList->package_id}}" value="{{$assignedPackageList->package_id}}"  class="radio_button_3D4D" data-rowname="{{ $assignedPackageList->package_name }}"
                                       @if(old('red_3D4D_package')==$assignedPackageList->package_id)
                                           checked="checked"
                                       @elseif($assignedPackageList->assigned && $assignedPackageList->package_id == $data['userToEdit']->assigned_red_package_3D4D_id)
                                       checked="checked"
                                       @endif
                                       @if (!$isLoginUserSystem && $assignedPackageList->red_package && $data['setting']['allow_red_package_control']->setting_value == 0)
                                       onclick="showRedPackagePermissionDeniedError()"
                                       @endif
                                       ></input>
                                       <label for="package3D4D_checkbox_{{$assignedPackageList->package_id}}"> 
                                       <a href="#package_3D4D_dialog" id="package3D4D_{{$assignedPackageList->package_id}}" data-toggle="modal" class="package_3D4D_table" 
                                          data-rowid="{{$assignedPackageList->package_id}}" data-rowtypeid="{{$assignedPackageList->package_type_id}}" data-rowuserid="{{$data['userToEdit']->id}}"
                                          data-rowpriviledge="0" data-rowredpack="1">
                                       {{$assignedPackageList->package_name}}</a></label>
                                    </div>
                                 </div>
                              </div>
                              @endforeach
                              @endif
                              @if ($packageRed3D4DCount >0)
                              <div class="col-md-2">
                                 <div class="form-group m-b-10">
                                    <div class="radio radio-css m-b-20">
                                       <input type="hidden" id="radio_button_3D4D_pre" />
                                       <input type="radio" id="red_3D4D_package" name="red_3D4D_package" value="0" class="radio_button_3D4D"
                                       @if (!$isLoginUserSystem && $data['setting']['allow_red_package_control']->setting_value == 0)
                                       onclick="showRedPackagePermissionDeniedError()"
                                       @endif
                                       />
                                       <label for="red_3D4D_package">@lang('package.UNASSIGNED_PACKAGE')</label>
                                    </div>
                                 </div>
                              </div>
                              @else
                              @lang('package.NO_PACKAGE')
                              @endif
                           </div>
                        </div>
                        <div class="tab-pane fade" id="default-tab-4">
                           <div class="row row-space-10">
                              <?php $packageRed5D6DCount = 0; ?>
                              @if(!$isEditSystemUser && count($data['package5D6DOption']) > 0)
                              @foreach($data['package5D6DOption'] as $assignedPackageList)
                              <?php if (!$assignedPackageList->red_package)
                                 continue;
                                 else
                                 $packageRed5D6DCount++;
                                 ?>
                              <div class="col-md-2">
                                 <div class="form-group m-b-10">
                                    <div class="radio radio-css m-b-20">
                                       <input type="radio" name="red_5D6D_package" id="package5D6D_checkbox_{{$assignedPackageList->package_id}}" value="{{$assignedPackageList->package_id}}"  class="radio_button_5D6D" data-rowname="{{ $assignedPackageList->package_name }}"
                                       @if(old('red_5D6D_package')==$assignedPackageList->package_id)
                                           checked="checked"
                                       @elseif($assignedPackageList->assigned && $assignedPackageList->package_id == $data['userToEdit']->assigned_red_package_5D6D_id)
                                       checked="checked"
                                       @endif
                                       @if (!$isLoginUserSystem && $assignedPackageList->red_package && $data['setting']['allow_red_package_control']->setting_value == 0)
                                       onclick="showRedPackagePermissionDeniedError()"
                                       @endif
                                       ></input>
                                       <label for="package5D6D_checkbox_{{$assignedPackageList->package_id}}"> 
                                       <a href="#package_5D6D_dialog" id="package5D6D_{{$assignedPackageList->package_id}}" data-toggle="modal" class="package_5D6D_table" 
                                          data-rowid="{{$assignedPackageList->package_id}}" data-rowtypeid="{{$assignedPackageList->package_type_id}}" data-rowuserid="{{$data['userToEdit']->id}}"
                                          data-rowpriviledge="0" data-rowredpack="1">
                                       {{$assignedPackageList->package_name}}</a></label>
                                    </div>
                                 </div>
                              </div>
                              @endforeach
                              @endif
                              @if ($packageRed5D6DCount > 0)
                              <div class="col-md-2">
                                 <div class="form-group m-b-10">
                                    <div class="radio radio-css m-b-20">
                                       <input type="hidden" id="radio_button_5D6D_pre" />
                                       <input type="radio" id="red_5D6D_package" name="red_5D6D_package" value="0" class="radio_button_5D6D" 
                                       @if (!$isLoginUserSystem && $data['setting']['allow_red_package_control']->setting_value == 0)
                                       onclick="showRedPackagePermissionDeniedError()"
                                       @endif
                                       />
                                       <label for="red_5D6D_package">@lang('package.UNASSIGNED_PACKAGE')</label>
                                    </div>
                                 </div>
                              </div>
                              @else
                              @lang('package.NO_PACKAGE')
                              @endif
                           </div>
                        </div>
                        <!-- end tab-pane -->
                     </div>
                     <!-- end tab-content -->
                  </div>
               </div>
               <!-- end panel -->	
               <!-- begin panel -->
               <div class="panel panel-inverse" <?php if($isSubAccount){ echo "style='display:none'";} ?>>
                  <!-- begin panel-heading -->
                  <div class="panel-heading">
                     <div class="panel-heading-btn">
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
                        <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
                     </div>
                     <h4 class="panel-title">@lang('user.BET_SETTING')</h4>
                  </div>
                  <!-- end panel-heading -->
                  <!-- begin panel-body -->
                  <div class="panel-body">
                     @if(!$isEditSystemUser)
                     <div class="row row-space-10">
                        <div class="col-md-4" id="div_3D4D"
                        @if (count($data['assigned3D4DPackageList']) == 0)
                        style="display:none"
                        @endif
                        >
                        <div class="form-group m-b-10">
                           <label>@lang('package.PACKAGE_3D4D') @lang('package.PACKAGE')</label>
                           <select class="form-control" name="activated_3D4D_package" id="activated_3D4D_package">
                           @foreach($data['assigned3D4DPackageList'] as $assignedPackageList)
                           <option value="{{$assignedPackageList->package_id}}"
                           @if($assignedPackageList->package_id == old('activated_3D4D_package',$data['userToEdit']->activated_package_3D4D_id))
                           selected
                           @endif
                           >{{$assignedPackageList->package_name}}</option>
                           @endforeach
                           </select>
                        </div>
                     </div>
                     <div class="col-md-4"  id="div_5D6D" @if (count($data['assigned5D6DPackageList']) == 0)
                     style="display:none"
                     @endif>
                     <div class="form-group m-b-10">
                        <div>
                           <label>@lang('package.PACKAGE_5D6D') @lang('package.PACKAGE')</label>
                           <select class="form-control" name="activated_5D6D_package" id="activated_5D6D_package">
                           @foreach($data['assigned5D6DPackageList'] as $assignedPackageList)
                           <option value="{{$assignedPackageList->package_id}}"
                           @if($assignedPackageList->package_id == old('activated_5D6D_package',$data['userToEdit']->activated_package_5D6D_id))
                           selected
                           @endif
                           >{{$assignedPackageList->package_name}}</option>
                           @endforeach
                           </select>
                        </div>
                     </div>
                  </div>
                  <div class="col-md-4" style="display:none">
                     <div class="form-group m-b-10">
                        <div>
                           <label>@lang('package.PACKAGE_3D4D') @lang('package.PACKAGE')</label>
                           <select class="form-control">
                           @foreach($data['assigned3D4DPackageList'] as $assignedPackageList)
                           <option value="{{$assignedPackageList->package_id}}"
                           @if($assignedPackageList->package_id == $data['userToEdit']->activated_package_3D4D_id)
                           selected
                           @endif
                           >{{$assignedPackageList->package_name}}</option>
                           @endforeach
                           </select>
                        </div>
                     </div>
                  </div>
               </div>
               @endif
               <div class="row row-space-10">
                  <div class="col-md-4">
                     <div class="form-group m-b-10">
                        <label for="exampleInputPassword1">@lang('user.ARRANGEMENT')</label>
                        <select class="form-control" name="arrangement" id="arrangement">
                        @foreach($data['arrangement'] as $arrangement)
                        <option value="{{$arrangement->arrangement_id}}"
                        @if (old('arrangement', $data['userToEdit']->arrangement_id) == $arrangement->arrangement_id)
                        selected
                        @endif
                        >
                        {{$arrangement->arrangement_name}}
                        </option>
                        @endforeach
                        </select>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group m-b-10">
                        <label for="exampleInputPassword1">@lang('user.MODE')</label>
                        <select class="form-control" name="mode" id="mode">
                        <option value="1"
                        @if (old('mode', $data['userToEdit']->mode) == 1)
                        selected
                        @endif
                        > * </option>
                        <option value="0"
                        @if (old('mode', $data['userToEdit']->mode) == 0)
                        selected
                        @endif
                        > / </option>
                        </select>
                     </div>
                  </div>
                  <div class="col-md-4">
                     <div class="form-group m-b-10">
                        <label for="exampleInputPassword1">@lang('user.BOX')</label>
                        <select class="form-control" name="boxes" id="boxes">
                        @foreach($data['boxes'] as $boxes)
                        <option value="{{$boxes->box_id}}"
                        @if (old('boxes', $data['userToEdit']->box_id) == $boxes->box_id)
                        selected
                        @endif
                        >
                        {{$boxes->box_format}}
                        </option>
                        @endforeach
                        </select>
                     </div>
                  </div>
               </div>
               <div class="row row-space-10">
                  <div class="col-md-4">
                     <div class="form-group m-b-10">
                        <label for="exampleInputPassword1">@lang('user.POOLS_SHORTCODE')</label>
                        <select class="form-control">
                           <option>MPTSBKW</option>
                        </select>
                     </div>
                  </div>
               </div>
               <div class="row">&nbsp;</div>
               <!-- end table-responsive -->
               <div class="col-md-12">
                  <button type="submit" id="btnCreateMember" class="btn btn_submit btn-primary"
                     <?php if(!($data['loginUser']->downline_create && $data['user']->downline_create && $data['loginUser']->state == 0) && !$isLoginUser) echo "onclick='showPermissionDeniedError()'" ?>
                     style="margin: 5px"><i class="fa fa-save"></i> &nbsp;@lang('user.SAVE')</button>
                  <a href="/memberlist/{{$data['userToEdit']->user_upline}}" class="btn"><i class="fa fa-arrow-left "></i> @lang('user.BACK_TO_MEMBER_LIST')</a>						
               </div>
         </div>
         <!-- end panel -->	
      </div>
      <input type="hidden"  id="unassignedPackage" name="unassignedPackage"/>
      <input type="hidden" value="{{$data['user']->id}}" id="id"/>
      </form>
      <!-- end #content -->
      <!-- begin scroll to top btn -->
      <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
      <!-- end scroll to top btn -->
   </div>
</div>
<div class="tab-pane fade" id="nav-tab-2">
	<!-- begin panel -->
   <div class="panel panel-inverse" >
      <!-- begin panel-heading -->
      <div class="panel-heading">
         <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
         </div>
         <h4 class="panel-title">Edit Eat Map Reference for: {{ $data['userToEdit']->username }}</h4>
      </div>
      <div id="errorMessageEatMap"></div>
      {!! Form::open(['url' => '/eatmap/savereference', 'class' => 'form-horizontal', 'data-parsley-validate' => 'true', 'id' => 'eatmapform']) !!}
      <div class="panel-body">      		
       		<div class="row">&nbsp;</div>
            <!-- start table 1 -->
            @include("include.eatMapUpline")
            
         	<div class="row">&nbsp;</div>
         <div class="col-md-12" style="padding-left:0px;">
            <button type="submit" id="btnSaveEatMapReference" class="btn_submit btn btn-primary" style="margin-bottom: 10px"><i class="fa fa-save"></i> Save</button>
         </div>
      </div>
      	<input type="hidden" id="hidUplineIdEatRef" name="hidUplineIdEatRef" value="{{ $data['user']->id }}" />
		<input type="hidden" id="hidTargetIdEatRef" name="hidTargetIdEatRef" value="{{ $data['userToEdit']->id }}" />
	  {!! Form::close() !!}
	</div>
	<!-- End Panel -->
   <!-- begin panel -->
   <div class="panel panel-inverse" >
      <!-- begin panel-heading -->
      <div class="panel-heading">
         <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
         </div>
         <h4 class="panel-title">Eat Map for: {{ $data['userToEdit']->username }} (View Only)</h4>
      </div>      
      <div class="panel-body">
       		<div class="row">&nbsp;</div>
       		
            <!-- start table 2 -->
            @include("include.targetMap")
            
         	<div class="row">&nbsp;</div>
         <div class="col-md-12" style="padding-left:0px;">
            <button id="btnSave" class="btn_submit btn btn-primary" style="margin-bottom: 10px"><i class="fa fa-save"></i> Save</button>
         </div>
      </div>
	</div>
	<!-- End Panel -->
</div>
<div class="tab-pane fade" id="nav-tab-3">
   <div class="panel panel-inverse">
      <!-- begin panel-heading -->
      <div class="panel-heading">
         <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
         </div>
         <h4 class="panel-title">Order Rate</h4>
      </div>
      <div class="panel-body">
         <div class="table-responsive">
            <table class="table table-striped table-bordered">
               <thead>
                  <th colspan="7" style="text-align:center;">Order Rate</th>
               </thead>
               <tbody>
                  <tr>
                     <td>å¤§</td>
                     <td>å°�</td>
                     <td>4A</td>
                     <td>4SB</td>
                     <td>4SC</td>
                     <td>4SD</td>
                     <td>4SE</td>
                  </tr>
                  <tr>
                     <td style="padding:0px;"><input type="text" value="1"></input></td>
                     <td style="padding:0px;"><input type="text" value="1"></input></td>
                     <td style="padding:0px;"><input type="text" value="1"></input></td>
                     <td style="padding:0px;"><input type="text" value="1"></input></td>
                     <td style="padding:0px;"><input type="text" value="1"></input></td>
                     <td style="padding:0px;"><input type="text" value="1"></input></td>
                     <td style="padding:0px;"><input type="text" value="1"></input></td>
                  </tr>
                  <tr>
                     <td>3A</td>
                     <td>ABC</td>
                     <td>2A</td>
                     <td>2ABC</td>
                     <td></td>
                     <td></td>
                     <td></td>
                  </tr>
                  <tr>
                     <td style="padding:0px;"><input type="text" value="1"></input></td>
                     <td style="padding:0px;"><input type="text" value="1"></input></td>
                     <td style="padding:0px;"><input type="text" value="1"></input></td>
                     <td style="padding:0px;"><input type="text" value="1"></input></td>
                     <td></td>
                     <td></td>
                     <td></td>
                  </tr>
                  <tr>
                     <td>5D</td>
                     <td>6D</td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                  </tr>
                  <tr>
                     <td style="padding:0px;"><input type="text" value="1"></input></td>
                     <td style="padding:0px;"><input type="text" value="1"></input></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                  </tr>
               </tbody>
            </table>
            <table class="table table-striped table-bordered">
               <thead>
                  <th colspan="7" style="text-align:center;">Fake Receipt Rate</th>
               </thead>
               <tbody>
                  <tr>
                     <td>å¤§</td>
                     <td>å°�</td>
                     <td>4A</td>
                     <td>4SB</td>
                     <td>4SC</td>
                     <td>4SD</td>
                     <td>4SE</td>
                  </tr>
                  <tr>
                     <td style="padding:0px;"><input type="text" value="1"></input></td>
                     <td style="padding:0px;"><input type="text" value="1"></input></td>
                     <td style="padding:0px;"><input type="text" value="1"></input></td>
                     <td style="padding:0px;"><input type="text" value="1"></input></td>
                     <td style="padding:0px;"><input type="text" value="1"></input></td>
                     <td style="padding:0px;"><input type="text" value="1"></input></td>
                     <td style="padding:0px;"><input type="text" value="1"></input></td>
                  </tr>
                  <tr>
                     <td>3A</td>
                     <td>ABC</td>
                     <td>2A</td>
                     <td>2ABC</td>
                     <td></td>
                     <td></td>
                     <td></td>
                  </tr>
                  <tr>
                     <td style="padding:0px;"><input type="text" value="1"></input></td>
                     <td style="padding:0px;"><input type="text" value="1"></input></td>
                     <td style="padding:0px;"><input type="text" value="1"></input></td>
                     <td style="padding:0px;"><input type="text" value="1"></input></td>
                     <td></td>
                     <td></td>
                     <td></td>
                  </tr>
                  <tr>
                     <td>5D</td>
                     <td>6D</td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                  </tr>
                  <tr>
                     <td style="padding:0px;"><input type="text" value="1"></input></td>
                     <td style="padding:0px;"><input type="text" value="1"></input></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                     <td></td>
                  </tr>
               </tbody>
            </table>
         </div>
         <div class="col-md-12" style="padding-left:0px;">
            <button id="btnSave" class="btn_submit btn btn-primary" style="margin-bottom: 10px"><i class="fa fa-save"></i> Save</button>
         </div>
         <!--begin spacing-->
         <br/><br/>
         <!--end spacing-->
         <div class="table-responsive">
            <table class="table table-striped table-bordered">
               <thead>
                  <th>Order Amount</th>
                  <th></th>
                  <th>å¤§</th>
                  <th>å°�</th>
                  <th>3A</th>
                  <th>3C</th>
                  <th>4A</th>
               </thead>
               <tbody>
                  <tr>
                     <td style="padding:0px;"><input type="text" value="0.00"></input></td>
                     <td>=</td>
                     <td style="padding:0px;"><input type="text" value="0.00"></input></td>
                     <td style="padding:0px;"><input type="text" value="0.00"></input></td>
                     <td style="padding:0px;"><input type="text" value="0.00"></input></td>
                     <td style="padding:0px;"><input type="text" value="0.00"></input></td>
                     <td style="padding:0px;"><input type="text" value="0.00"></input></td>
                  </tr>
                  <tr>
                     <td style="padding:0px;"><input type="text" value="0.00"></input></td>
                     <td>=</td>
                     <td style="padding:0px;"><input type="text" value="0.00"></input></td>
                     <td style="padding:0px;"><input type="text" value="0.00"></input></td>
                     <td style="padding:0px;"><input type="text" value="0.00"></input></td>
                     <td style="padding:0px;"><input type="text" value="0.00"></input></td>
                     <td style="padding:0px;"><input type="text" value="0.00"></input></td>
                  </tr>
                  <tr>
                     <td style="padding:0px;"><input type="text" value="0.00"></input></td>
                     <td>=</td>
                     <td style="padding:0px;"><input type="text" value="0.00"></input></td>
                     <td style="padding:0px;"><input type="text" value="0.00"></input></td>
                     <td style="padding:0px;"><input type="text" value="0.00"></input></td>
                     <td style="padding:0px;"><input type="text" value="0.00"></input></td>
                     <td style="padding:0px;"><input type="text" value="0.00"></input></td>
                  </tr>
                  <tr>
                     <td style="padding:0px;"><input type="text" value="0.00"></input></td>
                     <td>=</td>
                     <td style="padding:0px;"><input type="text" value="0.00"></input></td>
                     <td style="padding:0px;"><input type="text" value="0.00"></input></td>
                     <td style="padding:0px;"><input type="text" value="0.00"></input></td>
                     <td style="padding:0px;"><input type="text" value="0.00"></input></td>
                     <td style="padding:0px;"><input type="text" value="0.00"></input></td>
                  </tr>
               </tbody>
            </table>
         </div>
         <div class="col-md-12" style="padding-left:0px;">
            <button id="btnSave" class="btn_submit btn btn-primary" style="margin-bottom: 10px"><i class="fa fa-plus-circle"></i> Add Bao</button>
         </div>
      </div>
   </div>
</div>
<!-- end tab-pane -->
<!-- begin tab-pane -->
<div class="tab-pane fade" id="nav-tab-4">
   <div class="panel panel-inverse">
      <!-- begin panel-heading -->
      <div class="panel-heading">
         <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
         </div>
         <h4 class="panel-title">Account Setting</h4>
      </div>
      <div class="panel-body">
         <h5 style="width:100px; display:inline; margin-right:145px;">Status:</h5>
         <select class="form-control" style="width:125px; height:30px; margin-top:5px; margin-right:20px; font-size:12px; padding-left:5px; display:inline;">
            <option>ACTIVE</option>
            <option>SUSPEND</option>
         </select>
         <br/>
         <h5 style="width:100px; display:inline; margin-right:55px;">Allow Open Account:</h5>
         <select class="form-control" style="width:125px; height:30px; margin-top:5px; margin-right:20px; font-size:12px; padding-left:5px; display:inline;">
            <option>Yes</option>
            <option>No</option>
         </select>
         <br/>
         <h5 style="width:100px; display:inline; margin-right:95px;">Open Package:</h5>
         <select class="form-control" style="width:125px; height:30px; margin-top:5px; margin-right:20px; font-size:12px; padding-left:5px; display:inline;">
            <option>Yes</option>
            <option>No</option>
         </select>
         <br/>
         <h5 style="width:100px; display:inline; margin-right:110px;">Web Cancel:</h5>
         <select class="form-control" style="width:125px; height:30px; margin-top:5px; margin-right:20px; font-size:12px; padding-left:5px; display:inline;">
            <option>Yes</option>
            <option>No</option>
         </select>
         <br/>
         <h5 style="width:100px; display:inline; margin-right:75px;">Payment Records:</h5>
         <select class="form-control" style="width:125px; height:30px; margin-top:5px; margin-right:20px; font-size:12px; padding-left:5px; display:inline;">
            <option>Yes</option>
            <option>No</option>
         </select>
         <br/>
         <h5 style="width:100px; display:inline; margin-right:135px;">Jackpot:</h5>
         <select class="form-control" style="width:125px; height:30px; margin-top:5px; margin-right:20px; font-size:12px; padding-left:5px; display:inline;">
            <option>Yes</option>
            <option>No</option>
         </select>
      </div>
   </div>
</div>
<!-- end tab-pane -->
<!-- begin tab-pane -->
<div class="tab-pane fade" id="nav-tab-5">
   <div class="panel panel-inverse">
      <!-- begin panel-heading -->
      <div class="panel-heading">
         <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
         </div>
         <h4 class="panel-title">Company Bet Code</h4>
      </div>
      <div class="panel-body">
         <div class="table-responsive">
            <table class="table table-striped table-bordered">
               <thead>
                  <th></th>
                  <th>M</th>
                  <th>P</th>
                  <th>T</th>
                  <th>S</th>
                  <th>B</th>
                  <th>K</th>
                  <th>W</th>
               </thead>
               <tbody>
                  <tr>
                     <td>Upline</td>
                     <td>1</td>
                     <td>2</td>
                     <td>3</td>
                     <td>4</td>
                     <td>6</td>
                     <td>7</td>
                     <td>5</td>
                  </tr>
                  <tr>
                     <td>peterpan</td>
                     <td style="padding:0px;"><input type="text" value="1" style="text-align:left; padding-left:5px; height:40px;"></input></td>
                     <td style="padding:0px;"><input type="text" value="2" style="text-align:left; padding-left:5px; height:40px;"></input></td>
                     <td style="padding:0px;"><input type="text" value="3" style="text-align:left; padding-left:5px; height:40px;"></input></td>
                     <td style="padding:0px;"><input type="text" value="4" style="text-align:left; padding-left:5px; height:40px;"></input></td>
                     <td style="padding:0px;"><input type="text" value="6" style="text-align:left; padding-left:5px; height:40px;"></input></td>
                     <td style="padding:0px;"><input type="text" value="7" style="text-align:left; padding-left:5px; height:40px;"></input></td>
                     <td style="padding:0px;"><input type="text" value="5" style="text-align:left; padding-left:5px; height:40px;"></input></td>
                  </tr>
               </tbody>
            </table>
         </div>
      </div>
   </div>
</div>
</div>
</div>
<form class="form-horizontal" data-parsley-validate="true" name="package_3D4D_form" id="package_3D4D_form"  method="POST">
   {{ csrf_field() }}
   <div class="modal fade" id="package_3D4D_dialog">
      <div class="modal-dialog modal-lg">
         <div class="modal-content">
            <div class="modal-header">
               <h4 class="modal-title">@lang('package.EDIT_PACKAGE'): <label id="modal_package_3D4D"></label></h4>
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            </div>
            <div class="modal-body">
               <div class="panel-body">
                  <input type="hidden" value="{{csrf_token()}}" name="_token" id="_token" />
                  <div class="table-responsive">
                     @include("include.table4D")
                  </div>
                  <br/><br/><br/>
                  <div class="table-responsive">
                     @include("include.table4DS")
                  </div>
                  <div>
                     <input type="hidden" id="selected_3D4D_package_owner_id" name="selected_package_owner_user_id"/>
                     <input type="hidden" id="selected_3D4D_package_type_id" name="selected_package_type_id"/>
                     <input type="hidden" id="selected_3D4D_package_id" name="selected_package_id"/>
                  </div>
               </div>
               <!-- end panel-body -->
            </div>
            <div class="modal-footer">
               <a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">@lang('general.CLOSE')</a>
               @if ($data['loginUser']->package_control && $data['user']->package_control && $data['loginUser']->state == 0)
               <button class="btn btn-sm btn-success">@lang('general.SAVE')</button>
               @endif
            </div>
         </div>
      </div>
   </div>
</form>
<form class="form-horizontal" data-parsley-validate="true" name="package_5D6D_form" id="package_5D6D_form"  method="POST">
   {{ csrf_field() }}
   <div class="modal fade" id="package_5D6D_dialog">
      <div class="modal-dialog modal-lg">
         <div class="modal-content">
            <div class="modal-header">
               <h4 class="modal-title">@lang('package.EDIT_PACKAGE'): <label id="modal_package_5D6D"></label></h4>
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">x</button>
            </div>
            <div class="modal-body">
               <div class="panel-body">
                  <input type="hidden" value="{{csrf_token()}}" name="_token" id="_token" />
                  <div class="table-responsive">
                     @include("include.table5D6D")
                  </div>
                  <div>
                     <input type="hidden" id="selected_5D6D_package_owner_id" name="selected_package_owner_user_id"/>
                     <input type="hidden" id="selected_5D6D_package_type_id" name="selected_package_type_id"/>
                     <input type="hidden" id="selected_5D6D_package_id" name="selected_package_id"/>
                  </div>
               </div>
               <!-- end panel-body -->
            </div>
            <div class="modal-footer">
               <a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">@lang('general.CLOSE')</a>
               @if ($data['loginUser']->package_control && $data['user']->package_control && $data['loginUser']->state == 0)
               <button type="submit" class="btn btn-sm btn-success">@lang('general.SAVE')</button>
               @endif	
            </div>
         </div>
      </div>
   </div>
</form>
@stop
@section("page_script")
<script src="/assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="/assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
<script src="/assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script src="/assets/plugins/bootstrap-sweetalert/sweetalert.min.js"></script>
<script src="/assets/plugins/parsley/dist/parsley.js"></script>
    
	<script>
    var referencePackageUnassigned = [];
    var referencePackageEdit;
    
    function showRedPackagePermissionDeniedError()
    {
        alert("Not allow to check/uncheck RED Package");
        event.preventDefault();
        event.stopPropagation();
    }
    
	function recordReferencedPackageRemovel(checked,val)
    {
        if (!checked && referencePackageUnassigned.indexOf(val) != -1)
        {
            referencePackageUnassigned.push(val); 
        }
        else
        {
            removeArr(referencePackageUnassigned,val);
        }
        document.getElementById("unassignedPackage").value= referencePackageUnassigned;
    }
    
    function removeArr(arr) {
    var what, a = arguments, L = a.length, ax;
    while (L > 1 && arr.length) {
        what = a[--L];
        while ((ax= arr.indexOf(what)) !== -1) {
            arr.splice(ax, 1);
        }
    }
    return arr;
}
    
function showPermissionDeniedError()
{
    alert("Not allow to change setting");
    event.preventDefault();
    event.stopPropagation();
}

// This fuunction is to show the upline reference eat map from DB
function retrieveUplineReference(userId, targetId)
{
	$.ajax({
        type: 'GET',
        url: '\\eatMapReference\\' + userId + "\\" + targetId,
        success: function(data) {
            console.log(data);
        	renderUplineReference(data);
        }
    });
}

// This function is to show out the upline reference
// after getting from DB
function renderUplineReference(data)
{
	var allEatReferences = data.data.uplineReference;
	console.log(allEatReferences);
	var uplineDivs = document.getElementsByClassName("uplineDiv");
	for(var i = 0; i < uplineDivs.length; i++)
	{
		var _id = uplineDivs[i].getAttribute("id");
		_id = _id.replace("divUpline_", "");
		console.log(_id);
		uplineDivs[i].innerHTML = "(" + parseFloat(allEatReferences[_id].value).toFixed(2) + ")";
	}	
}
    
$(document).ready(function() {

	var cars = ["mg", "kd", "tt", "sg", "sb", "stc", "sw"];
	var car_images =
		[
			"../assets/img/lotto/bet-logo-magnum.png",
			"../assets/img/lotto/bet-logo-damacai-inversed.png",
			"../assets/img/lotto/bet-logo-toto-inversed.png",
			"../assets/img/lotto/bet-logo-singapore-pools.png",
			"../assets/img/lotto/bet-logo-big-cash.png",
			"../assets/img/lotto/bet-logo-sabah88.png",
			"../assets/img/lotto/bet-logo-stc.png"
			];

	// showing all the reference eat map
	retrieveUplineReference($('#hidUplineId').val(), $('#hidTargetId').val());

	// triggered when any input in the "eatmap_input" classes being changed
	var warningCount = 0;
	$(document).on('change', '.eatmap_input', function() {
		  
		$var = $(this).val();
        $refVarString = $(this).attr('id').replace("txt", "divUpline");
        $refvar = parseInt(document.getElementById($refVarString).innerHTML.replace("(","").replace(")",""));
        if($var !== "" && $.isNumeric($var))
        {
            if ($refvar != -1)
            {
            	if ($var > $refvar) {
                    warningCount++;
                    $(this).css('backgroundColor', 'yellow');
                } else 
                {
                    warningCount--;
                    $(this).css('backgroundColor', 'white');
                }
            } 
        }
	});

	// trigger when the save button is pressed to save the reference of eat map
	$('#eatmapform').on('submit', function(e){
       // code
	       if (warningCount > 0)
	       {
	    	   document.getElementById("errorMessageEatMap").innerHTML = "Some values are over range, please correct them.";
	    	   document.getElementById("errorMessageEatMap").classList.add("alert")
	    	   document.getElementById("errorMessageEatMap").classList.add("alert-danger");
	    	   e.preventDefault();
	       }
    });
	

    $('#radio_button_5D6D_pre').val($('input[name="red_5D6D_package"]:checked').val());
    $('#radio_button_3D4D_pre').val($('input[name="red_3D4D_package"]:checked').val());
    $('.checkbox_3D4D').change(function() {
        $needCheck = false;
        $value = $(this).val();
        $text = $(this).data("rowname");
        oldVal =0;
        <?php 
        if (old('activated_3D4D_package') == null)
            echo "oldVal = 0;";
        else
            echo "oldVal = ".old('activated_3D4D_package').";";
        ?>
        if (oldVal == $value)
        {
            $needCheck = true;
        }
        
        recordReferencedPackageRemovel(this.checked, $(this).val());
        $("#activated_3D4D_package option[value='" + $value + "']").remove();
        if (this.checked) 
        {
            $optionVal = "<option value='" + $value + "' ";
            if ($needCheck)
            {
                $optionVal = $optionVal +"selected";
            }
            
            $optionVal = $optionVal +" >" + $text + "</option>";
            
            $("#activated_3D4D_package").append($optionVal);
        }
        
        if ($('#activated_3D4D_package option').length == 0) 
        {
            $("#div_3D4D").css("display", 'none');
        } 
        else 
        {
            $("#div_3D4D").css("display", 'block');
        }
    });

    $('.checkbox_5D6D').change(function() {
        $value = $(this).val();
        $text = $(this).data("rowname");
        $needCheck = false;
        recordReferencedPackageRemovel(this.checked, $(this).val());
        oldVal =0;
        <?php 
        if (old('activated_5D6D_package') == null)
            echo "oldVal = 0;";
        else
            echo "oldVal = ".old('activated_5D6D_package').";";
        ?>
        if (oldVal == $value)
        {
            $needCheck = true;
        }
        $("#activated_5D6D_package option[value='" + $value + "']").remove();
        if (this.checked) 
        {
            $optionVal = "<option value='" + $value + "' ";
            
            if ($needCheck)
            {
                $optionVal = $optionVal +"selected";
            }
            $optionVal = $optionVal +" >" + $text + "</option>";
            
            $("#activated_5D6D_package").append($optionVal);
        }
        
        if ($('#activated_5D6D_package option').length == 0) 
        {
            $("#div_5D6D").css("display", 'none');
        } 
        else 
        {
            $("#div_5D6D").css("display", 'block');
        }
    });
    
    $('.radio_button_5D6D').change(function() 
    {
        $needCheck = false;
        $value = $(this).val();
        $text = $(this).data("rowname");
        <?php 
        if (old('activated_5D6D_package') == null)
            echo "oldVal = 0;";
        else
            echo "oldVal = ".old('activated_5D6D_package').";";
        ?>
        if (oldVal == $value)
        {
            $needCheck = true;
        }
        $("#activated_5D6D_package option[value='" + $('#radio_button_5D6D_pre').val() + "']").remove();
        if ( $value != 0)
        {
            $optionVal = "<option value='" + $value + "' ";
            
            if ($needCheck)
            {
                $optionVal = $optionVal +"selected";
            }
            $optionVal = $optionVal +" >" + $text + "</option>";
            
            $("#activated_5D6D_package").append($optionVal);
        }
        if ($('#activated_5D6D_package option').length == 0) 
        {
            $("#div_5D6D").css("display", 'none');
        } 
        else 
        {
            $("#div_5D6D").css("display", 'block');
        }
        $('#radio_button_5D6D_pre').val($(this).val());
    });
    
    $('.radio_button_3D4D').change(function() 
    {
        $needCheck = false;
        
        $value = $(this).val();
        $text = $(this).data("rowname");
        oldVal =0;
        <?php 
        if (old('activated_3D4D_package') == null)
            echo "oldVal = 0;";
        else
            echo "oldVal = ".old('activated_3D4D_package').";";
        ?>
        if (oldVal == $value)
        {
            $needCheck = true;
        }
        
        $("#activated_3D4D_package option[value='" + $('#radio_button_3D4D_pre').val() + "']").remove();
        if ( $value != 0)
        {
            $optionVal = "<option value='" + $value + "' ";
            
            if ($needCheck)
            {
                $optionVal = $optionVal +"selected";
            }
            $optionVal = $optionVal +" >" + $text + "</option>";
            
            $("#activated_3D4D_package").append($optionVal);
        }
        if ($('#activated_3D4D_package option').length == 0)
        {
            $("#div_3D4D").css("display", 'none');
        } 
        else
        {
            $("#div_3D4D").css("display", 'block');
        }
        $('#radio_button_3D4D_pre').val($(this).val());
    });
    
    $("#selectAll3D4D").change(function() {
        $('.select_all_3D4D').prop('checked', $(this).prop('checked'));
        $(".select_all_3D4D").trigger("change");
    });
    
    $("#selectAll5D6D").change(function() {
        $('.select_all_5D6D').prop('checked', $(this).prop('checked'));
        $(".select_all_5D6D").trigger("change");
    });

    $('.checkbox_3D4D').each(function () {
        $(this).trigger("change");
    });
    $('.checkbox_5D6D').each(function () {
        $(this).trigger("change");
    });
    $('.radio_button_3D4D:radio:checked').each(function () {
        $(this).trigger("change");
    });
    $('.radio_button_5D6D:radio:checked').each(function () {
        $(this).trigger("change");
    });
    
    $(".table_input").change(function() {
        $var = $(this).val();
        $refVarString = $(this).attr('id') + "_default";
        $refvar = parseInt($("#" + $refVarString).text());
        if($var !== "" && $.isNumeric($var))
        {
            if ($var > $refvar) {
                warningCount++;
                $(this).css('backgroundColor', 'yellow');
            } else 
            {
                warningCount--;
                $(this).css('backgroundColor', 'white');
            }
        }
    });
    
    $(".package_5D6D_table").click(function() {
        $canEdit = $(this).data("rowpriviledge");
        
        $isRedPack =  $(this).data("rowredpack");
        $(".table_input").css('background', 'white');
        $("#package_5D6D_form").parsley().reset();
        referencePackageEdit = $(this).data("rowreferencedcount");
        warningCount = 0;
        $.ajax({
            type: 'GET',
            url: '\\packageDetail\\' + $(this).data("rowuserid") + '\\' + $(this).data("rowtypeid") + '\\' + $(this).data("rowid"),
            success: function(data) {
                if ($canEdit == 0 || $canEdit == null || $canEdit.length === 0 || <?php echo($isLoginUser); ?>|| $isRedPack) {
                    $("#package_5D6D_form input").prop("disabled", true);
                    $(".save_btn").css("display", 'none');
                } else {
                    $("#package_5D6D_form input").prop("disabled", false);
                    $(".save_btn").css("display", 'inline');
                }
                updateView(data);
            }
        });
    });

    $(".package_3D4D_table").click(function() {
        $canEdit = $(this).data("rowpriviledge");
        $isRedPack =  $(this).data("rowredpack");
        $(".table_input").css('background', 'white');
        $("#package_3D4D_form").parsley().reset();
        referencePackageEdit = $(this).data("rowreferencedcount");
        warningCount = 0;
        $.ajax({
            type: 'GET',
            url: '\\packageDetail\\' + $(this).data("rowuserid") + '\\' + $(this).data("rowtypeid") + '\\' + $(this).data("rowid"),
            success: function(data) {
                if ($canEdit == 0 || $canEdit == null || $canEdit.length === 0 || <?php echo($isLoginUser); ?> || $isRedPack) {
                    $("#package_3D4D_form input").prop("disabled", true);
                    $(".save_btn").css("display", 'none');
                } else {
                    $("#package_3D4D_form input").prop("disabled", false);
                    $(".save_btn").css("display", 'inline');
                }
                updateView(data);
            }
        });
    });

    $('#edit_form').on('submit', function(e) {
        
        if ((referencePackageUnassigned.length>0) &&  !confirm('Warning, you have unassign package that reference by downline.'))
        {
            return false;
        }
    });
    
    $('#package_5D6D_form').on('submit', function(e) {
        $userId = $('#id').val();
        $packageId = $("#selected_5D6D_package_id").val();
        if ((warningCount > 0) && !confirm('Warning, you are adding value that is higher than reference value')) 
        {
            $('#package_5D6D_dialog').modal('toggle');
            return false;
        }
        
        if (referencePackageEdit > 0  && !confirm('Warning, you are changing package that referenced'))
        {
            $('#package_5D6D_dialog').modal('toggle');
            return false;
        }
        e.preventDefault();
        $(".save_btn").prop("disabled", true);
        $.ajax({
            type: 'POST',
            data: $('#package_5D6D_form').serialize(),
            url: '/downlinePackageUpdate/' + $userId,
            success: function(data) {
                $("#package5D6D_checkbox_" + $packageId).prop("checked", false).trigger("change");
                $("#package5D6D_" + $packageId).text(data.data['packageName']);
                $("#package5D6D_checkbox_" + $packageId).val(data.data['packageId']);
                $('#package_5D6D_dialog').modal('toggle');
                $("#package5D6D_checkbox_" + $packageId).attr('data-rowname', data.data['packageName']);
                $("#package5D6D_checkbox_" + $packageId).removeData('rowname');
                $("#package5D6D_" + $packageId).attr('data-rowid', data.data['packageId']);
                $("#package5D6D_" + $packageId).removeData('rowid');
                $("#package5D6D_checkbox_" + $packageId).prop("checked", true).trigger("change");
            }
        });
    });

    $('#package_3D4D_form').on('submit', function(e) {
        $userId = $('#id').val();
        $packageId = $("#selected_3D4D_package_id").val();
        if ((warningCount > 0) && !confirm('Warning, you are adding value that is higher than reference value')) 
        {
            $('#package_3D4D_dialog').modal('toggle');
            return false;
        }
        if (referencePackageEdit > 0  && !confirm('Warning, you are changing package that referenced'))
            {
            $('#package_5D6D_dialog').modal('toggle');
            return false;
        }
        $(".save_btn").prop("disabled", true);
        e.preventDefault();
        $.ajax({
            type: 'POST',
            data: $('#package_3D4D_form').serialize(),
            url: '/downlinePackageUpdate/' + $userId,
            success: function(data) {
                $("#package3D4D_checkbox_" + $packageId).prop("checked", false).trigger("change");
                $("#package3D4D_" + $packageId).text(data.data['packageName']);
                $("#package3D4D_checkbox_" + $packageId).val(data.data['packageId']);
                $('#package_3D4D_dialog').modal('toggle');
                $("#package3D4D_" + $packageId).attr('data-rowid', data.data['packageId']);
                $("#package3D4D_" + $packageId).removeData('rowid');
                $("#package3D4D_checkbox_" + $packageId).attr('data-rowname', data.data['packageName']);
                $("#package3D4D_checkbox_" + $packageId).removeData('rowname');
                $("#package3D4D_checkbox_" + $packageId).prop("checked", true).trigger("change");
            }
        });
    });

    function updateView(data) {
        $(".save_btn").prop("disabled", false);
        if (data.data['packageType']['package_type_name'] == "3D/4D") {
            $("#selected_3D4D_package_owner_id").val(data.data['user']['id']);
            $("#selected_3D4D_package_type_id").val(data.data['packageType']['package_type_id']);
            $("#selected_3D4D_package_id").val(data.data['packageId']);
            $("#modal_package_3D4D").text(data.data['selectedPackage']['package_name']);
            <?php
            foreach (TABLE_PROPERTIES as $x => $x_value)
            {
                foreach (TABLE_4D as $id) 
                {
                    echo "$('#".$x_value."_".$id."_default').text(data.data['referencePackage']['$id']['$x_value']);\r\n";
                    echo "$('#".$x_value."_$id').val(data.data['package']['$id']['$x_value']);\r\n";
                }
      
                foreach (TABLE_3D as $id) 
                {
                    echo "$('#".$x_value."_".$id."_default').text(data.data['referencePackage']['$id']['$x_value']);\r\n";
                    echo "$('#".$x_value."_$id').val(data.data['package']['$id']['$x_value']);\r\n";
                }
      
                foreach (TABLE_4DS as $id) 
                {
                    echo "$('#".$x_value."_".$id."_default').text(data.data['referencePackage']['$id']['$x_value']);\r\n";
                    echo "$('#".$x_value."_$id').val(data.data['package']['$id']['$x_value']);\r\n";
                }
            }
            ?>
        } else {
            //5d&6d
            $("#selected_5D6D_package_owner_id").val(data.data['user']['id']);
            $("#selected_5D6D_package_type_id").val(data.data['packageType']['package_type_id']);
            $("#selected_5D6D_package_id").val(data.data['packageId']);
            $("#modal_package_5D6D").text(data.data['selectedPackage']['package_name']);
            <?php 
            foreach (TABLE_PROPERTIES as $x => $x_value) 
            {
                foreach (TABLE_5D6D as $id) 
      	        {

      		        echo "$('#".$x_value."_".$id."_default').text(data.data['referencePackage']['$id']['$x_value']);\r\n";
      		        echo "$('#".$x_value."_$id').val(data.data['package']['$id']['$x_value']);\r\n";
      	        }
            }
            ?>
        }
    }
});
	</script>
	@stop