@extends("layouts.userlayout")

@section("headerScript")
<link href="/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/parsley/src/parsley.css" rel="stylesheet" />
<link href="/assets/css/package.css" rel="stylesheet" />
@stop
@section("body")
@include("include.phpVar")
<div>
   <!-- begin page-header -->
   <h1 class="page-header">@lang('user.CREATE_MEMBER') <small>Create a new member</small></h1>
   <!-- end page-header -->
   <form  action="/addMemberDetail/{{$data['user']->id}}" method="POST" data-parsley-validate="true">
      {{ csrf_field() }}
      <!-- begin panel -->
      <div class="panel panel-inverse">
         <!-- begin panel-heading -->
         <div class="panel-heading">
            <div class="panel-heading-btn">
               <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
               <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
               <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
               <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
            </div>
            <h4 class="panel-title">@lang('user.MEMBER_DETAIL')</h4>
         </div>
         <!-- end panel-heading -->
         <!-- begin panel-body -->
         @if(session('error'))
         <div class="alert alert-danger">{{Session::get('error')}}</div>
         @endif
         <div class="panel-body">
            <div class="row row-space-10">
               <div class="col-md-4">
                  <div class="form-group m-b-10">
                     <label for="username">@lang('user.USERNAME')</label>
                     <input type="text" class="form-control" name="username" id="username" value="{{ old('username') }}" data-parsley-pattern="^[_A-z0-9]*((-|\s)*[_A-z0-9])*$" data-parsley-required="true" placeholder="Enter username" />
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group m-b-10">
                     <label for="name">@lang('user.NICKNAME')</label>
                     <input type="text" class="form-control" id="name" name="name" value="{{ old('name') }}" data-parsley-pattern="^[_A-z0-9]*((-|\s)*[_A-z0-9])*$" data-parsley-required="true" placeholder="Enter name" />
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group m-b-10">
                     <label for="password">@lang('user.PASSWORD')</label>
                     <input type="password" class="form-control" id="password" value="{{ old('password') }}" name="password" data-parsley-pattern="^[_A-z0-9]*((-|\s)*[_A-z0-9])*$" data-parsley-required="true" placeholder="Enter password" />
                  </div>
               </div>
            </div>
            <div class="row row-space-10">
               <div class="col-md-4">
                  <div class="form-group m-b-10">
                     <label for="exampleInputPassword1">@lang('user.LEVEL')</label>
                     <select class="form-control" name="role" id="role">
                        @if(count($data['roleOption']) > 0)
                        @foreach($data['roleOption'] as $roleOption)
                        <option value="{{ $roleOption->role_id }}" <?php if(old('role') == $roleOption->role_id){echo "selected";} ?>>
                           {{ $roleOption->role_name }}
                        </option>
                        @endforeach
                        @endif
                     </select>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group m-b-10">
                     <label for="exampleInputPassword1">@lang('user.CURRENCY')</label>
                     <select class="form-control" name="currency" id="currency">
                        <option value="myr" <?php if(old('currency') == "myr"){echo "selected";} ?>>@lang('user.CURRENCY_MYR')</option>
                        <option value="sgd" <?php if(old('currency') == "sgd"){echo "selected";} ?>>@lang('user.CURRENCY_SGD')</option>
                     </select>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group m-b-10">
                     <label for="credit_limit">@lang('user.CREDIT_LIMIT') ({{$data['creditBalance']}})</label>
                     <input type="number" value="{{old('credit_limit')}}" data-parsley-required="true" class="form-control" name="credit_limit" id="credit_limit" min="0" max="{{ $data['creditBalance'] }}"
                        placeholder="Credit" data-parsley-type="number" />
                  </div>
               </div>
            </div>
            <div class="row row-space-10">
               <div class="col-md-4">
                  <div class="form-group m-b-10">
                     <label for="exampleInputPassword1">@lang('user.STATUS')</label>
                     <select class="form-control" name="state" id="state">
                        <option value="0" <?php if(old('state') == "0"){echo "selected";} ?>> @lang('user.STATUS_ACTIVE') </option>
                        <option value="1" <?php if(old('state') == "1"){echo "selected";} ?>> @lang('user.STATUS_INACTIVE') </option>
                        <option value="2" <?php if(old('state') == "2"){echo "selected";} ?>> @lang('user.STATUS_SUSPEND') </option>
                     </select>
                  </div>
               </div>
               <?php 
                   $real_position_3d4d = $data['user']->position_3d4d;
                   $real_position_5d6d = $data['user']->position_5d6d;               
               ?>
               <div class="col-md-4">
                  <div class="form-group m-b-10">
                     <label for="exampleInputPassword1">@lang('user.POSITION_3D4D')</label>                     
                     <select class="form-control" name="selPosition3d4d">
                     <?php 
                        for ($curr = $real_position_3d4d; $curr >= 0; $curr--)
                        {
                     ?>
                        <option><?php echo $curr ?>%</option>
                     <?php } ?>
                     </select>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group m-b-10">
                     <label for="exampleInputPassword1">@lang('user.POSITION_5D6D')</label>
                     <select class="form-control" name="selPosition5d6d">
                        <?php 
                            for ($curr = $real_position_5d6d; $curr >= 0; $curr--)
                            {
                         ?>
                        	<option>{{ $curr }}%</option>
                     	<?php } ?>
                     </select>
                  </div>
               </div>
            </div>
            <div class="row row-space-10">
               <div class="col-md-4">
                  <div class="form-group m-b-10">
                     <label for="exampleInputPassword1">@lang('user.MEMBER_CREATE')</label>
                     <div class="checkbox checkbox-css m-b-20">
                        <input type="checkbox" id="member_create" name="member_create" value="1" <?php if(old('member_create')){echo "checked";} ?> />
                        <label for="member_create"> </label>
                     </div>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group m-b-10">
                     <label for="exampleInputPassword1">@lang('user.FLOAT')</label>
                     <div class="checkbox checkbox-css m-b-20">
                        <input type="checkbox" id="nf_checkbox_css_2" id="float" name="float" value="1" <?php if(old('float')){echo "checked";} ?>  />
                        <label for="nf_checkbox_css_2"> </label>
                     </div>
                  </div>
               </div>
            </div>
            <!-- end panel-body -->
         </div>
      </div>
      <!-- end panel -->	
      <!-- begin panel -->
      <div class="panel panel-inverse">
         <!-- begin panel-heading -->
         <div class="panel-heading">
            <div class="panel-heading-btn">
               <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
               <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
               <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
               <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
            </div>
            <h4 class="panel-title">@lang('user.PACKAGE_TITLE')</h4>
         </div>
         <!-- end panel-heading -->
         <!-- begin panel-body -->
         <div class="panel-body">
            <!-- begin nav-tabs -->
            <ul class="nav nav-tabs">
               <li class="nav-items">
                  <a href="#default-tab-1" data-toggle="tab" class="nav-link active">
                  <span class="d-sm-none">@lang('package.PACKAGE_3D4D')</span>
                  <span class="d-sm-block d-none">@lang('package.PACKAGE_3D4D')</span>
                  </a>
               </li>
               <li class="nav-items">
                  <a href="#default-tab-2" data-toggle="tab" class="nav-link">
                  <span class="d-sm-none">@lang('package.PACKAGE_5D6D')</span>
                  <span class="d-sm-block d-none">@lang('package.PACKAGE_5D6D')</span>
                  </a>
               </li>
               <li class="">
                  <a href="#default-tab-3" data-toggle="tab" class="nav-link">
                  <span class="d-sm-none">@lang('package.PACKAGE_RED_3D4D')</span>
                  <span class="d-sm-block d-none">@lang('package.PACKAGE_RED_3D4D')</span>
                  </a>
               </li>
               <li class="">
                  <a href="#default-tab-4" data-toggle="tab" class="nav-link">
                  <span class="d-sm-none">@lang('package.PACKAGE_RED_5D6D')</span>
                  <span class="d-sm-block d-none">@lang('package.PACKAGE_RED_5D6D')</span>
                  </a>
               </li>
            </ul>
            <!-- end nav-tabs -->
            <!-- begin tab-content -->
            <div class="tab-content">
               <!-- begin tab-pane -->
               <div class="tab-pane fade active show" id="default-tab-1">
                  <div class="row row-space-10">
                  <?php $package3D4D = 0;
                        $i= 0;
                  ?>
                     @foreach($data['package3D4DOption'] as $package)
                     <?php if ($package->red_package)
                               continue;
                           else
                               $package3D4D++;
                        ?>
                     <div class="col-md-2">
                        <div class="form-group m-b-10">
                           <div class="checkbox checkbox-css m-b-20">
                              <input type="checkbox" name="package3D4DAssigned[]" data-rowname="{{$package->package_name}}" 
                                 value="{{$package->package_id}}" id="package3D4D_checkbox_{{$package->package_id}}" class="checkbox_3D4D select_all_3D4D"
                                <?php 
                                if(old('package3D4DAssigned') != null && in_array($package->package_id, old('package3D4DAssigned')))
                                 echo "checked";
                                ?>
                                />
                              <label for="package3D4D_checkbox_{{$package->package_id}}" >
                              <a href="#package_3D4D_dialog" id="package3D4D_{{$package->package_id}}" data-toggle="modal" class="package_3D4D_table" 
                                 data-rowid="{{$package->package_id}}" data-rowtypeid="{{$package->package_type_id}}">{{$package->package_name}}</a>
                              </label>
                           </div>
                        </div>
                     </div>
                     @endforeach
                  </div>
                  @if ($package3D4D ==0)
                        @lang('package.NO_PACKAGE')
                  @else
                  <p class="text-right m-b-0">
                  <div class="form-group m-b-10">
                     <div class="checkbox checkbox-css m-b-20">
                        <input type="checkbox" id="selectAll3D4D" />
                        <label for="selectAll3D4D">@lang('package.SELECT_ALL_PACKAGE')</label>
                     </div>
                  </div>
                  </p>
                  @endif
               </div>
               <!-- end tab-pane -->
               <!-- begin tab-pane -->
               <div class="tab-pane fade" id="default-tab-2">
                  <div class="row row-space-10">
                  <?php $package5D6D = 0; 
                        $i = 0;
                  ?>
                     @foreach($data['package5D6DOption'] as $package)
                     <?php if ($package->red_package)
                               continue;
                           else
                               $package5D6D++;
                        ?>
                     <div class="col-md-2">
                        <div class="form-group m-b-10">
                           <div class="checkbox checkbox-css m-b-20">
                              <input type="checkbox" name="package5D6DAssigned[]" data-rowname="{{$package->package_name}}" 
                                 value="{{$package->package_id}}" id="package5D6D_checkbox_{{$package->package_id}}" class="checkbox_5D6D select_all_5D6D" 
                                 <?php 
                                if(old('package5D6DAssigned') != NULL && in_array($package->package_id, old('package5D6DAssigned')))
                                 echo "checked";
                                ?>
                                 />
                              <label for="package5D6D_checkbox_{{$package->package_id}}" >
                              <a href="#package_5D6D_dialog" id="package5D6D_{{$package->package_id}}" data-toggle="modal" class="package_5D6D_table" 
                                 data-rowid="{{$package->package_id}}" data-rowtypeid="{{$package->package_type_id}}">{{$package->package_name}}</a>
                              </label>
                           </div>
                        </div>
                     </div>
                     @endforeach
                  </div>
                  @if ($package5D6D == 0)
                       @lang('package.NO_PACKAGE')
                 @else
                  <p class="text-right m-b-0">
                  <div class="form-group m-b-10">
                     <div class="checkbox checkbox-css m-b-20">
                        <input type="checkbox" id="selectAll5D6D" />
                        <label for="selectAll5D6D">@lang('package.SELECT_ALL_PACKAGE')</label>
                     </div>
                  </div>
                  </p>
                  @endif
               </div>
               <!-- end tab-pane -->
               <!-- begin tab-pane -->
               <div class="tab-pane fade" id="default-tab-3">
                  <div class="row row-space-10">
                  <?php $packageRed3D4D = 0; ?>
                     @foreach($data['package3D4DOption'] as $package)
                     <?php if (!$package->red_package)
                               continue;
                           else
                               $packageRed3D4D++;
                        ?>
                     <div class="col-md-2">
                        <div class="form-group m-b-10">
                           <div class="checkbox checkbox-css m-b-20">
                              <input type="checkbox" name="red_3D4D_package" data-rowname="{{$package->package_name}}"
                                 value="{{$package->package_id}}" id="package3D4D_checkbox_{{$package->package_id}}" class="checkbox_3D4D" 
                                 <?php if(old('red_3D4D_package') == $package->package_id){echo "checked";} ?> />
                              <label for="package3D4D_checkbox_{{$package->package_id}}" >
                              <a href="#package_3D4D_dialog" id="package3D4D_{{$package->package_id}}" data-toggle="modal" class="package_3D4D_table" 
                                 data-rowid="{{$package->package_id}}" data-rowtypeid="{{$package->package_type_id}}">{{$package->package_name}}</a>
                              </label>
                           </div>
                        </div>
                     </div>
                     @endforeach
                     @if ($packageRed3D4D == 0)
                         @lang('package.NO_PACKAGE')
                     @endif
                  </div>
               </div>
               <div class="tab-pane fade" id="default-tab-4">
                  <div class="row row-space-10">
                  <?php $packageRed5D6D = 0; ?>
                     @foreach($data['package5D6DOption'] as $package)
                     <?php if (!$package->red_package)
                               continue;
                           else
                               $packageRed5D6D++;
                        ?>
                     <div class="col-md-2">
                        <div class="form-group m-b-10">
                           <div class="checkbox checkbox-css m-b-20">
                              <input type="checkbox" name="red_5D6D_package" data-rowname="{{$package->package_name}}"
                                 value="{{$package->package_id}}" id="package5D6D_checkbox_{{$package->package_id}}" class="checkbox_5D6D" 
                                 <?php if(old('red_5D6D_package') == $package->package_id){echo "checked";} ?> />
                              <label for="package5D6D_checkbox_{{$package->package_id}}" >
                              <a href="#package_5D6D_dialog" id="package5D6D_{{$package->package_id}}" data-toggle="modal" class="package_5D6D_table" 
                                 data-rowid="{{$package->package_id}}" data-rowtypeid="{{$package->package_type_id}}">{{$package->package_name}}</a>
                              </label>
                           </div>
                        </div>
                     </div>
                     @endforeach
                     @if ($packageRed5D6D == 0)
                         @lang('package.NO_PACKAGE')
                     @endif
                  </div>
               </div>
               <!-- end tab-pane -->
            </div>
            <!-- end tab-content -->
         </div>
      </div>
      <!-- end panel -->	
      <!-- begin panel -->
      <div class="panel panel-inverse">
         <!-- begin panel-heading -->
         <div class="panel-heading">
            <div class="panel-heading-btn">
               <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
               <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
               <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
               <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
            </div>
            <h4 class="panel-title">@lang('user.BET_SETTING')</h4>
         </div>
         <!-- end panel-heading -->
         <!-- begin panel-body -->
         <div class="panel-body">
            <div class="row row-space-10">
               <div class="col-md-4" id="div_3D4D" style="display:none">
                  <div class="form-group m-b-10">
                     <label for="exampleInputPassword1">@lang('package.PACKAGE_3D4D') @lang('package.PACKAGE')</label>
                     <select class="form-control" name="activated_3D4D_package" id="activated_3D4D_package">
                     </select>
                  </div>
               </div>
               <div class="col-md-4" id="div_5D6D" style="display:none">
                  <div class="form-group m-b-10">
                     <label for="exampleInputPassword1">@lang('package.PACKAGE_5D6D') @lang('package.PACKAGE')</label>
                     <select class="form-control" name="activated_5D6D_package" id="activated_5D6D_package" >
                     </select>
                  </div>
               </div>
            </div>
            <div class="row row-space-10">
               <div class="col-md-4">
                  <div class="form-group m-b-10">
                     <label for="exampleInputPassword1">@lang('user.ARRANGEMENT')</label>
                     <select class="form-control" name="arrangement" id="arrangement">
                        @foreach($data['arrangement'] as $arrangement)
                        <option value="{{$arrangement->arrangement_id}}" <?php if(old('arrangement') == $arrangement->arrangement_id){echo "selected";} ?> >
                           {{$arrangement->arrangement_name}}
                        </option>
                        @endforeach
                     </select>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group m-b-10">
                     <label for="exampleInputPassword1">@lang('user.MODE')</label>
                     <select class="form-control" name="mode" id="mode" >
                        <option value="1" <?php if(old('mode') == "1"){echo "selected";} ?>> * </option>
                        <option value="0" <?php if(old('mode') == "0"){echo "selected";} ?>> / </option>
                     </select>
                  </div>
               </div>
               <div class="col-md-4">
                  <div class="form-group m-b-10">
                     <label for="exampleInputPassword1">@lang('user.BOX')</label>
                     <select class="form-control" name="boxes" id="boxes">
                        @foreach($data['boxes'] as $boxes)
                        <option value="{{$boxes->box_id}}" <?php if(old('boxes') == $boxes->box_id){echo "selected";} ?>>
                           {{$boxes->box_format}}
                        </option>
                        @endforeach
                     </select>
                  </div>
               </div>
            </div>
            <div class="row row-space-10">
               <div class="col-md-4">
                  <div class="form-group m-b-10">
                     <label for="exampleInputPassword1">@lang('user.POOLS_SHORTCODE')</label>
                     <select class="form-control" name="short_code" id="short_code">
                        <option value="MPTSBKW">MPTSBKW</option>
                     </select>
                  </div>
               </div>
            </div>
            <div class="row">&nbsp;</div>
            <!-- end table-responsive -->
            <div class="col-md-12">
               <button type="submit" id="btnCreateMember" class="btn btn_submit btn-primary" style="margin: 5px"><i class="fa fa-save"></i> &nbsp;@lang('user.SAVE')</button>
               <a href='user_list.html' class="btn"><i class="fa fa-arrow-left "></i>@lang('user.BACK_TO_MEMBER_LIST')</a>						
            </div>
         </div>
         <!-- end panel -->	
      </div>
   </form>
   <!-- end #content -->
</div>
<form class="form-horizontal" data-parsley-validate="true" name="package_3D4D_form" id="package_3D4D_form"  method="POST">
   {{ csrf_field() }}
   <div class="modal fade" id="package_3D4D_dialog">
      <div class="modal-dialog modal-lg">
         <div class="modal-content">
            <div class="modal-header">
               <h4 class="modal-title">@lang('package.EDIT_PACKAGE'): <label id="modal_package_3D4D"></label></h4>
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
               <div class="panel-body">
                  <div class="table-responsive">
                     @include("include.table4D")
                  </div>
                  <br/><br/><br/>
                  <div class="table-responsive">
                     @include("include.table4DS")
                  </div>
                  <div>
                     <input type="hidden" id="selected_3D4D_package_owner_id" name="selected_package_owner_user_id"/>
                     <input type="hidden" id="selected_3D4D_package_type_id" name="selected_package_type_id"/>
                     <input type="hidden" id="selected_3D4D_package_id" name="selected_package_id"/>
                  </div>
               </div>
               <!-- end panel-body -->
            </div>
            <div class="modal-footer">
               <a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">Close</a>
            </div>
         </div>
      </div>
   </div>
</form>
<form class="form-horizontal" data-parsley-validate="true" name="package_5D6D_form" id="package_5D6D_form"  method="POST">
   {{ csrf_field() }}
   <div class="modal fade" id="package_5D6D_dialog">
      <div class="modal-dialog modal-lg">
         <div class="modal-content">
            <div class="modal-header">
               <h4 class="modal-title">@lang('package.EDIT_PACKAGE'): <label id="modal_package_5D6D"></label></h4>
               <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
               <div class="panel-body">
                  <div class="table-responsive">
                     @include("include.table5D6D")
                  </div>
                  <div>
                     <input type="hidden" id="selected_5D6D_package_owner_id" name="selected_package_owner_user_id"/>
                     <input type="hidden" id="selected_5D6D_package_type_id" name="selected_package_type_id"/>
                     <input type="hidden" id="selected_5D6D_package_id" name="selected_package_id"/>
                  </div>
               </div>
               <!-- end panel-body -->
            </div>
            <div class="modal-footer">
               <a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">@lang('general.CLOSE')</a>
            </div>
         </div>
      </div>
   </div>
</form>

@stop
@section("page_script")
<script src="/assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="/assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
<script src="/assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script src="/assets/plugins/bootstrap-sweetalert/sweetalert.min.js"></script>
<script src="/assets/plugins/parsley/dist/parsley.js"></script>
<script>
function showPermissionDeniedError()
    {
        alert("Not allow to check/uncheck RED Package");
        event.preventDefault();
        event.stopPropagation();
    }
$(document).ready(function() {
    $(".table_input").prop("disabled", true);
    $(".default").css("display", "none");
    $(".package_5D6D_table").click(function() {
        $.ajax({
            type: 'GET',
            url: '\\packageDetail\\' + $(this).data("rowuserid") + '\\' + $(this).data("rowtypeid") + '\\' + $(this).data("rowid"),
            success: function(data) {
                updateView(data);
            }
        });
    });

    $(".package_3D4D_table").click(function() {
        $.ajax({
            type: 'GET',
            url: '\\packageDetail\\' + $(this).data("rowuserid") + '\\' + $(this).data("rowtypeid") + '\\' + $(this).data("rowid"),
            success: function(data) {
                updateView(data);
            }
        });
    });

    $('.checkbox_3D4D').change(function() {
        $value = $(this).val();
        $text = $(this).data("rowname");
        $needCheck = false;
        oldVal =0;
        <?php 
        if (old('activated_3D4D_package') == null)
            echo "oldVal = 0;";
        else
            echo "oldVal = ".old('activated_3D4D_package').";";
        ?>
        if (oldVal == $value)
        {
            $needCheck = true;
        }
        if (this.checked) 
        {
            $optionVal = "<option value='" + $value + "' ";
            
            if ($needCheck)
            {
                $optionVal = $optionVal +"selected";
            }
            $optionVal = $optionVal +" >" + $text + "</option>"
            $("#activated_3D4D_package").append($optionVal);
        } 
        else 
        {
            $("#activated_3D4D_package option[value='" + $value + "']").remove();
        }

        if ($('#activated_3D4D_package option').length == 0) {
            $("#div_3D4D").css("display", 'none');
        } else {
            $("#div_3D4D").css("display", 'block');
        }
    });

    $('.checkbox_5D6D').change(function() {
        $value = $(this).val();
        $text = $(this).data("rowname");
        $needCheck = false;
        oldVal =0;
        <?php 
        if (old('activated_5D6D_package') == null)
            echo "oldVal = 0;";
        else
            echo "oldVal = ".old('activated_5D6D_package').";";
        ?>
        if (oldVal == $value)
        {
            $needCheck = true;
        }
        if (this.checked) 
        {
            $optionVal = "<option value='" + $value + "' ";
            
            if ($needCheck)
            {
                $optionVal = $optionVal +"selected";
            }
            $optionVal = $optionVal +" >" + $text + "</option>"
            $("#activated_5D6D_package").append($optionVal);
        } 
        else 
        {
            $("#activated_5D6D_package option[value='" + $value + "']").remove();
        }
        if ($('#activated_5D6D_package option').length == 0) {
            $("#div_5D6D").css("display", 'none');
        } else {
            $("#div_5D6D").css("display", 'block');
        }
    });

    $("#selectAll3D4D").change(function() {
        $('.select_all_3D4D').prop('checked', $(this).prop('checked'));
        $(".select_all_3D4D").trigger("change");
    });
    
    $("#selectAll5D6D").change(function() {
        $('.select_all_5D6D').prop('checked', $(this).prop('checked'));
        $(".select_all_5D6D").trigger("change");
    });
    
    $('.checkbox_3D4D:checkbox:checked').each(function () {
        $(this).trigger("change");
    });
    $('.checkbox_5D6D:checkbox:checked').each(function () {
        $(this).trigger("change");
    });
    
    function updateView(data) {
        if (data.data['packageType']['package_type_name'] == "3D/4D") {
            <?php
            foreach (TABLE_PROPERTIES as $x => $x_value)
            {
                foreach (TABLE_4D as $id) 
                {
                    echo "$('#".$x_value."_".$id."_default').text(data.data['package']['$id']['$x_value']);\r\n";
                    echo "$('#".$x_value."_$id').val(data.data['package']['$id']['$x_value']);\r\n";
                }
      
                foreach (TABLE_3D as $id) 
                {
                    echo "$('#".$x_value."_".$id."_default').text(data.data['package']['$id']['$x_value']);\r\n";
                    echo "$('#".$x_value."_$id').val(data.data['package']['$id']['$x_value']);\r\n";
                }
      
                foreach (TABLE_4DS as $id) 
                {
                    echo "$('#".$x_value."_".$id."_default').text(data.data['package']['$id']['$x_value']);\r\n";
                    echo "$('#".$x_value."_$id').val(data.data['package']['$id']['$x_value']);\r\n";
                }
            }
            ?>
        } else {
            //5d&6d
            <?php 
            foreach (TABLE_PROPERTIES as $x => $x_value) 
            {
                foreach (TABLE_5D6D as $id) 
      	        {
      		        echo "$('#".$x_value."_".$id."_default').text(data.data['package']['$id']['$x_value']);\r\n";
      		        echo "$('#".$x_value."_$id').val(data.data['package']['$id']['$x_value']);\r\n";
      	        }
            }
            ?>
        }
    }
});
	</script>
	@stop