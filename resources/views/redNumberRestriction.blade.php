@extends("layouts.userlayout")
@section("title", "Red Number Restriction")
@section("header", "Edit Red Number Restriction")

@section('headerScript')
<link href="/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css" rel="stylesheet" />
<link href="/assets/plugins/parsley/src/parsley.css" rel="stylesheet" />
@stop

@section("body")
<div class="panel panel-inverse" >
	<div class="panel-heading">
		<div class="panel-heading-btn">
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
		</div>
		<h4 class="panel-title">Red Number Restriction List</h4>
	</div>
	
	@if (session('msg'))
	<div id="divMsg" class="alert {{session('class')}}">{{session('msg')}}</div>
	@else
	<div id="divMsg" style="display: none" class="alert alert-danger"></div>
	@endif

	<div class="panel-body">
		<form id="form_rednumber" method="POST" autocomplete="off" action="{{ action('MemberController@store_rednumberrestriction') }}">
			{{ csrf_field() }}
			<label>Add Red Number Restriction</label>
			<div class="table-responsive m-b-20">
				<table id="table_addrestriction" class="table table-bordered table-condensed table-valign-middle table-width-fix table-addbetlimit f-s-14">
					<thead>
						<tr>
							@foreach($pools AS $pool)
							<th width="1%" class="text-center">
								<img src="{{ $pool->images }}" style="width: 20px; height: 20px">
							</th>
							@endforeach
							<th>Type</th>
							<th>Big</th>
							<th>Small</th>
							<th>Big/Small</th>
							<th>A</th>
							<th>ABC</th>
							<th>A/ABC</th>
							<th>4A</th>
							<th>4B</th>
							<th>4C</th>
							<th>4D</th>
							<th>4E</th>
							<th>4ABC</th>
							<th>3A</th>
							<th>3ABC</th>
							<th>2A</th>
							<th>2ABC</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							@foreach($pools AS $pool)
							<td>
								<div class="checkbox checkbox-css">
									<input type="checkbox" id="chk{{ $pool->shortname }}" {{ !empty(old('chk_'.$pool->shortname)) ? ' . checked="" . ' : '' }} class="chk_pool" name="chk_{{ $pool->shortname }}" />
									<label for="chk{{ $pool->shortname }}"></label>
								</div>
							</td>
							@endforeach
							<td>
								<select name="ddl_type" class="form-control" style="width: 70px">
									<option value="4">P4</option>
									<option value="6" {{ (old('ddl_type') == '6') ? "selected" : "" }}>P6</option>
									<option value="12" {{ (old('ddl_type') == '12') ? "selected" : "" }}>P12</option>
								</select>
							</td>
							<td>
								<input type="text" class="form-control m_input" name="txt_big" value="{{ old('txt_big') }}">
							</td>
							<td>
								<input type="text" class="form-control m_input" name="txt_small" value="{{ old('txt_small') }}">
							</td>
							<td>
								<input type="text" class="form-control m_input" name="txt_big_small" value="{{ old('txt_big_small') }}">
							</td>
							<td>
								<input type="text" class="form-control m_input" name="txt_a" value="{{ old('txt_a') }}">
							</td>
							<td>
								<input type="text" class="form-control m_input" name="txt_abc" value="{{ old('txt_abc') }}">
							</td>
							<td>
								<input type="text" class="form-control m_input" name="txt_a_abc" value="{{ old('txt_a_abc') }}">
							</td>
							<td>
								<input type="text" class="form-control m_input" name="txt_4a" value="{{ old('txt_4a') }}">
							</td>
							<td>
								<input type="text" class="form-control m_input" name="txt_4b" value="{{ old('txt_4b') }}">
							</td>
							<td>
								<input type="text" class="form-control m_input" name="txt_4c" value="{{ old('txt_4c') }}">
							</td>
							<td>
								<input type="text" class="form-control m_input" name="txt_4d" value="{{ old('txt_4d') }}">
							</td>
							<td>
								<input type="text" class="form-control m_input" name="txt_4e" value="{{ old('txt_4e') }}">
							</td>
							<td>
								<input type="text" class="form-control m_input" name="txt_4abc" value="{{ old('txt_4abc') }}">
							</td>
							<td>
								<input type="text" class="form-control m_input" name="txt_3a" value="{{ old('txt_3a') }}">
							</td>
							<td>
								<input type="text" class="form-control m_input" name="txt_3abc" value="{{ old('txt_3abc') }}">
							</td>
							<td>
								<input type="text" class="form-control m_input" name="txt_2a" value="{{ old('txt_2a') }}">
							</td>
							<td>
								<input type="text" class="form-control m_input" name="txt_2abc" value="{{ old('txt_2abc') }}">
							</td>
						</tr>
					</tbody>
				</table>
			</div>

			<button id="btn_save" type="button" class="btn btn-primary m-b-20">
				<i class="fa fa-save m-r-5"></i>
				Save
			</button>
		</form>

		<form id="form_list" method="POST" action="{{ action('MemberController@delete_rednumberrestriction') }}" class="mt-4">
			{{ method_field('DELETE') }}
			{{ csrf_field() }}
			<div class="table-responsive m-b-20">
				<table id="table_rednumberrestriction" class="table table-bordered table-valign-middle f-s-14">
					<thead>
						<tr>
							<th width="1%">
								<div class="checkbox checkbox-css" style="padding-top: 20px">
									<input type="checkbox" id="chk_all"/>
									<label for="chk_all"></label>
								</div>
							</th>
							@foreach($pools AS $pool)
							<th width="1%" class="text-center">
								<img src="{{ $pool->images }}" style="width: 20px; height: 20px">
							</th>
							@endforeach
							<th width="1%">Type</th>
							<th>Big</th>
							<th>Small</th>
							<th>Big/Small</th>
							<th>A</th>
							<th>ABC</th>
							<th>A/ABC</th>
							<th>4A</th>
							<th>4B</th>
							<th>4C</th>
							<th>4D</th>
							<th>4E</th>
							<th>4ABC</th>
							<th>3A</th>
							<th>3ABC</th>
							<th>2A</th>
							<th>2ABC</th>
							<th width="1%"></th>
						</tr>
					</thead>
					<tbody>
						@foreach($numbers AS $ea)
						<tr>
							<td>
								<div class="checkbox checkbox-css">
									<input type="checkbox" id="chk_ea_{{ $ea->id }}" name="txt_id[]" value="{{ $ea->id }}" />
									<label for="chk_ea_{{ $ea->id }}" style="padding-top: 14px"></label>
								</div>
							</td>
							<td>@if($ea->mg) <i class="fa fa-check fa-lg" style="color: green"></i> @endif</td>
							<td>@if($ea->kd) <i class="fa fa-check fa-lg" style="color: green"></i> @endif</td>
							<td>@if($ea->tt) <i class="fa fa-check fa-lg" style="color: green"></i> @endif</td>
							<td>@if($ea->sg) <i class="fa fa-check fa-lg" style="color: green"></i> @endif</td>
							<td>@if($ea->sb) <i class="fa fa-check fa-lg" style="color: green"></i> @endif</td>
							<td>@if($ea->stc) <i class="fa fa-check fa-lg" style="color: green"></i> @endif</td>
							<td>@if($ea->sw) <i class="fa fa-check fa-lg" style="color: green"></i> @endif</td>
							<td width="1%" class="f-w-600">P{{ $ea->type }}</td>
							<td>{{ $ea->bet_big }}</td>
							<td>{{ $ea->bet_small }}</td>
							<td>{{ $ea->bet_big_small }}</td>
							<td>{{ $ea->bet_a }}</td>
							<td>{{ $ea->bet_abc }}</td>
							<td>{{ $ea->bet_a_abc }}</td>
							<td>{{ $ea->bet_4a }}</td>
							<td>{{ $ea->bet_4b }}</td>
							<td>{{ $ea->bet_4c }}</td>
							<td>{{ $ea->bet_4d }}</td>
							<td>{{ $ea->bet_4e }}</td>
							<td>{{ $ea->bet_4abc }}</td>
							<td>{{ $ea->bet_3a }}</td>
							<td>{{ $ea->bet_3abc }}</td>
							<td>{{ $ea->bet_2a }}</td>
							<td>{{ $ea->bet_2abc }}</td>
							<td>
								<button type="button" class="btn btn-primary btn-circle btn-icon btn-edit" data-id="{{ $ea->id }}">
									<i class="fa fa-edit"></i>
								</button>
							</td>
						</tr>
						@endforeach
					</tbody>
				</table>
			</div>

			<button id="btn_delete" type="button" class="btn btn-danger m-b-20">
				<i class="fa fa-trash m-r-5"></i>
				Delete
			</button>
		</form>
	</div>
</div>

<div class="modal fade" id="modal-edit">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Edit Red Number Restriction</h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div id="edit_divMsg" style="display: none" class="alert alert-danger"></div>
			<div class="modal-body">
				<form id="form_editred" autocomplete="off" method="POST">
					{{ csrf_field() }}
					<div class="table-responsive">
						<table class="table table-bordered table-condensed table-valign-middle table-width-fix table-addbetlimit">
							<thead>
								<tr>
									@foreach($pools AS $pool)
									<th width="1%" class="text-center">
										<img src="{{ $pool->images }}" style="width: 20px; height: 20px">
									</th>
									@endforeach
									<th>Type</th>
									<th>Big</th>
									<th>Small</th>
									<th>Big/Small</th>
									<th>A</th>
									<th>ABC</th>
									<th>A/ABC</th>
									<th>4A</th>
									<th>4B</th>
									<th>4C</th>
									<th>4D</th>
									<th>4E</th>
									<th>4ABC</th>
									<th>3A</th>
									<th>3ABC</th>
									<th>2A</th>
									<th>2ABC</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									@foreach($pools AS $pool)
									<td>
										<div class="checkbox checkbox-css">
											<input type="checkbox" id="edit_chk{{ $pool->shortname }}" class="edit_chk_pool" data-pool="{{ $pool->shortname }}" name="edit_chk_{{ $pool->shortname }}" />
											<label for="edit_chk{{ $pool->shortname }}"></label>
										</div>
									</td>
									@endforeach
									<td>
										<input type="text" class="form-control m_input" readonly="" id="edit_txt_type">
									</td>
									<td>
										<input type="text" class="form-control m_input" name="edit_txt_big">
									</td>
									<td>
										<input type="text" class="form-control m_input" name="edit_txt_small">
									</td>
									<td>
										<input type="text" class="form-control m_input" name="edit_txt_big_small">
									</td>
									<td>
										<input type="text" class="form-control m_input" name="edit_txt_a">
									</td>
									<td>
										<input type="text" class="form-control m_input" name="edit_txt_abc">
									</td>
									<td>
										<input type="text" class="form-control m_input" name="edit_txt_a_abc">
									</td>
									<td>
										<input type="text" class="form-control m_input" name="edit_txt_4a">
									</td>
									<td>
										<input type="text" class="form-control m_input" name="edit_txt_4b">
									</td>
									<td>
										<input type="text" class="form-control m_input" name="edit_txt_4c">
									</td>
									<td>
										<input type="text" class="form-control m_input" name="edit_txt_4d">
									</td>
									<td>
										<input type="text" class="form-control m_input" name="edit_txt_4e">
									</td>
									<td>
										<input type="text" class="form-control m_input" name="edit_txt_4abc">
									</td>
									<td>
										<input type="text" class="form-control m_input" name="edit_txt_3a">
									</td>
									<td>
										<input type="text" class="form-control m_input" name="edit_txt_3abc">
									</td>
									<td>
										<input type="text" class="form-control m_input" name="edit_txt_2a">
									</td>
									<td>
										<input type="text" class="form-control m_input" name="edit_txt_2abc">
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
					<button type="button" id="btn_update" class="btn btn-primary">Update Restriction</button>
				</div>
			</form>
		</div>
	</div>
</div>
@stop

@section("page_script")
<script src="/assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="/assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
<script src="/assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.28/dist/sweetalert2.all.min.js"></script>

<script type="text/javascript">
	$(function(){

		$(".m_input").blur(function(){
			var value = parseFloat($(this).val());
			if(isNaN(value)){
				$(this).val("0.00");
			} else {
				$(this).val(value.toFixed(2));
			}
		});

		$(document).on("click", ".btn-edit", function(){
			var id = $(this).attr("data-id");
			update_id = id;
			$.get('/getRedNumberRestriction/' + id, function(data){
				if(data.success){

					var red = data.red;
					var redArr = red.checkedArr;
					var disabledArr = red.disabledArr;

					redArr.forEach(function(elem, index){
						$("input[name='edit_chk_" + elem + "']").prop("checked", true);
					});

					disabledArr.forEach(function(elem, index){
						$("input[name='edit_chk_" + elem + "']").closest(".checkbox").hide();
					});

					$("#edit_txt_type").val("P" + red.type);
					$("input[name='edit_txt_big']").val(red.bet_big);
					$("input[name='edit_txt_small']").val(red.bet_small);
					$("input[name='edit_txt_big_small']").val(red.bet_big_small);
					$("input[name='edit_txt_a']").val(red.bet_a);
					$("input[name='edit_txt_abc']").val(red.bet_abc);
					$("input[name='edit_txt_a_abc']").val(red.bet_a_abc);
					$("input[name='edit_txt_4a]").val(red.bet_4a);
					$("input[name='edit_txt_4a]").val(red.bet_4ab);
					$("input[name='edit_txt_a_abc]").val(red.bet_a_abc);
					$("input[name='edit_txt_4a']").val(red.bet_4a);
					$("input[name='edit_txt_4b']").val(red.bet_4b);
					$("input[name='edit_txt_4c']").val(red.bet_4c);
					$("input[name='edit_txt_4d']").val(red.bet_4d);
					$("input[name='edit_txt_4e']").val(red.bet_4e);
					$("input[name='edit_txt_4abc']").val(red.bet_4abc);
					$("input[name='edit_txt_3a']").val(red.bet_3a);
					$("input[name='edit_txt_3abc']").val(red.bet_3abc);
					$("input[name='edit_txt_2a']").val(red.bet_2a);
					$("input[name='edit_txt_2abc']").val(red.bet_2abc);

					$("#form_editred").attr("action", "/rednumberrestriction/" + red.id);

					$("#modal-edit").modal("toggle");
				}
			});
		});

		$('#modal-edit').on('hidden.bs.modal', function () {
			$("#edit_divMsg").hide();
		});

		$("#btn_update").click(function(){

			if($("input[name^='edit_chk_']:checked").length == 0){
				showEditError("Please select as least one pool.");
				return;
			}

			$("#form_editred").submit();
		});

		$("#btn_save").click(function(){

			var valid = false;

			if($("input[name^='chk_']:checked").length == 0){
				showError("Please select as least one pool.");
				return;
			}

			$("#table_addrestriction .m_input").each(function(){
				if($(this).val() != "" && $(this).val() > 0){
					valid = true;
				}
			});

			if(!valid){
				showError("Please enter an amount for at least one of the betting category.");
				return;
			} else {
				$("#form_rednumber").submit();
			}
		});

		$("#table_rednumberrestriction").DataTable({
			"ordering": false
		});

		$("#chk_all").change(function(){
			if($(this).is(":checked")){
				$("input[id^='chk_ea_']").prop("checked", true);
			} else {
				$("input[id^='chk_ea_']").prop("checked", false);
			}
		});

		$("input[id^='chk_ea_']").change(function(){
			if(!$(this).is(":checked")){
				$("#chk_all").prop("checked", false);
			}
		});

		$("#btn_delete").click(function(){
			if($("input[id^='chk_ea_']:checked").length > 0){
				swal({
					title: "Confirm delete?",
					text: "Press Continue to proceed",
					type: "warning",
					showCancelButton: true,
					confirmButtonText: 'Continue',
					cancelButtonText: 'Cancel',
					reverseButtons: true
				}).then((result) => {
					if (result.value) {
						$("#form_list").submit();
					}
				});
			} else {
				$(this).blur();
			}
		});

		function showEditError(msg){
			$("#edit_divMsg").text(msg).show();
		}

		function showError(msg){
			$('#divMsg').addClass("alert alert-danger");
			$('#divMsg').text(msg).show();
		}
	});
</script>
@stop