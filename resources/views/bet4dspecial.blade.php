@extends("layouts.userlayout")
@section("title", "Bet Form")
@section("header", "Bet 4D Special")

@section('headerScript')
<link href="/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css" rel="stylesheet" />
<link href="/assets/plugins/parsley/src/parsley.css" rel="stylesheet" />
@stop

@section("body")

{!! Form::open(['url' => '/bet/makeBet4dSpecial', 'id' => 'betForm', 'name' => 'betForm', 'class' => 'form-horizontal']) !!}
<!-- begin panel -->
<div class="panel panel-inverse" >
	<!-- begin panel-heading -->
	<div class="panel-heading">
		<div class="panel-heading-btn">
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
		</div>
		<h4 class="panel-title">Bet Form 4D Special</h4>
	</div>
	<!-- end panel-heading -->
	<!-- begin panel-body -->
	@if (session('message'))
	<div class="{{session('class')}}">{{session('message')}}</div>
	@endif
	
	<div id="divMsg"></div>

	<div id="divTicket" style="display: none" class="alert alert-success">
		<span id="spanTicket"></span>
	</div>

	<div class="panel-body" >
	<!-- 	@if (session('message_ticket'))
		<div class="{{session('class_ticket')}}">{!! session('message_ticket') !!}</div>
		@endif -->

		<div class="row row-space-10">
			<div class="col-md-4">
				<div class="form-group m-b-10" data-parsley-validate="true">
					<label for="exampleInputEmail1">Bet Agent</label>
					<select class="form-control" name="betAgent">
						@foreach($data['downlines'] as $d)
						<option>{{ $d }}</option>               		
						@endforeach
					</select>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group m-b-10">
					<label for="exampleInputPassword1">Credit Left</label>
					<input type="text" class="form-control" name="txtCreditLeft" value="{{ $data['me']->credit_limit }}" readonly /> 
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group m-b-10">
					<label for="exampleInputPassword1">Telephone (SMS)</label>
					<input type="text" class="form-control" name="txtTelephone" value="" placeholder="Enter Phone (0102345678)" />
				</div>
			</div>
		</div>
		<div class="row row-space-10">
			<div class="col-md-8">
				<div class="form-group m-b-10" data-parsley-validate="true">
					<label for="exampleInputEmail1">Betting for Draw Dates</label>
					<div class="checkbox checkbox-css m-b-20">
<!-- 						@foreach($data['schedules'] as $s)
						<div class="checkbox checkbox-css checkbox-inline">
							<input type="hidden" name={{ "dd_".$s->drawdate  }} value="0" />
							<input type="checkbox" class="dd" name={{ "dd_".$s->drawdate  }} id={{ "dd_".$s->drawdate  }} />
							<label for={{ "dd_".$s->drawdate  }}>{{ $s->day }}</label>
						</div>
						@endforeach          -->      
						<?php 
						$now = \Carbon\Carbon::now()->format('Y-m-d');
						$days = 7;
						$current_day = \Carbon\Carbon::createFromFormat("Y-m-d", $now)->format("l");

						switch ($current_day) {
							case "Monday":
							$days = $days + 2;
							break;
							case "Tuesday":
							$days = $days + 1;
							break;
							case "Thursday":
							$days = $days + 2;
							break;
							case "Friday":
							$days = $days + 1;
							break;
						}

						for ($x = 0; $x <= $days; $x++) {?>  
							<div class="checkbox checkbox-css checkbox-inline">
								<input type="hidden" name="dd_<?php echo str_replace("-", "",$now) ?>" value="0" />
								<input type="checkbox" class="dd" name="dd_<?php echo str_replace("-", "",$now) ?>" id="dd_<?php echo str_replace("-", "",$now) ?>" />
								<label for="dd_<?php echo str_replace("-", "",$now) ?>"><?php echo \Carbon\Carbon::createFromFormat("Y-m-d", $now)->format("l")?></label>
							</div>
							<?php $now = \Carbon\Carbon::parse($now)->addDays(1)->format("Y-m-d"); } ?>    		                 
						</div>
					</div>
				</div> 
				<div class="col-md-4">
					<div class="form-group m-b-10">
						<label for="exampleInputPassword1">Remark</label>
						<input type="text" class="form-control" name="txtRemark" value=""  /> 
					</div>
				</div>        
			</div>
			<div class="row row-space-10">
				<div class="col-md-3">
					<div class="alert alert-success fade show m-b-10">
						<b>4B</b>&nbsp;:&nbsp;RM <span id="span4B"></span>
					</div>			
				</div>
				<div class="col-md-3">
					<div class="alert alert-success fade show m-b-10">
						<b>4C</b>&nbsp;:&nbsp;RM <span id="span4C"></span>
					</div>				
				</div>
				<div class="col-md-3">
					<div class="alert alert-success fade show m-b-10">
						<b>4D</b>&nbsp;:&nbsp;RM <span id="span4D"></span>
					</div>					
				</div>
				<div class="col-md-3">
					<div class="alert alert-success fade show m-b-10">
						<b>4E</b>&nbsp;:&nbsp;RM <span id="span4E"></span>
					</div>				
				</div>
			</div>
			<div class="row row-space-10">
				<div class="col-md-3">
					<div class="alert alert-success fade show m-b-10">
						<b>4ABC</b>&nbsp;:&nbsp;RM <span id="span4ABC"></span>
					</div>				
				</div>
				<div class="col-md-3">
					<div class="alert alert-success fade show m-b-10">
						<b>2A</b>&nbsp;:&nbsp;RM <span id="span2A"></span>
					</div>				
				</div>
				<div class="col-md-3">
					<div class="alert alert-success fade show m-b-10">
						<b>2ABC</b>&nbsp;:&nbsp;RM <span id="span2ABC"></span>
					</div>				
				</div>
				<div class="col-md-3">
					<div class="alert alert-success fade show m-b-10">
						<b>Grand Total</b>&nbsp;:&nbsp;RM <span id="spanTotal"></span>
						<input type="hidden" name="hidTotal" id="hidTotal" />
					</div>				
				</div>
			</div>
			<div class="row row-space-10">
				<div class="col-md-12">
					<div class="checkbox checkbox-css" style="margin-bottom: 8px;">
						<input type="checkbox" id="cbAutoTicket" name="autoTicketStatus" />
  						<label for="cbAutoTicket">Auto Ticketing</label>
  					</div>
					<button id="btnSubmit" type="submit" class="btn_submit btn btn-primary" style="margin-bottom: 10px; margin-right: 5px;"><i class="fa fa-save"></i> Submit Bet</button> <button id="btnClear" type="button" class="btn_clear btn btn-primary" style="margin-bottom: 10px"><i class="fa fa-eraser"></i> Clear</button> <img src="/loader.gif" class="submitLoader" />		
				</div>
			</div>
			<div class="table-responsive">
				<table id="data-table-default" class="table table-striped table-bordered table-width-fix table-valign-middle">
					<thead>
						<tr>
							<td width="5%">No</td>
							<td>Number</td>
							<td>Type</td>
							<td>4B</td>
							<td>4C</td>
							<td>4D</td>
							<td>4E</td>
							<td>4ABC</td>
							<td>2A</td>
							<td>2ABC</td>
							@foreach($data['pools'] as $pool)
							<td><img src="{{ $pool['images'] }}" class="allpools" id="{{ 'img_'.$pool['shortname'] }}" style="width: 20px; height: 20px;"></td>		
							@endforeach
							<td width="10%">&nbsp;</td>
						</tr>
					</thead>
					<tbody>
						<tr>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>&nbsp;</td>
							<td>
								<div class="checkbox checkbox-css">
									<input type="checkbox" id="selectAll_4B" class="selectAll" />
									<label for="selectAll_4B"> </label>
								</div>
							</td>
							<td>
								<div class="checkbox checkbox-css">
									<input type="checkbox" id="selectAll_4C" class="selectAll" />
									<label for="selectAll_4C"> </label>
								</div>
							</td>
							<td>
								<div class="checkbox checkbox-css">
									<input type="checkbox" id="selectAll_4D"  class="selectAll" />
									<label for="selectAll_4D"> </label>
								</div>
							</td>
							<td>
								<div class="checkbox checkbox-css">
									<input type="checkbox" id="selectAll_4E" class="selectAll" />
									<label for="selectAll_4E"> </label>
								</div>
							</td>
							<td>
								<div class="checkbox checkbox-css">
									<input type="checkbox" id="selectAll_4ABC" class="selectAll" />
									<label for="selectAll_4ABC"> </label>
								</div>
							</td>
							<td>
								<div class="checkbox checkbox-css">
									<input type="checkbox" id="selectAll_2A" class="selectAll" />
									<label for="selectAll_2A"> </label>
								</div>
							</td>
							<td>
								<div class="checkbox checkbox-css">
									<input type="checkbox" id="selectAll_2ABC" class="selectAll" />
									<label for="selectAll_2ABC"> </label>
								</div>
							</td>
							@foreach($data['pools'] as $pool)
							<td>
								<div class="checkbox checkbox-css">
									<input type="checkbox" id="{{ 'selectAll_'.$pool->shortname }}" class="selectPoolAll" />
									<label for="{{ 'selectAll_'.$pool->shortname }}"> </label>
								</div>
							</td>		
							@endforeach
							<td>&nbsp;</td>
						</tr>
						<?php 
						for($count = 1; $count <=30; $count++)
						{
							?>
							<tr>
								<td><input type="text" readonly id='txt_no_<?php echo $count ?>' name='txt_no_<?php echo $count ?>' data-parsley-type="number" class="form-control eatmap_input m_input" value="<?php echo old('txt_no_'.$count, $count) ?>"/></td>
								<td><input type="text" id='txt_number_<?php echo $count ?>' name='txt_number_<?php echo $count ?>' data-parsley-type="number" class="form-control drawnumber_input m_input" value="<?php echo old('txt_number_'.$count) ?>"/></td>
								<td><input type="text" readonly id='txt_type_<?php echo $count ?>' name='txt_type_<?php echo $count ?>' class="form-control eatmap_input" value="<?php echo old('txt_type_'.$count) ?>"/></td>
								<td><input type="text" id='txt_4B_<?php echo $count ?>' name='txt_4B_<?php echo $count ?>' data-parsley-type="number" class="form-control input_4B m_input" value="<?php echo old('txt_4B_'.$count) ?>"/></td>
								<td><input type="text" id='txt_4C_<?php echo $count ?>' name='txt_4C_<?php echo $count ?>' data-parsley-type="number" class="form-control input_4C m_input" value="<?php echo old('txt_4C_'.$count) ?>"/></td>
								<td><input type="text" id='txt_4D_<?php echo $count ?>' name='txt_4D_<?php echo $count ?>' data-parsley-type="number" class="form-control input_4D m_input" value="<?php echo old('txt_4D_'.$count) ?>"/></td>
								<td><input type="text" id='txt_4E_<?php echo $count ?>' name='txt_4E_<?php echo $count ?>' data-parsley-type="number" class="form-control input_4E m_input" value="<?php echo old('txt_4E_'.$count) ?>"/></td>
								<td><input type="text" id='txt_4ABC_<?php echo $count ?>' name='txt_4ABC_<?php echo $count ?>' data-parsley-type="number" class="form-control input_4ABC m_input" value="<?php echo old('txt_4ABC_'.$count) ?>"/></td>
								<td><input type="text" id='txt_2A_<?php echo $count ?>' name='txt_2A_<?php echo $count ?>' data-parsley-type="number" class="form-control input_2A m_input" value="<?php echo old('txt_2A_'.$count) ?>"/></td>
								<td><input type="text" id='txt_2ABC_<?php echo $count ?>' name='txt_2ABC_<?php echo $count ?>' data-parsley-type="number" class="form-control input_2ABC m_input" value="<?php echo old('txt_2ABC_'.$count) ?>"/></td>
								@foreach($data['pools'] as $pool)
								<td>
									<div class="checkbox checkbox-css">
										<input type="hidden" name="{{ 'select_'.$pool->shortname.'_'.$count }}" value="0" />
										<input type="checkbox" class="chkpools" name="{{ 'select_'.$pool->shortname.'_'.$count }}" id="{{ 'select_'.$pool->shortname.'_'.$count }}" />
										<label for="{{ 'select_'.$pool->shortname.'_'.$count }}"> </label>
									</div>
								</td>		
								@endforeach
								<td>
									<select class="form-control bt"  id="{{ 'selBetType'.$count }}" name="{{ 'selBetType'.$count }}">
										<option></option>
										<option>Box</option>
										<option>IBox</option>
										<option>P-H</option>
										<option>P-B</option>
									</select>
								</td>
							</tr>
							<?php 
						}
						?>
					</tbody>
				</table>	
			</div>	


			<!-- end panel-body -->
		</div>
		<!-- end panel -->
		{!! Form::close() !!}
		@stop

		@section("page_script")
		<script src="/assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
		<script src="/assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
		<script src="/assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
		<script src="/assets/plugins/bootstrap-sweetalert/sweetalert.min.js"></script>
		<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
		<script src="/assets/plugins/parsley/dist/parsley.js"></script>
		<script src="/js/ddlHelper.js"></script>
		<!-- ================== END PAGE LEVEL JS ================== -->
		<script>
			$(".submitLoader").hide();
			$(document).ready(function() {
				$('.m_input').keypress(function (e) {
					var regex = new RegExp("^[0-9|.]+$");
					str = $(this).attr('id');

					if (str.indexOf("txt_number_") >= 0)
					{
						regex = new RegExp("^[0-9]+$");
					}

					var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
					if (regex.test(str)) {
						return true;
					}

					e.preventDefault();
					return false;
				});

				$(".dd").on('change', function () {  recalculate(); });
				$(".chkpools").on('change', function () {  recalculate(); });
				$(".m_input").on('change', function () {  recalculate(); });
				$(".bt").on('change', function () {  recalculate(); });

				$('#betForm').on('submit', function(e){
					clearError();
					var entry_found = false;
		    var outstr = ""; // Blank if there is no error. Else, it's non-blank.

		    if (getDateCount() == 0)
		    {
		    	outstr = "Please select a date.";
		    }
		    
		    for (var i = 0; i < 30; ++i) {
		    	var o = isValidRow(i);
		    	if (o.is_valid && o._number != "") {
		    		entry_found = true;
		    	} else if (o.is_valid) {
		            // Do nothing, as bet number is blank. This entry would be ignored.
		        } else {
		        	outstr = "This betting line is incomplete / invalid: " + (i).toString();
		        	break;
		        }
		    }
		    if (outstr != "") {
		    	showError(outstr);
		    	e.preventDefault();
		    } else if (!entry_found) {
		    	showError("No bets found.");
		    	e.preventDefault();
		    }
		    else
		    {
				// assuming all entries are valid now
				// try to use ajax to submit betting

				$.ajax({
					type: 'POST',
					data: $('#betForm').serialize(),
					url: '/bet/makeBet4dSpecial',
					beforeSend: function() {
                    	$("#btnSubmit").prop( "disabled", true ); //Disable
                    	$(".submitLoader").show();
                	},
					success: function(data) {
						$("#btnSubmit").prop( "disabled", false ); //Enable
                    	$(".submitLoader").hide();
						
						if (data.success == false)
						{
							showError(data.message);
						}
						else
						{
							console.log(data);
							$("#spanTicket").html(data.ticket);
							$("#divTicket").show();
							clearAllInputs();
						}						
					}
				});
				
				e.preventDefault();
			} 
			
		});
			});

			function getString(s) {
				if (!s) s = "";
				s = s.trim();
				return s;
			}


    // this is to check if the row is with correct entries
    function isValidRow(row_id) {
        /*
        The followings would return FALSE
        Number:
         - contains non-numeric characters.
         - length is neither 3 nor 4.
         - IF NUMBER'S LENGTH IS 0, IT'S CONSIDERED TRUE.
        Amount:
         - contains non-numeric characters.
         - fails float-parse (e.g., contains 2 dots).
         - blank amount is equivalent to 0. this does not fail.
         - if number's length is 3, any non-zero value for big, small and 4a would fail out.
         */
         var retobj = {};
         retobj.is_valid = true;
         retobj._number = "";
         retobj._amtstr_4b = "";
         retobj._amtstr_4c = "";
         retobj._amtstr_4d = "";
         retobj._amtstr_4e = "";
         retobj._amtstr_4abc = "";
         retobj._amtstr_2a = "";
         retobj._amtstr_2abc = "";
         retobj._amtfloat_4b = 0;
         retobj._amtfloat_4c = 0;
         retobj._amtfloat_4d = 0;
         retobj._amtfloat_4e = 0;
         retobj._amtfloat_4abc = 0;
         retobj._amtfloat_2a = 0;
         retobj._amtfloat_2abc = 0;

         var s = row_id.toString();
         var num = getString($("#txt_number_" + s).val());
         if (num == "") return retobj;

         retobj._number = num;

         if($("#txt_4B_" + s).is("[readonly]") == false){
         	retobj._amtstr_4b = getString($("#txt_4B_" + s).val());
         }

         if($("#txt_4C_" + s).is("[readonly]") == false){
         	retobj._amtstr_4c = getString($("#txt_4C_" + s).val());
         }

         if($("#txt_4D_" + s).is("[readonly]") == false){
         	retobj._amtstr_4d = getString($("#txt_4D_" + s).val());
         }

         if($("#txt_4E_" + s).is("[readonly]") == false){
         	retobj._amtstr_4e = getString($("#txt_4E_" + s).val());
         }

         if($("#txt_4ABC_" + s).is("[readonly]") == false){
         	retobj._amtstr_4abc = getString($("#txt_4ABC_" + s).val());
         }

         if($("#txt_2A_" + s).is("[readonly]") == false){
         	retobj._amtstr_2a = getString($("#txt_2A_" + s).val());
         }

         if($("#txt_2ABC_" + s).is("[readonly]") == false){
         	retobj._amtstr_2abc = getString($("#txt_2ABC_" + s).val());
         }

        // retobj._amtstr_big = getString($("#txt_Big_" + s).val());
        // retobj._amtstr_small = getString($("#txt_Small_" + s).val());
        // retobj._amtstr_4a = getString($("#txt_4A_" + s).val());
        // retobj._amtstr_3a = getString($("#txt_3A_" + s).val());
        // retobj._amtstr_3c = getString($("#txt_3ABC_" + s).val());

        if (!/^\d{2,4}$/.test(retobj._number)) retobj.is_valid = false;
        else if (!/^\d*(\.?\d+)?$/.test(retobj._amtstr_4b)) retobj.is_valid = false;
        else if (!/^\d*(\.?\d+)?$/.test(retobj._amtstr_4c)) retobj.is_valid = false;
        else if (!/^\d*(\.?\d+)?$/.test(retobj._amtstr_4d)) retobj.is_valid = false;
        else if (!/^\d*(\.?\d+)?$/.test(retobj._amtstr_4e)) retobj.is_valid = false;
        else if (!/^\d*(\.?\d+)?$/.test(retobj._amtstr_4abc)) retobj.is_valid = false;
        else if (!/^\d*(\.?\d+)?$/.test(retobj._amtstr_2a)) retobj.is_valid = false;
        else if (!/^\d*(\.?\d+)?$/.test(retobj._amtstr_2abc)) retobj.is_valid = false;

        if (retobj.is_valid) {
        	if (retobj._amtstr_4b == "") retobj._amtstr_4b= "0";
        	if (retobj._amtstr_4c == "") retobj._amtstr_4c = "0";
        	if (retobj._amtstr_4d == "") retobj._amtstr_4d = "0";
        	if (retobj._amtstr_4e == "") retobj._amtstr_4e = "0";
        	if (retobj._amtstr_4abc == "") retobj._amtstr_4abc = "0";
        	if (retobj._amtstr_2a == "") retobj._amtstr_2a = "0";
        	if (retobj._amtstr_2abc == "") retobj._amtstr_2abc = "0";

        	retobj._amtfloat_4b = parseFloat(retobj._amtstr_4b);
        	retobj._amtfloat_4c = parseFloat(retobj._amtstr_4c);
        	retobj._amtfloat_4d = parseFloat(retobj._amtstr_4d);
        	retobj._amtfloat_4e = parseFloat(retobj._amtstr_4e);
        	retobj._amtfloat_4abc = parseFloat(retobj._amtstr_4abc);
        	retobj._amtfloat_2a = parseFloat(retobj._amtstr_2a);
        	retobj._amtfloat_2abc = parseFloat(retobj._amtstr_2abc);

        	var total_4d = retobj._amtfloat_4b + retobj._amtfloat_4c + retobj._amtfloat_4d + retobj._amtfloat_4e + retobj._amtfloat_4abc;
        	var total_2d = retobj._amtfloat_2a + retobj._amtfloat_2abc;
        	
        	if (retobj._number.length == 2 && total_4d > 0) retobj.is_valid = false;
        	else if (retobj._number.length == 2 && total_2d == 0) retobj.is_valid == false;
        	else if (retobj._number.length == 4 && total_2d + total_4d == 0) retobj.is_valid = false;

        	if (total_4d + total_2d == 0)retobj.is_valid = false;
        }
        return retobj;
    }

    // Get how many days being selected
    function getDateCount()
    {
    	count = 0;
    	$(".dd").each(function() {
    		if ($(this).is(":checked"))
    		{
    			count = count + 1;
    		}
    	});
    	return count;
    }

    // Get how many pools being selected for the row
    function getPoolsSelectedCount(rowId)
    {
    	count = 0;
    	$(".allpools").each(function() {
    		id = $(this).attr('id');
    		targetid = id.replace('img', 'select') + '_' + rowId;

    		if ($('#'+targetid).is(":checked"))
    		{
    			count = count + 1;
    		}
    	});
    	return count;
    }

    // Recalculate the number amount of the betting forms
    function recalculate()
    {
    	var total_4b = 0;
    	var total_4c = 0;
    	var total_4d = 0;
    	var total_4e = 0;
    	var total_4abc = 0;
    	var total_2a = 0;
    	var total_2abc = 0;
    	var total_amount = 0;
    	
    	for (var i = 0; i < 30; ++i) {

        	// change the row id to string
        	var s = i.toString();

            // get total how many pools being selected
            var pools = getPoolsSelectedCount(i);

            // get total how many days being selected
            var days = getDateCount();

            var o = isValidRow(i); 

            var timesfactors = 1;
            selected_type = $('#selBetType'+i).val();
            if (selected_type == '' || selected_type == 'IBox')
            {
            	timesfactors = pools * days;
            }
            else if (selected_type == "P-H" || selected_type == "P-B") {
            	timesfactors = pools * days * 10;
            }
            else if (selected_type == "Box") {
            	timesfactors = pools * days * getCombinationCount(o._number);
            }
            
			// start set the total of the betting form
			total_4b = total_4b + o._amtfloat_4b * timesfactors;
			total_4c = total_4c + o._amtfloat_4c * timesfactors;
			total_4d = total_4d + o._amtfloat_4d * timesfactors;
			total_4e = total_4e + o._amtfloat_4e * timesfactors;
			total_4abc = total_4abc + o._amtfloat_4abc * timesfactors;
			total_2a = total_2a + o._amtfloat_2a * timesfactors;
			total_2abc = total_2abc + o._amtfloat_2abc * timesfactors;
			
			total_amount = total_amount + o._amtfloat_4b * timesfactors + 
			o._amtfloat_4c * timesfactors + 
			o._amtfloat_4d * timesfactors + 
			o._amtfloat_4e * timesfactors + 
			o._amtfloat_4abc * timesfactors +
			o._amtfloat_2a * timesfactors +
			o._amtfloat_2abc * timesfactors;

			var combinations = 0;

			
            /*var pools_days = 1;
            
            if (muldiv_elem.val() == "MUL") pools_days = pools * days;
            if (type_elem.val() == "Normal" || type_elem.val() == "iBox") {
                $sub_total.val(pools_days * total_amount);
            } else if (type_elem.val() == "PH") {
                $sub_total.val(pools_days * total_amount * 10);
            } else if (type_elem.val() == "Box") {
                $sub_total.val(pools_days * total_amount * getCombinationCount(o._number));
            }*/
        }

        $('#span4B').text(total_4b);
        $('#span4C').text(total_4c);
        $('#span4D').text(total_4d);
        $('#span4E').text(total_4e);
        $('#span4ABC').text(total_4abc);
        $('#span2A').text(total_2a);
        $('#span2ABC').text(total_2abc);
        $("#spanTotal").text(total_amount);
        $('#hidTotal').val(total_amount);
    }

    // this is to get the total permutation of number
    function getCombinationCount(number) {
    	if (!/\d{2,4}/.test(number)) return 0;
    	var digit_counts = [];
    	for (var i = 0; i < 10; ++i) digit_counts.push(0);
    		var digits = [];
    	for (i = 0; i < number.length; ++i) {
    		var l = number.charAt(i);
    		if (digit_counts[l] == 0) digits.push(l);
    		++digit_counts[l];
    	}
    	var comb_count_4 = [0, 1, 4, 12, 24];
    	var comb_count_2 = [0, 1];
    	var numcode = "";
    	var retval = 0;
    	for (i = 0; i < digits.length; ++i) numcode = numcode + digit_counts[digits[i]].toString();

    		if (numcode == "22") {
    			retval = 6;
    		} else if (number.length == 2) {
    			retval = comb_count_2[digits.length];
    		} else if (number.length == 4) {
    			retval = comb_count_4[digits.length];
    		}
    		return retval;
    	}


    	$(document).on('change', '.drawnumber_input', function() {

    		$var = $(this).val();
    		$no = $(this).attr('id').replace("txt_number_", "");

    		if ($var.length == 2) {
    			$("#txt_type_"+$no).val("2D");
    			$(this).css('backgroundColor', 'white');

    			$("#txt_4B_"+$no).val("");
    			$("#txt_4B_"+$no).attr('readonly', true);

    			$("#txt_4C_"+$no).val("");
    			$("#txt_4C_"+$no).attr('readonly', true);
    			
    			$("#txt_4D_"+$no).val("");
    			$("#txt_4D_"+$no).attr('readonly', true);

    			$("#txt_4E_"+$no).val("");
    			$("#txt_4E_"+$no).attr('readonly', true);

    			$("#txt_4ABC_"+$no).val("");
    			$("#txt_4ABC_"+$no).attr('readonly', true);

    			$("#txt_2A_"+$no).attr('readonly', false);
    			$("#txt_2ABC_"+$no).attr('readonly', false);

    		} else if ($var.length == 4) {
    			$("#txt_type_"+$no).val("4D");
    			$(this).css('backgroundColor', 'white');

    			$("#txt_2A_"+$no).val("");
    			$("#txt_2A_"+$no).attr('readonly', true);

    			$("#txt_2ABC_"+$no).val("");
    			$("#txt_2ABC_"+$no).attr('readonly', true);

    			$("#txt_4B_"+$no).attr('readonly', false);
    			$("#txt_4C_"+$no).attr('readonly', false);
    			$("#txt_4D_"+$no).attr('readonly', false);
    			$("#txt_4E_"+$no).attr('readonly', false);
    			$("#txt_4ABC_"+$no).attr('readonly', false);
    		} else {
    			$("#txt_type_"+$no).val("Invalid");
    			$(this).css('backgroundColor', 'yellow');
    		}
    		recalculate();
    	});

    	$(document).on('change', '.selectAll', function() {

    		if(this.checked) {
            //Do stuff
            copyAllDown($(this));
        }
        else
        {
        	clearAllDown($(this))
        }
    });

    	$(document).on('change', '.selectPoolAll', function() {

    		if(this.checked) {
            //Do stuff
            checkAllDown($(this));
        }
        else
        {
        	uncheckAllDown($(this))
        }
    });

	// show error message
	function showError(msg)
	{
		$('#divMsg').addClass("alert alert-danger");
		$('#divMsg').text(msg);
	}

	function clearError()
	{
		$('#divMsg').removeClass("alert alert-danger");
		$('#divMsg').text('');
	}

    // copy all the textbox down
    function copyAllDown(control)
    {
    	functional_area = control.attr('id').replace("selectAll_", "txt_");
    	reference_value = document.getElementById(functional_area + "_" + 1).value;
    	for (count = 2; count <= 30; count++)
    	{
    		if($("#" + functional_area + "_" + count).is("[readonly]") == false){
    			document.getElementById(functional_area + "_" + count).value = (reference_value);
    		}
    	}   
    	recalculate();	    	
    }

    // clear all the textbox
    function clearAllDown(control)
    {
    	functional_area = control.attr('id').replace("selectAll_", "txt_");
    	for (count = 2; count <= 30; count++)
    	{
    		document.getElementById(functional_area + "_" + count).value = "";
    	}  
    	recalculate(); 	    	
    }

    // Make all the checkbox selected
    function checkAllDown(control)
    {
    	functional_area = control.attr('id').replace("selectAll_", "select_");
    	for (count = 1; count <= 30; count++)
    	{
    		document.getElementById(functional_area + "_" + count).checked = true;
    	}   
    	recalculate();	    	
    }

    // Make all the checkbox unselected
    function uncheckAllDown(control)
    {
    	functional_area = control.attr('id').replace("selectAll_", "select_");
    	for (count = 1; count <= 30; count++)
    	{
    		document.getElementById(functional_area + "_" + count).checked = false;
    	} 
    	recalculate();  	    	
    }

    $('#btnClear').click(function(){
    	clearAllInputs();
    });

    //Clear all inputs
    function clearAllInputs() {
    	$('.selectAll').prop('checked', false); // Unchecks it
    	$('.selectPoolAll').prop('checked', false); // Unchecks it
    	$('.dd').prop('checked', false); // Unchecks it
    	$('#cbAutoTicket').prop('checked', false); // Unchecks it
    	$('.chkpools').prop('checked', false); // Unchecks it
    	$('form').find("input[type=text].inputTelephone, input[type=text].inputRemark, td:nth-child(n+2) input[type=text]").val("");
    	$('.drawnumber_input').css('backgroundColor', 'white');
    	$('.selBetType').val(1);
    }
</script>
@stop