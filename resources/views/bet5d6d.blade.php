@extends("layouts.userlayout")
@section("title", "Bet Form")
@section("header", "Bet 5D/6D")

@section('headerScript')
<link href="/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css" rel="stylesheet" />
<link href="/assets/plugins/parsley/src/parsley.css" rel="stylesheet" />
@stop

@section("body")
{!! Form::open(['url' => '/bet/makeBet5d6d', 'id' => 'betForm', 'name' => 'betForm', 'class' => 'form-horizontal']) !!}
<div class="panel panel-inverse" >
	<!-- begin panel-heading -->
	<div class="panel-heading">
		<div class="panel-heading-btn">
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
		</div>
		<h4 class="panel-title">Bet Form 5D/6D</h4>
	</div>
	<!-- end panel-heading -->
	<!-- begin panel-body -->

	@if (session('message'))
	<div class="{{session('class')}}">{{session('message')}}</div>
	@endif

	<div id="divMsg"></div>

	<div id="divTicket" style="display: none" class="alert alert-success">
		<span id="spanTicket"></span>
	</div>

	<div class="panel-body">

		@if (session('message_ticket'))
		<div class="{{session('class_ticket')}}">{!! session('message_ticket') !!}</div>
		@endif

		<div class="row row-space-10">
			<div class="col-md-4">
				<div class="form-group m-b-10" data-parsley-validate="true">
					<label for="exampleInputEmail1">Bet Agent</label>
					<select class="form-control" name="betAgent">
						@foreach($data['downlines'] as $d)
						<option>{{ $d }}</option>               		
						@endforeach
					</select>
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group m-b-10">
					<label for="exampleInputPassword1">Credit Left</label>
					<input type="text" class="form-control" name="txtCreditLeft" value="{{ $data['me']->credit_limit }}" readonly /> 
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group m-b-10">
					<label for="exampleInputPassword1">Telephone (SMS)</label>
					<input type="text" class="form-control inputTelephone" name="txtTelephone" value="" placeholder="Enter Phone (0102345678)" />
				</div>
			</div>
		</div>
		<div class="row row-space-10">
			<div class="col-md-4">
				<div class="form-group m-b-10" data-parsley-validate="true">
					<label for="exampleInputEmail1">Betting for Draw Dates</label>
					<div class="checkbox checkbox-css m-b-20">
						@foreach($data['schedules'] as $s)
						<div class="checkbox checkbox-css checkbox-inline">
							<input type="hidden" name={{ "dd_".$s->drawdate  }} value="0" />
							<input type="checkbox" class="dd" name={{ "dd_".$s->drawdate  }} id={{ "dd_".$s->drawdate  }} />
							<label for={{ "dd_".$s->drawdate  }}>{{ $s->day }}</label>
						</div>
						@endforeach                                  
					</div>
				</div>
			</div> 
			<div class="col-md-8">
				<div class="form-group m-b-10">
					<label for="exampleInputPassword1">Remark</label>
					<input type="text" class="form-control inputRemark" name="txtRemark" value=""  /> 
				</div>
			</div>        
		</div>
		<div class="row row-space-10">
			<div class="col-md-2">
				<div class="alert alert-success fade show m-b-10">
					<b>5D</b>&nbsp;:&nbsp;RM <span id="span5D"></span>
				</div>					
			</div>
			<div class="col-md-2">
				<div class="alert alert-success fade show m-b-10">
					<b>6D</b>&nbsp;:&nbsp;RM <span id="span6D"></span>
				</div>				
			</div>
			<div class="col-md-2">
				<div class="alert alert-success fade show m-b-10">
					<b>Grand Total</b>&nbsp;:&nbsp;RM <span id="spanTotal"></span>
					<input type="hidden" name="hidTotal" id="hidTotal" />
				</div>				
			</div>
		</div>
		<div class="row row-space-10">
			<div class="col-md-12">
				<div class="checkbox checkbox-css" style="margin-bottom: 8px;">
					<input type="checkbox" id="cbAutoTicket" name="autoTicketStatus" />
  					<label for="cbAutoTicket">Auto Ticketing</label>
  				</div>
				<button id="btnSubmit" type="submit" class="btn_submit btn btn-primary" style="margin-bottom: 10px"><i class="fa fa-save"></i> Submit Bet</button> <button id="btnClear" type="button" class="btn_clear btn btn-primary" style="margin-bottom: 10px"><i class="fa fa-eraser"></i> Clear</button> <img src="/loader.gif" class="submitLoader" />		
			</div>
		</div>
		<div class="table-responsive">
			<table id="data-table-default" class="table table-striped table-bordered table-width-fix table-valign-middle">
				<thead>
					<tr>
						<td width="5%">No</td>
						<td width="20%">Number</td>
						<td>Type</td>
						<td>5D</td>
						<td>6D</td>
						<!-- <td width="1%">
							<img src="../assets/img/lotto/bet-logo-toto-inversed.png" class="allpools" id="img_tt" style="width: 20px; height: 20px;">
						</td> -->		
					</tr>
				</thead>
				<tbody>
					<tr>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>&nbsp;</td>
						<td>
							<div class="checkbox checkbox-css">
								<input type="checkbox" id="selectAll_5D"  class="selectAll" />
								<label for="selectAll_5D"> </label>
							</div>
						</td>
						<td>
							<div class="checkbox checkbox-css">
								<input type="checkbox" id="selectAll_6D" class="selectAll" />
								<label for="selectAll_6D"> </label>
							</div>
						</td>
						<!-- <td>
							<div class="checkbox checkbox-css">
								<input type="checkbox" id="selectAll_tt" class="selectPoolAll" />
								<label for="selectAll_tt"> </label>
							</div>
						</td>	 -->	
					</tr>
					<?php 
					for($count = 1; $count <=30; $count++)
					{
						?>
						<tr>
							<td><input type="text" readonly id='txt_no_<?php echo $count ?>' name='txt_no_<?php echo $count ?>' data-parsley-type="number" class="form-control eatmap_input m_input" value="<?php echo old('txt_no_'.$count, $count) ?>"/></td>
							<td><input type="text" id='txt_number_<?php echo $count ?>' name='txt_number_<?php echo $count ?>' data-parsley-type="number" class="form-control drawnumber_input m_input" value="<?php echo old('txt_number_'.$count) ?>"/></td>
							<td><input type="text" readonly id='txt_type_<?php echo $count ?>' name='txt_type_<?php echo $count ?>' class="form-control eatmap_input" value="<?php echo old('txt_type_'.$count) ?>"/></td>
							<td><input type="text" id='txt_5D_<?php echo $count ?>' name='txt_5D_<?php echo $count ?>' data-parsley-type="number" class="form-control input_5D m_input" value="<?php echo old('txt_5D_'.$count) ?>"/></td>
							<td><input type="text" id='txt_6D_<?php echo $count ?>' name='txt_6D_<?php echo $count ?>' data-parsley-type="number" class="form-control input_6D m_input" value="<?php echo old('txt_6D_'.$count) ?>"/></td>
							<!-- <td>
								<div class="checkbox checkbox-css">
									<input type="hidden" name="select_tt_{{ $count }}" value="0" />
									<input type="checkbox" class="chkpools" name="select_tt_{{ $count }}" id="select_tt_{{ $count }}" />
									<label for="select_tt_{{ $count }}"> </label>
								</div>
							</td>		 -->
						</tr>
						<?php 
					}
					?>
				</tbody>
			</table>
		</div>
	</div>
</div>
{!! Form::close() !!}
@stop

@section("page_script")
<script type="text/javascript">
	$(".submitLoader").hide();
	$(function(){
		$('.m_input').keypress(function (e) {
			var regex = new RegExp("^[0-9|.]+$");
			str = $(this).attr('id');

			if (str.indexOf("txt_number_") >= 0)
			{
				regex = new RegExp("^[0-9]+$");
			}

			var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
			if (regex.test(str)) {
				return true;
			}

			e.preventDefault();
			return false;
		});

		$(".dd").on('change', function () {  recalculate(); });
		$(".chkpools").on('change', function () {  recalculate(); });
		$(".m_input").on('change', function () {  recalculate(); });
		$(".bt").on('change', function () {  recalculate(); });

		$(document).on('change', '.drawnumber_input', function() {
			$var = $(this).val();
			$no = $(this).attr('id').replace("txt_number_", "");

			if ($var.length == 5){
				$("#txt_type_"+$no).val("5D");
				$(this).css('backgroundColor', 'white');

				$("#txt_6D_"+$no).val("");
				$("#txt_6D_"+$no).attr('readonly', true);

				$("#txt_5D_"+$no).attr('readonly', false);

				recalculate();
			} else if ($var.length == 6){
				$("#txt_type_"+$no).val("6D");
				$(this).css('backgroundColor', 'white');

				$("#txt_6D_"+$no).attr('readonly', false);

				$("#txt_5D_"+$no).val("");
				$("#txt_5D_"+$no).attr('readonly', true);

				recalculate();
			} else {
				$("#txt_type_"+$no).val("Invalid");
				$(this).css('backgroundColor', 'yellow');
			}
		});

		$(document).on('change', '.selectAll', function() {
			if(this.checked) {
				copyAllDown($(this));
			} else {
				clearAllDown($(this))
			}
		});

		$(document).on('change', '.selectPoolAll', function() {
			if(this.checked) {
				checkAllDown($(this));
			} else {
				uncheckAllDown($(this))
			}
		});

		$('#betForm').on('submit', function(e){
			recalculate();
			clearError();
			var entry_found = false;
		    var outstr = ""; // Blank if there is no error. Else, it's non-blank.

		    if (getDateCount() == 0)
		    {
		    	outstr = "Please select a date.";
		    }

		    for (var i = 0; i < 30; ++i) {
		    	var o = isValidRow(i);
		    	if (o.is_valid && o._number != "") {
		    		entry_found = true;
		    	} else if (o.is_valid) {
		            // Do nothing, as bet number is blank. This entry would be ignored.
		        } else {
		        	outstr = "This betting line is incomplete / invalid: " + (i).toString();
		        	break;
		        }
		    }
		    if (outstr != "") {
		    	showError(outstr);
		    	e.preventDefault();
		    } else if (!entry_found) {
		    	showError("No bets found.");
		    	e.preventDefault();
		    }
		    else
		    {
				// assuming all entries are valid now
				// try to use ajax to submit betting

				$("#divTicket").hide();

				$.ajax({
					type: 'POST',
					data: $('#betForm').serialize(),
					url: '/bet/makeBet5d6d',
					beforeSend: function() {
                    	$("#btnSubmit").prop( "disabled", true ); //Disable
                    	$(".submitLoader").show();
                	},
					success: function(data) {
						$("#btnSubmit").prop( "disabled", false ); //Enable
                    	$(".submitLoader").hide();

						if (data.success == false)
						{
							showError(data.message);
						}
						else
						{
							console.log(data);
							$("#spanTicket").html(data.ticket);
							$("#divTicket").show();
							clearAllInputs();
						}						
					}
				});
				
				e.preventDefault();

			} 

		});

		function recalculate()
		{
			var total_5D = 0;
			var total_6D = 0;
			var total_amount = 0;

			for (var i = 0; i < 30; ++i) {
				// change the row id to string
				var s = i.toString();

				// get total how many days being selected
				var days = getDateCount();

				var o = isValidRow(i);

				var sub_total_5D = o._amtfloat_5d * days;
				var sub_total_6D = o._amtfloat_6d * days;

				total_5D += sub_total_5D;
				total_6D += sub_total_6D;

				total_amount += sub_total_5D + sub_total_6D;
			}

			$('#span5D').text(total_5D);
			$('#span6D').text(total_6D);
			$("#spanTotal").text(total_amount);
			$('#hidTotal').val(total_amount);
		}

		// this is to check if the row is with correct entries
		function isValidRow(row_id) {
	        /*
	        The followings would return FALSE
	        Number:
	        - contains non-numeric characters.
	        - length is neither 3 nor 4.
	        - IF NUMBER'S LENGTH IS 0, IT'S CONSIDERED TRUE.
	        Amount:
	        - contains non-numeric characters.
	        - fails float-parse (e.g., contains 2 dots).
	        - blank amount is equivalent to 0. this does not fail.
	        - if number's length is 3, any non-zero value for big, small and 4a would fail out.
	        */

	        var retobj = {};
	        retobj.is_valid = true;
	        retobj._number = "";
	        retobj._amtstr_5d = "";
	        retobj._amtstr_6d = "";
	        retobj._amtfloat_5d = 0;
	        retobj._amtfloat_6d = 0;

	        var s = row_id.toString();
	        var num = getString($("#txt_number_" + s).val());
	        if (num == "") return retobj;

	        retobj._number = num;

	        if($("#txt_5D_" + s).is("[readonly]") == false){
	        	retobj._amtstr_5d = getString($("#txt_5D_" + s).val());
	        }

	        if($("#txt_6D_" + s).is("[readonly]") == false){
	        	retobj._amtstr_6d = getString($("#txt_6D_" + s).val());
	        }

	        if (!/^\d{5,6}$/.test(retobj._number)) retobj.is_valid = false;
	        else if (!/^\d*(\.?\d+)?$/.test(retobj._amtstr_5d)) retobj.is_valid = false;
	        else if (!/^\d*(\.?\d+)?$/.test(retobj._amtstr_6d)) retobj.is_valid = false;

	        if (retobj.is_valid) {
	        	if (retobj._amtstr_5d == "") retobj._amtstr_5d = "0";
	        	if (retobj._amtstr_6d == "") retobj._amtstr_6d = "0";

	        	retobj._amtfloat_5d = parseFloat(retobj._amtstr_5d);
	        	retobj._amtfloat_6d = parseFloat(retobj._amtstr_6d);

	        	if (retobj._number.length == 5 && retobj._amtfloat_5d == 0) retobj.is_valid = false;
	        	else if (retobj._number.length == 6 && retobj._amtfloat_6d == 0) retobj.is_valid = false;

	        	if (retobj._amtfloat_5d + retobj._amtfloat_6d == 0)retobj.is_valid = false;
	        }
	        return retobj;
	    }

	    function getString(s) {
	    	if (!s) s = "";
	    	s = s.trim();
	    	return s;
	    }

		// copy all the textbox down
		function copyAllDown(control)
		{
			functional_area = control.attr('id').replace("selectAll_", "txt_");
			reference_value = document.getElementById(functional_area + "_" + 1).value;
			for (count = 2; count <= 30; count++)
			{
				if($("#" + functional_area + "_" + count).is("[readonly]") == false){
					document.getElementById(functional_area + "_" + count).value = (reference_value);
				}
			}   
			recalculate();	    	
		}

	    // clear all the textbox
	    function clearAllDown(control)
	    {
	    	functional_area = control.attr('id').replace("selectAll_", "txt_");
	    	for (count = 2; count <= 30; count++)
	    	{
	    		document.getElementById(functional_area + "_" + count).value = "";
	    	}  
	    	recalculate(); 	    	
	    }

	    // Make all the checkbox selected
	    function checkAllDown(control)
	    {
	    	functional_area = control.attr('id').replace("selectAll_", "select_");
	    	for (count = 1; count <= 30; count++)
	    	{
	    		document.getElementById(functional_area + "_" + count).checked = true;
	    	}   
	    	recalculate();	    	
	    }

	    // Make all the checkbox unselected
	    function uncheckAllDown(control)
	    {
	    	functional_area = control.attr('id').replace("selectAll_", "select_");
	    	for (count = 1; count <= 30; count++)
	    	{
	    		document.getElementById(functional_area + "_" + count).checked = false;
	    	} 
	    	recalculate();  	    	
	    }

	    // Get how many days being selected
	    function getDateCount()
	    {
	    	count = 0;
	    	$(".dd").each(function() {
	    		if ($(this).is(":checked")){
	    			count = count + 1;
	    		}
	    	});
	    	return count;
	    }

	    // show error message
	    function showError(msg)
	    {
	    	$('#divMsg').addClass("alert alert-danger");
	    	$('#divMsg').text(msg);
	    }

	    function clearError()
	    {
	    	$('#divMsg').removeClass("alert alert-danger");
	    	$('#divMsg').text('');
	    }

	    $('#btnClear').click(function(){
	    	clearAllInputs();
	    });

    	//Clear all inputs
    	function clearAllInputs() {
    		$('.selectAll').prop('checked', false); // Unchecks it
    		$('.selectPoolAll').prop('checked', false); // Unchecks it
    		$('.dd').prop('checked', false); // Unchecks it
    		$('#cbAutoTicket').prop('checked', false); // Unchecks it
    		$('.chkpools').prop('checked', false); // Unchecks it
    		$('form').find("input[type=text].inputTelephone, input[type=text].inputRemark, td:nth-child(n+2) input[type=text]").val("");
    		$('.drawnumber_input').css('backgroundColor', 'white');
    		$('.selBetType').val(1);
    	}

});
</script>
@stop