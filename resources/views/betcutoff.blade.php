@extends("layouts.userlayout")
@section("title", "Betting Cut Off")
@section("header", "Edit Betting Cut Off")

@section('headerScript')
<link href="/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css" rel="stylesheet" />
<link href="/assets/plugins/parsley/src/parsley.css" rel="stylesheet" />
@stop

@section("body")
<!-- begin panel -->
<div class="panel panel-inverse" >
	<!-- begin panel-heading -->
	<div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        </div>
        <h4 class="panel-title">Bet Cut Off List</h4>
    </div>
    <!-- end panel-heading -->
    <!-- begin panel-body -->

    <div class="panel-body">
      @if (session('message'))
      <div class="{{session('class')}}">{{session('message')}}</div>
      @endif
      <!-- form-control-sm -->
      {!! Form::open(['url' => '/bet/saveCutOff', 'class' => 'form-horizontal']) !!}
      <?php 
      $count = 0;    	   
      for($count = 0; $count < $data['pools']->count(); $count++)
      {    	
        if ($count % 3 == 0){  
            if ($count != 0)
            {
                echo '</div>';
            }
            echo '<div class="row">'; 	           
        }    	   
        ?>

        <div class="col-md-2">
            <div class="form-group">
                <label>{{ $data['pools'][$count]->name }}</label>
                <select class="form-control width-full m-b-10" id="ddl_hour_{{ $data['pools'][$count]->shortname }}" name="ddl_hour_{{ $data['pools'][$count]->shortname }}">
                  @for($c=0; $c<24; $c++)
                  <?php 
                  $temp = $data['cutoffs'][$data['pools'][$count]->id]->hours;
                  $option = "";
                  if ($c == $temp)
                  {
                    $option = "selected";
                }
                ?>

                <option <?php echo $option; ?>>{{ str_pad($c, 2,"0",STR_PAD_LEFT) }}</option>
                @endfor
            </select>
        </div>
    </div>
    <div class="col-md-2">
     <div class="form-group">
        <label>&nbsp;</label>
        <select class="form-control width-full m-b-10" id="ddl_minute_{{ $data['pools'][$count]->shortname }}" name="ddl_minute_{{ $data['pools'][$count]->shortname }}">
          @for($c=0; $c<60; $c++)
          <?php 
          $temp = $data['cutoffs'][$data['pools'][$count]->id]->minutes;
          $option = "";
          if ($c == $temp)
          {
            $option = "selected";
        }
        ?>
        <option <?php echo $option; ?>>{{ str_pad($c, 2,"0",STR_PAD_LEFT) }}</option>
        @endfor
    </select>
</div>
</div>

<?php 
} 
echo '</div>';
?>
<button id="btnSave" class="btn_submit btn btn-primary"><i class="fa fa-save"></i> Save</button>
{!! Form::close() !!}
</div>
<!-- end panel-body -->
</div>
<!-- end panel -->	

<div class="panel panel-inverse">
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        </div>
        <h4 class="panel-title">Void Timeout</h4>
    </div>
    <div class="panel-body">
        @if (session('msg'))
        <div class="alert {{session('class1')}}">{{session('msg')}}</div>
        @endif
        <form method="POST" action="{{ action('MemberController@store_voidtimeout') }}" class="f-s-14">
            {{ csrf_field() }}
            <div class="row">
                <div class="col-md-2">
                    <div class="form-group">
                        <label>Time</label>
                        <div class="d-flex align-items-center">
                            <select name="void_timeout" class="form-control mr-2" style="width: 70px">
                                @for($i = 1; $i <= 60; $i++)
                                <option value="{{ $i }}">{{ str_pad($i, 2, "0", STR_PAD_LEFT) }}</option>
                                @endfor
                            </select>
                            <span>minutes</span>
                        </div>
                    </div>
                </div>
            </div>
            <button id="btn_savevoid" type="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
        </form>
    </div>
</div>

@stop

@section("page_script")
<script src="/assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="/assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
<script src="/assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script src="/assets/plugins/bootstrap-sweetalert/sweetalert.min.js"></script>
<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="/assets/plugins/parsley/dist/parsley.js"></script>
<script src="/js/ddlHelper.js"></script>
<!-- ================== END PAGE LEVEL JS ================== -->

<script>
    $(document).ready(function() {
        COLOR_GREEN = '#5AC8FA';
        $('#txtNewDate').datepicker({
            todayHighlight: true,
            orientation: 'bottom',
            weekStart: 1,
            format: 'yyyymmdd'
        });

        @if(!empty($data['void']))
        $("select[name='void_timeout']").val("{{ $data['void']->setting_value }}");
        @endif

        $("#data-table-default").length&&$("#data-table-default").DataTable({responsive:!0});
        $(document).on("click", ".delete", function (e) {
          swal({
             title: 'Delete Confirmation',
             text: 'Are you sure you want to delete this package?',
             icon: 'warning',
             buttons: {
                delete: {
                   text: 'Cancel',
                   value: null,
                   visible: true,
                   className: 'btn btn-primary',
                   closeModal: true,
               },
               cancel: {
                   text: 'Delete',
                   value: null,
                   visible: true,
                   className: 'btn btn-danger',
                   closeModal: true,
               }
           }
       });
          e.preventDefault();

		// now sending to build the array
		var d = new Date();
		var output = d.getFullYear();
		var output_int = parseInt(output);

		var yearArray = [
      { value: output, text: output }, 
      { value: output_int + 1, text: output_int + 1 }, 
      { value: output_int + 2, text: output_int + 2 }, 
      ];

		// ddlHelper
		populateDropdownList($('#ddlyears'), yearArray);

		
	}
    );
    });
</script>
@stop