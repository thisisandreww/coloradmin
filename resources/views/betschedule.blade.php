@extends("layouts.userlayout")
@section("title", "Betting Schedule")
@section("header", "Edit Betting Date of The Year")

@section('headerScript')
<link href="/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css" rel="stylesheet" />
<link href="/assets/plugins/parsley/src/parsley.css" rel="stylesheet" />
@stop

@section("body")
<!-- begin panel -->
<div class="panel panel-inverse" >
	<!-- begin panel-heading -->
	<div class="panel-heading">
		<div class="panel-heading-btn">
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
		</div>
		<h4 class="panel-title">Schedule List</h4>
	</div>
	<!-- end panel-heading -->
	<!-- begin panel-body -->
	
	<div class="panel-body">
		@if (session('message'))
		<div class="{{session('class')}}">{{session('message')}}</div>
		@endif
		<!-- form-control-sm -->
		{!! Form::open(['url' => '/betschedule/loadprimary', 'class' => 'form-horizontal']) !!}
		<div class="row">		
			<div class="col-md-4">
				<div class="form-group">
					<label>Select year to edit the schedule</label>
					<select class="form-control width-full m-b-10" id="ddlyears" name="ddlyears">
						<option value="2018">2018</option>
						<option value="2019">2019</option>
						<option value="2020">2020</option>
					</select>
				</div>
			</div>
			<div class="col-md-4">
				<label>Load primary schedule</label>
				<button type="submit" class="btn btn-primary width-full m-b-10">Load Primary</button>
			</div>
		</div>
		{!! Form::close() !!}
		{!! Form::open(['url' => '/betschedule/writeadate', 'class' => 'form-horizontal', 'data-parsley-validate' => 'true']) !!}
		<div class="row">		
			<div class="col-md-4">
				<div class="form-group">
					<label>- or - Enter a new date</label>
					<input type="text" autocomplete="off" class="form-control width-full m-b-10" id="txtNewDate" name="txtNewDate" />
				</div>
			</div>
			<div class="col-md-4">
				<div class="form-group">
					<label>Select available pool(s)</label>
					<div class="checkbox checkbox-css">
						<input type="hidden" name="chkmg" value="0" />
						<input type="checkbox" id="chkmg" name="chkmg" />
						<label for="chkmg">Magnum</label>
					</div>
					<div class="checkbox checkbox-css">
						<input type="hidden" name="chkkd" value="0" />
						<input type="checkbox" id="chkkd" name="chkkd" />
						<label for="chkkd">PMP</label>
					</div>
					<div class="checkbox checkbox-css">
						<input type="hidden" name="chktt" value="0" />
						<input type="checkbox" id="chktt" name="chktt" />
						<label for="chktt">Toto</label>
					</div>
					<div class="checkbox checkbox-css">
						<input type="hidden" name="chksg" value="0" />
						<input type="checkbox" id="chksg" name="chksg" />
						<label for="chksg">Singapore</label>
					</div>
					<div class="checkbox checkbox-css">
						<input type="hidden" name="chksb" value="0" />
						<input type="checkbox" id="chksb" name="chksb" />
						<label for="chksb">Sabah</label>
					</div>
					<div class="checkbox checkbox-css">
						<input type="hidden" name="chksw" value="0" />
						<input type="checkbox" id="chksw" name="chksw" />
						<label for="chksw">Sarawak</label>
					</div>
					<div class="checkbox checkbox-css">
						<input type="hidden" name="chkstc" value="0" />
						<input type="checkbox" id="chkstc" name="chkstc" />
						<label for="chkstc">STC</label>
					</div>
				</div>
			</div>
			<div class="col-md-4">
				<label>Submit New Date</label>
				<button type="submit" class="btn btn-primary width-full m-b-10">Submit</button>
			</div>
		</div>
		{!! Form::close() !!}
		<!-- form-control-sm -->
		<h5><i class="fas fa-exclamation-circle"></i> Uncheck to Delete</h5>
		<br/>
		{!! Form::open(['url' => '/betschedule/updateschedule', 'class' => 'form-horizontal', 'data-parsley-validate' => 'true']) !!}
		<table id="data-table-default" class="table table-striped table-bordered">
			<thead>
				<tr>
					<th>
						<input type="hidden" name="chkall" value="0" />
						<div class="checkbox checkbox-css">    					
							<input type="checkbox" id="chkall" name="chkall" value="" />
							<label for="chkall">&nbsp;</label>
						</div>
					</th>
					<th>Date</th> 
					<th>Day</th>
					<th><img src="../assets/img/lotto/bet-logo-magnum.png" style="width: 20px; height: 20px;"><br/></th> 
					<th><img src="../assets/img/lotto/bet-logo-damacai-inversed.png" style="width: 20px"><br/></th>
					<th><img src="../assets/img/lotto/bet-logo-toto-inversed.png" style="width: 20px"><br/></th> 
					<th><img src="../assets/img/lotto/bet-logo-singapore-pools.png" style="width: 20px"><br/></th>
					<th><img src="../assets/img/lotto/bet-logo-big-cash.png" style="width: 20px"><br/></th> 
					<th><img src="../assets/img/lotto/bet-logo-sabah88.png" style="width: 20px"><br/></th>
					<th><img src="../assets/img/lotto/bet-logo-stc.png" style="width: 20px"><br/></th>
				</tr>
			</thead>
			<tbody>
				@foreach ($schedules as $s)
				<tr class="odd gradeX">					
					<td width="1%" class="f-s-600 text-inverse">
						<input type="hidden" name="id[]" value={{ $s->id }} />
						<input type="hidden" name="chk{{ $s->id }}" value="0" />
						<div class="checkbox checkbox-css">
							<input type="checkbox" id="chk{{ $s->id }}" name="chk{{ $s->id }}" {{ $s->isdeleted == 1 ? '' : 'checked' }} />
							<label for="chk{{ $s->id }}">&nbsp;</label>
						</div>
					</td>
					<td>{{ $s->drawdate }}</td>
					<td>{{ $s->day }}</td>					
					<td>
						<input type="hidden" name="chkmg{{ $s->id }}" value="0" />
						<div class="checkbox checkbox-css">
							<input type="checkbox" id="chkmg{{ $s->id }}" name="chkmg{{ $s->id }}" {{ $s->mg ? 'checked' : '' }} />
							<label for="chkmg{{ $s->id }}"></label>
						</div>
					</td>
					<td>
						<input type="hidden" name="chkkd{{ $s->id }}" value="0" />
						<div class="checkbox checkbox-css">
							<input type="checkbox" id="chkkd{{ $s->id }}" name="chkkd{{ $s->id }}" {{ $s->kd ? 'checked' : '' }} />
							<label for="chkkd{{ $s->id }}"></label>
						</div>
					</td>
					<td>
						<input type="hidden" name="chktt{{ $s->id }}" value="0" />
						<div class="checkbox checkbox-css">
							<input type="checkbox" id="chktt{{ $s->id }}" name="chktt{{ $s->id }}" {{ $s->tt ? 'checked' : '' }} />
							<label for="chktt{{ $s->id }}"></label>
						</div>
					</td>
					<td>
						<input type="hidden" name="chksg{{ $s->id }}" value="0" />
						<div class="checkbox checkbox-css">
							<input type="checkbox" id="chksg{{ $s->id }}" name="chksg{{ $s->id }}" {{ $s->sg ? 'checked' : '' }} />
							<label for="chksg{{ $s->id }}"></label>
						</div>
					</td>
					<td>
						<input type="hidden" name="chksw{{ $s->id }}" value="0" />
						<div class="checkbox checkbox-css">
							<input type="checkbox" id="chksw{{ $s->id }}" name="chksw{{ $s->id }}" {{ $s->sb ? 'checked' : '' }} />
							<label for="chksw{{ $s->id }}"></label>
						</div>
					</td>
					<td>
						<input type="hidden" name="chksb{{ $s->id }}" value="0" />
						<div class="checkbox checkbox-css">
							<input type="checkbox" id="chksb{{ $s->id }}" name="chksb{{ $s->id }}" {{ $s->sw ? 'checked' : '' }} />
							<label for="chksb{{ $s->id }}"></label>
						</div>
					</td>
					<td>
						<input type="hidden" name="chkstc{{ $s->id }}" value="0" />
						<div class="checkbox checkbox-css">
							<input type="checkbox" id="chkstc{{ $s->id }}" name="chkstc{{ $s->id }}" {{ $s->stc ? 'checked' : '' }} />
							<label for="chkstc{{ $s->id }}"></label>
						</div>
					</td>
				</tr>
				@endforeach				
			</tbody>
		</table>
		<div class="col-md-12">
			<input type="text" hidden id="delete_id" name="delete_id" />
			<button id="btnCreateCategories" class="btn_submit btn btn-primary" style="margin-bottom: 10px"><i class="fa fa-save"></i> Save</button>
		</div>
		{!! Form::close() !!}
	</div>
	<!-- end panel-body -->
</div>
<!-- end panel -->	

@stop

@section("page_script")
<script src="/assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="/assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
<script src="/assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script src="/assets/plugins/bootstrap-sweetalert/sweetalert.min.js"></script>
<script src="/assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
<script src="/assets/plugins/parsley/dist/parsley.js"></script>
<script src="/js/ddlHelper.js"></script>
<!-- ================== END PAGE LEVEL JS ================== -->

<script>
	$(document).ready(function() {
		COLOR_GREEN = '#5AC8FA';
		$('#txtNewDate').datepicker({
			todayHighlight: true,
			orientation: 'bottom',
			weekStart: 1,
			format: 'yyyymmdd'
		});

		$("#data-table-default").length&&$("#data-table-default").DataTable({responsive:!0});
		$(document).on("click", ".delete", function (e) {
			swal({
				title: 'Delete Confirmation',
				text: 'Are you sure you want to delete this package?',
				icon: 'warning',
				buttons: {
					delete: {
						text: 'Cancel',
						value: null,
						visible: true,
						className: 'btn btn-primary',
						closeModal: true,
					},
					cancel: {
						text: 'Delete',
						value: null,
						visible: true,
						className: 'btn btn-danger',
						closeModal: true,
					}
				}
			});
			e.preventDefault();

		// now sending to build the array
		var d = new Date();
		var output = d.getFullYear();
		var output_int = parseInt(output);

		var yearArray = [
		{ value: output, text: output }, 
		{ value: output_int + 1, text: output_int + 1 }, 
		{ value: output_int + 2, text: output_int + 2 }, 
		];

		// ddlHelper
		populateDropdownList($('#ddlyears'), yearArray);

		
	}
	);
	});
</script>
@stop