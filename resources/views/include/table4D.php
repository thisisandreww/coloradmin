<table class="table_3D_4D table table-striped m-b-0">
							<thead>
								<tr>
									<th colspan="4" style="padding-left:5px;">4D</th>
									<th colspan="2" style="padding-left:5px;">3D</th>
								</tr>
								<tr>
									<th style="padding-right:5px; padding-left:5px;">Aspect</th>
									<th style="padding-right:5px; padding-left:5px;">Big</th>
									<th style="padding-right:5px; padding-left:5px;">Small</th>
									<th style="padding-right:5px; padding-left:5px;">4A</th>
									<th style="padding-right:5px; padding-left:5px;">3A</th>
									<th style="padding-right:5px; padding-left:5px;">3ABC</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td style="padding-right:5px; padding-left:5px;" nowrap>Ticket Price</td>
									<td class="with-form-control"><label class='default' id='<?php echo TICKET; ?>_<?php echo BIG; ?>_default'>0</label><input type="text" value="<?php echo old(TICKET.'_'.BIG, '0');?>" id='<?php echo TICKET; ?>_<?php echo BIG; ?>' name='<?php echo TICKET; ?>_<?php echo BIG; ?>' data-parsley-type="digits" class="form-control table_input" value="1" /></td>
									<td class="with-form-control"><label class='default' id='<?php echo TICKET; ?>_<?php echo SMALL; ?>_default'>0</label><input type="text" value="<?php echo old(TICKET.'_'.SMALL, '0');?>" id='<?php echo TICKET; ?>_<?php echo SMALL; ?>' name='<?php echo TICKET; ?>_<?php echo SMALL; ?>' data-parsley-type="digits" class="form-control table_input" value="1"/></td>
									<td class="with-form-control"><label class='default' id='<?php echo TICKET; ?>_<?php echo FOURA; ?>_default'>0</label><input type="text" value="<?php echo old(TICKET.'_'.FOURA, '0');?>"  id='<?php echo TICKET; ?>_<?php echo FOURA; ?>' name='<?php echo TICKET; ?>_<?php echo FOURA; ?>' data-parsley-type="digits" class="form-control table_input" value="1"/></td>
									<td class="with-form-control"><label class='default' id='<?php echo TICKET; ?>_<?php echo THREEA; ?>_default'>0</label><input type="text" value="<?php echo old(TICKET.'_'.THREEA, '0');?>"  id='<?php echo TICKET; ?>_<?php echo THREEA; ?>' name='<?php echo TICKET; ?>_<?php echo THREEA; ?>' data-parsley-type="digits" class="form-control table_input" value="1"/></td>
									<td class="with-form-control"><label class='default' id='<?php echo TICKET; ?>_<?php echo THREEABC; ?>_default'>0</label><input type="text" value="<?php echo old(TICKET.'_'.THREEABC, '0');?>"  id='<?php echo TICKET; ?>_<?php echo THREEABC; ?>' name='<?php echo TICKET; ?>_<?php echo THREEABC; ?>' data-parsley-type="digits" class="form-control table_input" value="1"/></td>
								</tr>
								<tr>
									<td style="padding-right:5px; padding-left:5px;" nowrap>Commission (%)</td>
									<td class="with-form-control"><label class='default' id='<?php echo COMMISSION; ?>_<?php echo BIG; ?>_default'>0</label><input type="text" value="<?php echo old(COMMISSION.'_'.BIG, '0');?>" id='<?php echo COMMISSION; ?>_<?php echo BIG; ?>' name='<?php echo COMMISSION; ?>_<?php echo BIG; ?>' data-parsley-type="number" class="form-control table_input" value="0"/></td>
									<td class="with-form-control"><label class='default' id='<?php echo COMMISSION; ?>_<?php echo SMALL; ?>_default'>0</label><input type="text" value="<?php echo old(COMMISSION.'_'.SMALL, '0');?>" id='<?php echo COMMISSION; ?>_<?php echo SMALL; ?>' name='<?php echo COMMISSION; ?>_<?php echo SMALL; ?>' data-parsley-type="number" class="form-control table_input" value="0"/></td>
									<td class="with-form-control"><label class='default' id='<?php echo COMMISSION; ?>_<?php echo FOURA; ?>_default'>0</label><input type="text" value="<?php echo old(COMMISSION.'_'.FOURA, '0');?>" id='<?php echo COMMISSION; ?>_<?php echo FOURA; ?>' name='<?php echo COMMISSION; ?>_<?php echo FOURA; ?>' data-parsley-type="number" class="form-control table_input" value="0"/></td>
									<td class="with-form-control"><label class='default' id='<?php echo COMMISSION; ?>_<?php echo THREEA; ?>_default'>0</label><input type="text" value="<?php echo old(COMMISSION.'_'.THREEA, '0');?>" id='<?php echo COMMISSION; ?>_<?php echo THREEA; ?>' name='<?php echo COMMISSION; ?>_<?php echo THREEA; ?>' data-parsley-type="number" class="form-control table_input" value="0"/></td>
									<td class="with-form-control"><label class='default' id='<?php echo COMMISSION; ?>_<?php echo THREEABC; ?>_default'>0</label><input type="text" value="<?php echo old(COMMISSION.'_'.THREEABC, '0');?>" id='<?php echo COMMISSION; ?>_<?php echo THREEABC; ?>' name='<?php echo COMMISSION; ?>_<?php echo THREEABC; ?>' data-parsley-type="number" class="form-control table_input" value="0"/></td>
								</tr>
								<tr>
									<td style="padding-right:5px; padding-left:5px;" nowrap>First Prize</td>
									<td class="with-form-control"><label class='default' id='<?php echo FIRST; ?>_<?php echo BIG; ?>_default'>0</label><input type="text" value="<?php echo old(FIRST.'_'.BIG, '0');?>" id='<?php echo FIRST; ?>_<?php echo BIG; ?>' name='<?php echo FIRST; ?>_<?php echo BIG; ?>' data-parsley-type="number" class="form-control table_input" value="0"/></td>
									<td class="with-form-control"><label class='default' id='<?php echo FIRST; ?>_<?php echo SMALL; ?>_default'>0</label><input type="text" value="<?php echo old(FIRST.'_'.SMALL, '0');?>" id='<?php echo FIRST; ?>_<?php echo SMALL; ?>' name='<?php echo FIRST; ?>_<?php echo SMALL; ?>' data-parsley-type="number" class="form-control table_input" value="0"/></td>
									<td class="with-form-control"><label class='default' id='<?php echo FIRST; ?>_<?php echo FOURA; ?>_default'>0</label><input type="text" value="<?php echo old(FIRST.'_'.FOURA, '0');?>" id='<?php echo FIRST; ?>_<?php echo FOURA; ?>' name='<?php echo FIRST; ?>_<?php echo FOURA; ?>' data-parsley-type="number" class="form-control table_input" value="0"/></td>
									<td class="with-form-control"><label class='default' id='<?php echo FIRST; ?>_<?php echo THREEA; ?>_default'>0</label><input type="text" value="<?php echo old(FIRST.'_'.THREEA, '0');?>" id='<?php echo FIRST; ?>_<?php echo THREEA; ?>' name='<?php echo FIRST; ?>_<?php echo THREEA; ?>' data-parsley-type="number" class="form-control table_input" value="0"/></td>
									<td class="with-form-control"><label class='default' id='<?php echo FIRST; ?>_<?php echo THREEABC; ?>_default'>0</label><input type="text" value="<?php echo old(FIRST.'_'.THREEABC, '0');?>" id='<?php echo FIRST; ?>_<?php echo THREEABC; ?>' name='<?php echo FIRST; ?>_<?php echo THREEABC; ?>' data-parsley-type="number" class="form-control table_input" value="0"/></td>
								</tr>
								<tr>
									<td style="padding-right:5px; padding-left:5px;" nowrap>Second Prize</td>
									<td class="with-form-control"><label class='default' id='<?php echo SECOND; ?>_<?php echo BIG; ?>_default'>0</label><input type="text" value="<?php echo old(SECOND.'_'.BIG, '0');?>" id='<?php echo SECOND; ?>_<?php echo BIG; ?>' name='<?php echo SECOND; ?>_<?php echo BIG; ?>' data-parsley-type="number" class="form-control table_input" value="0"/></td>
									<td class="with-form-control"><label class='default' id='<?php echo SECOND; ?>_<?php echo SMALL; ?>_default'>0</label><input type="text" value="<?php echo old(SECOND.'_'.SMALL, '0');?>" id='<?php echo SECOND; ?>_<?php echo SMALL; ?>' name='<?php echo SECOND; ?>_<?php echo SMALL; ?>' data-parsley-type="number" class="form-control table_input" value="0"/></td>
									<td class="with-form-control"></td>
									<td class="with-form-control"></td>
									<td class="with-form-control"><label class='default' id='<?php echo SECOND; ?>_<?php echo THREEABC; ?>_default'>0</label><input type="text" value="<?php echo old(SECOND.'_'.THREEABC, '0');?>" id='<?php echo SECOND; ?>_<?php echo THREEABC; ?>' name='<?php echo SECOND; ?>_<?php echo THREEABC; ?>' data-parsley-type="number" class="form-control table_input" value="0"/></td>
								</tr>	
								<tr>
									<td style="padding-right:5px; padding-left:5px;" nowrap>Third Prize</td>
									<td class="with-form-control"><label class='default' id='<?php echo THIRD; ?>_<?php echo BIG; ?>_default'>0</label><input type="text" value="<?php echo old(THIRD.'_'.BIG, '0');?>" id='<?php echo THIRD; ?>_<?php echo BIG; ?>' name='<?php echo THIRD; ?>_<?php echo BIG; ?>' data-parsley-type="number" class="form-control table_input" value="0"/></td>
									<td class="with-form-control"><label class='default' id='<?php echo THIRD; ?>_<?php echo SMALL; ?>_default'>0</label><input type="text" value="<?php echo old(THIRD.'_'.SMALL, '0');?>" id='<?php echo THIRD; ?>_<?php echo SMALL; ?>' name='<?php echo THIRD; ?>_<?php echo SMALL; ?>' data-parsley-type="number" class="form-control table_input" value="0"/></td>
									<td class="with-form-control"></td>
									<td class="with-form-control"></td>
									<td class="with-form-control"><label class='default' id='<?php echo THIRD; ?>_<?php echo THREEABC; ?>_default'>0</label><input type="text" value="<?php echo old(THIRD.'_'.THREEABC, '0');?>" id='<?php echo THIRD; ?>_<?php echo THREEABC; ?>' name='<?php echo THIRD; ?>_<?php echo THREEABC; ?>' data-parsley-type="number" class="form-control table_input" value="0"/></td>
								</tr>	
								<tr>
									<td style="padding-right:5px; padding-left:5px;" nowrap>Starter Prize</td>
									<td class="with-form-control"><label class='default' id='<?php echo SPECIAL; ?>_<?php echo BIG; ?>_default'>0</label><input type="text" value="<?php echo old(SPECIAL.'_'.BIG, '0');?>" id='<?php echo SPECIAL; ?>_<?php echo BIG; ?>' name='<?php echo SPECIAL; ?>_<?php echo BIG; ?>' data-parsley-type="number" class="form-control table_input" value="0"/></td>
									<td class="with-form-control"></td>
									<td class="with-form-control"></td>
									<td class="with-form-control"></td>
									<td class="with-form-control"></td>
								</tr>
								<tr>
									<td style="padding-right:5px; padding-left:5px;" nowrap>Consolation Prize</td>
									<td class="with-form-control"><label class='default' id='<?php echo CONSOLATION; ?>_<?php echo BIG; ?>_default'>0</label><input type="text" value="<?php echo old(CONSOLATION.'_'.BIG, '0');?>" id='<?php echo CONSOLATION; ?>_<?php echo BIG; ?>' name='<?php echo CONSOLATION; ?>_<?php echo BIG; ?>' data-parsley-type="number" class="form-control table_input" value="0"/></td>
									<td class="with-form-control"></td>
									<td class="with-form-control"></td>
									<td class="with-form-control"></td>
									<td class="with-form-control"></td>
								</tr>
							</tbody>
						</table>