
<div class="table-responsive">
   <table class="table table-striped table-bordered table-width-fix">
      <thead>
         <tr>
            <th>Total</th>
            <th>Big</th>
            <th>Small</th>
            <th>4A</th>  
            <th>4B</th>
            <th>4C</th>
            <th>4D</th>
            <th>4E</th>
            <th>4ABC</th>
            <th>3A</th>
            <th>3ABC</th>
            <th>2A</th>
            <th>2ABC</th>
            <th>5D</th>
            <th>6D</th>                  
         </tr>
      </thead>                  
      <tbody id="tbody">  
 		<tr class="odd gradeX">
            <td><div id="divUpline_Total__" class="text-danger uplineDiv">(0.00)</div></td>
            <td><div id="divUpline_Amount_Big_" class="text-danger uplineDiv">(0.00)</div></td>
            <td><div id="divUpline_Amount_Small_" class="text-danger uplineDiv">(0.00)</div></td>
            <td><div id="divUpline_Amount_4A_" class="text-danger uplineDiv">(0.00)</div></td>
            <td><div id="divUpline_Amount_4B_" class="text-danger uplineDiv">(0.00)</div></td>
            <td><div id="divUpline_Amount_4C_" class="text-danger uplineDiv">(0.00)</div></td>
            <td><div id="divUpline_Amount_4D_" class="text-danger uplineDiv">(0.00)</div></td>                                               
            <td><div id="divUpline_Amount_4E_" class="text-danger uplineDiv">(0.00)</div></td>
            <td><div id="divUpline_Amount_4ABC_" class="text-danger uplineDiv">(0.00)</div></td>
            <td><div id="divUpline_Amount_3A_" class="text-danger uplineDiv">(0.00)</div></td>
            <td><div id="divUpline_Amount_3ABC_" class="text-danger uplineDiv">(0.00)</div></td>
            <td><div id="divUpline_Amount_2A_" class="text-danger uplineDiv">(0.00)</div></td>
            <td><div id="divUpline_Amount_2ABC_" class="text-danger uplineDiv">(0.00)</div></td>
            <td><div id="divUpline_Amount_5D_" class="text-danger uplineDiv">(0.00)</div></td>
            <td><div id="divUpline_Amount_6D_" class="text-danger uplineDiv">(0.00)</div></td>
         </tr>  
         <tr class="odd gradeX">
            <td><input type="text" id='txt_Total__' name='txt_Total__' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Total__', number_format($data['myMapReference']['Total__']->value, 2, '.', '')) ?>"/></td>
            <td><input type="text" id='txt_Amount_Big_' name='txt_Amount_Big_' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_Big_', number_format($data['myMapReference']['Amount_Big_']->value, 2, '.', '')) ?>"/></td>
            <td><input type="text" id='txt_Amount_Small_' name='txt_Amount_Small_' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_Small_', number_format($data['myMapReference']['Amount_Small_']->value, 2, '.', '')) ?>"/></td>
            <td><input type="text" id='txt_Amount_4A_' name='txt_Amount_4A_' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_4A_', number_format($data['myMapReference']['Amount_4A_']->value, 2, '.', '')) ?>"/></td>
            <td><input type="text" id='txt_Amount_4B_' name='txt_Amount_4B_' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_4B_', number_format($data['myMapReference']['Amount_4B_']->value, 2, '.', '')) ?>"/></td>
            <td><input type="text" id='txt_Amount_4C_' name='txt_Amount_4C_' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_4C_', number_format($data['myMapReference']['Amount_4C_']->value, 2, '.', '')) ?>"/></td>
            <td><input type="text" id='txt_Amount_4D_' name='txt_Amount_4D_' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_4D_', number_format($data['myMapReference']['Amount_4D_']->value, 2, '.', '')) ?>"/></td>                                               
            <td><input type="text" id='txt_Amount_4E_' name='txt_Amount_4E_' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_4E_', number_format($data['myMapReference']['Amount_4E_']->value, 2, '.', '')) ?>"/></td>
            <td><input type="text" id='txt_Amount_4ABC_' name='txt_Amount_4ABC_' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_4ABC_', number_format($data['myMapReference']['Amount_4ABC_']->value, 2, '.', '')) ?>"/></td>
            <td><input type="text" id='txt_Amount_3A_' name='txt_Amount_3A_' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_3A_', number_format($data['myMapReference']['Amount_3A_']->value, 2, '.', '')) ?>"/></td>
            <td><input type="text" id='txt_Amount_3ABC_' name='txt_Amount_3ABC_' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_3ABC_', number_format($data['myMapReference']['Amount_3ABC_']->value, 2, '.', '')) ?>"/></td>
            <td><input type="text" id='txt_Amount_2A_' name='txt_Amount_2A_' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_2A_', number_format($data['myMapReference']['Amount_2A_']->value, 2, '.', '')) ?>"/></td>
            <td><input type="text" id='txt_Amount_2ABC_' name='txt_Amount_2ABC_' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_2ABC_', number_format($data['myMapReference']['Amount_2ABC_']->value, 2, '.', '')) ?>"/></td>
            <td><input type="text" id='txt_Amount_5D_' name='txt_Amount_5D_' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_5D_', number_format($data['myMapReference']['Amount_5D_']->value, 2, '.', '')) ?>"/></td>
            <td><input type="text" id='txt_Amount_6D_' name='txt_Amount_6D_' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_6D_', number_format($data['myMapReference']['Amount_6D_']->value, 2, '.', '')) ?>"/></td>
         </tr>
      </tbody>
   </table>	
</div>
<div class="row">
<div class="col-md-9">
   <table class="table table-striped table-bordered table-width-fix">
      <thead>
         <tr>
            <th>4D Group</th>
            <th>Big</th>
            <th>Small</th>
            <th>4A</th>                                        
         </tr>
      </thead>                  
      <tbody id="tbody"> 
      	 <?php foreach(ARRAY_GROUP_4D as $ag4d)
      	 {
      	 ?>
      	 	<tr class="odd gradeX">
                <td rowspan="2"><?php echo $ag4d ?></td>
                <td><div id='divUpline_Group_<?php echo $ag4d; ?>_Big' class="text-danger uplineDiv">(0.00)</div></td>
                <td><div id='divUpline_Group_<?php echo $ag4d; ?>_Small' class="text-danger uplineDiv">(0.00)</div></td>
                <td><div id='divUpline_Group_<?php echo $ag4d; ?>_4A' class="text-danger uplineDiv">(0.00)</div></td>
         	</tr>
         	<tr class="odd gradeX">
                <td><input type="text" id='txt_Group_<?php echo $ag4d; ?>_Big' name='txt_Group_<?php echo $ag4d; ?>_Big' class="form-control eatmap_input" data-parsley-type="number" value="<?php echo old('txt_Group_'.$ag4d.'_Big', number_format($data['myMapReference']['Group_'.$ag4d.'_Big']->value, 2, '.', '')) ?>" /></td>
                <td><input type="text" id='txt_Group_<?php echo $ag4d; ?>_Small' name='txt_Group_<?php echo $ag4d; ?>_Small' class="form-control eatmap_input" data-parsley-type="number" value="<?php echo old('txt_Group_'.$ag4d.'_Small', number_format($data['myMapReference']['Group_'.$ag4d.'_Small']->value, 2, '.', '')) ?>" /></td>
                <td><input type="text" id='txt_Group_<?php echo $ag4d; ?>_4A' name='txt_Group_<?php echo $ag4d; ?>_4A' class="form-control eatmap_input" data-parsley-type="number" value="<?php echo old('txt_Group_'.$ag4d.'_4A', number_format($data['myMapReference']['Group_'.$ag4d.'_4A']->value, 2, '.', '')) ?>" /></td>
         	</tr>        	 
      	 <?php } ?>            	 
      </tbody>
   </table>	
</div>
<div class="col-md-3">
   <table class="table table-striped table-bordered table-width-fix">
      <thead>
         <tr>
            <th>3D Group</th>
            <th>3A</th>
            <th>3ABC</th>                             
         </tr>
      </thead>                  
      <tbody id="tbody"> 
      	 <?php foreach(ARRAY_GROUP_3D as $ag3d)
      	 {
      	 ?>
      	 	<tr class="odd gradeX">
                <td rowspan="2"><?php echo $ag3d ?></td>
                <td><div id='divUpline_Group_<?php echo $ag3d; ?>_3A' class="text-danger uplineDiv">(0.00)</div></td>
                <td><div id='divUpline_Group_<?php echo $ag3d; ?>_3ABC' class="text-danger uplineDiv">(0.00)</div></td>
         	</tr>
         	<tr class="odd gradeX">
                <td><input type="text" id='txt_Group_<?php echo $ag3d; ?>_3A' name='txt_Group_<?php echo $ag3d; ?>_3A' class="form-control eatmap_input" data-parsley-type="number" value="<?php echo old('txt_Group_'.$ag3d.'_3A', number_format($data['myMapReference']['Group_'.$ag3d.'_3A']->value, 2, '.', '')) ?>" /></td>
                <td><input type="text" id='txt_Group_<?php echo $ag3d; ?>_3ABC' name='txt_Group_<?php echo $ag3d; ?>_3ABC' class="form-control eatmap_input" data-parsley-type="number" value="<?php echo old('txt_Group_'.$ag3d.'_3ABC', number_format($data['myMapReference']['Group_'.$ag3d.'_3ABC']->value, 2, '.', '')) ?>" /></td>
         	</tr>        	 
      	 <?php } ?>            	 
      </tbody>
   </table>	
</div>
</div>
