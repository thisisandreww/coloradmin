<?php 
define("BIG", "Big");
define("SMALL", "Small");
define("FOURA", "4A");
define("FIVED", "5D");
define("SIXD", "6D");
define("FOURB", "4B");
define("FOURC", "4C");
define("FOURD", "4D");
define("FOURE", "4E");
define("FOURABC", "4ABC");
define("TWOABC", "2ABC");
define("TWOA", "2A");
define("THREEA", "3A");
define("THREEABC", "3ABC");

define("TICKET", "ticket_prize");
define("COMMISSION", "commission");
define("FIRST", "first_prize");
define("SECOND", "second_prize");
define("THIRD", "third_prize");
define("SPECIAL", "special_prize");
define("CONSOLATION", "consolation_prize");
define("SIXTH", "sixth_prize");

define("TABLE_5D6D", array(FIVED, SIXD));
define("TABLE_3D", array(THREEA, THREEABC));
define("TABLE_4DS", array(FOURB, FOURC, FOURD,FOURE,FOURABC,TWOABC,TWOA));
define("TABLE_4D", array(BIG, SMALL, FOURA));

define("TABLE_PROPERTIES",array('票价'=>'ticket_prize',
                             '佣金'=>'commission',
							 '头奖'=>'first_prize',
							 '二奖'=>'second_prize',
							 '三奖'=>'third_prize',
							 '入围奖'=>'special_prize',
							 '安慰奖'=>'consolation_prize',
							 '六奖'=>'sixth_prize'));
?>