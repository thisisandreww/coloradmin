<table class="table table-striped m-b-0">
							<thead>
								<tr>
									<th width="15%">Aspect</th>
									<th>5D</th>
									<th>6D</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td>Ticket Price</td>
									<td class="with-form-control"><label class='default' id='<?php echo TICKET; ?>_<?php echo FIVED; ?>_default'>0</label><input type="text" value="<?php echo old(TICKET.'_'.FIVED, '0');?>" id='<?php echo TICKET; ?>_<?php echo FIVED; ?>' name='<?php echo TICKET; ?>_<?php echo FIVED; ?>' data-parsley-type="digits" class="form-control table_input" value="1"  /></td>
									<td class="with-form-control"><label class='default' id='<?php echo TICKET; ?>_<?php echo SIXD; ?>_default'>0</label><input type="text" value="<?php echo old(TICKET.'_'.SIXD, '0');?>" id='<?php echo TICKET; ?>_<?php echo SIXD; ?>' name='<?php echo TICKET; ?>_<?php echo SIXD; ?>' data-parsley-type="digits" class="form-control table_input" value="1" /></td>									
								</tr>
								<tr>
									<td>Commission (%)</td>
									<td class="with-form-control"><label class='default' id='<?php echo COMMISSION; ?>_<?php echo FIVED; ?>_default'>0</label><input type="text" value="<?php echo old(COMMISSION.'_'.FIVED, '0');?>" id='<?php echo COMMISSION; ?>_<?php echo FIVED; ?>' name='<?php echo COMMISSION; ?>_<?php echo FIVED; ?>' data-parsley-type="number" id="textComm5d" class="form-control table_input" value="0" /></td>
									<td class="with-form-control"><label class='default' id='<?php echo COMMISSION; ?>_<?php echo SIXD; ?>_default'>0</label><input type="text" value="<?php echo old(COMMISSION.'_'.SIXD, '0');?>" id='<?php echo COMMISSION; ?>_<?php echo SIXD; ?>' name='<?php echo COMMISSION; ?>_<?php echo SIXD; ?>' data-parsley-type="number" id="textComm6d" class="form-control table_input" value="0" /></td>
								</tr>
								<tr>
									<td>1st Prize</td>
									<td class="with-form-control"><label class='default' id='<?php echo FIRST; ?>_<?php echo FIVED; ?>_default'>0</label><input type="text" value="<?php echo old(FIRST.'_'.FIVED, '0');?>" id='<?php echo FIRST; ?>_<?php echo FIVED; ?>' name='<?php echo FIRST; ?>_<?php echo FIVED; ?>' data-parsley-type="number" id="text15d" class="form-control table_input" value="0" /></td>
									<td class="with-form-control"><label class='default' id='<?php echo FIRST; ?>_<?php echo SIXD; ?>_default'>0</label><input type="text" value="<?php echo old(FIRST.'_'.SIXD, '0');?>" id='<?php echo FIRST; ?>_<?php echo SIXD; ?>' name='<?php echo FIRST; ?>_<?php echo SIXD; ?>' data-parsley-type="number" id="text16d" class="form-control table_input" value="0" /></td>
								</tr>
								<tr>
									<td>2nd Prize</td>
									<td class="with-form-control"><label class='default' id='<?php echo SECOND; ?>_<?php echo FIVED; ?>_default'>0</label><input type="text" value="<?php echo old(SECOND.'_'.FIVED, '0');?>" id='<?php echo SECOND; ?>_<?php echo FIVED; ?>' name='<?php echo SECOND; ?>_<?php echo FIVED; ?>' data-parsley-type="number" id="text25d" class="form-control table_input" value="0" /></td>
									<td class="with-form-control"><label class='default' id='<?php echo SECOND; ?>_<?php echo SIXD; ?>_default'>0</label><input type="text" value="<?php echo old(SECOND.'_'.SIXD, '0');?>" id='<?php echo SECOND; ?>_<?php echo SIXD; ?>' name='<?php echo SECOND; ?>_<?php echo SIXD; ?>' data-parsley-type="number" id="text26d" class="form-control table_input" value="0" /></td>
								</tr>	
								<tr>
									<td>3rd Prize</td>
									<td class="with-form-control"><label class='default' id='<?php echo THIRD; ?>_<?php echo FIVED; ?>_default'>0</label><input type="text" value="<?php echo old(THIRD.'_'.FIVED, '0');?>" id='<?php echo THIRD; ?>_<?php echo FIVED; ?>' name='<?php echo THIRD; ?>_<?php echo FIVED; ?>' data-parsley-type="number" id="text35d" class="form-control table_input" value="0" /></td>
									<td class="with-form-control"><label class='default' id='<?php echo THIRD; ?>_<?php echo SIXD; ?>_default'>0</label><input type="text" value="<?php echo old(THIRD.'_'.SIXD, '0');?>" id='<?php echo THIRD; ?>_<?php echo SIXD; ?>' name='<?php echo THIRD; ?>_<?php echo SIXD; ?>' data-parsley-type="number" id="text36d" class="form-control table_input" value="0" /></td>
								</tr>
								<tr>
									<td>4th Prize</td>
									<td class="with-form-control"><label class='default' id='<?php echo SPECIAL; ?>_<?php echo FIVED; ?>_default'>0</label><input type="text" value="<?php echo old(SPECIAL.'_'.FIVED, '0');?>" id='<?php echo SPECIAL; ?>_<?php echo FIVED; ?>' name='<?php echo SPECIAL; ?>_<?php echo FIVED; ?>' data-parsley-type="number" id="text45d" class="form-control table_input" value="0" /></td>
									<td class="with-form-control"><label class='default' id='<?php echo SPECIAL; ?>_<?php echo SIXD; ?>_default'>0</label><input type="text" value="<?php echo old(SPECIAL.'_'.SIXD, '0');?>" id='<?php echo SPECIAL; ?>_<?php echo SIXD; ?>' name='<?php echo SPECIAL; ?>_<?php echo SIXD; ?>' data-parsley-type="number" id="text46d" class="form-control table_input" value="0" /></td>
								</tr>								
								<tr>
									<td>5th Prize</td>
									<td class="with-form-control"><label class='default' id='<?php echo CONSOLATION; ?>_<?php echo FIVED; ?>_default'>0</label><input type="text" value="<?php echo old(CONSOLATION.'_'.FIVED, '0');?>" id='<?php echo CONSOLATION; ?>_<?php echo FIVED; ?>' name='<?php echo CONSOLATION; ?>_<?php echo FIVED; ?>' data-parsley-type="number" id="text55d" class="form-control table_input" value="0" /></td>
									<td class="with-form-control"><label class='default' id='<?php echo CONSOLATION; ?>_<?php echo SIXD; ?>_default'>0</label><input type="text" value="<?php echo old(CONSOLATION.'_'.SIXD, '0');?>" id='<?php echo CONSOLATION; ?>_<?php echo SIXD; ?>' name='<?php echo CONSOLATION; ?>_<?php echo SIXD; ?>' data-parsley-type="number" id="text56d" class="form-control table_input" value="0" /></td>
								</tr>
								<tr>
									<td>6th Prize</td>
									<td class="with-form-control"><label class='default' id='<?php echo SIXTH; ?>_<?php echo FIVED; ?>_default'>0</label><input type="text" value="<?php echo old(SIXTH.'_'.FIVED, '0');?>" id='<?php echo SIXTH; ?>_<?php echo FIVED; ?>' name='<?php echo SIXTH; ?>_<?php echo FIVED; ?>' data-parsley-type="number" id="text65d" class="form-control table_input" value="0" /></td>
								</tr>
							</tbody>
						</table>