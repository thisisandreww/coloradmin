<div class="table-responsive">
   <table class="table table-striped table-bordered table-width-fix">
      <thead>
         <tr>
         	<th></th>
            <th>Total</th>
            <th>Big</th>
            <th>Small</th>
            <th>4A</th>  
            <th>4B</th>
            <th>4C</th>
            <th>4D</th>
            <th>4E</th>
            <th>4ABC</th>
            <th>3A</th>
            <th>3ABC</th>
            <th>2A</th>
            <th>2ABC</th>
            <th>5D</th>
            <th>6D</th>                  
         </tr>
      </thead>                  
      <tbody id="tbody">  
 		<tr class="odd gradeX">
 			<td></td>
            <td><div id="divUpline_Total__" class="text-danger uplineDiv">(<?php echo number_format($data['myMapReference']['Total__']->value, 2, '.', '') ?>)</div></td>
            <td><div id="divUpline_Amount_Big_" class="text-danger uplineDiv">(<?php echo number_format($data['myMapReference']['Amount_Big_']->value, 2, '.', '') ?>)</div></td>
            <td><div id="divUpline_Amount_Small_" class="text-danger uplineDiv">(<?php echo number_format($data['myMapReference']['Amount_Small_']->value, 2, '.', '') ?>)</div></td>
            <td><div id="divUpline_Amount_4A_" class="text-danger uplineDiv">(<?php echo number_format($data['myMapReference']['Amount_4A_']->value, 2, '.', '') ?>)</div></td>
            <td><div id="divUpline_Amount_4B_" class="text-danger uplineDiv">(<?php echo number_format($data['myMapReference']['Amount_4B_']->value, 2, '.', '') ?>)</div></td>
            <td><div id="divUpline_Amount_4C_" class="text-danger uplineDiv">(<?php echo number_format($data['myMapReference']['Amount_4C_']->value, 2, '.', '') ?>)</div></td>
            <td><div id="divUpline_Amount_4D_" class="text-danger uplineDiv">(<?php echo number_format($data['myMapReference']['Amount_4D_']->value, 2, '.', '') ?>)</div></td>                                               
            <td><div id="divUpline_Amount_4E_" class="text-danger uplineDiv">(<?php echo number_format($data['myMapReference']['Amount_4E_']->value, 2, '.', '') ?>)</div></td>
            <td><div id="divUpline_Amount_4ABC_" class="text-danger uplineDiv">(<?php echo number_format($data['myMapReference']['Amount_4ABC_']->value, 2, '.', '') ?>)</div></td>
            <td><div id="divUpline_Amount_3A_" class="text-danger uplineDiv">(<?php echo number_format($data['myMapReference']['Amount_3A_']->value, 2, '.', '') ?>)</div></td>
            <td><div id="divUpline_Amount_3ABC_" class="text-danger uplineDiv">(<?php echo number_format($data['myMapReference']['Amount_3ABC_']->value, 2, '.', '') ?>)</div></td>
            <td><div id="divUpline_Amount_2A_" class="text-danger uplineDiv">(<?php echo number_format($data['myMapReference']['Amount_2A_']->value, 2, '.', '') ?>)</div></td>
            <td><div id="divUpline_Amount_2ABC_" class="text-danger uplineDiv">(<?php echo number_format($data['myMapReference']['Amount_2ABC_']->value, 2, '.', '') ?>)</div></td>
            <td><div id="divUpline_Amount_5D_" class="text-danger uplineDiv">(<?php echo number_format($data['myMapReference']['Amount_5D_']->value, 2, '.', '') ?>)</div></td>
            <td><div id="divUpline_Amount_6D_" class="text-danger uplineDiv">(<?php echo number_format($data['myMapReference']['Amount_6D_']->value, 2, '.', '') ?>)</div></td>
         </tr>  
         <?php 
            foreach($data['pools'] as $pool)
            { 
         ?>
         	<tr class="odd gradeX">
         		<td><img src="<?php echo $pool['images'] ?>" style="width: 20px; height: 20px;"></td>
                <td><input type="text" id='txt_Total___<?php echo $pool["shortname"] ?>' name='txt_Total___<?php echo $pool["shortname"] ?>' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Total___'.$pool->shortname, number_format($data['myMap']['Total___'.$pool->shortname]['value'], 2, '.', '')) ?>"/></td>
                <td><input type="text" id='txt_Amount_Big__<?php echo $pool["shortname"] ?>' name='txt_Amount_Big__<?php echo $pool["shortname"] ?>' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_Big__'.$pool->shortname, number_format($data['myMap']['Amount_Big__'.$pool->shortname]['value'], 2, '.', '')) ?>"/></td>
                <td><input type="text" id='txt_Amount_Small__<?php echo $pool["shortname"] ?>' name='txt_Amount_Small__<?php echo $pool["shortname"] ?>' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_Small__'.$pool->shortname, number_format($data['myMap']['Amount_Small__'.$pool->shortname]['value'], 2, '.', '')) ?>"/></td>
                <td><input type="text" id='txt_Amount_4A__<?php echo $pool["shortname"] ?>' name='txt_Amount_4A__<?php echo $pool["shortname"] ?>' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_4A__'.$pool->shortname, number_format($data['myMap']['Amount_4A__'.$pool->shortname]['value'], 2, '.', '')) ?>"/></td>
                <td><input type="text" id='txt_Amount_4B__<?php echo $pool["shortname"] ?>' name='txt_Amount_4B__<?php echo $pool["shortname"] ?>' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_4B__'.$pool->shortname, number_format($data['myMap']['Amount_4B__'.$pool->shortname]['value'], 2, '.', '')) ?>"/></td>
                <td><input type="text" id='txt_Amount_4C__<?php echo $pool["shortname"] ?>' name='txt_Amount_4C__<?php echo $pool["shortname"] ?>' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_4C__'.$pool->shortname, number_format($data['myMap']['Amount_4C__'.$pool->shortname]['value'], 2, '.', '')) ?>"/></td>
                <td><input type="text" id='txt_Amount_4D__<?php echo $pool["shortname"] ?>' name='txt_Amount_4D__<?php echo $pool["shortname"] ?>' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_4D__'.$pool->shortname, number_format($data['myMap']['Amount_4D__'.$pool->shortname]['value'], 2, '.', '')) ?>"/></td>                                               
                <td><input type="text" id='txt_Amount_4E__<?php echo $pool["shortname"] ?>' name='txt_Amount_4E__<?php echo $pool["shortname"] ?>' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_4E__'.$pool->shortname, number_format($data['myMap']['Amount_4E__'.$pool->shortname]['value'], 2, '.', '')) ?>"/></td>
                <td><input type="text" id='txt_Amount_4ABC__<?php echo $pool["shortname"] ?>' name='txt_Amount_4ABC__<?php echo $pool["shortname"] ?>' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_4ABC__'.$pool->shortname, number_format($data['myMap']['Amount_4ABC__'.$pool->shortname]['value'], 2, '.', '')) ?>"/></td>
                <td><input type="text" id='txt_Amount_3A__<?php echo $pool["shortname"] ?>' name='txt_Amount_3A__<?php echo $pool["shortname"] ?>' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_3A__'.$pool->shortname, number_format($data['myMap']['Amount_3A__'.$pool->shortname]['value'], 2, '.', '')) ?>"/></td>
                <td><input type="text" id='txt_Amount_3ABC__<?php echo $pool["shortname"] ?>' name='txt_Amount_3ABC__<?php echo $pool["shortname"] ?>' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_3ABC__'.$pool->shortname, number_format($data['myMap']['Amount_3ABC__'.$pool->shortname]['value'], 2, '.', '')) ?>"/></td>
                <td><input type="text" id='txt_Amount_2A__<?php echo $pool["shortname"] ?>' name='txt_Amount_2A__<?php echo $pool["shortname"] ?>' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_2A__'.$pool->shortname, number_format($data['myMap']['Amount_2A__'.$pool->shortname]['value'], 2, '.', '')) ?>"/></td>
                <td><input type="text" id='txt_Amount_2ABC__<?php echo $pool["shortname"] ?>' name='txt_Amount_2ABC__<?php echo $pool["shortname"] ?>' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_2ABC__'.$pool->shortname, number_format($data['myMap']['Amount_2ABC__'.$pool->shortname]['value'], 2, '.', '')) ?>"/></td>
                <td><?php if ($pool->name === "Toto") { ?><input type="text" id='txt_Amount_5D__<?php echo $pool["shortname"] ?>' name='txt_Amount_5D__<?php echo $pool["shortname"] ?>' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_5D__'.$pool->shortname, number_format($data['myMap']['Amount_5D__'.$pool->shortname]['value'], 2, '.', '')) ?>"/><?php } ?></td>
                <td><?php if ($pool->name === "Toto") { ?><input type="text" id='txt_Amount_6D__<?php echo $pool["shortname"] ?>' name='txt_Amount_6D__<?php echo $pool["shortname"] ?>' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_6D__'.$pool->shortname, number_format($data['myMap']['Amount_6D__'.$pool->shortname]['value'], 2, '.', '')) ?>"/><?php } ?></td>
             </tr>
         <?php 
            }         
         ?>         
      </tbody>
   </table>	
</div>
<div class="row">
    <div class="col-md-12">
	<div class="table-responsive">
        <table class="table table-striped table-bordered table-width-fix">
          <thead>
             <tr>
                <th>4D Group</th>
                <th colspan="10">Big</th>
                <th colspan="10">Small</th>
                <th colspan="10">4A</th>                                        
             </tr>
             <tr>
             	<th>&nbsp;</th>
             	<?php 
             	  foreach(ARRAY_GROUP_4D as $ag4d)
             	  {
             	?>
             		<th><?php echo $ag4d ?></th>
             	<?php } ?>
             	<?php 
             	  foreach(ARRAY_GROUP_4D as $ag4d)
             	  {
             	?>
             		<th><?php echo $ag4d ?></th>
             	<?php } ?>
             	<?php 
             	  foreach(ARRAY_GROUP_4D as $ag4d)
             	  {
             	?>
             		<th><?php echo $ag4d ?></th>
             	<?php } ?>
             </tr>
          </thead>                  
          <tbody id="tbody">  
          
          		<tr class="odd gradeX">
                    <td>&nbsp;</td>
                    <?php 
             	  foreach(ARRAY_GROUP_4D as $ag4d)
             	  {
                 	?>
                 		<td><div id='divUpline_Group_<?php echo $ag4d; ?>_Big' class="text-danger uplineDiv">(<?php echo number_format($data['myMapReference']['Group_'.$ag4d.'_Big']->value, 2, '.', '') ?>)</div></td>
                  <?php } ?>
                  <?php 
             	  foreach(ARRAY_GROUP_4D as $ag4d)
             	  {
                 	?>
                 		<td><div id='divUpline_Group_<?php echo $ag4d; ?>_Small' class="text-danger uplineDiv">(<?php echo number_format($data['myMapReference']['Group_'.$ag4d.'_Small']->value, 2, '.', '') ?>)</div></td>
                  <?php } ?>
                  <?php 
             	  foreach(ARRAY_GROUP_4D as $ag4d)
             	  {
                 	?>
                 		<td><div id='divUpline_Group_<?php echo $ag4d; ?>_4A' class="text-danger uplineDiv">(<?php echo number_format($data['myMapReference']['Group_'.$ag4d.'_4A']->value, 2, '.', '') ?>)</div></td>
                  <?php } ?>
                    
             	</tr>
          
          	<?php         	  
              	foreach($data['pools'] as $pool)
                { 
          	 ?>
          	 	
             	<tr class="odd gradeX">
             		<td><img src="<?php echo $pool['images'] ?>" style="width: 20px; height: 20px;"></td>
             		<?php 
             	  foreach(ARRAY_GROUP_4D as $ag4d)
             	  {
                 	?>
                 		<td><input type="text" id='txt_Group_<?php echo $ag4d; ?>_Big_<?php echo $pool["shortname"] ?>' name='txt_Group_<?php echo $ag4d; ?>_Big_<?php echo $pool["shortname"] ?>' class="form-control eatmap_input" data-parsley-type="number" value="<?php echo old('txt_Group_'.$ag4d.'_Big_'.$pool["shortname"], number_format($data['myMap']['Group_'.$ag4d.'_Big_'.$pool["shortname"]]['value'], 2, '.', '')) ?>" /></td>
                  <?php } ?>
                  <?php 
             	  foreach(ARRAY_GROUP_4D as $ag4d)
             	  {
                 	?>
                 		<td><input type="text" id='txt_Group_<?php echo $ag4d; ?>_Small_<?php echo $pool["shortname"] ?>' name='txt_Group_<?php echo $ag4d; ?>_Small_<?php echo $pool["shortname"] ?>' class="form-control eatmap_input" data-parsley-type="number" value="<?php echo old('txt_Group_'.$ag4d.'_Small_'.$pool["shortname"], number_format($data['myMap']['Group_'.$ag4d.'_Small_'.$pool["shortname"]]['value'], 2, '.', '')) ?>" /></td>
                  <?php } ?>
                  <?php 
             	  foreach(ARRAY_GROUP_4D as $ag4d)
             	  {
                 	?>
                 		<td><input type="text" id='txt_Group_<?php echo $ag4d; ?>_4A_<?php echo $pool["shortname"] ?>' name='txt_Group_<?php echo $ag4d; ?>_4A_<?php echo $pool["shortname"] ?>' class="form-control eatmap_input" data-parsley-type="number" value="<?php echo old('txt_Group_'.$ag4d.'_4A_'.$pool["shortname"], number_format($data['myMap']['Group_'.$ag4d.'_4A_'.$pool["shortname"]]['value'], 2, '.', '')) ?>" /></td>
                  <?php } ?>
             	</tr>        	 
          	 <?php } ?>            	 
          </tbody>
        </table>
     </div>
     </div>
</div>
<div class="row">&nbsp;</div>
<div class="row">
	<div class="col-md-12">
	<div class="table-responsive">
        <table class="table table-striped table-bordered table-width-fix">
          <thead>
             <tr>
                <th>3D Group</th>
                <th colspan="10">3A</th>
                <th colspan="10">3ABC</th>                                       
             </tr>
             <tr>
             	<th>&nbsp;</th>
             	<?php 
             	foreach(ARRAY_GROUP_3D as $ag3d)
             	  {
             	?>
             		<th><?php echo $ag3d ?></th>
             	<?php } ?>
             	<?php 
             	foreach(ARRAY_GROUP_3D as $ag3d)
             	  {
             	?>
             		<th><?php echo $ag3d ?></th>
             	<?php } ?>
             </tr>
          </thead>                  
          <tbody id="tbody">  
          		<tr class="odd gradeX">
                    <td></td>
                    <?php 
                    foreach(ARRAY_GROUP_3D as $ag3d)
             	  {
                 	?>
                 		<td><div id='divUpline_Group_<?php echo $ag3d; ?>_3A' class="text-danger uplineDiv">(<?php echo number_format($data['myMapReference']['Group_'.$ag3d.'_3A']->value, 2, '.', '') ?>)</div></td>
                  <?php } ?>
                  <?php 
                  foreach(ARRAY_GROUP_3D as $ag3d)
             	  {
                 	?>
                 		<td><div id='divUpline_Group_<?php echo $ag3d; ?>_3ABC' class="text-danger uplineDiv">(<?php echo number_format($data['myMapReference']['Group_'.$ag3d.'_3ABC']->value, 2, '.', '') ?>)</div></td>
                  <?php } ?>                    
             	</tr>
          	<?php         	  
              	foreach($data['pools'] as $pool)
                { 
          	 ?>
          	 	
             	<tr class="odd gradeX">
             		<td><img src="<?php echo $pool['images'] ?>" style="width: 20px; height: 20px;"></td>
             		<?php 
             		foreach(ARRAY_GROUP_3D as $ag3d)
             	  {
                 	?>
                 		<td><input type="text" id='txt_Group_<?php echo $ag3d; ?>_3A_<?php echo $pool["shortname"] ?>' name='txt_Group_<?php echo $ag3d; ?>_3A_<?php echo $pool["shortname"] ?>' class="form-control eatmap_input" data-parsley-type="number" value="<?php echo old('txt_Group_'.$ag3d.'_3A_'.$pool["shortname"], number_format($data['myMap']['Group_'.$ag3d.'_3A_'.$pool["shortname"]]['value'], 2, '.', '')) ?>" /></td>
                  <?php } ?>
                  <?php 
                  foreach(ARRAY_GROUP_3D as $ag3d)
             	  {
                 	?>
                 		<td><input type="text" id='txt_Group_<?php echo $ag3d; ?>_3ABC_<?php echo $pool["shortname"] ?>' name='txt_Group_<?php echo $ag3d; ?>_3ABC_<?php echo $pool["shortname"] ?>' class="form-control eatmap_input" data-parsley-type="number" value="<?php echo old('txt_Group_'.$ag3d.'_3ABC_'.$pool["shortname"], number_format($data['myMap']['Group_'.$ag3d.'_3ABC_'.$pool["shortname"]]['value'], 2, '.', '')) ?>" /></td>
                  <?php } ?>
             	</tr>        	 
          	 <?php } ?>            	 
          </tbody>
        </table>	
   	</div>
   	</div>   
</div>