<div class="table-responsive">
   <table class="table table-striped table-bordered table-width-fix">
      <thead>
         <tr>
         	<th></th>
            <th>Total</th>
            <th>Big</th>
            <th>Small</th>
            <th>4A</th>  
            <th>4B</th>
            <th>4C</th>
            <th>4D</th>
            <th>4E</th>
            <th>4ABC</th>
            <th>3A</th>
            <th>3ABC</th>
            <th>2A</th>
            <th>2ABC</th>
            <th>5D</th>
            <th>6D</th>                  
         </tr>
      </thead>                  
      <tbody id="tbody">   
         <?php 
            foreach($data['pools'] as $pool)
            { 
         ?>
         	<tr class="odd gradeX">
         		<td><img src="<?php echo $pool['images'] ?>" style="width: 20px; height: 20px;"></td>
                <td><input type="text" id='txt_Total___<?php echo $pool["shortname"] ?>' name='txt_Total___<?php echo $pool["shortname"] ?>' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Total___'.$pool->shortname, number_format($data['myMap']['Total___'.$pool->shortname]['value'], 2)) ?>" readonly /></td>
                <td><input type="text" id='txt_Amount_Big__<?php echo $pool["shortname"] ?>' name='txt_Amount_Big__<?php echo $pool["shortname"] ?>' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_Big__'.$pool->shortname, number_format($data['myMap']['Amount_Big__'.$pool->shortname]['value'], 2)) ?>" readonly /></td>
                <td><input type="text" id='txt_Amount_Small__<?php echo $pool["shortname"] ?>' name='txt_Amount_Small__<?php echo $pool["shortname"] ?>' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_Small__'.$pool->shortname, number_format($data['myMap']['Amount_Small__'.$pool->shortname]['value'], 2)) ?>" readonly /></td>
                <td><input type="text" id='txt_Amount_4A__<?php echo $pool["shortname"] ?>' name='txt_Amount_4A__<?php echo $pool["shortname"] ?>' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_4A__'.$pool->shortname, number_format($data['myMap']['Amount_4A__'.$pool->shortname]['value'], 2)) ?>" readonly /></td>
                <td><input type="text" id='txt_Amount_4B__<?php echo $pool["shortname"] ?>' name='txt_Amount_4B__<?php echo $pool["shortname"] ?>' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_4B__'.$pool->shortname, number_format($data['myMap']['Amount_4B__'.$pool->shortname]['value'], 2)) ?>" readonly /></td>
                <td><input type="text" id='txt_Amount_4C__<?php echo $pool["shortname"] ?>' name='txt_Amount_4C__<?php echo $pool["shortname"] ?>' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_4C__'.$pool->shortname, number_format($data['myMap']['Amount_4C__'.$pool->shortname]['value'], 2)) ?>" readonly /></td>
                <td><input type="text" id='txt_Amount_4D__<?php echo $pool["shortname"] ?>' name='txt_Amount_4D__<?php echo $pool["shortname"] ?>' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_4D__'.$pool->shortname, number_format($data['myMap']['Amount_4D__'.$pool->shortname]['value'], 2)) ?>" readonly /></td>                                               
                <td><input type="text" id='txt_Amount_4E__<?php echo $pool["shortname"] ?>' name='txt_Amount_4E__<?php echo $pool["shortname"] ?>' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_4E__'.$pool->shortname, number_format($data['myMap']['Amount_4E__'.$pool->shortname]['value'], 2)) ?>" readonly /></td>
                <td><input type="text" id='txt_Amount_4ABC__<?php echo $pool["shortname"] ?>' name='txt_Amount_4ABC__<?php echo $pool["shortname"] ?>' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_4ABC__'.$pool->shortname, number_format($data['myMap']['Amount_4ABC__'.$pool->shortname]['value'], 2)) ?>" readonly /></td>
                <td><input type="text" id='txt_Amount_3A__<?php echo $pool["shortname"] ?>' name='txt_Amount_3A__<?php echo $pool["shortname"] ?>' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_3A__'.$pool->shortname, number_format($data['myMap']['Amount_3A__'.$pool->shortname]['value'], 2)) ?>" readonly /></td>
                <td><input type="text" id='txt_Amount_3ABC__<?php echo $pool["shortname"] ?>' name='txt_Amount_3ABC__<?php echo $pool["shortname"] ?>' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_3ABC__'.$pool->shortname, number_format($data['myMap']['Amount_3ABC__'.$pool->shortname]['value'], 2)) ?>" readonly /></td>
                <td><input type="text" id='txt_Amount_2A__<?php echo $pool["shortname"] ?>' name='txt_Amount_2A__<?php echo $pool["shortname"] ?>' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_2A__'.$pool->shortname, number_format($data['myMap']['Amount_2A__'.$pool->shortname]['value'], 2)) ?>" readonly /></td>
                <td><input type="text" id='txt_Amount_2ABC__<?php echo $pool["shortname"] ?>' name='txt_Amount_2ABC__<?php echo $pool["shortname"] ?>' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_2ABC__'.$pool->shortname, number_format($data['myMap']['Amount_2ABC__'.$pool->shortname]['value'], 2)) ?>" readonly /></td>
                <td><?php if ($pool->name === "Toto") { ?><input type="text" id='txt_Amount_5D__<?php echo $pool["shortname"] ?>' name='txt_Amount_5D__<?php echo $pool["shortname"] ?>' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_5D__'.$pool->shortname, number_format($data['myMap']['Amount_5D__'.$pool->shortname]['value'], 2)) ?>" readonly /><?php } ?></td>
                <td><?php if ($pool->name === "Toto") { ?><input type="text" id='txt_Amount_6D__<?php echo $pool["shortname"] ?>' name='txt_Amount_6D__<?php echo $pool["shortname"] ?>' data-parsley-type="number" class="form-control eatmap_input" value="<?php echo old('txt_Amount_6D__'.$pool->shortname, number_format($data['myMap']['Amount_6D__'.$pool->shortname]['value'], 2)) ?>" readonly /><?php } ?></td>
             </tr>
         <?php 
            }         
         ?>         
      </tbody>
   </table>	
</div>
<div class="row">
    <div class="col-md-12">
	<div class="table-responsive">
        <table class="table table-striped table-bordered table-width-fix">
          <thead>
             <tr>
                <th>4D Group</th>
                <th colspan="10">Big</th>
                <th colspan="10">Small</th>
                <th colspan="10">4A</th>                                        
             </tr>
             <tr>
             	<th>&nbsp;</th>
             	<?php 
             	  foreach(ARRAY_GROUP_4D as $ag4d)
             	  {
             	?>
             		<th><?php echo $ag4d ?></th>
             	<?php } ?>
             	<?php 
             	  foreach(ARRAY_GROUP_4D as $ag4d)
             	  {
             	?>
             		<th><?php echo $ag4d ?></th>
             	<?php } ?>
             	<?php 
             	  foreach(ARRAY_GROUP_4D as $ag4d)
             	  {
             	?>
             		<th><?php echo $ag4d ?></th>
             	<?php } ?>
             </tr>
          </thead>                  
          <tbody id="tbody">           
          	<?php         	  
              	foreach($data['pools'] as $pool)
                { 
          	 ?>
          	 	
             	<tr class="odd gradeX">
             		<td><img src="<?php echo $pool['images'] ?>" style="width: 20px; height: 20px;"></td>
             		<?php 
             	  foreach(ARRAY_GROUP_4D as $ag4d)
             	  {
                 	?>
                 		<td><input type="text" id='txt_Group_<?php echo $ag4d; ?>_Big_<?php echo $pool["shortname"] ?>' name='txt_Group_<?php echo $ag4d; ?>_Big_<?php echo $pool["shortname"] ?>' class="form-control eatmap_input" data-parsley-type="number" value="<?php echo old('txt_Group_'.$ag4d.'_Big_'.$pool["shortname"], number_format($data['myMap']['Group_'.$ag4d.'_Big_'.$pool["shortname"]]['value'], 2)) ?>" readonly /></td>
                  <?php } ?>
                  <?php 
             	  foreach(ARRAY_GROUP_4D as $ag4d)
             	  {
                 	?>
                 		<td><input type="text" id='txt_Group_<?php echo $ag4d; ?>_Small_<?php echo $pool["shortname"] ?>' name='txt_Group_<?php echo $ag4d; ?>_Small_<?php echo $pool["shortname"] ?>' class="form-control eatmap_input" data-parsley-type="number" value="<?php echo old('txt_Group_'.$ag4d.'_Small_'.$pool["shortname"], number_format($data['myMap']['Group_'.$ag4d.'_Small_'.$pool["shortname"]]['value'], 2)) ?>" readonly /></td>
                  <?php } ?>
                  <?php 
             	  foreach(ARRAY_GROUP_4D as $ag4d)
             	  {
                 	?>
                 		<td><input type="text" id='txt_Group_<?php echo $ag4d; ?>_4A_<?php echo $pool["shortname"] ?>' name='txt_Group_<?php echo $ag4d; ?>_4A_<?php echo $pool["shortname"] ?>' class="form-control eatmap_input" data-parsley-type="number" value="<?php echo old('txt_Group_'.$ag4d.'_4A_'.$pool["shortname"], number_format($data['myMap']['Group_'.$ag4d.'_4A_'.$pool["shortname"]]['value'], 2)) ?>" readonly /></td>
                  <?php } ?>
             	</tr>        	 
          	 <?php } ?>            	 
          </tbody>
        </table>
     </div>
     </div>
</div>
<div class="row">&nbsp;</div>
<div class="row">
	<div class="col-md-12">
	<div class="table-responsive">
        <table class="table table-striped table-bordered table-width-fix">
          <thead>
             <tr>
                <th>3D Group</th>
                <th colspan="10">3A</th>
                <th colspan="10">3ABC</th>                                       
             </tr>
             <tr>
             	<th>&nbsp;</th>
             	<?php 
             	foreach(ARRAY_GROUP_3D as $ag3d)
             	  {
             	?>
             		<th><?php echo $ag3d ?></th>
             	<?php } ?>
             	<?php 
             	foreach(ARRAY_GROUP_3D as $ag3d)
             	  {
             	?>
             		<th><?php echo $ag3d ?></th>
             	<?php } ?>
             </tr>
          </thead>                  
          <tbody id="tbody">  
          	<?php         	  
              	foreach($data['pools'] as $pool)
                { 
          	 ?>
          	 	
             	<tr class="odd gradeX">
             		<td><img src="<?php echo $pool['images'] ?>" style="width: 20px; height: 20px;"></td>
             		<?php 
             		foreach(ARRAY_GROUP_3D as $ag3d)
             	  {
                 	?>
                 		<td><input type="text" id='txt_Group_<?php echo $ag3d; ?>_3A_<?php echo $pool["shortname"] ?>' name='txt_Group_<?php echo $ag3d; ?>_3A_<?php echo $pool["shortname"] ?>' class="form-control eatmap_input" data-parsley-type="number" value="<?php echo old('txt_Group_'.$ag3d.'_3A_'.$pool["shortname"], number_format($data['myMap']['Group_'.$ag3d.'_3A_'.$pool["shortname"]]['value'], 2)) ?>" readonly /></td>
                  <?php } ?>
                  <?php 
                  foreach(ARRAY_GROUP_3D as $ag3d)
             	  {
                 	?>
                 		<td><input type="text" id='txt_Group_<?php echo $ag3d; ?>_3ABC_<?php echo $pool["shortname"] ?>' name='txt_Group_<?php echo $ag3d; ?>_3ABC_<?php echo $pool["shortname"] ?>' class="form-control eatmap_input" data-parsley-type="number" value="<?php echo old('txt_Group_'.$ag3d.'_3ABC_'.$pool["shortname"], number_format($data['myMap']['Group_'.$ag3d.'_3ABC_'.$pool["shortname"]]['value'], 2)) ?>" readonly /></td>
                  <?php } ?>
             	</tr>        	 
          	 <?php } ?>            	 
          </tbody>
        </table>	
   	</div>
   	</div>   
</div>