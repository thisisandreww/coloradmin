<table class="table_3D_4D table table-striped m-b-0">
							<thead>
								<tr>
									<th colspan="6">4D</th>
									<th colspan="2">2D</th>
								</tr>
								<tr>
									<th width="15%">Aspect</th>
									<th>4B</th>
									<th>4C</th>
									<th>4D</th>
									<th>4E</th>
									<th>4ABC</th>
									<th>2A</th>
									<th>2ABC</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									<td nowrap>Ticket Price</td>
									<td class="with-form-control"><label class='default' id='<?php echo TICKET; ?>_<?php echo FOURB; ?>_default'>0</label><input type="text" value="<?php echo old(TICKET.'_'.FOURB, '0');?>" id='<?php echo TICKET; ?>_<?php echo FOURB; ?>' name='<?php echo TICKET; ?>_<?php echo FOURB; ?>' data-parsley-type="digits" class="form-control table_input" value="1"  /></td>
									<td class="with-form-control"><label class='default' id='<?php echo TICKET; ?>_<?php echo FOURC; ?>_default'>0</label><input type="text" value="<?php echo old(TICKET.'_'.FOURC, '0');?>" id='<?php echo TICKET; ?>_<?php echo FOURC; ?>' name='<?php echo TICKET; ?>_<?php echo FOURC; ?>' data-parsley-type="digits" class="form-control table_input" value="1" /></td>
									<td class="with-form-control"><label class='default' id='<?php echo TICKET; ?>_<?php echo FOURD; ?>_default'>0</label><input type="text" value="<?php echo old(TICKET.'_'.FOURD, '0');?>" id='<?php echo TICKET; ?>_<?php echo FOURD; ?>' name='<?php echo TICKET; ?>_<?php echo FOURD; ?>' data-parsley-type="digits" class="form-control table_input" value="1" /></td>
									<td class="with-form-control"><label class='default' id='<?php echo TICKET; ?>_<?php echo FOURE; ?>_default'>0</label><input type="text" value="<?php echo old(TICKET.'_'.FOURE, '0');?>" id='<?php echo TICKET; ?>_<?php echo FOURE; ?>' name='<?php echo TICKET; ?>_<?php echo FOURE; ?>' data-parsley-type="digits" class="form-control table_input" value="1" /></td>
									<td class="with-form-control"><label class='default' id='<?php echo TICKET; ?>_<?php echo FOURABC; ?>_default'>0</label><input type="text" value="<?php echo old(TICKET.'_'.FOURABC, '0');?>" id='<?php echo TICKET; ?>_<?php echo FOURABC; ?>' name='<?php echo TICKET; ?>_<?php echo FOURABC; ?>' data-parsley-type="digits" class="form-control table_input" value="1" /></td>
									<td class="with-form-control"><label class='default' id='<?php echo TICKET; ?>_<?php echo TWOA; ?>_default'>0</label><input type="text" value="<?php echo old(TICKET.'_'.TWOA, '0');?>" id='<?php echo TICKET; ?>_<?php echo TWOA; ?>' name='<?php echo TICKET; ?>_<?php echo TWOA; ?>' data-parsley-type="digits" class="form-control table_input" value="1" /></td>
									<td class="with-form-control"><label class='default' id='<?php echo TICKET; ?>_<?php echo TWOABC; ?>_default'>0</label><input type="text" value="<?php echo old(TICKET.'_'.TWOABC, '0');?>" id='<?php echo TICKET; ?>_<?php echo TWOABC; ?>' name='<?php echo TICKET; ?>_<?php echo TWOABC; ?>' data-parsley-type="digits" class="form-control table_input" value="1" /></td>
								</tr>
								<tr>
									<td nowrap>Commission (%)</td>
									<td class="with-form-control"><label class='default' id='<?php echo COMMISSION; ?>_<?php echo FOURB; ?>_default'>0</label><input type="text" value="<?php echo old(COMMISSION.'_'.FOURB, '0');?>" id='<?php echo COMMISSION; ?>_<?php echo FOURB; ?>' name='<?php echo COMMISSION; ?>_<?php echo FOURB; ?>' data-parsley-type="number" class="form-control table_input" value="0" /></td>
									<td class="with-form-control"><label class='default' id='<?php echo COMMISSION; ?>_<?php echo FOURC; ?>_default'>0</label><input type="text" value="<?php echo old(COMMISSION.'_'.FOURC, '0');?>" id='<?php echo COMMISSION; ?>_<?php echo FOURC; ?>' name='<?php echo COMMISSION; ?>_<?php echo FOURC; ?>' data-parsley-type="number" class="form-control table_input" value="0" /></td>
									<td class="with-form-control"><label class='default' id='<?php echo COMMISSION; ?>_<?php echo FOURD; ?>_default'>0</label><input type="text" value="<?php echo old(COMMISSION.'_'.FOURD, '0');?>" id='<?php echo COMMISSION; ?>_<?php echo FOURD; ?>' name='<?php echo COMMISSION; ?>_<?php echo FOURD; ?>' data-parsley-type="number" class="form-control table_input" value="0" /></td>
									<td class="with-form-control"><label class='default' id='<?php echo COMMISSION; ?>_<?php echo FOURE; ?>_default'>0</label><input type="text" value="<?php echo old(COMMISSION.'_'.FOURE, '0');?>" id='<?php echo COMMISSION; ?>_<?php echo FOURE; ?>' name='<?php echo COMMISSION; ?>_<?php echo FOURE; ?>' data-parsley-type="number" class="form-control table_input" value="0" /></td>
									<td class="with-form-control"><label class='default' id='<?php echo COMMISSION; ?>_<?php echo FOURABC; ?>_default'>0</label><input type="text" value="<?php echo old(COMMISSION.'_'.FOURABC, '0');?>" id='<?php echo COMMISSION; ?>_<?php echo FOURABC; ?>' name='<?php echo COMMISSION; ?>_<?php echo FOURABC; ?>' data-parsley-type="number" class="form-control table_input" value="0" /></td>
									<td class="with-form-control"><label class='default' id='<?php echo COMMISSION; ?>_<?php echo TWOA; ?>_default'>0</label><input type="text" value="<?php echo old(COMMISSION.'_'.TWOA, '0');?>" id='<?php echo COMMISSION; ?>_<?php echo TWOA; ?>' name='<?php echo COMMISSION; ?>_<?php echo TWOA; ?>' data-parsley-type="number" class="form-control table_input" value="0" /></td>
									<td class="with-form-control"><label class='default' id='<?php echo COMMISSION; ?>_<?php echo TWOABC; ?>_default'>0</label><input type="text" value="<?php echo old(COMMISSION.'_'.TWOABC, '0');?>" id='<?php echo COMMISSION; ?>_<?php echo TWOABC; ?>' name='<?php echo COMMISSION; ?>_<?php echo TWOABC; ?>' data-parsley-type="number" class="form-control table_input" value="0" /></td>
								</tr>
								<tr>
									<td nowrap>First Prize</td>
									<td class="with-form-control"></td>
									<td class="with-form-control"></td>
									<td class="with-form-control"></td>
									<td class="with-form-control"></td>
									<td class="with-form-control"><label class='default' id='<?php echo FIRST; ?>_<?php echo FOURABC; ?>_default'>0</label><input type="text" value="<?php echo old(FIRST.'_'.FOURABC, '0');?>" id='<?php echo FIRST; ?>_<?php echo FOURABC; ?>' name='<?php echo FIRST; ?>_<?php echo FOURABC; ?>' data-parsley-type="number" class="form-control table_input" value="0" /></td>
									<td class="with-form-control"><label class='default' id='<?php echo FIRST; ?>_<?php echo TWOA; ?>_default'>0</label><input type="text" value="<?php echo old(FIRST.'_'.TWOA, '0');?>" id='<?php echo FIRST; ?>_<?php echo TWOA; ?>' name='<?php echo FIRST; ?>_<?php echo TWOA; ?>' data-parsley-type="number" class="form-control table_input" value="0" /></td>
									<td class="with-form-control"><label class='default' id='<?php echo FIRST; ?>_<?php echo TWOABC; ?>_default'>0</label><input type="text" value="<?php echo old(FIRST.'_'.TWOABC, '0');?>" id='<?php echo FIRST; ?>_<?php echo TWOABC; ?>' name='<?php echo FIRST; ?>_<?php echo TWOABC; ?>' data-parsley-type="number" class="form-control table_input" value="0" /></td>
								</tr>
								<tr>
									<td nowrap>Second Prize</td>
									<td class="with-form-control"><label class='default' id='<?php echo SECOND; ?>_<?php echo FOURB; ?>_default'>0</label><input type="text" value="<?php echo old(SECOND.'_'.FOURB, '0');?>" id='<?php echo SECOND; ?>_<?php echo FOURB; ?>' name='<?php echo SECOND; ?>_<?php echo FOURB; ?>' data-parsley-type="number" class="form-control table_input" value="0" /></td>
									<td class="with-form-control"></td>
									<td class="with-form-control"></td>
									<td class="with-form-control"></td>
									<td class="with-form-control"><label class='default' id='<?php echo SECOND; ?>_<?php echo FOURABC; ?>_default'>0</label><input type="text" value="<?php echo old(SECOND.'_'.FOURABC, '0');?>" id='<?php echo SECOND; ?>_<?php echo FOURABC; ?>' name='<?php echo SECOND; ?>_<?php echo FOURABC; ?>' data-parsley-type="number" class="form-control table_input" value="0" /></td>
									<td class="with-form-control"></td>
									<td class="with-form-control"><label class='default' id='<?php echo SECOND; ?>_<?php echo TWOABC; ?>_default'>0</label><input type="text" value="<?php echo old(SECOND.'_'.TWOABC, '0');?>" id='<?php echo SECOND; ?>_<?php echo TWOABC; ?>' name='<?php echo SECOND; ?>_<?php echo TWOABC; ?>' data-parsley-type="number" class="form-control table_input" value="0" /></td>
								</tr>	
								<tr>
									<td nowrap>Third Prize</td>
									<td class="with-form-control"></td>
									<td class="with-form-control"><label class='default' id='<?php echo THIRD; ?>_<?php echo FOURC; ?>_default'>0</label><input type="text" value="<?php echo old(THIRD.'_'.FOURC, '0');?>" id='<?php echo THIRD; ?>_<?php echo FOURC; ?>' name='<?php echo THIRD; ?>_<?php echo FOURC; ?>' data-parsley-type="number" class="form-control table_input" value="0" /></td>
									<td class="with-form-control"></td>
									<td class="with-form-control"></td>
									<td class="with-form-control"><label class='default' id='<?php echo THIRD; ?>_<?php echo FOURABC; ?>_default'>0</label><input type="text" value="<?php echo old(THIRD.'_'.FOURABC, '0');?>" id='<?php echo THIRD; ?>_<?php echo FOURABC; ?>' name='<?php echo THIRD; ?>_<?php echo FOURABC; ?>' data-parsley-type="number" class="form-control table_input" value="0" /></td>
									<td class="with-form-control"></td>
									<td class="with-form-control"><label class='default' id='<?php echo THIRD; ?>_<?php echo TWOABC; ?>_default'>0</label><input type="text" value="<?php echo old(THIRD.'_'.TWOABC, '0');?>" id='<?php echo THIRD; ?>_<?php echo TWOABC; ?>' name='<?php echo THIRD; ?>_<?php echo TWOABC; ?>' data-parsley-type="number" class="form-control table_input" value="0" /></td>
								</tr>	
								<tr>
									<td nowrap>Starter Prize</td>
									<td class="with-form-control"></td>
									<td class="with-form-control"></td>
									<td class="with-form-control"><label class='default' id='<?php echo SPECIAL; ?>_<?php echo FOURD; ?>_default'>0</label><input type="text" value="<?php echo old(SPECIAL.'_'.FOURD, '0');?>" id='<?php echo SPECIAL; ?>_<?php echo FOURD; ?>' name='<?php echo SPECIAL; ?>_<?php echo FOURD; ?>' data-parsley-type="number" class="form-control table_input" value="0" /></td>
									<td class="with-form-control"></td>
									<td class="with-form-control"></td>
									<td class="with-form-control"></td>
									<td class="with-form-control"></td>
								</tr>
								<tr>
									<td nowrap>Consolation Prize</td>
									<td class="with-form-control"></td>
									<td class="with-form-control"></td>
									<td class="with-form-control"></td>
									<td class="with-form-control"><label class='default' id='<?php echo CONSOLATION; ?>_<?php echo FOURE; ?>_default'>0</label><input type="text" value="<?php echo old(CONSOLATION.'_'.FOURE, '0');?>" id='<?php echo CONSOLATION; ?>_<?php echo FOURE; ?>' name='<?php echo CONSOLATION; ?>_<?php echo FOURE; ?>' data-parsley-type="number" class="form-control table_input" value="0" /></td>
									<td class="with-form-control"></td>
									<td class="with-form-control"></td>
									<td class="with-form-control"></td>
								</tr>
							</tbody>
						</table>