@extends("layouts.userlayout")

@section("headerScript")
<link href="/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/parsley/src/parsley.css" rel="stylesheet" />
<link href="/assets/css/package.css" rel="stylesheet" />
@stop
@section("body")
@include("include.phpVarEatMap")	
<!-- begin page-header -->
<h1 class="page-header">Edit My Eat Map</h1>
<!-- end page-header -->
<!-- begin panel -->
<div class="panel panel-inverse" >
	<!-- begin panel-heading -->
	<div class="panel-heading">
	<div class="panel-heading-btn">
		<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
		<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
		<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
		<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
	</div>
		<h4 class="panel-title">Eat Map Details</h4>
	</div>
	<!-- end panel-heading -->
	<!-- begin panel-body -->
	<div class="panel-body">
        @if (session('message'))
        	<div class="{{session('class')}}">{{session('message')}}</div>
        @endif
        <div id="errorMessageEatMap"></div>
		{!! Form::open(['url' => '/eatmap/savemap', 'class' => 'form-horizontal', 'data-parsley-validate' => 'true', 'id' => 'eatmapform']) !!}
		
		<!-- select eat types -->
		<div class="row row-space-10">
         <div class="col-md-4">
            <div class="form-group m-b-10" data-parsley-validate="true">
               <label for="exampleInputEmail1">3D/4D Eat Type</label>
               <select class="form-control" name="selEatType3d4d">
                  <option <?php if ($data['me']->eat_type_3d4d == "Amount"){ echo "selected"; } else { echo ""; } ?>>Amount</option>
                  <option <?php if ($data['me']->eat_type_3d4d == "Total Payout"){ echo "selected"; } else { echo ""; } ?>>Total Payout</option>
                  <option <?php if ($data['me']->eat_type_3d4d == "Group"){ echo "selected"; } else { echo ""; } ?>>Group</option>
               </select>
            </div>
         </div>
         <div class="col-md-4">
            <div class="form-group m-b-10">
               <label for="exampleInputPassword1">5D/6D Eat Type</label>
               <select class="form-control" name="selEatType5d6d">
                  <option <?php if ($data['me']->eat_type_5d6d == "Amount"){ echo "selected"; } else { echo ""; } ?>>Amount</option>
                  <option <?php if ($data['me']->eat_type_5d6d == "Total Payout"){ echo "selected"; } else { echo ""; } ?>>Total Payout</option>
               </select>
            </div>
         </div>
         <div class="col-md-4">
            <div class="form-group m-b-10">
               <label for="exampleInputPassword1">Spcl 4D Eat Type</label>
               <select class="form-control" name="selEatType4dSpcl">
                  <option <?php if ($data['me']->eat_type_4dspcl == "Amount"){ echo "selected"; } else { echo ""; } ?>>Amount</option>
                  <option <?php if ($data['me']->eat_type_4dspcl == "Total Payout"){ echo "selected"; } else { echo ""; } ?>>Total Payout</option>
               </select>
            </div>
         </div>
      </div>
		
		@include("include.myEatMap")
		<!--begin spacing-->
		<br/>
		<!--end spacing-->
		
		<input type="text" hidden id="delete_id" name="delete_id" />
		<button id="btnSave" class="btn_submit btn btn-primary" style="margin-bottom: 10px"><i class="fa fa-save"></i> Save</button>
		{!! Form::close() !!}
	</div>
	
		
	</div>
	<!-- end panel-body -->
</div>
<!-- end panel -->		
@stop

@section("page_script")
<script src="/assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="/assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
<script src="/assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script src="/assets/plugins/bootstrap-sweetalert/sweetalert.min.js"></script>
<script src="/assets/plugins/parsley/dist/parsley.js"></script>
<script>
    $(document).ready(function() {
    	// triggered when any input in the "eatmap_input" classes being changed
    	var warningCount = 0;
    	$(document).on('change', '.eatmap_input', function() {
    		  
    		$var = $(this).val();
            $refVarString = $(this).attr('id').replace("txt", "divUpline")
            				 	.replace("_mg","")
            					.replace("_kd","")
            					.replace("_tt","")
            					.replace("_sg","")
            					.replace("_sb","")
            					.replace("_stc","")
            					.replace("_sw","")
            					;
            $refvar = parseInt(document.getElementById($refVarString).innerHTML.replace("(","").replace(")","").replace(",",""));
            if($var !== "" && $.isNumeric($var))
            {
                if ($refvar != -1)
                {
                	if ($var > $refvar) {
                        warningCount++;
                        $(this).css('backgroundColor', 'yellow');
                    } else 
                    {
                        warningCount--;
                        $(this).css('backgroundColor', 'white');
                    }
                } 
            }
    	});

    	// trigger when the save button is pressed to save the reference of eat map
    	$('#eatmapform').on('submit', function(e){
        	
       		// code
	       if (warningCount > 0)
	       {
	    	   document.getElementById("errorMessageEatMap").innerHTML = "Some values are over range, please correct them.";
	    	   document.getElementById("errorMessageEatMap").classList.add("alert")
	    	   document.getElementById("errorMessageEatMap").classList.add("alert-danger");
	    	   e.preventDefault();
	       }
        });
    });
 </script>
 @stop