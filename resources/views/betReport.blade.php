@extends("layouts.userlayout")
@section("title", "Bet Statistic Report")
@section("header", "Bet Statistic Report")

@section('headerScript')
<link href="/assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
<link href="/assets/plugins/bootstrap-eonasdan-datetimepicker/build/css/bootstrap-datetimepicker.min.css" rel="stylesheet" />
<link href="/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/bootstrap-select/bootstrap-select.min.css" rel="stylesheet" />
<link href="/assets/plugins/parsley/src/parsley.css" rel="stylesheet" />
@stop

@section("body")
<!-- begin panel -->
<div class="panel panel-inverse" >
    <!-- begin panel-heading -->
    <div class="panel-heading">
        <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
        </div>
        <h4 class="panel-title">Bet Statistic Report</h4>
    </div>
    <!-- end panel-heading -->
    <!-- begin panel-body -->
    <div class="panel-body">
        @if (session('success'))
        <div class="alert alert-success" style="white-space:pre-wrap;">{{Session::get('success')}}</div>
        @endif
        @if (session('error'))
        <div class="alert alert-danger" style="white-space:pre-wrap;">{{Session::get('error')}}</div>
        @endif
        <form class="form-horizontal" id="winLossForm" method="get" action="\report\bet\{{$type}}" data-parsley-validate="true">
            <div class="form-group row m-b-10">
                <label class="col-md-2 col-form-label">Report Date Ranges</label>
                <div class="col-md-4">
                    <div class="input-group" id="searchRange">
                        <input type="text" name="searchRange" class="form-control" value="<?php if(app('request')->has('searchRange'))
                            {
                                echo app('request')->input('searchRange');
                            }
                            ?>" data-parsley-required="true" placeholder="click to select the date range" />
                        <span class="input-group-append">
                        <span class="input-group-text"><i class="fa fa-calendar"></i></span>
                        </span>
                    </div>
                </div>
            </div>
            <div class="form-group row m-b-10">
                <label class="col-md-2 col-form-label">User</label>
                <div class="col-md-4">
                    <select name="userId" class="form-control selectpicker"  data-live-search="true" data-style="btn-white">
                        <option value="{{$user->id}}">{{$user->name}}</option>
                        @foreach($downline as $ea)
                        <option value="{{$ea->id}}">{{$ea->name}}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-3">
                    <button type="submit" id="searchBtn" class="btn btn-primary">Search</button>
                </div>
            </div>
            <br /><br />
            @if ($arr != null)
            <?php $sumArr = array(); ?>
            <div class="form-group m-b-15">
                <div class="col-md-12">
                    <table id="reportTable" class="table table-striped table-bordered" width="100%">
                        <thead>
                            <tr>
                                <th>Pool</th>
                                @foreach($arr as $key => $value)
                                <th class="text-nowrap">{{$key}}</th>
                                @endforeach
                            </tr>
                        </thead>
                        <tbody>
                            @if ($record != null)
                            @foreach($record as $ea)
                            <?php $i = 0; ?>
                            <?php $row = (array)$ea; ?>
                            <tr>
                                <td>{{$ea->pool}}</td>
                                @foreach($arr as $key => $value)
                                <td>{{$row[$value]}}</td>
                                <?php
                                    if (count($sumArr) < count($arr))
                                        array_push($sumArr, $row[$value]);
                                    else
                                        $sumArr[$i] += $row[$value];
                                    ++$i; 
                                    ?>
                                @endforeach
                            </tr>
                            @endforeach
                            @endif
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Total</th>
                                <?php for($j = 0; $j < count($sumArr); $j++){ ?>
                                <th>{{$sumArr[$j]}}</th>
                                <?php } ?>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
            <div class="form-group row">
                <div class="col-md-3">
                    <a href="javascript:;" onclick="navigate({{$user->user_upline}})" class="btn btn-primary">Back To Parent</a>
                </div>
            </div>
            @endif
        </form>
    </div>
    <!-- end panel-body -->
</div>
<!-- end panel -->

@stop

@section("page_script")
<script src="/assets/plugins/moment/moment.min.js"></script>
<script src="/assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
<script src="/assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
	<script src="/assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
	<script src="/assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
    <script src="/assets/plugins/bootstrap-select/bootstrap-select.min.js"></script>
<script src="/assets/plugins/parsley/dist/parsley.js"></script>
    <script>
    $(document).ready(function() 
    {
        $('.selectpicker').selectpicker();
        $('#reportTable').DataTable({
            dom: 'lBfrtip',
            buttons: [
                { extend: 'csv', className: 'btn-sm' },
                { extend: 'excel', className: 'btn-sm' },
                { extend: 'pdf', className: 'btn-sm' },
                { extend: 'print', className: 'btn-sm' }
            ],
            responsive: true,
            autoFill: true,
            colReorder: true,
            keys: true,
            rowReorder: true,
            select: true
        });
        
        $('#searchRange').daterangepicker({
        opens: 'right',
        format: 'YYYY/MM/DD',
        separator: ' to ',
        startDate: <?php if (isset($start)){ echo "'".$start."'";}else{echo "moment()";} ?>,
        endDate: <?php if (isset($end)){echo "'".$end."'";}else{echo "moment()";} ?>,
        minDate: '01/01/2012',
        maxDate: '12/31/2018',
    },
    function (start, end) {
        $('#searchRange input').val(start.format('YYYY-MM-DD') + ' - ' + end.format('YYYY-MM-DD'));
    });
    });
    function navigate($id)
    {
        window.location.href = "/report/bet/{{$type}}/"+$id+"?"+window.location.search.substring(1);
    }
    </script>
@stop