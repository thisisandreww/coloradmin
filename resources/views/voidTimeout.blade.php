@extends("layouts.userlayout")
@section("title", "Void Timeout")
@section("header", "Edit Void Timeout")

@section('headerScript')
<link href="/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css" rel="stylesheet" />
<link href="/assets/plugins/parsley/src/parsley.css" rel="stylesheet" />
@stop

@section("body")
<div class="panel panel-inverse" >
	<div class="panel-heading">
		<div class="panel-heading-btn">
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
		</div>
		<h4 class="panel-title">Void Timeout</h4>
	</div>
	<div class="panel-body">
		<form method="POST" action="{{ action('MemberController@store_voidtimeout') }}">
			{{ csrf_field() }}
			<div class="row">
				<div class="col-md-2">
					<div class="form-group">
						<label>Time</label>
						<div class="d-flex">
							<select class="form-control" style="width: 70px">
								@for($i = 0; $i <= 60; $i++)
								<option>{{ str_pad($i, 2, "0", STR_PAD_LEFT) }}</option>
								@endfor
							</select>
							<span>minutes</span>
						</div>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@stop