@extends("layouts.userlayout")
@section("title", "Change Password")
@section("header", "Change own password")

@section('headerScript')
<link href="/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css" rel="stylesheet" />
<link href="/assets/plugins/parsley/src/parsley.css" rel="stylesheet" />
@stop

@section("body")
<div>
   <!-- begin panel -->
   <div class="panel panel-inverse">
      <!-- begin panel-heading -->
      <div class="panel-heading">
         <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
         </div>
         <h4 class="panel-title">@lang('user.CHANGE_PASSWORD')</h4>
      </div>
      <!-- end panel-heading -->
      <div class="panel-body">
<<<<<<< HEAD
         <form class="form-horizontal" action="validate/{{$user->id}}" data-parsley-validate="true" method="POST">
=======
         <form class="form-horizontal" action="/resetPassword/validate" method="POST">
>>>>>>> 886ec43e6bff081158a409523b3f52b2be720a7a
            {{ csrf_field() }}
            @if (session('success'))
            <div class="alert alert-success">Password is reseted</div>
            @endif
            @if (session('error'))
            <div class="alert alert-danger">{{Session::get('error')}}</div>
            @endif
            
            <div class="row">		
        		<div class="col-md-4">
                    <div class="form-group">
                        <label>@lang('user.OLD_PASSWORD')</label>
            			<input type="password" name="oldPassword" id="oldPwdTxt" data-parsley-required class="form-control width-full m-b-10" placeholder="Password" ></input>
                    </div>
                </div>
                <div class="col-md-4">
        			<div class="form-group">
                        <label>@lang('user.NEW_PASSWORD')</label>
            			<input type="password" name="newPassword" id="newPwdTxt" data-parsley-required class="form-control width-full m-b-10" placeholder="New Password" ></input>
                    </div>
                </div>
                <div class="col-md-4">
        			<div class="form-group">
                        <label>@lang('user.RETYPE_PASSWORD')</label>
            			<input type="password" name="retypePassword" id="retypePwdTxt" data-parsley-required class="form-control width-full m-b-10" placeholder="Confirm New Password" ></input>
                    </div>
                </div>
            </div>
            <div class="row"></div>
            <div class="col-md-12">
    			<button id="btnSave" type="submit" class="btn btn-primary"><i class="fas fa-save fa-fw"></i>&nbsp;@lang('general.SAVE')</button>
    		</div>
         </form>
      </div>
   </div>
   <!-- end panel -->			
</div>
@stop