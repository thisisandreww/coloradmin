@extends("layouts.loginlayout")

@section("body")
<!-- begin #page-loader -->
<div id="page-loader" class="fade show"><span class="spinner"></span></div>
<!-- end #page-loader -->

<!-- begin #page-container -->
<div id="page-container" class="fade">
	<!-- begin login -->
	<div class="login login-with-news-feed">
		<!-- begin news-feed -->
		<div class="news-feed">
			<div class="news-image" style="background-image: url(../assets/img/login-bg/login-bg-11.jpg)"></div>
			<div class="news-caption">
				<h4 class="caption-title"><b>水为财</b> 发财网</h4>
				<p>
					我们提供专业的发财资讯，让您掌握时代的尖端。
				</p>
			</div>
		</div>
		<!-- end news-feed -->
		<!-- begin right-content -->
		<div class="right-content">
			<!-- begin login-header -->
			<div class="login-header">
				<div class="brand">
					<span class="logo"><i class="ion-social-snapchat-outline"></i></span> <b>水为财</b> 发财网
					<small>我们时刻为您的将来服务</small>
				</div>
				<div class="icon">
					<i class="fa fa-sign-in"></i>
				</div>
			</div>
			<!-- end login-header -->
			<!-- begin login-content -->
			<div class="login-content">            
				{!! Form::open(['url'=> 'login', 'class' => 'margin-bottom-0']) !!}
				@if (session('error'))
				<div class="alert alert-danger fade in m-b-15">
					<strong>ERROR: {{session('error')}}</strong>
				</div>
				@endif
				<div class="form-group m-b-15">
					{{Form::text('username', '',['class'=>'form-control form-control-lg', 'placeholder'=>'User Name', 'required' => 'required'])}}
				</div>
				<div class="form-group m-b-15">
					{{Form::password('user_password', ['class'=>'form-control form-control-lg', 'placeholder'=>'Password', 'required' => 'required'])}}
				</div>


				<div class="form-group{{ $errors->has('captcha') ? ' has-error' : '' }}">
					<div class="row">
						<div class="col-md-5 captcha">
							{!! captcha_img() !!}
						</div>
						<div class="col-md-5">
							<input id="captcha" type="text" class="form-control form-control-lg" placeholder="Enter Captcha" name="captcha" autocomplete="off">
						</div>
						<div class="col-md-2">
							<button type="button" class="btn btn-success btn-refresh"><i class="fa fa-undo"></i></button>
						</div>
					</div>
					@if ($errors->has('captcha'))
					<span class="help-block">
						<strong>{{ $errors->first('captcha') }}</strong>
					</span>
					@endif
					<!-- </div> -->
				</div>
				<div class="checkbox checkbox-css m-b-30">
					<input type="checkbox" id="remember_me_checkbox" name="remember_me_checkbox" value="" />
					<label for="remember_me_checkbox">
						Remember Me
					</label>
				</div>
				<div class="login-buttons">
					{{Form::submit('Sign in', ['class'=>'btn btn-success btn-block btn-lg'])}}
				</div>
				<p class="text-center text-grey-darker m-t-10">
					&copy; 水为财 All Right Reserved 2018
				</p>
				{!! Form::close() !!}
			</div>
			<!-- end login-content -->
		</div>
		<!-- end right-container -->
	</div>
	<!-- end login -->
</div>
<!-- end page container -->
@stop

@section("page_script")
<script src="/assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
<script src="/assets/plugins/js-cookie/js.cookie.js"></script>
<script src="/assets/plugins/jquery-captcha/dist/jquery-captcha.min.js"></script>
<script src="/assets/plugins/bootstrap-sweetalert/sweetalert.min.js"></script>
<script src="/assets/js/theme/apple.min.js"></script>
<script src="/assets/js/apps.min.js"></script>	
<script>
	$(document).ready(function() {		
		App.init();				
	});

	$(".btn-refresh").click(function(){
		$.ajax({
			type:'GET',
			url:'/refreshCaptcha',
			success:function(data){
				$(".captcha").html(data.captcha);
			}
		});
	});

	$(document).on("click", "#btnLogin", function (e) {
		var errMsg = ValidateInputs();
		if (errMsg != "") {
			swal({
				title: 'Please correct below error(s):',
				text: errMsg,
				icon: 'error',
				buttons: {
					cancel: {
						text: 'OK',
						value: null,
						visible: true,
						className: 'btn btn-default',
						closeModal: true,
					}
				}
			});
			e.preventDefault();
		}
	});

	function ValidateInputs() {
		var msg = "";
		var obj = {
			userName: $("#txtUserName").val()
			, password: $("#txtPassword").val()
		}
		for (var prop in obj) {
			if (obj.hasOwnProperty(prop)) obj[prop] = obj[prop].trim();
		}
		if (obj.userName == "") msg += "\nUsername is required.";
		if (obj.password == "") msg += "\nPassword is required.";
		return msg;
	}
</script>
@stop