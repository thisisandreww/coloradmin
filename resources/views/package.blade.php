@extends("layouts.userlayout")

@section("title")
@lang("package.MASTER_PACKAGE_TITLE")
@stop

@section("header")
<?php 
$isLoginUserSystem = 0;
if( $data['user']->role_name=="System")
	$isLoginUserSystem = 1;

$is3D4D = 0;
if($data['packageType'] =="1" || $data['packageType'] =="3")
	$is3D4D = 1;
?>

@if ($data['packageType'] == "3" || $data['packageType'] == "4")
@lang('package.RED_PACKAGE')
@endif

@if ($is3D4D)
@lang("package.PACKAGE_3D4D") @lang("package.PACKAGE_TITLE") 
@else
@lang("package.PACKAGE_5D6D") @lang("package.PACKAGE_TITLE")
@endif
@stop

@section("headerScript")
<link href="/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/css/package.css" rel="stylesheet" />
@stop
@section("body")
@include("include.phpVar")
<input type="hidden" value="{{$data['user']->id}}" id="id"/>

<div>
	<!-- begin panel -->
	<div class="panel panel-inverse">
		<!-- begin panel-heading -->
		<div class="panel-heading">
			<div class="panel-heading-btn">
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
				<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
			</div>
			<h4 class="panel-title">@lang('package.ALL_PACKAGE')</h4>
		</div>
		<!-- end panel-heading -->
		<!-- begin panel-body -->
		<input type="hidden" name="red_package" value="$data['packageType']" />
		<div class="panel-body">
			@if (session('error'))
			<div class="alert alert-danger">{{Session::get('error')}}</div>
			@endif
			<table id="packageTable" class="table table-striped table-bordered">
				<thead>
					<tr>
						<th width="1%">@lang('general.ID')</th>
						<th class="text-nowrap">@lang('package.PACKAGE_NAME')</th>
						<th class="text-nowrap">@lang('general.SUMMMARY')</th>
						<th class="text-nowrap">@lang('general.CREATED_DATE')</th>
						<th class="text-nowrap all">@lang('general.ACTION')</th>
					</tr>
				</thead>
				<tbody>
					@if(count($data['packageList']) > 0)
					<?php $count = 0; ?>
					@foreach($data['packageList'] as $package)
					<tr class="odd gradeX">
						<td width="1%" class="f-s-600 text-inverse"><?php echo ++$count; ?></td>
						<td>{{ $package->package_name }}</td>
						<td>22%/2300/6600/10000/80/50</td>
						<td>{{ $package->created_at }}</td>
						<td class="all">
							@if($data['packageType'] =="1" || $data['packageType'] =="3")
							<a href="#package_3D4D_dialog" data-rowid="{{$package->package_id}}" data-rowtypeid="{{$package->package_type_id}}" data-rowuserid="{{$data['user']->id}}"
								data-rowpriviledge="{{$data['user']->package_control & $data['loginUser']->package_control & $data['loginUser']->state == 0 & $isLoginUserSystem}}" data-rowreferenced="{{$package->reference_count}}" data-toggle="modal" class="btn btn-primary edit btn-icon btn-circle package_3D4D_table"><i class='fa fa-edit'></i></a>
								@else
								<a href="#package_5D6D_dialog" data-rowid="{{$package->package_id}}" data-rowtypeid="{{$package->package_type_id}}" data-rowuserid="{{$data['user']->id}}"
									data-rowpriviledge="{{$data['user']->package_control & $data['loginUser']->package_control & $data['loginUser']->state == 0 & $isLoginUserSystem}}" data-rowreferenced="{{$package->reference_count}}" data-toggle="modal" class="btn btn-primary edit btn-icon btn-circle package_5D6D_table"><i class='fa fa-edit'></i></a>
									@endif
									@if($data['user']->package_control && $data['loginUser']->package_control && $data['loginUser']->state == 0 && $isLoginUserSystem)
									<button data-rowreferencedcount="{{$package->reference_count}}" data-rowid="{{ $package->package_id }}" data-rowuserid="{{$data['user']->id}}" data-rowname="{{$package->package_name}}" class='btn btn-danger btn-circle btn-icon delete'><i class='fa fa-trash'></i></button>
									@endif
								</td>
							</tr>
							@endforeach
							@endif
						</tbody>
					</table>
					<div class="col-md-12">
						<input type="text" hidden id="delete_id" name="delete_id" />
						@if ($data['loginUser']->package_control && $isLoginUserSystem)
						<button onclick="location.href = '/packageAdd/{{$data['packageType']}}'" id="btnCreateCategories" class="btn_submit btn btn-primary" style="margin: 10px"><i class="fa fa-plus-circle"></i> @lang('package.CREATE_PACKAGE')</button>
						@endif
					</div>
				</div>
				<!-- end panel-body -->
			</div>
			<!-- end panel -->			
		</div>
		<form class="form-horizontal" data-parsley-validate="true" name="package_3D4D_form" id="package_3D4D_form"  method="POST">
			{{ csrf_field() }}
			<div class="modal fade" id="package_3D4D_dialog">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">@lang('package.EDIT_PACKAGE'): <label id="modal_package_3D4D"></label></h4>
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						</div>
						<div class="modal-body">
							<div class="panel-body">
								<input type="hidden" value="{{csrf_token()}}" name="_token" id="_token" />
								<div class="table-responsive">
									@include("include.table4D")
								</div>
								<br/><br/><br/>
								<div class="table-responsive">
									@include("include.table4DS")
								</div>
								<div>
									<input type="hidden" id="selected_3D4D_package_owner_id" name="selected_package_owner_id"/>
									<input type="hidden" id="selected_3D4D_package_type_id" name="selected_package_type_id"/>
									<input type="hidden" id="selected_3D4D_package_id" name="selected_package_id"/>
								</div>
							</div>
							<!-- end panel-body -->
						</div>
						<div class="modal-footer">
							<a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">@lang('general.CLOSE')</a>
							@if($data['loginUser']->package_control && $isLoginUserSystem)
							<button class="btn btn-sm btn-success">@lang('general.SAVE')</button>
							@endif
						</div>
					</div>
				</div>
			</div>
		</form>
		<form class="form-horizontal" data-parsley-validate="true" name="package_5D6D_form" id="package_5D6D_form"  method="POST">
			{{ csrf_field() }}
			<div class="modal fade" id="package_5D6D_dialog">
				<div class="modal-dialog modal-lg">
					<div class="modal-content">
						<div class="modal-header">
							<h4 class="modal-title">@lang('package.EDIT_PACKAGE'): <label id="modal_package_5D6D"></label></h4>
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
						</div>
						<div class="modal-body">
							<div class="panel-body">
								<input type="hidden" value="{{csrf_token()}}" name="_token" id="_token" />
								<div class="table-responsive">
									@include("include.table5D6D")
								</div>
								<div>
									<input type="hidden" id="selected_5D6D_package_owner_id" name="selected_package_owner_id"/>
									<input type="hidden" id="selected_5D6D_package_type_id" name="selected_package_type_id"/>
									<input type="hidden" id="selected_5D6D_package_id" name="selected_package_id"/>
								</div>
							</div>
							<!-- end panel-body -->
						</div>
						<div class="modal-footer">
							<a href="javascript:;" class="btn btn-sm btn-white" data-dismiss="modal">@lang('general.CLOSE')</a>
							@if($data['loginUser']->package_control && $isLoginUserSystem)
							<button type="submit" class="btn btn-sm btn-success">@lang('general.SAVE')</button>
							@endif	
						</div>
					</div>
				</div>
			</div>
		</form>
		@stop
		@section("page_script")
		<script src="/assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
		<script src="/assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
		<script src="/assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
		<script src="/assets/plugins/bootstrap-sweetalert/sweetalert.min.js"></script>
		<script>
			$(document).ready(function() {
				$(".default").css("display", "none");
				$currentViewPackagedReferenced =0;
				$('#packageTable').DataTable( {
					responsive: true
				});

				$(document).on("click", ".delete", function (e) {
					swal({
						title: 'Delete Package ?',
						text: "You are about to delete record "+$(this).data("rowname")+", User that assigned this package will be unassigned on confirm",
						icon: 'warning',
						buttons: {
							delete: {
								text: 'Cancel',
								value: 0,
								visible: true,
								className: 'btn btn-primary',
								closeModal: true,
							},
							cancel: {
								text: 'Delete',
								value: 1,
								visible: true,
								className: 'btn btn-danger',
								closeModal: true,
							}
						}
					}).then((value) => {
						if(value == 1)
						{
							if ($(this).data("rowreferencedcount") == 0)
							{
								$.ajax({
									url: "/package/delete/"+$(this).data("rowid"),
									type: 'POST',
									data: {_token: '{{csrf_token()}}'},
									success: function (data) {
										location.href = "/package/{{$data['packageType']}}";
									}
								});
							}
							else
							{
								swal("Error", "Record is referenced by member, not allowed to delete", "error");
							}
						}
					});
					e.preventDefault();
				});

				$(".package_5D6D_table").click(function() {
					$canEdit = $(this).data("rowpriviledge");
					$("input").css('background', 'white');
					$currentViewPackagedReferenced = $(this).data("rowreferenced");
					$.ajax({
						type:'GET',
						url:'\\packageDetail\\'+$(this).data("rowuserid")+'\\'+$(this).data("rowtypeid")+'\\'+$(this).data("rowid"),
						success:function(data)
						{
							if ($canEdit == null || $canEdit.length ===0 || $canEdit == 0)
								$("#package_5D6D_form input").prop("disabled", true);
							else
								$("#package_5D6D_form input").prop("disabled", false);

							if ($currentViewPackagedReferenced != 0)
								$(".red_package").prop("disabled", true);
							else
								$(".red_package").prop("disabled", false);
							updateView(data);
						}
					});
				});

				$(".package_3D4D_table").click(function() {
					$canEdit = $(this).data("rowpriviledge");
					$("input").css('background', 'white');
					$currentViewPackagedReferenced = $(this).data("rowreferenced");
					$.ajax({
						type:'GET',
						url:'\\packageDetail\\'+$(this).data("rowuserid")+'\\'+$(this).data("rowtypeid")+'\\'+$(this).data("rowid"),
						success:function(data)
						{
							if ($canEdit == null || $canEdit.length ===0 || $canEdit == 0)
								$("#package_3D4D_form input").prop("disabled", true);
							else
								$("#package_3D4D_form input").prop("disabled", false);

							if ($currentViewPackagedReferenced != 0)
								$(".red_package").prop("disabled", true);
							else
								$(".red_package").prop("disabled", false);
							updateView(data);
						}
					});
				});

				$('#package_5D6D_form').on('submit', function (e)
				{
					if ($currentViewPackagedReferenced !=0 && !confirm('Warning, Packaged Assigned, changing value will cause all user to be affected')) 
					{
						return false;
					}
					$packageId = $('#selected_5D6D_package_id').val();
					e.preventDefault();
					$.ajax({
						type: 'POST',
						data: $('#package_5D6D_form').serialize(),
						url: '/package/'+$packageId,
						success: function (data) 
						{
							if (data.data['red_package'] == 0)
								$('#red_package_'+data.data['package_id']).text("No");
							else
								$('#red_package_'+data.data['package_id']).text("Yes");
							$('#package_5D6D_dialog').modal('toggle');
						}
					});
				});

				$('#package_3D4D_form').on('submit', function (e)
				{
					if ($currentViewPackagedReferenced !=0 && !confirm('Warning, Packaged Assigned, changing value will cause all user to be affected')) 
					{
						return false;
					}
					$packageId = $('#selected_3D4D_package_id').val();
					e.preventDefault();
					$.ajax({
						type: 'POST',
						data: $('#package_3D4D_form').serialize(),
						url: '/package/'+$packageId,
						success: function (data) 
						{
							if (data.data['red_package'] == 0)
								$('#red_package_'+data.data['package_id']).text("No");
							else
								$('#red_package_'+data.data['package_id']).text("Yes");
							$('#package_3D4D_dialog').modal('toggle');
						}
					});
				});

				$("input").change( function() {
					$var = $(this).val();
					$refVarString = $(this).attr('id')+ "_default";
					$refvar = parseInt($("#"+$refVarString).text());

					if($var !== "" && $.isNumeric($var))
					{
						if ($var > $refvar)
						{
							$(this).css('background', 'yellow');
						}
						else
						{
							$(this).css('background', 'white');
						}
					}
				});

				function updateView(data)
				{
					if (data.data['packageType']['package_type_name'] == "3D/4D")
					{
						$("#selected_3D4D_package_owner_id").val(data.data['user']['id']);
						$("#selected_3D4D_package_type_id").val(data.data['packageType']['package_type_id']);
						$("#selected_3D4D_package_id").val(data.data['packageId']);
						$("#modal_package_3D4D").text(data.data['selectedPackage']['package_name']);

						<?php
						foreach (TABLE_PROPERTIES as $x => $x_value)
						{
							foreach (TABLE_4D as $id) 
							{
								echo "$('#".$x_value."_".$id."_default').text(data.data['package']['$id']['$x_value']);\r\n";
								echo "$('#".$x_value."_$id').val(data.data['package']['$id']['$x_value']);\r\n";
							}

							foreach (TABLE_3D as $id) 
							{
								echo "$('#".$x_value."_".$id."_default').text(data.data['package']['$id']['$x_value']);\r\n";
								echo "$('#".$x_value."_$id').val(data.data['package']['$id']['$x_value']);\r\n";
							}

							foreach (TABLE_4DS as $id) 
							{
								echo "$('#".$x_value."_".$id."_default').text(data.data['package']['$id']['$x_value']);\r\n";
								echo "$('#".$x_value."_$id').val(data.data['package']['$id']['$x_value']);\r\n";
							}
						}
						?>
					}
					else
					{
            //5d&6d
            $("#selected_5D6D_package_owner_id").val(data.data['user']['id']);
            $("#selected_5D6D_package_type_id").val(data.data['packageType']['package_type_id']);
            $("#selected_5D6D_package_id").val(data.data['packageId']);
            $("#modal_package_5D6D").text(data.data['selectedPackage']['package_name']);
            <?php 
            foreach (TABLE_PROPERTIES as $x => $x_value) 
            {
            	foreach (TABLE_5D6D as $id) 
            	{
            		echo "$('#".$x_value."_".$id."_default').text(data.data['package']['$id']['$x_value']);\r\n";
            		echo "$('#".$x_value."_$id').val(data.data['package']['$id']['$x_value']);\r\n";
            	}
            }
            ?>
        }
    }
});   	
</script>
@stop