@extends("layouts.userlayout")
@section("body")

<?php 
if ($data['loginUser']->user_main_account != $data['loginUser']->id)
    $isSubAccount = 1;
else 
    $isSubAccount = 0;
?>

<div>
   <!-- begin page-header -->
   <h1 class="page-header">@lang('user.SUB_ACCOUNT')</h1>
   <!-- end page-header -->
   <!-- begin panel -->
   <div class="panel panel-inverse" >
      <!-- begin panel-heading -->
      <div class="panel-heading">
         <div class="panel-heading-btn">
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
            <a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
         </div>
         <h4 class="panel-title">@lang('user.SUB_ACCOUNT')</h4>
      </div>
      <!-- end panel-heading -->
      <!-- begin panel-body -->
      <div class="panel-body">
         <form class="form-horizontal" id="sub_acc_form" method="POST">
            {{ csrf_field() }}
            @if (session('error'))
            <div class="alert alert-danger">{{Session::get('error')}}</div>
            @endif
            @if (session('success'))
                <div class="alert alert-success">{{Session::get('success')}}</div>
            @endif
            <div class="left" style="margin-right:0px; float:left; ">
               <h5>@lang('user.MEMBER_ID'): </h5>
               <input type="text" id="username" name="username" value="<?php if($isSubAccount){echo $data['loginUser']->username;} ?>" style="width:125px; height:30px; margin-top:5px; margin-right:20px; font-size:12px; padding-left:5px;"></input>
               <!--begin spacing-->
               <br/><br/>
               <!--end spacing-->
               <h5>@lang('user.PASSWORD'):</h5>
               <input type="password" id="password" name="password" value="<?php if($isSubAccount){echo $data['loginUser']->password;} ?>" style="width:125px; height:30px; margin-top:5px; margin-right:20px; font-size:12px; padding-left:5px;"></input>
               <!--begin spacing-->
               <br/><br/>
               <!--end spacing-->
               <label class="checkbox-inline" style ="font-size:15px;">
               <input type="checkbox" name="betting_capability" id="betting_capability" value="1"
               @if ($isSubAccount && $data['loginUser']->betting_capability)
               checked
               @elseif ($data['loginUser']->betting_capability == 0)
               disabled
               @endif
               <?php if($isSubAccount) echo "disabled"; ?> /> @lang('user.BETTING_CAPABILIY') &nbsp; &nbsp; &nbsp;
               </label>
               <label class="checkbox-inline" style ="font-size:15px;">
               <input type="checkbox" name="report_view" id="report_view" value="1"
               @if ($isSubAccount && $data['loginUser']->report_view)
               checked
               @elseif ($data['loginUser']->report_view == 0)
               disabled
               @endif
               <?php if($isSubAccount) echo "disabled"; ?> />&nbsp; @lang('user.REPORT_VIEWING') &nbsp; &nbsp; &nbsp;
               </label>
               <label class="checkbox-inline" style ="font-size:15px;">
               <input type="checkbox" name="package_control" id="package_control" value="1"
               @if ($isSubAccount && $data['loginUser']->package_control)
               checked
               @elseif ($data['loginUser']->package_control == 0)
               disabled
               @endif
               <?php if($isSubAccount) echo "disabled"; ?> />&nbsp; @lang('user.PACKAGE_SETTING') &nbsp; &nbsp; &nbsp;
               </label>
               <label class="checkbox-inline" style ="font-size:15px;">
               <input type="checkbox" name="downline_control" id="downline_control" value="1"
               @if ($isSubAccount && $data['loginUser']->downline_create)
               checked
               @elseif ($data['loginUser']->downline_create == 0)
               disabled
               @endif
               <?php if($isSubAccount) echo "disabled"; ?> />&nbsp; @lang('user.ACCOUNT_SETTING') &nbsp; &nbsp; &nbsp;
               </label>
            </div>
            <!--end class left-->
            <!--begin class right-->
            <div class="right" style="margin:0px; float-right: 0px;">
               <div id="saveShow">
                  <div <?php if($isSubAccount){echo "style='display:none'";} ?>>
                  <h5>@lang('user.SUB_ACCOUNT'):</h5>
                  <select id="account_option"  style="width:125px; height:30px; margin-top:5px; margin-right:20px; font-size:12px;">
                     <option value="0"> @lang('user.ACCOUNT_CHOOSE') </option>
                     @foreach($data['subAccountList'] as $subAccount)
                     <option value="{{$subAccount->id}}"> {{$subAccount->username}} </option>
                     @endforeach
                  </select>
                  <br/><br/>
                  </div>
                  <div class="col-md-12" style="padding-left:0px;">
                     <button class="btn btn-success" id="btnSave" style="margin-bottom: 10px;<?php if ($isSubAccount){echo "display:inline";}else{echo "display:none";}?>" onclick="editSubAccount()" ><i class="fa fa-save"></i>&nbsp; @lang('general.SAVE')</button>
                     <button class="btn btn-primary" id="btnNew" style="margin-bottom: 10px;<?php if ($isSubAccount){echo "display:none";}?>" onclick="addSubAccount()"><i class="fa fa-plus-circle"></i>&nbsp; @lang('general.NEW')</button>
                  </div>
               </div>
            </div>
            <input type="hidden" id="main_account" value="{{$data['mainAcc']->id}}" />
         </form>
      </div>
   </div>
   <!-- end panel-body -->
</div>

@stop
@section("page_script")
<script>
   	function addSubAccount()
    {
        var mainAcc = document.getElementById('main_account').value;
        var form = document.getElementById("sub_acc_form");
        form.action = '/subAccountAdd/' + mainAcc;
        return true;
    }
    function editSubAccount()
    {
        var subAcc = document.getElementById('account_option').value;
        var form = document.getElementById("sub_acc_form");
        
        if (subAcc == 0)
        {
            form.action = "/subAccountEdit/{{$data['loginUser']->id}}";
        }
        else
        {
            form.action = '/subAccountEdit/' + subAcc;
        }
        return true;
    }
    $(document).ready(function() {
   	$('#account_option').change(function(){
        if ($(this).val() != 0)
        {
            $("#btnSave").css("display", 'inline');
        }
        else
        {
             $("#btnSave").css("display", 'none');
        }
        var accInfo = {!! json_encode($data['subAccountList']) !!};
          document.getElementById("username").value = accInfo[$(this).val()]['username'];
          document.getElementById("password").value = accInfo[$(this).val()]['password'];
          $("#betting_capability").prop("checked", accInfo[$(this).val()]['betting_capability']);
          $("#package_control").prop("checked", accInfo[$(this).val()]['package_control']);
          $("#report_view").prop("checked", accInfo[$(this).val()]['report_view']);
          $("#downline_control").prop("checked", accInfo[$(this).val()]['downline_create']);
          });
    });
</script>
@stop