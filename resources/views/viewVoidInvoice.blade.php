@extends("layouts.userlayout")
@section("title", "View / Void Invoice")
@section("header", "View / Void Invoice")

@section('headerScript')
<link href="/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css" rel="stylesheet" />
<link href="/assets/plugins/parsley/src/parsley.css" rel="stylesheet" />
@stop

@section("body")
@if(session('msg'))
<div class="alert {{ session('class') }}">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{!! session('msg') !!}
</div>
@endif

<div class="panel panel-inverse">
	<div class="panel-heading">
		<div class="panel-heading-btn">
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
		</div>
		<h4 class="panel-title">Invoice List</h4>
	</div>

	<div id="divMsg" style="display: none" class="alert alert-danger"></div>
	
	<div class="panel-body">
		<table id="table_invoice" class="table table-bordered table-striped table-condensed table-valign-top f-s-14">
			<thead>
				<tr>
					<th width="1%" class="p-r-0">
						<div class="checkbox checkbox-css p-t-20">
							<input type="checkbox" id="chk_bet" />
							<label for="chk_bet"></label>
						</div>
					</th>
					<th>Bet Info</th>
					<th>Bet</th>
					<th>Slip</th>
					<th style="width: 200px">SMS</th>
					<th width="1%"></th>
				</tr>
			</thead>
			<tbody>
				@foreach($tickets AS $tk)
				<tr>
					<td>
						<div class="checkbox checkbox-css p-t-20">
							<input type="checkbox" id="chk_bet" />
							<label for="chk_bet"></label>
						</div>
					</td>
					<td width="20%">
						<p class="m-b-0">
							Account: {{ $tk->getUser->username }}<br>
							Bet Date: {{ $tk->betTime->format('Ymd') }}<br>
							Bet Time: {{ $tk->betTime->format('h:i A') }}<br>
							Currency: {{ $tk->currency }}<br>
							Page No.: {{ $tk->pageid }}<br>
							MasterNo.: <br>
							Receipt Amount: {{ $tk->actual_amount }} <br>
							Bet By: {{ $tk->getUser->username }}<br>
							Bet Method: {{ $tk->bet_method }}<br>
							Bet Format: {{ $tk->bet_format }}<br>
							Draw Format: {{ $tk->draw_format }}<br>
							Box / I-Box: {{ $tk->box_ibox }}
						</p>
					</td>
					<td width="20%">
						<div style="height: 240px; overflow-y: auto">{!! $tk->betstring !!}</div>
					</td>
					<td>
						{!! $tk->inbox !!}
					</td>
					<td width="1%" style="padding: 7px;">
						<div class="d-flex">
							<input type="text" class="form-control">
							<button type="button" class="btn btn-primary m-l-5"><i class="fas fa-paper-plane"></i></button>
						</div>
					</td>
					<td width="1%" style="padding: 7px;">
						<button type="button" class="btn btn-primary btn-block f-s-14 mb-2">Rebuy</button>
						@if($tk->created_at->diffInMinutes() <= $timeout && $tk->status == 0)
						<form method="POST" action="{{ action('InboxController@voidInvoice') }}">
							{{ csrf_field() }}
							<input hidden name="txt_ticket" value="{{ $tk->id }}">
							<button type="button" class="btn btn-warning btn-block f-s-14 btn-void">Void</button>
						</form>
						@endif
					</td>
				</tr>
				@endforeach
			</tbody>
		</table>
	</div>
</div>
@stop

@section("page_script")
<script src="/assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="/assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
<script src="/assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.28/dist/sweetalert2.all.min.js"></script>

<script type="text/javascript">
	$(function(){
		$("#table_invoice").DataTable({
			"ordering": false
		});

		$(document).on("click", ".btn-void", function(){
			var form = $(this).closest("form");
			swal({
				title: "Confirm void invoice?",
				text: "Press Continue to proceed",
				type: "warning",
				showCancelButton: true,
				confirmButtonText: 'Continue',
				cancelButtonText: 'Cancel',
				reverseButtons: true
			}).then((result) => {
				if (result.value) {
					form.submit();
				}
			});
		});
	});
</script>
@stop