@extends("layouts.userlayout")
@section("title", "Betting Records")
@section("header", "Betting Records")

@section('headerScript')
<link href="/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css" rel="stylesheet" />
<link href="/assets/plugins/parsley/src/parsley.css" rel="stylesheet" />
@stop

@section("body")
@if(session('msg'))
<div class="alert {{ session('class') }}">
	<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
	{!! session('msg') !!}
</div>
@endif

<div class="panel panel-inverse">
	<div class="panel-heading">
		<div class="panel-heading-btn">
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
		</div>
		<h4 class="panel-title">Betting Records List</h4>
	</div>

	<div id="divMsg" style="display: none" class="alert alert-danger"></div>
	
	<div class="panel-body">
		<form method="GET">
			<div class="row m-b-20">
				<div class="col-md-1">
					<div class="form-group">
						<label>Type</label>
						<select class="form-control" name="pool">
							<option value="all">All</option>
							@foreach($pools AS $pool)
							<option value="{{ $pool->code }}" {{ ($query_pool == $pool->code) ? 'selected' : '' }} >{{ $pool->name }}</option>
							@endforeach
						</select>
					</div>
				</div>
				<div class="col-md-1">
					<div class="form-group">
						<label>Number</label>
						<input type="text" class="filter form-control" name="number" value="{{ $query_number }}">
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label>User</label>
						<input type="text" class="filter form-control" name="user" value="{{ $query_user }}">
					</div>
				</div>
				<div class="col-md-3">
					<div class="form-group">
						<label>Date Range</label>
						<div class="input-group input-daterange">
							<input type="text" id="start_date" class="filter form-control" autocomplete="off" name="start_date" placeholder="Date Start" value="{{ $query_startdate }}" />
							<span class="input-group-addon">to</span>
							<input type="text" id="end_date" class="filter form-control" autocomplete="off" name="end_date" placeholder="Date End" value="{{ $query_enddate }}" style="border-radius: 0 3px 3px 0" />
						</div>
					</div>
				</div>
				<div class="col-md-1">
					<div class="form-group">
						<label>Page</label>
						<input type="text" class="filter form-control" name="page" value="{{ $query_page }}">
					</div>
				</div>
				<div class="col-md-2">
					<div class="form-group">
						<label>Remark</label>
						<input type="text" class="filter form-control" name="remark">
					</div>
				</div>
				<div class="col-md-12">
					<button id="btn_search" type="button" class="btn btn-primary m-r-5">Search</button>
					<a href="{{ action('ReportController@bettingRecords') }}" class="btn btn-default">Reset</a>
				</div>
			</div>
		</form>
		<div class="table-responsive">
			<table id="table_betrecords" class="table table-bordered table-striped table-valign-middle f-s-14 m-b-20">
				<thead>
					<tr>
						<th width="1%">Date</th>
						<th width="1%" class="no-wrap">Page</th>
						<th width="1%" class="no-wrap">Type</th>
						<th width="1%" class="no-wrap">Draw Date</th>
						<th>Number</th>
						<th>Big</th>
						<th>Small</th>
						<th>4A</th>
						<th>4B</th>
						<th>4C</th>
						<th>4D</th>
						<th>4E</th>
						<th>4ABC</th>
						<th>3A</th>
						<th>3ABC</th>
						<th>2A</th>
						<th>2ABC</th>
						<th>Total</th>
						<th>Status</th>
						<th>Action</th>
					</tr>
				</thead>
				<tbody>
					@foreach($bets AS $bet)
					<tr>
						<td class="no-wrap">{{ $bet->created_at->format('d/m/Y h:i A') }}</td>
						<td>{{ $bet->getPage() }}</td>
						<td>{{ $bet->getType->name }}</td>
						<td>{{ $bet->getDrawDate() }}</td>
						<td width="1%" class="no-wrap">{{ $bet->number }}</td>
						<td width="1%" class="no-wrap">{{ $bet->actualbig }}</td>
						<td width="1%" class="no-wrap">{{ $bet->actualsmall }}</td>
						<td width="1%" class="no-wrap">{{ $bet->actual4a }}</td>
						<td width="1%" class="no-wrap">{{ $bet->actual4b }}</td>
						<td width="1%" class="no-wrap">{{ $bet->actual4c }}</td>
						<td width="1%" class="no-wrap">{{ $bet->actual4d }}</td>
						<td width="1%" class="no-wrap">{{ $bet->actual4e }}</td>
						<td width="1%" class="no-wrap">{{ $bet->actual4abc }}</td>
						<td width="1%" class="no-wrap">{{ $bet->actual3a }}</td>
						<td width="1%" class="no-wrap">{{ $bet->actual3abc }}</td>
						<td width="1%" class="no-wrap">{{ $bet->actual2a }}</td>
						<td width="1%" class="no-wrap">{{ $bet->actual2abc }}</td>
						<td width="1%" class="no-wrap">{{ $bet->getTotal() }}</td>
					</tr>
					@endforeach
				</tbody>
				<tfoot>
					<tr>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td></td>
						<td>Total:</td>
						<td>{{ number_format($bets->sum('actualbig'), 2) }}</td>
						<td>{{ number_format($bets->sum('actualsmall'), 2) }}</td>
						<td>{{ number_format($bets->sum('actual4a'), 2) }}</td>
						<td>{{ number_format($bets->sum('actual4b'), 2) }}</td>
						<td>{{ number_format($bets->sum('actual4c'), 2) }}</td>
						<td>{{ number_format($bets->sum('actual4d'), 2) }}</td>
						<td>{{ number_format($bets->sum('actual4e'), 2) }}</td>
						<td>{{ number_format($bets->sum('actual4abc'), 2) }}</td>
						<td>{{ number_format($bets->sum('actual3a'), 2) }}</td>
						<td>{{ number_format($bets->sum('actual3abc'), 2) }}</td>
						<td>{{ number_format($bets->sum('actual2a'), 2) }}</td>
						<td>{{ number_format($bets->sum('actual2abc'), 2) }}</td>
						<td>{{ number_format($bets->sum('total'), 2) }}</td>
					</tr>
				</tfoot>
			</table>
		</div>
	</div>
</div>
@stop

@section("page_script")
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" />

<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.22.2/moment.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/js/bootstrap-datepicker.min.js"></script>
<script src="/assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="/assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
<script src="/assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script src="https://cdn.datatables.net/plug-ins/1.10.16/sorting/datetime-moment.js"></script>

<script type="text/javascript">
	$(function(){

		$(".input-daterange").datepicker({
			todayHighlight: false,
			format: 'dd/mm/yyyy',
			autoclose: true
		});

		$("input").attr("autocomplete", "off");

		$('#start_date').on('changeDate', function(ev){
			$("#end_date").focus();
		});

		$.fn.dataTable.moment('DD/MM/YYYY');
		$.fn.dataTable.moment('DD/MM/YYYY hh:mm A');

		$("#table_betrecords").DataTable({
			"columnDefs": [
			{ "orderable": false, "targets": [1,2,3,5] }
			],
			"order": [[ 0, "desc" ]],
		});

		$("#btn_search").click(function(){
			if($("#start_date").val() == ""){
				$(".input-daterange input").removeAttr("name");
			}

			$(".filter").each(function(){
				if($(this).val() == ""){
					$(this).removeAttr("name");
				}
			});

			$(this).closest("form").submit();
		});
	});
</script>
@stop