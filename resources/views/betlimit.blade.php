@extends("layouts.userlayout")
@section("title", "Bet Limit")
@section("header", "Edit Bet Limit")

@section('headerScript')
<link href="/assets/plugins/DataTables/media/css/dataTables.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/DataTables/extensions/Responsive/css/responsive.bootstrap.min.css" rel="stylesheet" />
<link href="/assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css" rel="stylesheet" />
<link href="/assets/plugins/parsley/src/parsley.css" rel="stylesheet" />
@stop

@section("body")
<div class="panel panel-inverse" >
	<div class="panel-heading">
		<div class="panel-heading-btn">
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-default" data-click="panel-expand"><i class="fa fa-expand"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-success" data-click="panel-reload"><i class="fa fa-redo"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-warning" data-click="panel-collapse"><i class="fa fa-minus"></i></a>
			<a href="javascript:;" class="btn btn-xs btn-icon btn-circle btn-danger" data-click="panel-remove"><i class="fa fa-times"></i></a>
		</div>
		<h4 class="panel-title">Bet Limit List</h4>
	</div>

	<div id="divMsg" style="display: none" class="alert alert-danger"></div>
	
	<div class="panel-body">
		@if (session('message'))
		<div class="{{session('class')}}">{{session('message')}}</div>
		@endif

		<form id="form_betlimit" autocomplete="off" method="POST" action="{{ action('MemberController@store_betlimit') }}">
			{{ csrf_field() }}
			<label>Add Bet Limit</label>
			<div class="table-responsive m-b-20">
				<table class="table table-bordered table-condensed table-valign-middle table-width-fix table-addbetlimit f-s-14">
					<thead>
						<tr>
							@foreach($pools AS $pool)
							<th width="1%" class="text-center">
								<img src="{{ $pool->images }}" style="width: 20px; height: 20px">
							</th>
							@endforeach
							<th width="1%">Box</th>
							<th>Number</th>
							<th>Big</th>
							<th>Small</th>
							<th>Big/Small</th>
							<th>A</th>
							<th>ABC</th>
							<th>A/ABC</th>
							<th>4A</th>
							<th>4B</th>
							<th>4C</th>
							<th>4D</th>
							<th>4E</th>
							<th>4ABC</th>
							<th>3A</th>
							<th>3ABC</th>
							<th>2A</th>
							<th>2ABC</th>
						</tr>
					</thead>
					<tbody>
						<tr>
							@foreach($pools AS $pool)
							<td>
								<div class="checkbox checkbox-css">
									<input type="checkbox" id="chk{{ $pool->shortname }}" class="chk_pool" name="chk_{{ $pool->shortname }}" />
									<label for="chk{{ $pool->shortname }}"></label>
								</div>
							</td>
							@endforeach
							<td>
								<div class="checkbox checkbox-css">
									<input type="checkbox" id="chk_box" name="chk_box" />
									<label for="chk_box"></label>
								</div>
							</td>
							<td>
								<input type="text" class="form-control m_input" name="txt_number" maxlength="6">
							</td>
							<td>
								<input type="text" class="form-control m_input" name="txt_big">
							</td>
							<td>
								<input type="text" class="form-control m_input" name="txt_small">
							</td>
							<td>
								<input type="text" class="form-control m_input" name="txt_big_small">
							</td>
							<td>
								<input type="text" class="form-control m_input" name="txt_a">
							</td>
							<td>
								<input type="text" class="form-control m_input" name="txt_abc">
							</td>
							<td>
								<input type="text" class="form-control m_input" name="txt_a_abc">
							</td>
							<td>
								<input type="text" class="form-control m_input" name="txt_4a">
							</td>
							<td>
								<input type="text" class="form-control m_input" name="txt_4b">
							</td>
							<td>
								<input type="text" class="form-control m_input" name="txt_4c">
							</td>
							<td>
								<input type="text" class="form-control m_input" name="txt_4d">
							</td>
							<td>
								<input type="text" class="form-control m_input" name="txt_4e">
							</td>
							<td>
								<input type="text" class="form-control m_input" name="txt_4abc">
							</td>
							<td>
								<input type="text" class="form-control m_input" name="txt_3a">
							</td>
							<td>
								<input type="text" class="form-control m_input" name="txt_3abc">
							</td>
							<td>
								<input type="text" class="form-control m_input" name="txt_2a">
							</td>
							<td>
								<input type="text" class="form-control m_input" name="txt_2abc">
							</td>
						</tr>
					</tbody>
				</table>
			</div>

			<button id="btn_save" type="button" class="btn btn-primary m-b-20">
				<i class="fa fa-save m-r-5"></i>
				Save
			</button>
		</form>

		<div class="table-responsive">
			<table id="table_betlimit" class="table table-bordered table-valign-middle f-s-14 mt-4">
				<thead>
					<tr>
						<th width="1%">
							<div class="checkbox checkbox-css" style="padding-top: 20px">
								<input type="checkbox" id="chk_all"/>
								<label for="chk_all"></label>
							</div>
						</th>
						@foreach($pools AS $pool)
						<th width="1%" class="text-center">
							<img src="{{ $pool->images }}" style="width: 20px; height: 20px">
						</th>
						@endforeach
						<th width="1%">Box</th>
						<th width="1%">Number</th>
						<th>Big</th>
						<th>Small</th>
						<th>Big/Small</th>
						<th>A</th>
						<th>ABC</th>
						<th>A/ABC</th>
						<th>4A</th>
						<th>4B</th>
						<th>4C</th>
						<th>4D</th>
						<th>4E</th>
						<th>4ABC</th>
						<th>3A</th>
						<th>3ABC</th>
						<th>2A</th>
						<th>2ABC</th>
						<th></th>
					</tr>
				</thead>
				<tbody>
					@foreach($limits AS $ea)
					<tr>
						<td>
							<div class="checkbox checkbox-css">
								<input type="checkbox" id="chk_all"/>
								<label for="chk_all" style="padding-top: 14px"></label>
							</div>
						</td>
						<td>@if($ea->mg) <i class="fa fa-check fa-lg" style="color: green"></i> @endif</td>
						<td>@if($ea->kd) <i class="fa fa-check fa-lg" style="color: green"></i> @endif</td>
						<td>@if($ea->tt) <i class="fa fa-check fa-lg" style="color: green"></i> @endif</td>
						<td>@if($ea->sg) <i class="fa fa-check fa-lg" style="color: green"></i> @endif</td>
						<td>@if($ea->sb) <i class="fa fa-check fa-lg" style="color: green"></i> @endif</td>
						<td>@if($ea->stc) <i class="fa fa-check fa-lg" style="color: green"></i> @endif</td>
						<td>@if($ea->sw) <i class="fa fa-check fa-lg" style="color: green"></i> @endif</td>
						<td>@if($ea->box) <i class="fa fa-check fa-lg" style="color: green"></i> @endif</td>
						<td width="1%">{{ $ea->number }}</td>
						<td>{{ $ea->bet_big }}</td>
						<td>{{ $ea->bet_small }}</td>
						<td>{{ $ea->bet_big_small }}</td>
						<td>{{ $ea->bet_a }}</td>
						<td>{{ $ea->bet_abc }}</td>
						<td>{{ $ea->bet_a_abc }}</td>
						<td>{{ $ea->bet_4a }}</td>
						<td>{{ $ea->bet_4b }}</td>
						<td>{{ $ea->bet_4c }}</td>
						<td>{{ $ea->bet_4d }}</td>
						<td>{{ $ea->bet_4e }}</td>
						<td>{{ $ea->bet_4abc }}</td>
						<td>{{ $ea->bet_3a }}</td>
						<td>{{ $ea->bet_3abc }}</td>
						<td>{{ $ea->bet_2a }}</td>
						<td>{{ $ea->bet_2abc }}</td>
						<td width="1%">
							<div style="white-space: nowrap;">
								<button class="btn btn-primary btn-circle btn-icon btn-edit" data-id="{{ $ea->id }}">
									<i class="fa fa-edit"></i>
								</button>
								<form method="POST" action="{{ action('MemberController@delete_betlimit', $ea->id) }}" style="display: inline">
									{{ csrf_field() }}
									<button type="button" class="btn btn-danger btn-circle btn-icon btn-delete" data-id="{{ $ea->id }}">
										<i class="fa fa-trash"></i>
									</button>
								</form>
							</div>
						</td>
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
	</div>

</div>

<div class="modal fade" id="modal-edit">
	<div class="modal-dialog modal-lg">
		<div class="modal-content">
			<div class="modal-header">
				<h4 class="modal-title">Edit Bet Limit - </h4>
				<button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
			</div>
			<div id="edit_divMsg" style="display: none" class="alert alert-danger"></div>
			<div class="modal-body">
				<form id="form_editbetlimit" method="POST">
					{{ csrf_field() }}
					<div class="table-responsive m-b-20">
						<table class="table table-bordered table-condensed table-valign-middle table-width-fix table-addbetlimit">
							<thead>
								<tr>
									@foreach($pools AS $pool)
									<th width="1%" class="text-center">
										<img src="{{ $pool->images }}" style="width: 20px; height: 20px">
									</th>
									@endforeach
									<th width="1%">Box</th>
									<th>Number</th>
									<th>Big</th>
									<th>Small</th>
									<th>Big/Small</th>
									<th>A</th>
									<th>ABC</th>
									<th>A/ABC</th>
									<th>4A</th>
									<th>4B</th>
									<th>4C</th>
									<th>4D</th>
									<th>4E</th>
									<th>4ABC</th>
									<th>3A</th>
									<th>3ABC</th>
									<th>2A</th>
									<th>2ABC</th>
								</tr>
							</thead>
							<tbody>
								<tr>
									@foreach($pools AS $pool)
									<td>
										<div class="checkbox checkbox-css">
											<input type="checkbox" id="edit_chk{{ $pool->shortname }}" class="edit_chk_pool" name="edit_chk_{{ $pool->shortname }}" />
											<label for="edit_chk{{ $pool->shortname }}"></label>
										</div>
									</td>
									@endforeach
									<td>
										<div class="checkbox checkbox-css">
											<input type="checkbox" id="edit_chk_box" name="edit_chk_box" />
											<label for="edit_chk_box"></label>
										</div>
									</td>
									<td>
										<input type="text" class="form-control m_input" name="edit_txt_number" maxlength="6">
									</td>
									<td>
										<input type="text" class="form-control m_input" name="edit_txt_big">
									</td>
									<td>
										<input type="text" class="form-control m_input" name="edit_txt_small">
									</td>
									<td>
										<input type="text" class="form-control m_input" name="edit_txt_big_small">
									</td>
									<td>
										<input type="text" class="form-control m_input" name="edit_txt_a">
									</td>
									<td>
										<input type="text" class="form-control m_input" name="edit_txt_abc">
									</td>
									<td>
										<input type="text" class="form-control m_input" name="edit_txt_a_abc">
									</td>
									<td>
										<input type="text" class="form-control m_input" name="edit_txt_4a">
									</td>
									<td>
										<input type="text" class="form-control m_input" name="edit_txt_4b">
									</td>
									<td>
										<input type="text" class="form-control m_input" name="edit_txt_4c">
									</td>
									<td>
										<input type="text" class="form-control m_input" name="edit_txt_4d">
									</td>
									<td>
										<input type="text" class="form-control m_input" name="edit_txt_4e">
									</td>
									<td>
										<input type="text" class="form-control m_input" name="edit_txt_4abc">
									</td>
									<td>
										<input type="text" class="form-control m_input" name="edit_txt_3a">
									</td>
									<td>
										<input type="text" class="form-control m_input" name="edit_txt_3abc">
									</td>
									<td>
										<input type="text" class="form-control m_input" name="edit_txt_2a">
									</td>
									<td>
										<input type="text" class="form-control m_input" name="edit_txt_2abc">
									</td>
								</tr>
							</tbody>
						</table>
					</div>
				</div>
				<div class="modal-footer">
					<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Close</a>
					<button type="button" id="btn_update" class="btn btn-primary">Update Bet Limit</button>
				</div>
			</form>
		</div>
	</div>
</div>
@stop

@section("page_script")
<script src="/assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
<script src="/assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
<script src="/assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
<script src="/assets/plugins/parsley/dist/parsley.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.26.28/dist/sweetalert2.all.min.js"></script>

<script type="text/javascript">
	$(function(){

		var update_id = 0;

		$(document).on("click", ".btn-edit", function(){
			var id = $(this).attr("data-id");
			update_id = id;
			$.get('/getBetLimit/' + id, function(data){
				if(data.success){

					var limit = data.limit;
					var limitArr = limit.checkedArr;
					// if(limit.mg == 1){
					// 	$("input[name='edit_chk_mg']").prop("checked", true);
					// }
					limitArr.forEach(function(elem, index){
						$("input[name='edit_chk_" + elem + "']").prop("checked", true);
					});

					if(limit.box == true){
						$("input[name='edit_chk_box']").prop("checked", true);
					}

					$("input[name='edit_txt_number']").val(limit.number);
					$("input[name='edit_txt_big']").val(limit.bet_big);
					$("input[name='edit_txt_small']").val(limit.bet_small);
					$("input[name='edit_txt_big_small']").val(limit.bet_big_small);
					$("input[name='edit_txt_a']").val(limit.bet_a);
					$("input[name='edit_txt_abc']").val(limit.bet_abc);
					$("input[name='edit_txt_a_abc']").val(limit.bet_a_abc);
					$("input[name='edit_txt_4a]").val(limit.bet_4a);
					$("input[name='edit_txt_4a]").val(limit.bet_4ab);
					$("input[name='edit_txt_a_abc]").val(limit.bet_a_abc);
					$("input[name='edit_txt_4a']").val(limit.bet_4a);
					$("input[name='edit_txt_4b']").val(limit.bet_4b);
					$("input[name='edit_txt_4c']").val(limit.bet_4c);
					$("input[name='edit_txt_4d']").val(limit.bet_4d);
					$("input[name='edit_txt_4e']").val(limit.bet_4e);
					$("input[name='edit_txt_4abc']").val(limit.bet_4abc);
					$("input[name='edit_txt_3a']").val(limit.bet_3a);
					$("input[name='edit_txt_3abc']").val(limit.bet_3abc);
					$("input[name='edit_txt_2a']").val(limit.bet_2a);
					$("input[name='edit_txt_2abc']").val(limit.bet_2abc);

					$("#form_editbetlimit").attr("action", "/betlimit/" + limit.id);

					$("#modal-edit").modal("toggle");
				}
			});
		});

		$("#table_betlimit").DataTable({
			"ordering": false
		});

		$('.m_input').keypress(function (e) {
			var regex = new RegExp("^[0-9|.]+$");
			str = $(this).attr('id');

			if($(this).attr("name") == "txt_number"){
				regex = new RegExp("^[0-9]+$");
			}

			var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
			if (regex.test(str)) {
				return true;
			}

			e.preventDefault();
			return false;
		});

		$("#btn_update").click(function(){
			var num = $("input[name='edit_txt_number']").val();

			if(num == ""){
				showEditError("Please enter a valid number.");
				return false;
			} else {
				$.ajax({
					url : "/checkBetLimitNum/" + num + "/" + update_id,
					type : "GET",
					async: false,
					success : function(response) {
						if(response.success){

							if($(".edit_chk_pool").is(":checked") == false){
								showEditError("Please select at least 1 pool.");
								return false;
							} else {
								$("#edit_divMsg").hide();
							}

							var valid = false;

							var elems = $("#form_editbetlimit").find(".m_input:not([name='edit_txt_number'])");

							elems.each(function(){
								var value = parseFloat($(this).val());
								if(isNaN(value) == false && value != 0){
									valid = true;
									$(this).val(value);
								} else {
									$(this).val("0.00");
								}
							});

							if(!valid){
								showEditError("Please enter an amount for at least one of the betting category.");
								return false;
							}

							$("#form_editbetlimit").submit();

						} else {
							showEditError("Number has already been added.");
							return false;
						}
					}
				});
			}
		});

		$(document).on("click", ".btn-delete", function(){
			var $btn = $(this);
			swal({
				title: "Confirm delete?",
				text: "Press Continue to proceed",
				type: "warning",
				showCancelButton: true,
				confirmButtonText: 'Continue',
				cancelButtonText: 'Cancel',
				reverseButtons: true
			}).then((result) => {
				if (result.value) {
					$btn.closest("form").submit();
				}
			});
		});

		$("#btn_save").click(function(){

			var num = $("input[name='txt_number']").val();

			if(num == ""){
				showError("Please enter a valid number.");
				return false;
			} else {
				$.ajax({
					url : "/checkBetLimitNum/" + num + "/0",
					type : "GET",
					async: false,
					success : function(response) {
						if(response.success){

							if($(".chk_pool").is(":checked") == false){
								showError("Please select at least 1 pool.");
								return false;
							} else {
								$("#divMsg").hide();
							}

							var valid = false;

							$(".m_input:not([name='txt_number'])").each(function(){
								var value = parseFloat($(this).val());
								if(isNaN(value) == false && value != 0){
									valid = true;
									$(this).val(value);
								} else {
									$(this).val("");
								}
							});

							if(!valid){
								showError("Please enter an amount for at least one of the betting category.");
								return false;
							}

							$("#form_betlimit").submit();

						} else {
							showError("Number has already been added.");
							return false;
						}
					}
				});
			}
		});

		function showError(msg){
			$("#divMsg").text(msg).show();
		}

		function showEditError(msg){
			$("#edit_divMsg").text(msg).show();
		}
	});
</script>
@stop