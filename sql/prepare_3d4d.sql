DROP PROCEDURE IF EXISTS debug_msg;
DROP PROCEDURE IF EXISTS prepare_3d4d;
DELIMITER //
    CREATE PROCEDURE debug_msg(enabled INTEGER, msg LONGTEXT)
    BEGIN
      IF enabled THEN BEGIN
        select concat("** ", msg) AS '** DEBUG:';
      END; END IF;
    END;

 CREATE PROCEDURE prepare_3d4d(IN sqljson LONGTEXT)
   BEGIN
        DECLARE json LONGTEXT;
        DECLARE numbers, product VARCHAR(4000);
        DECLARE bet_agent, sec_package_name VARCHAR(255);
        DECLARE user_id int;
        DECLARE sec_row_id, sec_usr_id, sec_package_id int;
        DECLARE var_drawdate VARCHAR(8);
        DECLARE var_number VARCHAR(8);
        DECLARE var_pool VARCHAR(1);
        DECLARE var_eat_type VARCHAR(100);
        DECLARE v_finished INTEGER DEFAULT 0;
        DECLARE bet_package int;
        
        DECLARE i INT DEFAULT 0;
        DECLARE secondary_cursor CURSOR FOR 
        SELECT userid, rowid, package_id, drawdate, number, pool, eat_type_3d4d
        FROM sec_table;       
        
        DECLARE CONTINUE HANDLER 
        FOR NOT FOUND SET v_finished = 1;
        
        SELECT sqljson INTO json;

        # Create a temporary table
        DROP TABLE IF EXISTS input_table;
        CREATE TEMPORARY TABLE input_table SELECT *  
        FROM JSON_TABLE(json,
         "$[*]"
         COLUMNS(
            rowid FOR ORDINALITY,
            number VARCHAR(10) PATH "$.number" DEFAULT '0' ON ERROR DEFAULT '0' ON EMPTY,
            drawdate VARCHAR(8) PATH "$.draw_date",
            pool VARCHAR(1) PATH "$.pool",
            agent VARCHAR(100) PATH "$.agent",
            package int PATH "$.package",
            amount_Big float PATH "$.amount_Big",
            amount_Small float PATH "$.amount_Small",
            amount_3A float PATH "$.amount_3A",
            amount_3ABC float PATH "$.amount_3ABC",
            amount_4A float PATH "$.amount_4A"
            )
            ) AS tt;
            
        SELECT 
    agent, package
INTO bet_agent , bet_package FROM
    input_table
WHERE
    rowid = 1;
        
SELECT 
    id
INTO user_id FROM
    users
WHERE
    username = bet_agent;
        
SELECT 
    v.package_name
INTO sec_package_name FROM
    assigned_package_view v 
INNER JOIN 
    packages p
ON p.package_id = v.package_id
WHERE
    p.package_id = bet_package
GROUP BY v.package_name;
        
        # Now loop to get all the parents of the agent until meeting the agent "company"
        DROP TABLE IF EXISTS sec_table;
        CREATE TEMPORARY TABLE sec_table
        (
            agent_seq int,
            rowid int,
            userid int,
            eat_type_3d4d varchar(100),
            position_3d4d int,            
            package_id int,
            package int,
            package_name varchar(255),
            amount_Big decimal(12,4),
            amount_Small decimal(12,4),
            amount_3A decimal(12,4),
            amount_3ABC decimal(12,4),
            amount_4A decimal(12,4),
            number varchar(4),
            drawdate varchar(8),
            pool varchar(1)
        );
        
        INSERT INTO sec_table (agent_seq, rowid, userid, eat_type_3d4d, position_3d4d, package_id, package, package_name, amount_Big,
                                       amount_Small, amount_3A, amount_3ABC, amount_4A, number, drawdate, pool)
        SELECT c.lvl, c.rowid, u.id AS userid, u.eat_type_3d4d, u.position_3d4d, v.package_id, c.package, v.package_name, c.amount_Big,
               c.amount_Small, c.amount_3A, c.amount_3ABC, c.amount_4A, c.number, c.drawdate, c.pool                
        FROM
        (
        SELECT * FROM
        (
        SELECT T2.id, T2.username, lvl
            FROM (
                SELECT
                    @r AS _id,
                    (SELECT @r := user_upline FROM users WHERE id = _id) AS user_upline,
                    @l := @l + 1 AS lvl
                FROM
                    (SELECT @r := user_id, @l := 0) vars,
                    users m
                WHERE @r > 1) T1
            JOIN users T2
            ON T1._id = T2.id
            ORDER BY T1.lvl DESC) b,
        input_table i) c
        inner join
        users u
        on c.id = u.id
        inner join 
        assigned_package_view v
        on c.id = v.userid
        and REPLACE(v.package_name, '*', '') = REPLACE(sec_package_name, '*', '')
        ORDER BY number, drawdate, pool, userid desc;
        
        -- select * from sec_table;
        
        
        /*SELECT T2.id, T2.username
            FROM (
                SELECT
                    @r AS _id,
                    (SELECT @r := user_upline FROM users WHERE id = _id) AS user_upline,
                    @l := @l + 1 AS lvl
                FROM
                    (SELECT @r := user_id, @l := 1) vars,
                    users m
                WHERE @r > 1) T1
            JOIN users T2
            ON T1._id = T2.id
            ORDER BY T1.lvl DESC
        ) b, (SELECT @n := 0) m
        ;*/
        
        
        # knowing if it is amount based/total based/group based           
        DROP TABLE IF EXISTS working_table;
        CREATE TEMPORARY TABLE working_table
        (
            uniqueid int NOT NULL AUTO_INCREMENT Primary Key,
            rowid int,
            userid int,
            hierarchy int,
            eat_type_3d4d varchar(100),
            position_3d4d int,
            number varchar(4),
            drawdate varchar(8),
            pool varchar(1),
            package int,
            amount_Big decimal(12,4),
            amount_Small decimal(12,4),
            amount_3A decimal(12,4),
            amount_3ABC decimal(12,4),
            amount_4A decimal(12,4),
            eat_Big decimal(12,4) default 0.00,
            eat_Small decimal(12,4) default 0.00,
            eat_3A decimal(12,4) default 0.00,
            eat_3ABC decimal(12,4) default 0.00,
            eat_4A decimal(12,4) default 0.00,
            commission JSON,
            payrate JSON,
            thelimit JSON
        );
        
        SET SESSION group_concat_max_len=66550;
        OPEN secondary_cursor;
        # get all the ticketing information of the date
        ticket_loop: LOOP
            FETCH secondary_cursor INTO sec_usr_id, sec_row_id, sec_package_id, var_drawdate, var_number, var_pool, var_eat_type;
            IF v_finished = 1 THEN 
            LEAVE ticket_loop;
            END IF;
                        
            # get the package details
            INSERT INTO working_table (rowid, hierarchy, userid, eat_type_3d4d, number, drawdate, pool, package, amount_Big,
                                       amount_Small, amount_3A, amount_3ABC, amount_4A, position_3d4d)
            SELECT rowid, agent_seq, userid, eat_type_3d4d, number, drawdate, pool, package_id, amount_Big,
                                       amount_Small, amount_3A, amount_3ABC, amount_4A, position_3d4d  
            FROM sec_table
            WHERE userid = sec_usr_id
            AND rowid = sec_row_id
            
            ;
            
            SET SQL_SAFE_UPDATES = 0;
                       
UPDATE working_table w
        INNER JOIN
    (SELECT 
        CONCAT('[', GROUP_CONCAT(JSON_OBJECT('type', p.package_detail_subcategory_name, 'commission', a.commission)), ']') AS Commission,
            CONCAT('[', GROUP_CONCAT(JSON_OBJECT('type', p.package_detail_subcategory_name, 'value', CONCAT('[', JSON_OBJECT('type', 'first_prize', 'rate', a.first_prize), ',', JSON_OBJECT('type', 'second_prize', 'rate', a.second_prize), ',', JSON_OBJECT('type', 'third_prize', 'rate', a.third_prize), ',', JSON_OBJECT('type', 'special_prize', 'rate', a.special_prize), ',', JSON_OBJECT('type', 'consolation_prize', 'rate', a.consolation_prize), ']'))), ']') AS pay_rate,
            a.package_id
    FROM
        package_details a
    INNER JOIN package_detail_categories p ON a.package_detail_category_id = p.package_detail_category_id
    WHERE
        (p.package_detail_category_name = '3D'
            OR p.package_detail_category_name = '4D')
    GROUP BY a.package_id) j ON j.package_id = w.package 
SET 
    w.commission = j.commission,
    w.payrate = j.pay_rate;
                
            -- Now is time to get recorded ticket
UPDATE working_table w
        INNER JOIN
    (SELECT 
        r.userid,
            IFNULL(SUM(r.eat3a), 0) AS eat3a,
            IFNULL(SUM(r.eat3abc), 0) AS eat3abc,
            r.pool,
            r.number,
            SUBSTR(r.number, 1, 3) AS number3d
    FROM
        eat_information_view r
    WHERE
        SUBSTR(r.number, 1, 3) = SUBSTR(var_number, 1, 3)
            AND r.pool = var_pool
            AND r.drawdate = var_drawdate
            AND LENGTH(r.number) >= 3
            AND r.userid = sec_usr_id
    GROUP BY number3d) a ON SUBSTR(var_number, 1, 3) = a.number3d 
SET 
    w.eat_3A = eat3a,
    w.eat_3ABC = eat3abc
WHERE w.userid = sec_usr_id;
                
            -- For 4D
UPDATE working_table w
        INNER JOIN
    (SELECT 
        r.userid,
            IFNULL(SUM(r.eatbig), 0) AS eatbig,
            IFNULL(SUM(r.eatsmall), 0) AS eatsmall,
            IFNULL(SUM(r.eat4a), 0) AS eat4a,
            r.pool,
            r.number
    FROM
        eat_information_view r
    WHERE
        r.number = var_number
            AND r.pool = var_pool
            AND r.drawdate = var_drawdate
            AND LENGTH(r.number) = 4
            AND r.userid = sec_usr_id
    GROUP BY r.number) a ON var_number = a.number 
SET 
    w.eat_Big = eatbig,
    w.eat_Small = eatsmall,
    w.eat_4A = eat4a
WHERE w.userid = sec_usr_id;
            
            -- now finding the eat limit for each of every one
            IF (var_eat_type = 'Amount') THEN 
            
                UPDATE working_table w
                SET w.thelimit =
                (
                    SELECT CONCAT
                        (
                            '[', 
                                GROUP_CONCAT(JSON_OBJECT('type', c.eat_detail_subtype_name, 'value', e.value)),
                                ']'                                            
                        ) AS thelimit
                    FROM eat_maps e
                    INNER JOIN eat_detail_categories c
                    ON e.pdc_id = c.id
                    INNER JOIN pools p
                    ON e.pool_id = p.id
                    WHERE e.user_id = sec_usr_id
                    and c.eat_detail_type_name = 'Amount'
                    and p.code = var_pool
                ) 
                WHERE w.userid = sec_usr_id
                AND w.rowid = sec_row_id;
            
            ELSEIF (var_eat_type = 'Total Payout') THEN
                UPDATE working_table w
                SET w.thelimit =
                (
                    SELECT CONCAT
                    (
                        '[', 
                            GROUP_CONCAT(JSON_OBJECT('type', 'total', 'value', e.value)),
                            ']'                                            
                    ) AS thelimit
                    FROM eat_maps e
                    INNER JOIN eat_detail_categories c
                    ON e.pdc_id = c.id
                    INNER JOIN pools p
                    ON e.pool_id = p.id
                    WHERE e.user_id = sec_usr_id
                    and c.eat_detail_type_name = 'Total'
                    and p.code = var_pool
                )
                WHERE w.userid = sec_usr_id
                AND w.rowid = sec_row_id;

            ELSEIF (var_eat_type = 'Group') THEN
                UPDATE working_table w
                SET w.thelimit =
                (
                    SELECT CONCAT
                    (
                        '[', 
                            GROUP_CONCAT(JSON_OBJECT('type', 'group', 'sub_type', c.eat_detail_subtype_name, 'tertier_type', c.eat_detail_tertiertype_name, 'value', e.value)),
                            ']'                                            
                    ) AS thelimit
                    FROM eat_maps e
                    INNER JOIN eat_detail_categories c
                    ON e.pdc_id = c.id
                    INNER JOIN pools p
                    ON e.pool_id = p.id
                    WHERE e.user_id = sec_usr_id
                    and c.eat_detail_type_name = 'Group'
                    and p.code = var_pool
                )
                WHERE w.userid = sec_usr_id
                AND w.rowid = sec_row_id;
            END IF;
                  
            END LOOP ticket_loop;
            
        CLOSE secondary_cursor;
        
SELECT 
    *
FROM
    working_table
ORDER BY number, drawdate, pool, userid desc;
        
        END //
 DELIMITER ;