use mpw;

CREATE OR REPLACE VIEW eat_information_view AS
SELECT
	d.id, d.userid, d.status, sum(d.eatbig) as eatbig, sum(d.eatsmall) as eatsmall, sum(d.eat4a) as eat4a, 
    sum(d.eat3a) as eat3a, sum(d.eat3abc) as eat3abc, sum(d.eat4b) as eat4b, sum(d.eat4c) as eat4c, 
    sum(d.eat4d) as eat4d, sum(d.eat4e) as eat4e, sum(d.eat4abc) as eat4abc, sum(d.eat2a) as eat2a, 
    sum(d.eat2abc) as eat2abc, sum(d.eat5d) as eat5d, sum(d.eat6d) as eat6d, b.drawdate, b.pool, b.number           
FROM stage_details d 
INNER JOIN bet_stages b
ON d.stageid = b.id
WHERE b.status = 0
GROUP BY b.drawdate, b.pool, b.number, d.userid