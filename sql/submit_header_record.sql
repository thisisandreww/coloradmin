Use mpw;

DROP PROCEDURE IF EXISTS submit_header_record;
DELIMITER //

CREATE PROCEDURE submit_header_record
(
	IN Currency VARCHAR(255),
    IN UserId int,
    IN PackageId int,
    IN BetType varchar(255),
    IN BetMethod varchar(255),
    IN Inbox LONGTEXT,
    IN BetString LONGTEXT,
    IN InitialAmouont DECIMAL(12,4),
    IN ActualAmount DECIMAL(12,4),
    IN BetFormat varchar(255),
    IN DrawFormat varchar(255),
    IN Box_Ibox varchar(255)
)
BEGIN

DECLARE lastpageid int;

SELECT IFNULL((
SELECT pageid 
FROM tickets
WHERE userid = UserId
AND DATE(created_at) = CURDATE()
ORDER BY pageid desc
limit 1), 0) into lastpageid;

/*
SELECT IFNULL((
SELECT max(pageid)
FROM tickets
WHERE userid = UserId
AND DATE(created_at) = CURDATE()
GROUP BY userid, created_at)
, 0) into lastpageid;*/

IF (BetType = '3D/4D') THEN
INSERT INTO tickets(pageid, currency, userid, packageid_3d4d, packageid_5d6d, packageid_4dspecial, type, bet_method, inbox, betstring, initial_amount, actual_amount, status,
					bet_format, draw_format, box_ibox, created_at, bettime)
VALUES 
(
	lastpageid + 1, Currency, UserId, PackageId, 0, 0, BetType, BetMethod, Inbox, BetString, InitialAmouont, ActualAmount, 0,
    BetFormat, DrawFormat, Box_Ibox, current_timestamp(), current_timestamp()
);

ELSEIF (BetType = '4D Special') THEN
INSERT INTO tickets(pageid, currency, userid, packageid_3d4d, packageid_5d6d, packageid_4dspecial, type, bet_method, inbox, betstring, initial_amount, actual_amount, status,
                    bet_format, draw_format, box_ibox, created_at, bettime)
VALUES 
(
    lastpageid + 1, Currency, UserId, 0, 0, PackageId, BetType, BetMethod, Inbox, BetString, InitialAmouont, ActualAmount, 0,
    BetFormat, DrawFormat, Box_Ibox, current_timestamp(), current_timestamp()
);

ELSE
INSERT INTO tickets(pageid, currency, userid, packageid_3d4d, packageid_5d6d, packageid_4dspecial, type, bet_method, inbox, betstring, initial_amount, actual_amount, status,
                    bet_format, draw_format, box_ibox, created_at, bettime)
VALUES 
(
    lastpageid + 1, Currency, UserId, 0, PackageId, 0, BetType, BetMethod, Inbox, BetString, InitialAmouont, ActualAmount, 0,
    BetFormat, DrawFormat, Box_Ibox, current_timestamp(), current_timestamp()
);
END IF;

SELECT last_insert_id() As id;

END //
DELIMITER ;