use mpw;

CREATE OR REPLACE VIEW assigned_package_view AS
    SELECT 
        p.*, u.username, u.id AS userid
    FROM
        users u
            INNER JOIN
        package_assignments a ON u.id = a.user_id
            INNER JOIN
        packages p ON a.package_id = p.package_id