<?php

use Illuminate\Http\Request;


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['auth:api'])->group(function () {
	Route::get('/test', 'ApiController@index');
	Route::get('/permuteNumber/{number}', 'ApiController@permuteNumber');
});


Route::get('/pool', 'ApiController@pool');
Route::post('/parse', 'ApiController@parse');
Route::get('/drawresult/{date}/{pool}', 'ApiController@result');


