<?php
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'login', 'uses' => 'BaseController@login']);
Route::get('/php', ['as' => 'php', 'uses' => 'BaseController@php']);
Route::get('/refreshCaptcha', ['as' => 'refreshCaptcha', 'uses' => 'LoginController@refreshCaptcha']);
Route::post('/', ['as' => 'login', 'uses' => 'BaseController@login']);
Route::post('/login', 'LoginController@loginValidate');

Route::get('lang/{lang}', 'BaseController@switch_language');

Route::get('/scrape', 'Controller@scraper');

Route::middleware(['auth'])->group(function () {
	Route::get('/logout', function () {
		Auth::logout();
		return redirect("/");
	});

	Route::get('/index', ['as' => 'index', 'uses' => 'MemberController@index']);

	//for password reset use
	Route::get('/resetPassword', ['as' => 'resetPassword', 'uses' => 'MemberController@openResetPasswordView']);
	Route::post('/resetPassword/validate', 'MemberController@validateResetPassword');

	Route::get('/addMemberDetail/{id}', ['as' => 'memberDetail', 'uses' => 'MemberController@openAddMemberView']);
	Route::get('/editMemberDetail/{id}/{selectedId}', ['as' => 'memberSelectedDetail', 'uses' => 'MemberController@openEditMemberView']);
	Route::post('/addMemberDetail/{id}', ['as' => 'memberDetailAdd', 'uses' => 'MemberController@addNewDownline']);
	Route::post('/editMemberDetail/{id}', ['as' => 'memberDetailEdit', 'uses' => 'MemberController@editDownlineDetail']);
	Route::post('/updateMemberState/{id}/{state}', ['as' => 'updateMemberState', 'uses' => 'MemberController@updateAccountState']);

	Route::get('/subAccount', ['as' => 'subAccount', 'uses' => 'MemberController@showSubAccountView']);
	Route::post('/subAccountAdd/{id}', ['as' => 'subAccountAdd', 'uses' => 'MemberController@addSubAccountView']);
	Route::post('/subAccountEdit/{id}', ['as' => 'subAccountEdit', 'uses' => 'MemberController@editSubAccountView']);

	//for downline page operation
	Route::get('/memberlist/{id}', ['as' => 'memberlist', 'uses' => 'MemberController@showUserView']);
	//temporary disabled
	//Route::get('/memberlist/delete/{id}', ['as' => 'memberDelete', 'uses' => 'MemberController@deleteMember']);
	Route::post('/memberUpdate', ['as' => 'memberUpdate', 'uses' => 'MemberController@updateMember']);

	/*For changing master package*/
	Route::post('/package/delete/{packageId}', ['as' => 'packageDelete', 'uses' => 'PackageController@deletePackages']);
	Route::get('/package/{type}', ['as' => 'package', 'uses' => 'PackageController@showPackageView']);
	Route::post('/package/{packageId}', ['as' => 'package', 'uses' => 'PackageController@updateExistingPackage']);

	/*For member package acquisition and edit*/
	Route::post('/downlinePackageUpdate/{id}', ['as' => 'downlinePackageUpdate', 'uses' => 'PackageController@updateMemberPackageDetail']);
	Route::get('/packageDetail/{id}/{packageTypeId}/{packageId}', ['as' => 'packageDetail', 'uses' => 'PackageController@showPackageDetailView']);

	//for package creation
	Route::post('/packageAdd/{packageTypeId}', ['as' => 'packageAdd', 'uses' => 'PackageController@addNewPackage']);
	Route::get('/packageAdd/{packageTypeId}', ['as' => 'packageAdd', 'uses' => 'PackageController@showAddPackageView']);

	// for bet schedule
	Route::get('/betschedule', ['as' => 'betschedule', 'uses' => 'MemberController@betschedule']);
	Route::post('/betschedule/loadprimary', ['as' => 'loadprimary', 'uses' => 'MemberController@loadPrimary']);
	Route::post('/betschedule/writeadate', ['as' => 'writeadate', 'uses' => 'MemberController@writeADate']);
	Route::post('/betschedule/updateschedule', ['as' => 'updateschedule', 'uses' => 'MemberController@updatesShedule']);

	Route::get('/index', ['as' => 'index', 'uses' => 'MemberController@index']);

	//for password reset use
	Route::get('/resetPassword', ['as' => 'resetPassword', 'uses' => 'MemberController@openResetPasswordView']);
	Route::post('/resetPassword/validate', 'MemberController@validateResetPassword');

	Route::get('/addMemberDetail/{id}', ['as' => 'memberDetail', 'uses' => 'MemberController@openAddMemberView']);
	Route::get('/editMemberDetail/{id}/{selectedId}', ['as' => 'memberSelectedDetail', 'uses' => 'MemberController@openEditMemberView']);
	Route::post('/addMemberDetail/{id}', ['as' => 'memberDetailAdd', 'uses' => 'MemberController@addNewDownline']);
	Route::post('/editMemberDetail/{id}', ['as' => 'memberDetailEdit', 'uses' => 'MemberController@editDownlineDetail']);
	Route::post('/updateMemberState/{id}/{state}', ['as' => 'updateMemberState', 'uses' => 'MemberController@updateAccountState']);

	Route::get('/subAccount', ['as' => 'subAccount', 'uses' => 'MemberController@showSubAccountView']);
	Route::post('/subAccountAdd/{id}', ['as' => 'subAccountAdd', 'uses' => 'MemberController@addSubAccountView']);
	Route::post('/subAccountEdit/{id}', ['as' => 'subAccountEdit', 'uses' => 'MemberController@editSubAccountView']);

	//for downline page operation
	Route::get('/memberlist/{id}', ['as' => 'memberlist', 'uses' => 'MemberController@showUserView']);
	//temporary disabled
	//Route::get('/memberlist/delete/{id}', ['as' => 'memberDelete', 'uses' => 'MemberController@deleteMember']);
	Route::post('/memberUpdate', ['as' => 'memberUpdate', 'uses' => 'MemberController@updateMember']);

	/*For changing master package*/
	Route::post('/package/delete/{packageId}', ['as' => 'packageDelete', 'uses' => 'PackageController@deletePackages']);
	Route::get('/package/{type}', ['as' => 'package', 'uses' => 'PackageController@showPackageView']);
	Route::post('/package/{packageId}', ['as' => 'package', 'uses' => 'PackageController@updateExistingPackage']);

	/*For member package acquisition and edit*/
	Route::post('/downlinePackageUpdate/{id}', ['as' => 'downlinePackageUpdate', 'uses' => 'PackageController@updateMemberPackageDetail']);
	Route::get('/packageDetail/{id}/{packageTypeId}/{packageId}', ['as' => 'packageDetail', 'uses' => 'PackageController@showPackageDetailView']);

	// for package creation
	Route::post('/packageAdd/{packageTypeId}', ['as' => 'packageAdd', 'uses' => 'PackageController@addNewPackage']);
	Route::get('/packageAdd/{packageTypeId}', ['as' => 'packageAdd', 'uses' => 'PackageController@showAddPackageView']);

	// for bet schedule
	Route::get('/betschedule', ['as' => 'betschedule', 'uses' => 'MemberController@betschedule']);
	Route::post('/betschedule/loadprimary', ['as' => 'loadprimary', 'uses' => 'MemberController@loadPrimary']);
	Route::post('/betschedule/writeadate', ['as' => 'writeadate', 'uses' => 'MemberController@writeADate']);

	// for bet limit
	Route::get('/betlimit', 'MemberController@index_betlimit');
	Route::post('/betlimit', 'MemberController@store_betlimit');
	Route::get('/getBetLimit/{id}', 'MemberController@getBetLimit');
	Route::get('/checkBetLimitNum/{number}/{id}', 'MemberController@checkBetLimitNum');
	Route::post('/betlimit/{id}', 'MemberController@update_betlimit');
	Route::post('/deletebetlimit/{id}', 'MemberController@delete_betlimit');

	// for red number restriction
	Route::get('/rednumberrestriction', 'MemberController@index_rednumberrestriction');
	Route::post('/rednumberrestriction', 'MemberController@store_rednumberrestriction');
	Route::delete('/delete_rednumberrestriction', 'MemberController@delete_rednumberrestriction');
	Route::get('/getRedNumberRestriction/{id}', 'MemberController@getRedNumberRestriction');
	Route::post('/rednumberrestriction/{id}', 'MemberController@update_rednumberrestriction');

	// for void timeout
	Route::post('/void-timeout', 'MemberController@store_voidtimeout');

	// for draw result
	Route::get('/drawresult', ['as' => 'drawresult', 'uses' => 'MemberController@drawResult']);
	Route::get('/getallresult/{drawdate}', ['as' => 'getallresult', 'uses' => 'MemberController@getAllResult']);
	Route::post('/drawresult/manageresult', ['as' => 'manageresult', 'uses' => 'MemberController@drawResultPostback']);

	// for eat map references
	Route::get('/eatMapReference/{id}/{targetid}', ['as' => 'eatMapReference', 'uses' => 'EatMapController@showReferenceEatMap']);
	Route::post('/eatmap/savereference', ['as' => 'saveMapReference', 'uses' => 'EatMapController@saveMapReference']);

	// for editting eat map
	Route::get('/editEatMap', ['as' => 'editEatMap', 'uses' => 'EatMapController@editEatMap']);
	Route::post('/eatmap/savemap', ['as' => 'savemap', 'uses' => 'EatMapController@saveEditMap']);

	// for bet 3d4d
	Route::get('/bet/bet3d4d', ['as' => 'bet3d4d', 'uses' => 'BetController@bet3d4d']);
	Route::post('/bet/makeBet3d4d', ['as' => 'makeBet3d4d', 'uses' => 'BetController@makeBet3d4d']);

	// for bet 5d6d
	Route::get('/bet/bet5d6d', 'BetController@bet5d6d');
	Route::post('/bet/makeBet5d6d', 'BetController@makeBet5d6d');
	
	// for bet cut off
	Route::get('/bet/cuttoff', ['as' => 'cuttoff', 'uses' => 'BetController@cuttoff']);
	Route::post('/bet/saveCutOff', ['as' => 'saveCutOff', 'uses' => 'BetController@saveCutOff']);

	Route::get('/report/winLossDetail/{id}', ['as' => 'winLossDetail', 'uses' => 'ReportController@getWinLossDetailReport']);
	Route::get('/report/winLossSimple/{id}', ['as' => 'winLossSimple', 'uses' => 'ReportController@getWinLossSimpleReport']);
	Route::get('/report/totalReport/{id}', ['as' => 'totalReport', 'uses' => 'ReportController@getTotalReport']);
	Route::get('/report/eatMap/{id}/{type}', ['as' => 'eatMap', 'uses' => 'ReportController@getEatMap']);
	Route::get('/report/bet/{type}/{id?}', ['as' => 'betReport', 'uses' => 'ReportController@getBetReport']);

    // for betting records
	Route::get('/bettingRecords', 'ReportController@bettingRecords');

    // for view/void invoice
	Route::get('/viewVoidInvoice', 'InboxController@viewVoidInvoice');


    // for bet 4d special
	Route::get('/bet/bet4dspecial', ['as' => 'bet4dspecial', 'uses' => 'BetController@bet4dspecial']);
	Route::post('/bet/makeBet4dSpecial', ['as' => 'makeBet4dSpecial', 'uses' => 'BetController@makeBet4dSpecial']);

	// for auto ticketing
	Route::get('/autoTicketing', ['as' => 'autoTicketing', 'uses' => 'ReportController@autoTicketing']);
	Route::get('/getBetStages/{ticketid}', 'ReportController@getBetStages');
	Route::post('/autoTicketing/change_status/{id}/{status}', 'ReportController@changeAutoTicketing');
	Route::post('/autoTicketing/delete/{id}', 'ReportController@deleteAutoTicketing');

	Route::post('/voidInvoice', 'InboxController@voidInvoice');

});

